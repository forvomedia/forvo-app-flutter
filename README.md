# Forvo

* Install [flutter](https://flutter.io/get-started/install/) 
**Current version: 2.5.2**
````
  $ fvm flutter --version
  Flutter 2.5.2 • channel stable • https://github.com/flutter/flutter.git
  Framework • revision 3595343e20 (hace 8 meses) • 2021-09-30 12:58:18 -0700
  Engine • revision 6ac856380f
  Tools • Dart 2.14.3
````
* Install Dart/Flutter plugins on IDE
* [Install Android SDK / iOS Xcode](https://docs.google.com/document/d/1rXtVCXkSx8fomqjy3gFR5Md2ktWD5uNsPxXrB-5ETx0/edit?usp=sharing)
* Create lib/main.dart from lib/main_dist.dart replacing api URL with your local service URL
* Run lib/main.dart (or any other environment file) **DEFINE ````--flavor dev```` FLAG**

## Linter
Run the following command in a terminal
````
cd .git/hooks && ln ../../.hooks/pre-commit pre-commit && cd -
````

## Localization

- New languages must be enabled in the supported language list from `lib/constants/language_constants.dart`. For the iOS build, before this, new locale codes must be added to the `CFBundleLocalizations` array in `info.plist` file.
- Add texts to `assets/l10n/en.json` or other defined language files.

## Run

- To run the app in debug mode, launch from Run option in IDE, or by command:
```
fvm flutter run -t lib/main.dart
```

## Release
* Generate your own signing key [https://flutter.io/docs/deployment/android#create-a-keystore]()
* Create the APP signing configuration for release [https://flutter.io/docs/deployment/android#reference-the-keystore-from-the-app]()
* Run:

    - iOS:
    ```
    fvm flutter build ios --release --no-tree-shake-icons --flavor=prod -t lib/main_prod.dart
    ```

    - Android:
    ```
    fvm flutter build appbundle --release --no-tree-shake-icons --flavor=prod -t lib/main_prod.dart
    ```

[comment]: <> (## Generate screenshots)

[comment]: <> (To auto generate screenshots for iOS App Store and Google Play, in different)

[comment]: <> (locale languages and different devices, the dart package `screenshots` is used.)

[comment]: <> (Follow the instructions to install all the dependencies needed.)

[comment]: <> (&#40;[https://pub.dev/packages/screenshots]&#40;&#41;&#41;)

[comment]: <> (The are 2 config file named `screenshots_ios.yaml` and `screenshots_android.yaml`)

[comment]: <> (on the root of the project. Each one is used to define the list of locale)

[comment]: <> (languages and devices that will be used to generate screenshots of the app.)

[comment]: <> (To generate or update the screenshots, copy the original file of the respective)

[comment]: <> (system to a new `screenshots.yaml` file in the root of the project, and then via)

[comment]: <> (command line project execute `screenshots`. This will execute a process)

[comment]: <> (using the integration tests located in the `test_driver` folder in each of the )

[comment]: <> (devices listed &#40;android emulator must be launched previously&#41;.)

[comment]: <> (* Note: For the android simulator, an image of the `Nexus 6P` device &#40;without Google)

[comment]: <> (Play Services&#41; must be installed and running previously to execute the command.)

## Apple Store Upload
To upload a candidate release app build to Testflight:
* open `XCode` on the project directory, wait for the program to index all project files.
* select the `prod` profile and `Any iOS Device (arm64, armv7)` option on the upper menu.
* click on menu `Product > Archive` and wait for XCode to generate a valid archive.
* select `Validate app`, follow the steps and select the correct iOS Certificate and Provisioning Profiles and wait until the end of the validation process.
* select `Distribute app`, follow the steps and select the correct iOS Certificate and Provisioning Profile and wait until the end of the upload process.
* open the `Apple Store Connect` page, go to `Apps > Forvo > Testfligth` and complete the upload build requirements to be able to start the tests.

## Google Play upload
To upload a candidate release app build to Google Play:
* open the app production generated file by the Android command and placed in `build/app/outputs/bundle/prodRelease/app-prod-release.aab` and upload to the Google Play Store page in the valid track (`Prueba interna`, `Prueba cerrada`).

[comment]: <> (## Fastlane)

[comment]: <> (Fastlane [https://docs.fastlane.tools/]&#40;&#41; is used to manage the metadata of the)

[comment]: <> (iOS App Store and Google Play. To generate the files needed for each build,)

[comment]: <> (enter in the ios/android folder via command line and execute `fastlane init`.)

[comment]: <> (This will create a `fastlane` folder with the configuration file `fastfile`, and)

[comment]: <> (new folders for store metadata and screenshots.)

[comment]: <> (To upload the files to the store:)

[comment]: <> (* iOS App Store:)

[comment]: <> (    `cd ios/`)
    
[comment]: <> (    `bundle exec fastlane deliver`)

[comment]: <> (* Google Play:)

[comment]: <> (    `cd android/`)
    
[comment]: <> (    `bundle exec fastlane android deploy`)