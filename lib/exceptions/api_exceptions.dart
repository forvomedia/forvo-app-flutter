class NotConnectedException implements Exception {}

class NotFoundException implements Exception {}

class EmptyException implements Exception {}