import 'package:forvo/config/app_environment.dart';
import 'package:forvo/launcher.dart';

void main() async {
  launchApp(AppEnvironment.prod());
}
