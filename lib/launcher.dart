import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:forvo/common/notifiers/theme_changer.dart';
import 'package:forvo/config/app_config.dart';
import 'package:forvo/config/app_environment.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/service/notification/local_notification_service.dart';
import 'package:forvo/ui/app.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:provider/provider.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

void launchApp(AppEnvironment appEnvironment) async {
  WidgetsFlutterBinding.ensureInitialized();
  LocalNotificationService.initialize();
  await Firebase.initializeApp();

  /// Set the background messaging handler early on, as a named top-level
  /// function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  bool _requestPermission = false;
  if (!deviceIsAndroid()) {
    String deviceVersion = await getDeviceOSVersion();
    final splitVersion = deviceVersion.split('.');
    _requestPermission = !deviceIsAndroid() &&
        ((splitVersion.isNotEmpty && int.parse(splitVersion[0]) > 14) ||
            (splitVersion.length >= 2 &&
                (int.parse(splitVersion[0]) == 14 &&
                    int.parse(splitVersion[1]) >= 5)));
  }
  AppsFlyerOptions appsFlyerOptions = AppsFlyerOptions(
    afDevKey: APPSFLYER_DEV_KEY,
    appId: deviceIsAndroid() ? ANDROID_APP_ID : IOS_APP_ID,
    timeToWaitForATTUserAuthorization: _requestPermission
        ? APPSFLYER_TIME_TO_WAIT_FOR_ATT_USER_AUTHORIZATION
        : null,
  );

  AppsflyerSdk appsflyerSdk = AppsflyerSdk(appsFlyerOptions);
  appsflyerSdk.initSdk(
    registerConversionDataCallback: true,
    registerOnAppOpenAttributionCallback: true,
    registerOnDeepLinkingCallback: true,
  );

  ThemeData _lightTheme = await getUserLightTheme();
  ThemeData _darkTheme = await getUserDarkTheme();

  Widget app = AppConfig(
    environment: appEnvironment,
    child: App(),
  );

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ThemeChanger(_lightTheme, _darkTheme),
        ),
      ],
      child: app,
    ),
  );
}
