import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

SnackBar themedSnackBar(BuildContext context, String message,
    {String type, Duration duration}) {
  var themeData = Theme.of(context);
  TextStyle standardSnackBarNormalTextStyle =
      themeData.textTheme.bodyText2.copyWith(
    fontSize: 16,
  );
  TextStyle standardSnackBarBoldTextStyle =
      standardSnackBarNormalTextStyle.copyWith(
    fontWeight: FontWeight.bold,
  );
  TextStyle otherSnackBarNormalTextStyle =
      standardSnackBarNormalTextStyle.copyWith(
    color: colorWhite,
  );
  TextStyle otherSnackBarBoldTextStyle = otherSnackBarNormalTextStyle.copyWith(
    fontWeight: FontWeight.bold,
  );
  return SnackBar(
    backgroundColor: type == SNACKBAR_TYPE_ERROR
        ? colorSnackBarRed
        : type == SNACKBAR_TYPE_OK
            ? colorSnackBarGreen
            : Colors.transparent,
    behavior: SnackBarBehavior.fixed,
    duration: duration ?? Duration(seconds: SNACKBAR_DURATION_IN_SECONDS),
    content: Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              decoration:
                  type != SNACKBAR_TYPE_ERROR && type != SNACKBAR_TYPE_OK
                      ? BoxDecoration(
                          color: themeData.primaryColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            color: themeData.primaryColorLight,
                          ),
                        )
                      : null,
              padding: type != SNACKBAR_TYPE_ERROR && type != SNACKBAR_TYPE_OK
                  ? EdgeInsets.symmetric(
                      horizontal: 20.0,
                      vertical: 10.0,
                    )
                  : const EdgeInsetsDirectional.only(start: 15),
              child: HtmlTagTextFormatted(
                text: message,
                openTag: '<b>',
                closeTag: '</b>',
                normalTextStyle:
                    type == SNACKBAR_TYPE_ERROR || type == SNACKBAR_TYPE_OK
                        ? otherSnackBarNormalTextStyle
                        : standardSnackBarNormalTextStyle,
                boldTextStyle:
                    type == SNACKBAR_TYPE_ERROR || type == SNACKBAR_TYPE_OK
                        ? otherSnackBarBoldTextStyle
                        : standardSnackBarBoldTextStyle,
              ),
            ),
          ),
          if (type == SNACKBAR_TYPE_ERROR || type == SNACKBAR_TYPE_OK)
            Padding(
              padding: EdgeInsetsDirectional.only(start: 5),
              child: InkWell(
                child: Icon(
                  Icons.cancel,
                  size: 25,
                  color: colorBlack.withOpacity(0.35),
                ),
                onTap: ScaffoldMessenger.of(context).hideCurrentSnackBar,
              ),
            ),
        ],
      ),
    ),
  );
}
