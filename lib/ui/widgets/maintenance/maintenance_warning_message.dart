import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class MaintenanceWarningMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        color: colorSnackBarRed.withOpacity(0.9),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 8.0,
        ),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(
                Icons.warning,
                color: colorWhite,
                size: 35,
              ),
            ),
            Expanded(
              child: HtmlTagTextFormatted(
                text: localizations.translate('shared.maintenanceMessage'),
                openTag: '<b>',
                closeTag: '</b>',
                textAlign: TextAlign.center,
                boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                  color: colorWhite,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
                normalTextStyle: themeData.textTheme.bodyText2.copyWith(
                  color: colorWhite,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
