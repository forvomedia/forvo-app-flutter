import 'package:flutter/material.dart';

class CleanTextFormField extends StatefulWidget {
  final ThemeData themeData;
  final TextEditingController editingController;
  final bool required;
  final bool isLastElement;
  final Function(String) onValidate;
  final Function(String) onSubmit;

  CleanTextFormField({
    this.themeData,
    this.editingController,
    this.required,
    this.isLastElement = false,
    this.onValidate,
    this.onSubmit,
  });

  @override
  _CleanTextFormFieldState createState() => _CleanTextFormFieldState();
}

class _CleanTextFormFieldState extends State<CleanTextFormField> {
  @override
  Widget build(BuildContext context) => TextFormField(
        autocorrect: true,
        controller: widget.editingController,
        decoration: InputDecoration(
          errorMaxLines: 5,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: widget.isLastElement
                  ? Colors.transparent
                  : widget.themeData.dividerColor,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: widget.isLastElement
                  ? Colors.transparent
                  : widget.themeData.dividerColor,
            ),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red),
          ),
          fillColor: Theme.of(context).primaryColor,
        ),
        style: widget.themeData.textTheme.bodyText2.copyWith(
          fontStyle: FontStyle.normal,
        ),
        validator: widget.required ? widget.onValidate : null,
        onFieldSubmitted: widget.onSubmit,
      );
}
