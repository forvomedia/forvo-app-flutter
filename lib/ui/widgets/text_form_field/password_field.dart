import 'package:flutter/material.dart';

class PasswordField extends StatefulWidget {
  final ThemeData themeData;
  final Key fieldKey;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final String hintText;
  final String labelText;
  final String helperText;
  final TextStyle style;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final TextEditingController controller;
  final VoidCallback onEditingComplete;
  final ValueChanged<String> onFieldSubmitted;
  final FocusNode focusNode;

  const PasswordField({
    this.themeData,
    this.fieldKey,
    this.keyboardType,
    this.textInputAction,
    this.hintText,
    this.labelText,
    this.helperText,
    this.style,
    this.onSaved,
    this.validator,
    this.controller,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.focusNode,
  });

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) => TextFormField(
        key: widget.fieldKey,
        focusNode: widget.focusNode,
        keyboardType: widget.keyboardType,
        textInputAction: widget.textInputAction,
        obscureText: _obscureText,
        onSaved: widget.onSaved,
        validator: widget.validator,
        controller: widget.controller,
        onFieldSubmitted: widget.onFieldSubmitted,
        onEditingComplete: widget.onEditingComplete,
        style: widget.style,
        decoration: InputDecoration(
          border: widget.themeData.inputDecorationTheme.border,
          enabledBorder: widget.themeData.inputDecorationTheme.enabledBorder,
          hintText: widget.hintText,
          labelText: widget.labelText,
          helperText: widget.helperText,
          suffixIcon: GestureDetector(
            onTap: () => setState(() {
              _obscureText = !_obscureText;
            }),
            child: Icon(
              _obscureText ? Icons.visibility : Icons.visibility_off,
              color: Theme.of(context).iconTheme.color,
            ),
          ),
        ),
      );
}
