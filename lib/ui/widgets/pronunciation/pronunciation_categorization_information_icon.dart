import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/menu/word_categorization_group_info_modal_menu.dart';

class PronunciationCategorizationInformationIcon extends StatelessWidget {
  final WordCategorizationGroup categorizationGroup;
  final String languageCode;

  PronunciationCategorizationInformationIcon({
    @required this.categorizationGroup,
    @required this.languageCode,
  });

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);

    return IconButton(
      icon: Icon(
        Icons.info,
        color: colorBlue,
        semanticLabel: localizations.translate('shared.moreInformation'),
        size: 30,
      ),
      tooltip: localizations.translate('shared.moreInformation'),
      onPressed: () {
        onWordCategorizationGroupInfoMenuOpen(
          context,
          categorizationGroup,
          languageCode,
        );
      },
    );
  }
}
