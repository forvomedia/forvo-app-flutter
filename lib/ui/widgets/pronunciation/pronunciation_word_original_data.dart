import 'package:flutter/material.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_categorization_information_icon.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class PronunciationWordOriginalData extends StatelessWidget {
  final Pronunciation pronunciation;

  PronunciationWordOriginalData({@required this.pronunciation});

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    if (pronunciation.pronunciationGroupIndex != null &&
        pronunciation.wordCategorizationGroup != null) {
      List<Widget> widgets = [];
      if (pronunciation.wordCategorizationGroup.minimumCategories != null &&
          pronunciation.wordCategorizationGroup.minimumCategories.isNotEmpty) {
        for (WordCategory category
            in pronunciation.wordCategorizationGroup.minimumCategories) {
          for (WordCategorySpelling categorySpelling in category.data) {
            String categoryName =
                pronunciation.languageCode == state.locale.languageCode
                    ? categorySpelling.nativeCategoryName
                    : categorySpelling.englishCategoryName;
            widgets.add(
              HtmlTagTextFormatted(
                text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
                openTag: '<b>',
                closeTag: '</b>',
                boldTextStyle: themeData.textTheme.bodyText2,
                normalTextStyle: themeData.textTheme.subtitle1,
              ),
            );
          }
        }
      }
      if (widgets.isNotEmpty) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: widgets,
              ),
            ),
            pronunciation.wordCategorizationGroup.showFullInformation
                ? PronunciationCategorizationInformationIcon(
                    categorizationGroup: pronunciation.wordCategorizationGroup,
                    languageCode: pronunciation.languageCode,
                  )
                : Container()
          ],
        );
      }
    }

    List<Widget> widgets = [];
    if (pronunciation.wordInformation != null &&
        pronunciation.wordInformation.original.isNotEmpty) {
      widgets.add(
        Text(
          pronunciation.wordInformation.original,
          style: themeData.textTheme.bodyText2,
        ),
      );
    } else {
      widgets.add(
        Text(
          pronunciation.original,
          style: themeData.textTheme.bodyText2,
        ),
      );
    }

    if (pronunciation.wordInformation != null &&
        pronunciation.wordInformation.alternativeSpellings != null &&
        pronunciation.wordInformation.alternativeSpellings.isNotEmpty) {
      for (WordSpelling spelling
          in pronunciation.wordInformation.alternativeSpellings) {
        if (spelling.spelling.isNotEmpty) {
          widgets.add(
            Text(
              spelling.spelling,
              style: themeData.textTheme.bodyText2,
            ),
          );
        }
      }
    }

    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: widgets,
          ),
        ),
      ],
    );
  }
}
