import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class BlueBubbleSuggestionMessage extends StatelessWidget {
  final String message;
  final Function onTap;
  final Icon secondaryIcon;

  BlueBubbleSuggestionMessage({
    @required this.message,
    this.onTap,
    this.secondaryIcon,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);

    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            const Radius.circular(10),
          ),
          color: colorLightBlue,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 15.0,
          vertical: 15.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.notifications_none,
              size: 25,
              color: colorBlue,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 14.0),
                child: HtmlTagTextFormatted(
                  text: message,
                  openTag: '<b>',
                  closeTag: '</b>',
                  normalTextStyle: themeData.textTheme.headline4,
                  boldTextStyle: themeData.textTheme.headline4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            secondaryIcon ?? Container(),
          ],
        ),
      ),
    );
  }
}
