import 'package:flutter/material.dart';

class RecordButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => null,
        child: Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/record.png'),
            ),
          ),
        ),
      );
}
