import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/playable_file.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/audio_util.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/file_util.dart';
import 'package:forvo/util/snackbar_util.dart';

class PlayButton extends StatefulWidget {
  final int id;
  final String url;
  final String folderPath;

  PlayButton({
    @required this.id,
    @required this.url,
    @required this.folderPath,
  });

  @override
  _PlayButtonState createState() => _PlayButtonState();
}

class _PlayButtonState extends State<PlayButton> {
  bool _isPlaying = false;

  _play({
    @required BuildContext context,
    @required PlayableFile file,
  }) async {
    var localizations = AppLocalizations.of(context);
    setState(() {
      _isPlaying = true;
    });
    try {
      await playAudio(context, file);
    } on Exception catch (_) {
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      Timer(const Duration(milliseconds: 4500), () {
        if (mounted) {
          setState(() {
            _isPlaying = false;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () async {
          AppLocalizations localizations = AppLocalizations.of(context);

          String _url = widget.url;
          int _id = widget.id;
          String _folderPath = widget.folderPath;
          PlayableFile _file;
          bool _localFileExists = false;

          String _filename = getFilenameFromUrl(_url);
          String _localFilePath = '$_folderPath/$_filename';

          bool _directoryExists = await directoryExists(_folderPath);
          if (_directoryExists != null && _directoryExists) {
            _localFileExists = await fileExists(_localFilePath);
          } else {
            await createDirectory(_folderPath);
          }

          _file = PlayableFile(
            id: _id,
            url: _url,
            localPath: _localFilePath,
            isLocal: _localFileExists,
          );

          var _deviceHasConnection = await deviceHasConnection();
          if (!_localFileExists &&
              _deviceHasConnection != null &&
              _deviceHasConnection == false) {
            showConnectivityWarningDialog(context, localizations);
          } else {
            await _play(
              context: context,
              file: _file,
            );
          }
        },
        child: Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                _isPlaying
                    ? 'assets/images/play.gif'
                    : 'assets/images/play.png',
              ),
            ),
          ),
        ),
      );
}
