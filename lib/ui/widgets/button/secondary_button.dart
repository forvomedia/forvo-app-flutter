import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class SecondaryButton extends StatefulWidget {
  final String caption;
  final GestureTapCallback onPressed;
  final bool isEnabled;
  final bool isLoading;
  final Color buttonTextColor;

  SecondaryButton({
    @required this.onPressed,
    this.caption,
    this.isEnabled = true,
    this.isLoading = false,
    this.buttonTextColor = colorBlue,
  });

  @override
  _SecondaryButtonState createState() => _SecondaryButtonState();
}

class _SecondaryButtonState extends State<SecondaryButton> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);

    List<Widget> _getButtonContent() {
      final List<Widget> buttonWidgets = <Widget>[];

      if (widget.isLoading) {
        buttonWidgets.add(
          SizedBox(
            height: 20.0,
            width: 20.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(colorBlue),
            ),
          ),
        );
      }

      double disabledButtonTextOpacity = 0.6;
      TextStyle _buttonTextStyle = themeData.textTheme.button.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 15.0,
        color: widget.isEnabled
            ? widget.buttonTextColor
            : themeIsDark(context)
                ? colorDarkThemeGrey.withOpacity(disabledButtonTextOpacity)
                : colorLightBlack.withOpacity(disabledButtonTextOpacity),
      );

      buttonWidgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Text(
            widget.caption,
            style: _buttonTextStyle,
          ),
        ),
      );

      return buttonWidgets;
    }

    return RawMaterialButton(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _getButtonContent(),
      ),
      onPressed: widget.isEnabled
          ? !widget.isLoading
              ? widget.onPressed
              : () => null
          : null,
    );
  }
}
