import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class MainButton extends StatefulWidget {
  final String caption;
  final GestureTapCallback onPressed;
  final bool isEnabled;
  final bool isLoading;
  final Color buttonTextColor;
  final Color buttonColor;
  final Color buttonInactiveColor;
  final double buttonMinimumWidth;
  final ShapeBorder shape;
  final bool usePadding;
  final Color isLoadingCircularIndicatorColor;
  final double isLoadingCircularIndicatorSize;

  MainButton({
    @required this.onPressed,
    @required this.caption,
    this.isEnabled = true,
    this.isLoading = false,
    this.buttonTextColor = colorWhite,
    this.buttonColor = colorBlue,
    this.buttonInactiveColor = colorLightThemeGrey,
    this.buttonMinimumWidth = 54,
    this.shape,
    this.usePadding = true,
    this.isLoadingCircularIndicatorColor = colorWhite,
    this.isLoadingCircularIndicatorSize = 20.0,
  });

  @override
  _MainButtonState createState() => _MainButtonState();
}

class _MainButtonState extends State<MainButton> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);

    List<Widget> _getButtonContent() {
      final List<Widget> buttonWidgets = <Widget>[];
      if (widget.isLoading) {
        buttonWidgets.add(
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: SizedBox(
              height: widget.isLoadingCircularIndicatorSize,
              width: widget.isLoadingCircularIndicatorSize,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  widget.isLoadingCircularIndicatorColor,
                ),
              ),
            ),
          ),
        );
      }

      double disabledButtonTextOpacity = 0.6;
      TextStyle _buttonTextStyle = themeData.textTheme.button.copyWith(
        color: widget.isEnabled
            ? widget.buttonTextColor
            : themeIsDark(context)
                ? colorDarkThemeGrey.withOpacity(disabledButtonTextOpacity)
                : colorLightBlack.withOpacity(disabledButtonTextOpacity),
      );

      buttonWidgets.add(
        Padding(
          padding: widget.usePadding
              ? const EdgeInsets.only(left: 10.0)
              : const EdgeInsets.only(left: 0.0),
          child: Text(
            widget.caption,
            style: _buttonTextStyle,
          ),
        ),
      );

      return buttonWidgets;
    }

    return ElevatedButton(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _getButtonContent(),
      ),
      onPressed: widget.isEnabled
          ? !widget.isLoading
              ? widget.onPressed
              : () => null
          : null,
      style: ElevatedButton.styleFrom(
        primary: widget.buttonColor,
        onSurface: themeIsDark(context)
            ? widget.buttonInactiveColor
            : colorDarkThemeGrey,
        minimumSize: Size(widget.buttonMinimumWidth, 54.0),
        elevation: 2.0,
        shape: widget.shape ?? themeData.buttonTheme.shape,
      ),
    );
  }
}
