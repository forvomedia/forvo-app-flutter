import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';

class CustomTooltip extends StatelessWidget {
  final String message;
  final Widget child;

  CustomTooltip({
    @required this.message,
    @required this.child,
  });

  @override
  Widget build(BuildContext context) => Tooltip(
        message: message,
        padding: const EdgeInsets.symmetric(
          horizontal: 15.0,
          vertical: 15.0,
        ),
        margin: const EdgeInsets.symmetric(
          horizontal: 15.0,
          vertical: 5.0,
        ),
        preferBelow: false,
        showDuration:
            const Duration(seconds: CUSTOM_TOOLTIP_SHOW_DURATION_SECONDS),
        waitDuration:
            const Duration(seconds: CUSTOM_TOOLTIP_WAIT_DURATION_SECONDS),
        child: child,
      );
}
