import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RightSwitchExtended extends StatefulWidget {
  final String title;
  final String subtitle;
  final Function onChange;
  final bool value;
  final String subtitleExtended;
  final Function onChangeExtended;
  final bool valueExtended;

  const RightSwitchExtended({
    @required this.title,
    @required this.subtitle,
    @required this.onChange,
    @required this.value,
    @required this.subtitleExtended,
    @required this.onChangeExtended,
    @required this.valueExtended,
  });

  @override
  _RightSwitchExtendedState createState() => _RightSwitchExtendedState();
}

class _RightSwitchExtendedState extends State<RightSwitchExtended> {
  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Switch(
              value: widget.value,
              onChanged: (value) {
                widget.onChange(value);
              },
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.title,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  Text(
                    widget.subtitle,
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 20.0),
                    //symmetric(vertical: 20.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Switch(
                          value: widget.valueExtended,
                          onChanged: widget.onChangeExtended != null
                              ? (value) {
                                  widget.onChangeExtended(value);
                                }
                              : null,
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.subtitleExtended,
                                style: Theme.of(context).textTheme.subtitle1,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
}
