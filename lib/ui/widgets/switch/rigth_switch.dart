import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RightSwitch extends StatefulWidget {
  final String title;
  final String subtitle;
  final Function onChange;
  final bool value;

  const RightSwitch({
    @required this.title,
    @required this.subtitle,
    @required this.onChange,
    @required this.value,
  });

  @override
  _RightSwitchState createState() => _RightSwitchState();
}

class _RightSwitchState extends State<RightSwitch> {
  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Row(
          crossAxisAlignment:
              (widget.subtitle != null && widget.subtitle.isNotEmpty)
                  ? CrossAxisAlignment.start
                  : CrossAxisAlignment.center,
          children: <Widget>[
            Switch(
              value: widget.value,
              onChanged: (value) {
                widget.onChange(value);
              },
            ),
            (widget.subtitle != null && widget.subtitle.isNotEmpty)
                ? Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.title,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        Text(
                          widget.subtitle,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ],
                    ),
                  )
                : Text(
                    widget.title,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
          ],
        ),
      );
}
