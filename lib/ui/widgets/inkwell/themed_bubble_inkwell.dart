import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/theme_util.dart';

class ThemedBubbleInkWell extends StatelessWidget {
  final String message;
  final String iconType;
  final Function onTap;

  ThemedBubbleInkWell({
    @required this.message,
    @required this.iconType,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var _themeIsDark = themeIsDark(context);
    double iconSize = 20;
    IconData iconData = ForvoIcons.addWord;
    if (iconType == THEMED_BUBBLE_ICON_TYPE_PRONUNCIATION) {
      iconData = ForvoIcons.mic;
    }
    Icon icon = Icon(
      iconData,
      size: iconSize,
      color: _themeIsDark ? colorWhite : colorBlue,
    );

    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            const Radius.circular(10),
          ),
          color: _themeIsDark ? colorDarkThemeLightGrey : colorLightBlue,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 15.0,
          vertical: 15.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            icon,
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: HtmlTagTextFormatted(
                text: message,
                openTag: '<b>',
                closeTag: '</b>',
                normalTextStyle: themeData.textTheme.headline4.copyWith(
                  fontSize: 15,
                  color: _themeIsDark ? colorWhite : null,
                ),
                boldTextStyle: themeData.textTheme.headline4.copyWith(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: _themeIsDark ? colorWhite : null,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
