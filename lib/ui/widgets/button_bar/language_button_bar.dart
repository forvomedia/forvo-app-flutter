import 'package:flutter/material.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/theme_util.dart';

class LanguageButtonBar extends StatefulWidget {
  final List<Language> languages;
  final Function(int) onPressed;
  final Color buttonTextColor;
  final Color buttonColor;
  final Color buttonSelectedColor;
  final int selectedIndex;

  LanguageButtonBar({
    @required this.languages,
    @required this.onPressed,
    this.selectedIndex = 0,
    this.buttonTextColor = colorWhite,
    this.buttonColor = colorDarkBlue,
    this.buttonSelectedColor = colorBlue,
  });

  @override
  _LanguageButtonBarState createState() => _LanguageButtonBarState();
}

class _LanguageButtonBarState extends State<LanguageButtonBar> {
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.selectedIndex != null ? widget.selectedIndex : 0;
  }

  @override
  Widget build(BuildContext context) {
    Widget createButton(Language language) {
      double buttonWidth = getDeviceWidth(context) / 4.10;
      int index = widget.languages.indexOf(language);

      return language.name.isNotEmpty
          ? ButtonTheme(
              minWidth: buttonWidth,
              buttonColor: colorBlue,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: ContinuousRectangleBorder(
                    side: BorderSide(
                      width: 0.5,
                      color: themeIsDark(context) ? colorBlack : colorWhite,
                    ),
                  ),
                  primary: index == _selectedIndex
                      ? widget.buttonSelectedColor
                      : widget.buttonColor,
                  onPrimary: widget.buttonTextColor,
                ),
                child: Text(language.name),
                onPressed: () {
                  setState(() {
                    _selectedIndex = index;
                  });
                  widget.onPressed(index);
                },
              ),
            )
          : Container();
    }

    return Container(
      color: widget.buttonColor,
      height: 55,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: widget.languages.map(createButton).toList(),
      ),
    );
  }
}
