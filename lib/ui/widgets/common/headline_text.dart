import 'package:flutter/material.dart';

class HeadlineText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;

  HeadlineText({
    @required this.text,
    @required this.textStyle,
  });

  @override
  Widget build(BuildContext context) => Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Text(
        text,
        style: textStyle,
      ),
    );
}
