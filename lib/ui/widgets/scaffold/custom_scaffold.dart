import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_bar.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/theme_util.dart';

class CustomScaffold extends StatefulWidget {
  final bool headerAppBarHidden;
  final String headerAppBarTitle;
  final bool headerAppBarCenterTitle;
  final int headerAppBarIndex;
  final bool headerAppBarHideActions;
  final List<Widget> headerAppBarActions;
  final int bottomNavBarIndex;
  final bool bottomNavBarIndexIsActive;
  final Function(int) bottomNavBarOnTap;
  final bool bottomNavBarHidden;
  final bool resizeToAvoidBottomInset;
  final Widget body;
  final Color scaffoldBackgroundColor;
  final bool floatingActionButtonActive;
  final FloatingActionButtonLocation floatingActionButtonLocation;
  final Widget floatingActionButton;
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey;

  CustomScaffold({
    Key key,
    this.headerAppBarHidden = false,
    this.headerAppBarTitle,
    this.headerAppBarCenterTitle = true,
    this.headerAppBarIndex,
    this.headerAppBarHideActions = false,
    this.headerAppBarActions,
    this.bottomNavBarIndex,
    this.bottomNavBarIndexIsActive = true,
    this.bottomNavBarOnTap,
    this.bottomNavBarHidden = false,
    this.resizeToAvoidBottomInset = true,
    this.body,
    this.scaffoldBackgroundColor,
    this.floatingActionButtonActive = false,
    this.floatingActionButtonLocation,
    this.floatingActionButton,
    this.scaffoldMessengerKey,
  }) : super(key: key);

  @override
  CustomScaffoldState createState() => CustomScaffoldState();
}

class CustomScaffoldState extends State<CustomScaffold> {
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey = GlobalKey<
      ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
  }

  showSnackBar(SnackBar snackBar) {
    if (widget.scaffoldMessengerKey != null){
      widget.scaffoldMessengerKey.currentState.showSnackBar(snackBar);
    } else {
      _scaffoldMessengerKey.currentState.showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return SafeArea(
      bottom: deviceIsAndroid() || widget.bottomNavBarHidden,
      child: ScaffoldMessenger(
        key: widget.scaffoldMessengerKey ?? _scaffoldMessengerKey,
        child: Scaffold(
          resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
          backgroundColor: widget.scaffoldBackgroundColor ??
              themeData.scaffoldBackgroundColor,
          appBar: widget.headerAppBarHidden
              ? null
              : HeaderAppBar(
            context: context,
            localizations: localizations,
            currentIndex: widget.headerAppBarIndex,
            title: widget.headerAppBarTitle,
            centerTitle: widget.headerAppBarCenterTitle,
            hideActions: widget.headerAppBarHideActions,
            headerActions: widget.headerAppBarActions,
          ),
          bottomNavigationBar: widget.bottomNavBarHidden
              ? null
              : DashboardBottomBar(
            themeData: themeData,
            localizations: localizations,
            currentIndex: widget.bottomNavBarIndex,
            currentIndexIsActive: widget.bottomNavBarIndexIsActive,
            themeIsDark: themeIsDark(context),
            onTap: widget.bottomNavBarOnTap ??
                    (int index) {
                  if (index == BOTTOM_BAR_SECTION_SEARCH) {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.HOME,
                          (route) => false,
                    );
                  } else if (index == BOTTOM_BAR_SECTION_FAVORITES) {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.MY_FAVORITE_PRONUNCIATIONS,
                          (route) => false,
                    );
                  } else if (index == BOTTOM_BAR_SECTION_NOTIFICATIONS) {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.NOTIFICATIONS,
                          (route) => false,
                    );
                  } else {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.MY_ACCOUNT,
                          (route) => false,
                    );
                  }
                },
          ),
          body: widget.body,
          floatingActionButton: widget.floatingActionButtonActive
              ? widget.floatingActionButton
              : null,
          floatingActionButtonLocation:
          widget.floatingActionButtonLocation != null
              ? widget.floatingActionButtonLocation
              : FloatingActionButtonLocation.centerFloat,
        ),
      ),
    );
  }
}
