import 'package:flutter/material.dart';

class HtmlTagTextFormatted extends StatelessWidget {
  final String text;
  final String openTag;
  final String closeTag;
  final TextStyle normalTextStyle;
  final TextStyle boldTextStyle;
  final TextAlign textAlign;

  HtmlTagTextFormatted({
    @required this.text,
    @required this.openTag,
    @required this.closeTag,
    @required this.normalTextStyle,
    @required this.boldTextStyle,
    this.textAlign,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String separator = '-_-_-_';

    String openTagReplacedText =
        text.replaceAll(RegExp(openTag), '$separator$openTag');
    String closedTagReplacedText =
        openTagReplacedText.replaceAll(RegExp(closeTag), separator);

    List<String> pieces = closedTagReplacedText.split(RegExp(separator));

    List<TextSpan> piecesFormatted = _getTextPiecesFormatted(pieces);

    return RichText(
      textAlign: textAlign ?? TextAlign.start,
      text: TextSpan(
        text: '',
        style: normalTextStyle,
        children: piecesFormatted,
      ),
    );
  }

  List<TextSpan> _getTextPiecesFormatted(
    List<String> pieces,
  ) =>
      pieces
          .map(
            (String piece) => TextSpan(
              style:
                  piece.startsWith(openTag) ? boldTextStyle : normalTextStyle,
              text: piece.replaceAll(RegExp(openTag), ''),
            ),
          )
          .toList();
}
