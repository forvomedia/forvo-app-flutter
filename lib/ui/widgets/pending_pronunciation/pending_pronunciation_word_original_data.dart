import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_categorization_information_icon.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class PendingPronunciationWordOriginalData extends StatelessWidget {
  final PendingPronounceWord word;
  final TextStyle textStyle;
  final TextAlign textAlign;
  final CrossAxisAlignment columnCrossAxisAlignment;

  PendingPronunciationWordOriginalData({
    @required this.word,
    this.textStyle,
    this.textAlign,
    this.columnCrossAxisAlignment,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;
    if (word.pronunciationGroupIndex != null &&
        word.wordCategorizationGroup != null) {
      List<Widget> widgets = [];
      if (word.wordCategorizationGroup.minimumCategories != null &&
          word.wordCategorizationGroup.minimumCategories.isNotEmpty) {
        for (WordCategory category
            in word.wordCategorizationGroup.minimumCategories) {
          for (WordCategorySpelling categorySpelling in category.data) {
            String categoryName = word.languageCode == state.locale.languageCode
                ? categorySpelling.nativeCategoryName
                : categorySpelling.englishCategoryName;
            widgets.add(
              HtmlTagTextFormatted(
                text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
                openTag: '<b>',
                closeTag: '</b>',
                boldTextStyle: textStyle ?? themeData.textTheme.bodyText2,
                normalTextStyle: themeData.textTheme.subtitle1,
                textAlign: textAlign,
              ),
            );
          }
        }
      }
      if (widgets.isNotEmpty) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment:
                    columnCrossAxisAlignment ?? CrossAxisAlignment.start,
                children: widgets,
              ),
            ),
            word.wordCategorizationGroup.showFullInformation
                ? PronunciationCategorizationInformationIcon(
                    categorizationGroup: word.wordCategorizationGroup,
                    languageCode: word.languageCode,
                  )
                : Container()
          ],
        );
      }
    }

    List<Widget> widgets = [];
    if (word.wordInformation != null &&
        word.wordInformation.original.isNotEmpty) {
      widgets.add(
        Text(
          word.wordInformation.original,
          style: textStyle ?? themeData.textTheme.bodyText2,
        ),
      );
    } else {
      widgets.add(
        Text(
          word.original,
          textAlign: textAlign ?? TextAlign.start,
          style: textStyle ?? themeData.textTheme.bodyText2,
        ),
      );
    }

    if (word.wordInformation != null &&
        word.wordInformation.alternativeSpellings != null &&
        word.wordInformation.alternativeSpellings.isNotEmpty) {
      for (WordSpelling spelling in word.wordInformation.alternativeSpellings) {
        if (spelling.spelling.isNotEmpty) {
          widgets.add(
            Text(
              spelling.spelling,
              style: textStyle ?? themeData.textTheme.bodyText2,
            ),
          );
        }
      }
    }

    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment:
                columnCrossAxisAlignment ?? CrossAxisAlignment.start,
            children: widgets,
          ),
        ),
      ],
    );
  }
}
