import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/menu/slide_bar.dart';
import 'package:forvo/util/language_util.dart';

const menuOptionHeight = 55.0;

onLanguageMenuOpen(
  BuildContext context, {
  List<Language> languages,
  List<Language> popularLanguages,
  List<Language> userLanguages,
  List<Language> suggestedLanguages,
  Function(Language) onChangeLanguage,
}) async {
  double bottomSheetChildSize = 0.5;
  int totalOptions = languages.length + popularLanguages?.length ??
      0 + userLanguages?.length ??
      0 + suggestedLanguages?.length ??
      0;

  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: ModalBottomSheepBaseShape(),
    builder: (builderContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        height: menuOptionHeight * totalOptions,
        color: Colors.transparent,
        child: Container(
          child: BottomNavigationMenu(
            context: context,
            languages: languages,
            onChangeLanguage: onChangeLanguage,
            popularLanguages: popularLanguages,
            userLanguages: userLanguages,
            suggestedLanguages: suggestedLanguages,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20),
            ),
          ),
        ),
      ),
    ),
  );
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final List<Language> languages;
  final List<Language> popularLanguages;
  final List<Language> userLanguages;
  final List<Language> suggestedLanguages;
  final Function(Language) onChangeLanguage;

  BottomNavigationMenu({
    @required this.context,
    @required this.languages,
    @required this.onChangeLanguage,
    this.popularLanguages,
    this.userLanguages,
    this.suggestedLanguages,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    List<Widget> widgets = [];

    if (widget.suggestedLanguages != null &&
        widget.suggestedLanguages.isNotEmpty) {
      widgets.add(
        _menuOption(
          themeData,
          localizations.translate('language.suggested'),
          () => null,
          fontWeight: FontWeight.bold,
        ),
      );
      widgets.addAll(
        widget.suggestedLanguages
            .map(
              (Language language) => _menuOption(
                themeData,
                getLanguageNameAndCode(language),
                () => widget.onChangeLanguage(language),
                forcePaddingLeft: true,
              ),
            )
            .toList(),
      );
    }

    if (widget.userLanguages != null) {
      widgets.add(
        _menuOption(
          themeData,
          localizations.translate('language.recent'),
          () => null,
          fontWeight: FontWeight.bold,
        ),
      );
      widgets.addAll(
        widget.userLanguages
            .map(
              (Language language) => _menuOption(
                themeData,
                getLanguageNameAndCode(language),
                () => widget.onChangeLanguage(language),
                forcePaddingLeft: true,
              ),
            )
            .toList(),
      );
    }

    if (widget.popularLanguages != null) {
      widgets.add(
        _menuOption(
          themeData,
          localizations.translate('language.popular'),
          () => null,
          fontWeight: FontWeight.bold,
        ),
      );
      widgets.addAll(
        widget.popularLanguages
            .map(
              (Language language) => _menuOption(
                themeData,
                getLanguageNameAndCode(language),
                () => widget.onChangeLanguage(language),
                forcePaddingLeft: true,
              ),
            )
            .toList(),
      );
    }

    widgets.add(
      _menuOption(
        themeData,
        localizations.translate('language.alphabetically'),
        () => null,
        fontWeight: FontWeight.bold,
      ),
    );
    widgets.addAll(
      widget.languages
          .map(
            (Language language) => _menuOption(
              themeData,
              getLanguageNameAndCode(language),
              () => widget.onChangeLanguage(language),
              forcePaddingLeft: true,
            ),
          )
          .toList(),
    );

    return Column(
      children: [
        SlideBar(),
        Expanded(
          child: ListView(
            children: widgets,
          ),
        ),
      ],
    );
  }

  Widget _menuOption(
    ThemeData themeData,
    String title,
    Function() onTap, {
    FontWeight fontWeight = FontWeight.normal,
    bool forcePaddingLeft = false,
  }) =>
      Container(
        height: menuOptionHeight,
        padding: EdgeInsets.only(left: forcePaddingLeft ? 30.0 : 0.0),
        child: ListTile(
          title: Text(
            title,
            style: themeData.textTheme.bodyText2.copyWith(
              fontWeight: fontWeight,
            ),
          ),
          onTap: onTap,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );
}
