import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/menu/slide_bar.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/theme_util.dart';

const menuOptionHeight = 55.0;
const totalOptions = 8;

void onWordCategorizationGroupsMenuOpen(
  BuildContext context,
  String wordLanguageCode,
  List<WordCategorizationGroup> categorizationGroups,
  Function(int) onSelectGroup,
) {
  double bottomSheetChildSize = 0.5;

  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: ModalBottomSheepBaseShape(),
    builder: (builderContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        height: menuOptionHeight * totalOptions,
        color: Colors.transparent,
        child: Container(
          child: BottomNavigationMenu(
            context: context,
            wordLanguageCode: wordLanguageCode,
            categorizationGroups: categorizationGroups,
            onSelectGroup: onSelectGroup,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20),
            ),
          ),
        ),
      ),
    ),
  );
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final String wordLanguageCode;
  final List<WordCategorizationGroup> categorizationGroups;
  final Function onSelectGroup;

  BottomNavigationMenu({
    @required this.context,
    @required this.wordLanguageCode,
    @required this.categorizationGroups,
    @required this.onSelectGroup,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    List<Widget> widgets = [];
    widgets.add(
      Text(
        localizations.translate('word.selectGroup'),
        style: themeData.textTheme.bodyText2.copyWith(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: themeIsDark(context) ? colorWhite : colorBlack,
        ),
      ),
    );
    widgets.add(
      Divider(
        color: themeData.dividerColor,
        thickness: 3,
      ),
    );

    for (WordCategorizationGroup categorizationGroup
        in widget.categorizationGroups) {
      List<Widget> categorizationGroupCategories = [];

      for (WordCategory category in categorizationGroup.minimumCategories) {
        for (WordCategorySpelling categorySpelling in category.data) {
          String categoryName =
              widget.wordLanguageCode == state.locale.languageCode
                  ? categorySpelling.nativeCategoryName
                  : categorySpelling.englishCategoryName;

          categorizationGroupCategories.add(
            HtmlTagTextFormatted(
              text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
              openTag: '<b>',
              closeTag: '</b>',
              boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
              normalTextStyle: themeData.textTheme.subtitle1.copyWith(
                fontSize: 14,
              ),
            ),
          );
        }
      }
      widgets.add(
        _menuOption(
          themeData,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: categorizationGroupCategories,
          ),
          () => widget.onSelectGroup(categorizationGroup.groupIndex),
        ),
      );
    }

    return Column(
      children: [
        SlideBar(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: ListView(
              children: widgets,
            ),
          ),
        ),
      ],
    );
  }

  Widget _menuOption(
    ThemeData themeData,
    Widget data,
    Function onTap,
  ) =>
      Container(
        // height: menuOptionHeight,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: ListTile(
            title: data,
            onTap: onTap,
          ),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );
}
