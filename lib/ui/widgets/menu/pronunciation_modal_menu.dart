import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/layout_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/playable_file.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/followed_user_api_service.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/account/public_profile.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_detail.dart';
import 'package:forvo/ui/report/report_pronunciation.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/menu/slide_bar.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/download_util.dart';
import 'package:forvo/util/file_util.dart';
import 'package:forvo/util/maintenance_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:share_plus/share_plus.dart';

const menuOptionHeight = 55.0;

void onPronunciationMenuOpen(
  BuildContext ctx,
  Pronunciation pronunciation,
  Function onFavorite, {
  LanguageWordInformation wordInformation,
}) {
  var state = AppStateManager.of(ctx).state;
  double bottomSheetChildSize = state.isUserLogged &&
          pronunciation.username == state.userInfo.user.username
      ? getDeviceHeight(ctx) > LayoutConstants.LAYOUT_HEIGHT_BREAKPOINT
          ? 0.35
          : 0.45
      : 0.5;
  showModalBottomSheet(
    context: ctx,
    isScrollControlled: true,
    shape: ModalBottomSheepBaseShape(),
    builder: (builderContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        child: BottomNavigationMenu(
          context: ctx,
          pronunciation: pronunciation,
          wordInformation: wordInformation,
          onFavorite: onFavorite,
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(20),
            topRight: const Radius.circular(20),
          ),
        ),
      ),
    ),
  );
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final Pronunciation pronunciation;
  final Function onFavorite;
  final LanguageWordInformation wordInformation;

  BottomNavigationMenu({
    @required this.context,
    @required this.pronunciation,
    @required this.onFavorite,
    @required this.wordInformation,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  bool _isUserLogged = false;
  UserInfo _user;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;
    _isUserLogged = state?.isUserLogged ?? false;
    _user = state?.userInfo;
    bool _isUserContributor = _user.user?.isContributor;
    bool _isMaintenanceActive = state.isMaintenanceReadOnlyActive;

    _favoritePronunciation() async {
      String favoriteAction = widget.pronunciation.markedAsFavorite
          ? PRONUNCIATION_FAVORITE_ACTION_NO
          : PRONUNCIATION_FAVORITE_ACTION_YES;

      try {
        Status status = await favoritePronunciation(
          context,
          widget.pronunciation.id,
          favoriteAction,
        );

        if (status.statusIsOk) {
          setState(() {
            widget.pronunciation.favorite =
                !widget.pronunciation.markedAsFavorite;
          });
          Navigator.pop(widget.context);
          showCustomSnackBar(
            widget.context,
            themedSnackBar(
              context,
              localizations.translate(widget.pronunciation.markedAsFavorite
                  ? 'menu.modal.pronunciation.markedAsFavorite'
                  : 'menu.modal.pronunciation.unmarkedAsFavorite'),
              type: SNACKBAR_TYPE_OK,
            ),
          );
          widget.onFavorite();
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String message;
        for (ApiError _error in errors) {
          String translate =
              localizations.translate('error.api.${_error.errorApiCode}');
          if (message == null) {
            message = translate;
          } else {
            message = '$message\n$translate';
          }
        }
        if (message.isNotEmpty) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate(widget.pronunciation.markedAsFavorite
                ? 'error.modal.pronunciation.markedAsFavorite'
                : 'error.modal.pronunciation.unmarkedAsFavorite'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    }

    _vote(String voteAction, int voteValue) async {
      debugPrint('VOTE > PRONUNCIATION ID: ${widget.pronunciation.id}');

      try {
        Status status = await votePronunciation(
          context,
          widget.pronunciation.id,
          voteAction,
        );

        if (status.statusIsOk && mounted) {
          setState(() {
            widget.pronunciation.vote = voteValue;
          });
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String message;
        for (ApiError _error in errors) {
          String translate =
              localizations.translate('error.api.${_error.errorApiCode}');
          if (message == null) {
            message = translate;
          } else {
            message = '$message\n$translate';
          }
        }
        if (message.isNotEmpty) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.modal.pronunciation.vote'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    }

    _votePositive() async {
      String voteAction = !widget.pronunciation.votedAsGood
          ? PRONUNCIATION_VOTE_ACTION_POSITIVE
          : PRONUNCIATION_VOTE_ACTION_NEGATIVE;

      int voteValue = !widget.pronunciation.votedAsGood
          ? PRONUNCIATION_VOTE_VALUE_POSITIVE
          : PRONUNCIATION_VOTE_VALUE_NEUTRAL;

      await _vote(voteAction, voteValue);
    }

    _voteNegative() async {
      String voteAction = !widget.pronunciation.votedAsBad
          ? PRONUNCIATION_VOTE_ACTION_NEGATIVE
          : PRONUNCIATION_VOTE_ACTION_POSITIVE;

      int voteValue = !widget.pronunciation.votedAsBad
          ? PRONUNCIATION_VOTE_VALUE_NEGATIVE
          : PRONUNCIATION_VOTE_VALUE_NEUTRAL;

      await _vote(voteAction, voteValue);
    }

    _reportPronunciation() async {
      ReportPronunciationArguments arguments = ReportPronunciationArguments(
        pronunciation: widget.pronunciation,
        wordInformation: widget.wordInformation,
      );
      var response = await Navigator.pushNamed(
        context,
        RouteConstants.REPORT_PRONUNCIATION,
        arguments: arguments,
      );
      if (response != null) {
        Navigator.pop(context);
      }
    }

    _followingUser() async {
      String followingAction = widget.pronunciation.markedAsFollowing
          ? PRONUNCIATION_FOLLOWING_USER_ACTION_NO
          : PRONUNCIATION_FOLLOWING_USER_ACTION_YES;

      try {
        Status status = await followUnfollowUser(
          context,
          widget.pronunciation.username,
          widget.pronunciation.languageCode,
          followingAction,
        );

        if (status.statusIsOk && mounted) {
          setState(() {
            widget.pronunciation.following =
                !widget.pronunciation.markedAsFollowing;
          });
          Navigator.pop(widget.context);
          showCustomSnackBar(
              widget.context,
              themedSnackBar(
                context,
                localizations.translate(
                  widget.pronunciation.markedAsFollowing
                      ? 'shared.followingUser'
                      : 'shared.notFollowingUser',
                  params: {'userName': widget.pronunciation.username},
                ),
                type: SNACKBAR_TYPE_OK,
              ));
          widget.onFavorite();
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String message;
        for (ApiError _error in errors) {
          String translate =
              localizations.translate('error.api.${_error.errorApiCode}');
          if (message == null) {
            message = translate;
          } else {
            message = '$message\n$translate';
          }
        }
        if (message.isNotEmpty) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              localizations.translate(
                widget.pronunciation.markedAsFollowing
                    ? 'error.modal.pronunciation.notFollowUser'
                    : 'error.modal.pronunciation.followUser',
                params: {'userName': widget.pronunciation.username},
              ),
              type: SNACKBAR_TYPE_ERROR,
            ));
      }
    }

    void _goToChat() => widget.pronunciation.pronouncerAllowsUserMessages
        ? Navigator.pushNamed(
            context,
            RouteConstants.CHAT_DETAIL,
            arguments: ChatConversationDetailArguments(
              chatConversation: null,
              userTo: widget.pronunciation.username,
            ),
          )
        : showCustomSnackBar(
            widget.context,
            themedSnackBar(
              context,
              localizations.translate(
                'messages.notAllowedUserMessagesWarning',
                params: {'userName': widget.pronunciation.username},
              ),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );

    void _shareContent() async {
      String _url = widget.pronunciation.audioRealMp3;
      int _id = widget.pronunciation.id;
      String _folderPath = widget.pronunciation.languageCode;
      PlayableFile _file;
      bool _localFileExists = false;

      String _filename = getFilenameFromUrl(_url);
      String _localFilePath = '$_folderPath/$_filename';

      bool _directoryExists = await directoryExists(_folderPath);
      if (_directoryExists != null && _directoryExists) {
        _localFileExists = await fileExists(_localFilePath);
      } else {
        await createDirectory(_folderPath);
      }

      _file = PlayableFile(
        id: _id,
        url: _url,
        localPath: _localFilePath,
        isLocal: _localFileExists,
      );

      String filePath = await getLocalFilePath(_file.localPath);
      if (!_localFileExists) {
        await downloadFile(_file.url, filePath);
      }

      String _content = localizations.translate(
        'menu.modal.pronunciation.shareText',
        params: {
          'word': widget.pronunciation.word,
          'wordOriginal': widget.pronunciation.original,
          'languageName': widget.pronunciation.languageName,
          'languageCode': widget.pronunciation.languageCode,
        },
      );

      Share.shareFiles([filePath], text: _content);
    }

    return Column(
      children: <Widget>[
        SlideBar(),
        Expanded(
          child: ListView(
            children: [
              _menuOption(
                themeData,
                localizations.translate(
                  'menu.modal.pronunciation.pronouncedBy',
                  params: {
                    'userName': !widget.pronunciation.userIsAnonymous
                        ? widget.pronunciation.username
                        : localizations.translate('shared.anonymousUser')
                  },
                ),
                null,
                () => null,
                fontWeight: FontWeight.bold,
                userIsActive: widget.pronunciation.userIsActive &&
                    !widget.pronunciation.userIsAnonymous,
              ),
              widget.pronunciation.userIsActive
                  ? _menuOption(
                      themeData,
                      localizations.translate(
                          'menu.modal.pronunciation.userInformation'),
                      Icon(
                        ForvoIcons.publicProfile,
                        size: 20,
                        color: colorBlue,
                      ),
                      () async {
                        var _updatedFollowingUser =
                            await Navigator.of(context).pushNamed(
                          RouteConstants.PUBLIC_PROFILE,
                          arguments: PublicProfileArguments(
                            username: widget.pronunciation.username,
                          ),
                        );
                        if (_updatedFollowingUser != null &&
                            widget.pronunciation.following !=
                                _updatedFollowingUser) {
                          setState(() {
                            widget.pronunciation.following =
                                _updatedFollowingUser;
                          });
                        }
                      },
                      forcePaddingLeft: true,
                    )
                  : Container(),
              _isUserLogged &&
                      _user.user.username != widget.pronunciation.username &&
                      widget.pronunciation.userIsActive
                  ? _menuOption(
                      themeData,
                      localizations.translate(
                        widget.pronunciation.markedAsFollowing
                            ? 'menu.modal.pronunciation.unfollowUser'
                            : 'menu.modal.pronunciation.followUser',
                        params: {'userName': widget.pronunciation.username},
                      ),
                      Icon(
                        ForvoIcons.followUsers,
                        //Icons.person_add,
                        size: 20,
                        color: widget.pronunciation.markedAsFollowing
                            ? colorFavouriteMarked
                            : colorBlue,
                      ),
                      _isMaintenanceActive
                          ? () =>
                              showMaintenanceSnackBar(context, localizations)
                          : _followingUser,
                      forcePaddingLeft: true,
                    )
                  : Container(),
              _isUserLogged &&
                      _user.user.username != widget.pronunciation.username &&
                      widget.pronunciation.userIsActive
                  ? _menuOption(
                      themeData,
                      localizations.translate(
                        'menu.modal.pronunciation.messageTo',
                      ),
                      Icon(
                        ForvoIcons.chat,
                        size: 20,
                        color: colorBlue,
                      ),
                      _goToChat,
                      forcePaddingLeft: true,
                    )
                  : Container(),
              widget.pronunciation.userIsActive &&
                      !widget.pronunciation.userIsAnonymous
                  ? _menuOption(
                      themeData,
                      localizations.translate(
                        'shared.options',
                      ),
                      null,
                      () => null,
                      fontWeight: FontWeight.bold,
                    )
                  : Container(),
              _menuOption(
                themeData,
                localizations.translate(widget.pronunciation.markedAsFavorite
                    ? 'menu.modal.pronunciation.unmarkAsFavorite'
                    : 'menu.modal.pronunciation.markAsFavorite'),
                Icon(
                  ForvoIcons.favoritesActive,
                  size: 20,
                  color: widget.pronunciation.markedAsFavorite
                      ? colorFavouriteMarked
                      : colorBlue,
                ),
                _isMaintenanceActive
                    ? () => showMaintenanceSnackBar(context, localizations)
                    : _favoritePronunciation,
                forcePaddingLeft: true,
              ),
              _isUserLogged &&
                      _user.user.username != widget.pronunciation.username
                  ? _menuOption(
                      themeData,
                      localizations.translate(widget.pronunciation.votedAsGood
                          ? 'menu.modal.pronunciation.removeVoteAsGood'
                          : 'menu.modal.pronunciation.voteAsGood'),
                      Icon(
                        Icons.thumb_up,
                        size: 20,
                        color: widget.pronunciation.votedAsGood
                            ? colorGreenGood
                            : colorBlue,
                      ),
                      _isMaintenanceActive
                          ? () =>
                              showMaintenanceSnackBar(context, localizations)
                          : _isUserContributor
                              ? _votePositive
                              : () => showLinguisticProfileAlertDialog(context),
                      forcePaddingLeft: true,
                    )
                  : Container(),
              _isUserLogged &&
                      _user.user.username != widget.pronunciation.username
                  ? _menuOption(
                      themeData,
                      localizations.translate(widget.pronunciation.votedAsBad
                          ? 'menu.modal.pronunciation.removeVoteAsBad'
                          : 'menu.modal.pronunciation.voteAsBad'),
                      Icon(
                        Icons.thumb_down,
                        size: 20,
                        color: widget.pronunciation.votedAsBad
                            ? colorRedBad
                            : colorBlue,
                      ),
                      _isMaintenanceActive
                          ? () =>
                              showMaintenanceSnackBar(context, localizations)
                          : _isUserContributor
                              ? _voteNegative
                              : () => showLinguisticProfileAlertDialog(context),
                      forcePaddingLeft: true,
                    )
                  : Container(),
              _menuOption(
                themeData,
                localizations
                    .translate('menu.modal.pronunciation.reportProblem'),
                Icon(
                  Icons.report,
                  size: 20,
                  color: colorBlue,
                ),
                _isMaintenanceActive
                    ? () => showMaintenanceSnackBar(context, localizations)
                    : _isUserContributor
                        ? _reportPronunciation
                        : () => showLinguisticProfileAlertDialog(context),
                forcePaddingLeft: true,
              ),
              _menuOption(
                themeData,
                localizations.translate('menu.modal.pronunciation.share'),
                Icon(
                  Icons.share,
                  size: 20,
                  color: colorBlue,
                ),
                _isMaintenanceActive
                    ? () => showMaintenanceSnackBar(context, localizations)
                    : _shareContent,
                forcePaddingLeft: true,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _menuOption(
    ThemeData themeData,
    String title,
    Icon icon,
    Function() onTap, {
    FontWeight fontWeight = FontWeight.normal,
    bool forcePaddingLeft = false,
    bool userIsActive = true,
  }) =>
      Container(
        padding: EdgeInsets.only(left: forcePaddingLeft ? 30.0 : 0.0),
        child: ListTile(
          leading: icon,
          title: title.contains('<b>')
              ? HtmlTagTextFormatted(
                  text: title,
                  openTag: '<b>',
                  closeTag: '</b>',
                  normalTextStyle: themeData.textTheme.bodyText2.copyWith(
                    fontWeight: fontWeight,
                  ),
                  boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                    fontWeight: fontWeight,
                    color: userIsActive
                        ? colorBlue
                        : themeIsDark(context)
                            ? colorDarkThemeGrey
                            : colorLightThemeGrey,
                  ),
                )
              : Text(
                  title,
                  style: themeData.textTheme.bodyText2.copyWith(
                    fontWeight: fontWeight,
                  ),
                ),
          onTap: onTap,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );
}
