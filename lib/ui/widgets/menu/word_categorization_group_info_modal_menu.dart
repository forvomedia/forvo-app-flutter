import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/menu/slide_bar.dart';
import 'package:forvo/util/theme_util.dart';

const menuOptionHeight = 70.0;
const totalOptions = 4;

void onWordCategorizationGroupInfoMenuOpen(
  BuildContext context,
  WordCategorizationGroup wordCategorizationGroup,
  String wordLanguageCode,
) {
  double bottomSheetChildSize = 0.5;

  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: ModalBottomSheepBaseShape(),
    builder: (builderContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        height: menuOptionHeight * totalOptions,
        color: Colors.transparent,
        child: Container(
          child: BottomNavigationMenu(
            context: context,
            categorizationGroup: wordCategorizationGroup,
            wordLanguageCode: wordLanguageCode,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20),
            ),
          ),
        ),
      ),
    ),
  );
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final WordCategorizationGroup categorizationGroup;
  final String wordLanguageCode;

  BottomNavigationMenu({
    @required this.context,
    @required this.categorizationGroup,
    @required this.wordLanguageCode,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    List<Widget> widgets = [];
    widgets.add(
      Text(
        localizations.translate('shared.moreInformation'),
        style: themeData.textTheme.bodyText2.copyWith(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: themeIsDark(context) ? colorWhite : colorBlack,
        ),
      ),
    );
    widgets.add(
      Divider(
        color: themeData.dividerColor,
        thickness: 3,
      ),
    );

    for (WordCategory category in widget.categorizationGroup.categories) {
      for (WordCategorySpelling spelling in category.data) {
        String categoryName = '';
        String explanation = '';
        if (widget.wordLanguageCode == state.locale.languageCode) {
          categoryName = spelling.nativeCategoryName;
          explanation = spelling.nativeExplanation;
        } else {
          categoryName = spelling.englishCategoryName;
          explanation = spelling.englishExplanation;
        }

        String title = spelling.spelling;
        String subtitle = categoryName;
        if (explanation.isNotEmpty) {
          subtitle = '$subtitle: $explanation';
        }

        widgets.add(_menuOption(themeData, title, subtitle));
      }
    }

    return Column(
      children: <Widget>[
        SlideBar(),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: ListView(
              children: widgets,
            ),
          ),
        ),
      ],
    );
  }

  Widget _menuOption(ThemeData themeData, String title, String subtitle) =>
      Container(
        // height: menuOptionHeight,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: ListTile(
            title: Text(
              title,
              style: themeData.textTheme.bodyText2,
            ),
            subtitle: Text(
              subtitle,
              style: themeData.textTheme.subtitle1,
            ),
          ),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );
}
