import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/followed_user_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/snackbar_util.dart';

const menuOptionHeight = 70.0;
const menuOptionHeightLang = 55.0;
const totalOptions = 8;

Future<bool> onFollowedUsersMenuOpen(
  BuildContext ctx,
  String followedUserName,
  Function onFollowing, {
  List<Language> languages,
}) async {
  double bottomSheetChildSize = 0.6;
  return showModalBottomSheet(
    context: ctx,
    isScrollControlled: true,
    enableDrag: false,
    shape: ModalBottomSheepBaseShape(),
    builder: (builderContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        height: menuOptionHeight * languages.length,
        color: Colors.transparent,
        child: Container(
          child: BottomNavigationMenu(
            context: context,
            followedUserName: followedUserName,
            onFollow: onFollowing,
            languages: languages,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20),
            ),
          ),
        ),
      ),
    ),
  ).then((value) => value);
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final String followedUserName;
  final Function onFollow;
  final List<Language> languages;

  BottomNavigationMenu({
    @required this.context,
    @required this.followedUserName,
    @required this.onFollow,
    @required this.languages,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  List<Language> _following;
  List<Language> _languages;
  bool _isLoading = false;
  bool _hasChanged = false;
  List<String> _loadingLanguages = [];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _languages = widget.languages;
    _fetchFollowedUser();
  }

  bool _contains(List<Language> list, String languageName) {
    bool contains = false;
    if (list != null && list.isNotEmpty) {
      for (var i in list) {
        if (i.name == languageName) {
          contains = true;
        }
      }
    }
    return contains;
  }

  void _remove(List<Language> list, Language language) {
    if (list != null && list.isNotEmpty) {
      Language remove;
      for (var i in list) {
        if (i.name == language.name) {
          remove = i;
        }
      }
      if (remove != null) {
        list.remove(remove);
      }
    }
  }

  _fetchFollowedUser() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    _languages = widget.languages;
    try {
      setState(() {
        _isLoading = true;
      });
      _following = await followingUserLangs(
        context,
        widget.followedUserName,
        interfaceLanguageCode: state.locale.languageCode,
      );
      setState(() {});
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String messages;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          default:
            String errorMessage =
                localizations.translate('error.api.${_error.errorApiCode}');
            if (messages == null) {
              messages = errorMessage;
            } else {
              messages = '$messages\n$errorMessage';
            }
            break;
        }
      }
      if (messages.isNotEmpty) {
        showCustomSnackBar(
          widget.context,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          widget.context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          widget.context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    _followingUser(Language language) async {
      if (!_loadingLanguages.contains(language.name)) {
        setState(() {
          _loadingLanguages.add(language.name);
        });
      }

      String followingAction = _contains(_following, language.name)
          ? PRONUNCIATION_FOLLOWING_USER_ACTION_NO
          : PRONUNCIATION_FOLLOWING_USER_ACTION_YES;

      try {
        Status status = await followUnfollowUser(
          context,
          widget.followedUserName,
          language.code,
          followingAction,
        );
        if (status.statusIsOk) {
          _hasChanged = true;
          if (_contains(_following, language.name)) {
            _remove(_following, language);
          } else {
            _following.add(language);
          }
          setState(() {});
          widget.onFollow();
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String messages;
        for (ApiError _error in errors) {
          switch (_error.errorApiCode) {
            default:
              String errorMessage =
                  localizations.translate('error.api.${_error.errorApiCode}');
              if (messages == null) {
                messages = errorMessage;
              } else {
                messages = '$messages\n$errorMessage';
              }
              break;
          }
        }
        if (messages.isNotEmpty) {
          showCustomSnackBar(
            widget.context,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        showCustomSnackBar(
          widget.context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        if (_loadingLanguages.contains(language.name)) {
          setState(() {
            _loadingLanguages.remove(language.name);
          });
        }
      }
    }

    return WillPopScope(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: _menuOption(
                        themeData: themeData,
                        title: localizations.translate(
                          'menu.modal.follow.title',
                          params: {
                            'userName': widget.followedUserName,
                          },
                        ),
                        height: menuOptionHeightLang,
                        onTap: null,
                        textStyle: themeData.textTheme.bodyText2,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.cancel),
                      onPressed: () {
                        Navigator.pop(context, _hasChanged);
                      },
                    )
                  ],
                ),
                _menuOption(
                  themeData: themeData,
                  title: localizations.translate(
                    'menu.modal.follow.subtitle',
                    params: {
                      'userName': widget.followedUserName,
                    },
                  ),
                  height: menuOptionHeight,
                  onTap: null,
                  textStyle: themeData.textTheme.subtitle1,
                ),
              ],
            ),
            Expanded(
              child: ListView(
                children: _languages.isNotEmpty
                    ? _languages
                        .map(
                          (language) => _menuOptionLang(
                            themeData: themeData,
                            title: language.name,
                            localizations: localizations,
                            onTap: () {
                              _followingUser(language);
                            },
                          ),
                        )
                        .toList()
                    : [],
              ),
            ),
          ],
        ),
        onWillPop: () async {
          Navigator.pop(context, _hasChanged);
          return _hasChanged;
        });
  }

  Widget _menuOption({
    ThemeData themeData,
    String title,
    double height,
    Icon icon,
    Function() onTap,
    TextStyle textStyle,
  }) =>
      Container(
        height: height,
        child: ListTile(
          leading: icon,
          title: Text(
            title,
            style: textStyle ?? themeData.textTheme.bodyText2,
          ),
          onTap: onTap,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );

  Widget _menuOptionLang({
    ThemeData themeData,
    String title,
    AppLocalizations localizations,
    Function() onTap,
  }) =>
      Container(
        height: menuOptionHeightLang,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              ForvoIcons.followUsers,
              size: 20,
              color: _contains(_following, title)
                  ? colorBlue
                  : colorLightThemeGrey,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  title,
                  style: themeData.textTheme.bodyText2.copyWith(fontSize: 16),
                ),
              ),
            ),
            _isLoading
                ? SizedBox(
                    height: 20.0,
                    width: 20.0,
                    child: CustomCircularProgressIndicator(),
                  )
                : Padding(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: _contains(_following, title)
                        ? MainButton(
                            caption: _contains(_following, title)
                                ? localizations
                                    .translate('menu.modal.follow.following')
                                : localizations
                                    .translate('menu.modal.follow.follow'),
                            onPressed: onTap,
                            isLoading: _loadingLanguages.contains(title),
                            isEnabled: !_loadingLanguages.contains(title),
                            usePadding: false,
                            buttonMinimumWidth: 120,
                            isLoadingCircularIndicatorColor: colorDarkThemeGrey,
                            isLoadingCircularIndicatorSize: 10,
                          )
                        : MainButton(
                            caption: _contains(_following, title)
                                ? localizations
                                    .translate('menu.modal.follow.following')
                                : localizations
                                    .translate('menu.modal.follow.follow'),
                            onPressed: onTap,
                            isLoading: _loadingLanguages.contains(title),
                            isEnabled: !_loadingLanguages.contains(title),
                            usePadding: false,
                            buttonColor: colorWhite,
                            buttonTextColor: colorBlue,
                            buttonMinimumWidth: 120,
                            isLoadingCircularIndicatorColor: colorDarkThemeGrey,
                            isLoadingCircularIndicatorSize: 10,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(
                                color: colorBlue,
                                width: 2,
                              ),
                            ),
                          ),
                  ),
          ],
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
        ),
      );
}
