import 'package:flutter/material.dart';

class ModalBottomSheepBaseShape extends RoundedRectangleBorder {
  ModalBottomSheepBaseShape()
      : super(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.0),
          ),
        );
}
