import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class SlideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(
          vertical: 12.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 32.0,
              height: 5.5,
              decoration: BoxDecoration(
                color: themeIsDark(context)
                    ? colorDarkThemeGrey
                    : colorLightThemeGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(12.0),
                ),
              ),
            ),
          ],
        ),
      );
}
