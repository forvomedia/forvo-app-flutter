import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/translation_language_group.dart';
import 'package:forvo/model/translation_language_pair.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/menu/modal_bottom_sheet_base_shape.dart';
import 'package:forvo/ui/widgets/menu/slide_bar.dart';
import 'package:forvo/util/theme_util.dart';

const menuOptionHeight = 55.0;

onSearchModeMenuOpen(
  BuildContext context, {
  List<TranslationLanguageGroup> translationLanguageGroups,
  Function(TranslationLanguagePair) onChangeSearchMode,
}) async {
  double bottomSheetChildSize = 0.5;
  int totalOptions = 0;

  for (TranslationLanguageGroup group in translationLanguageGroups) {
    totalOptions = totalOptions + group.children.length;
  }

  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: ModalBottomSheepBaseShape(),
    builder: (buildContext) => DraggableScrollableSheet(
      initialChildSize: bottomSheetChildSize,
      expand: false,
      maxChildSize: bottomSheetChildSize,
      minChildSize: bottomSheetChildSize,
      builder: (BuildContext context, ScrollController scrollController) =>
          Container(
        height: menuOptionHeight * totalOptions,
        color: Colors.transparent,
        child: Container(
          child: BottomNavigationMenu(
            context: context,
            languages: translationLanguageGroups,
            onChangeSearchMode: onChangeSearchMode,
          ),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(20),
              topRight: const Radius.circular(20),
            ),
          ),
        ),
      ),
    ),
  );
}

class BottomNavigationMenu extends StatefulWidget {
  final BuildContext context;
  final List<TranslationLanguageGroup> languages;
  final Function(TranslationLanguagePair) onChangeSearchMode;

  BottomNavigationMenu({
    @required this.context,
    @required this.languages,
    @required this.onChangeSearchMode,
  });

  @override
  _BottomNavigationMenuState createState() => _BottomNavigationMenuState();
}

class _BottomNavigationMenuState extends State<BottomNavigationMenu> {
  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    List<Widget> widgets = [];

    widgets.add(
      _menuOption(
        themeData,
        localizations.translate('menu.modal.search.word'),
        () => null,
        fontWeight: FontWeight.bold,
      ),
    );

    widgets.add(
      _menuOption(
        themeData,
        localizations.translate('shared.pronunciation'),
        () => widget.onChangeSearchMode(null),
        forcePaddingLeft: true,
      ),
    );

    widgets.add(
      _menuOption(
        themeData,
        localizations.translate('menu.modal.search.selectTranslationLanguage'),
        () => null,
        fontWeight: FontWeight.bold,
      ),
    );

    for (TranslationLanguageGroup group in widget.languages) {
      widgets.add(
        _menuOption(
          themeData,
          group.father.name,
          () => null,
          fontWeight: FontWeight.bold,
          isGroupHeader: true,
        ),
      );

      for (TranslationLanguagePair languagePair in group.children) {
        widgets.add(
          _menuOption(
            themeData,
            languagePair.pairName,
            () => widget.onChangeSearchMode(languagePair),
            forcePaddingLeft: true,
          ),
        );
      }
    }

    return Column(
      children: [
        SlideBar(),
        Expanded(
          child: ListView(
            children: widgets,
          ),
        ),
      ],
    );
  }

  Widget _menuOption(
    ThemeData themeData,
    String title,
    Function() onTap, {
    FontWeight fontWeight = FontWeight.normal,
    bool forcePaddingLeft = false,
    bool isGroupHeader = false,
  }) =>
      Container(
        height: menuOptionHeight,
        padding: EdgeInsets.only(left: forcePaddingLeft ? 30.0 : 0.0),
        child: ListTile(
          title: Text(
            title,
            style: themeData.textTheme.bodyText2.copyWith(
              fontWeight: fontWeight,
            ),
          ),
          onTap: onTap,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: themeData.dividerColor),
          ),
          color: isGroupHeader
              ? themeIsDark(context)
                  ? themeData.inputDecorationTheme.border.borderSide.color
                  : colorLightThemeLightGrey
              : Colors.transparent,
        ),
      );
}
