import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';

class CustomCircularProgressIndicator extends StatelessWidget {
  final Color valueColor;
  final bool isCentered;
  final double strokeWidth;

  CustomCircularProgressIndicator({
    this.valueColor = colorBlue,
    this.isCentered = true,
    this.strokeWidth = 4.0,
  });

  @override
  Widget build(BuildContext context) => isCentered
      ? Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(valueColor),
            strokeWidth: strokeWidth,
          ),
        )
      : CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(valueColor),
          strokeWidth: strokeWidth,
        );
}
