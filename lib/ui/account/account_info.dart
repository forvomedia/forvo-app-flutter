import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/gender_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/registration/widgets/accent_location_selector.dart';
import 'package:forvo/ui/registration/widgets/birthdate_selector.dart';
import 'package:forvo/ui/registration/widgets/gender_selector.dart';
import 'package:forvo/ui/registration/widgets/language_selector.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/menu/language_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text_form_field/password_field.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/form_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;

class AccountInfo extends StatefulWidget {
  @override
  _AccountInfoState createState() => _AccountInfoState();
}

class _AccountInfoState extends State<AccountInfo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      GlobalKey<FormFieldState<String>>();
  final TextEditingController _repeatedPasswordController =
      TextEditingController();
  final GlobalKey<FormFieldState<String>> _repeatedPasswordFieldKey =
      GlobalKey<FormFieldState<String>>();
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  FocusNode _emailFocusNode = FocusNode(debugLabel: 'email');
  FocusNode _passwordFocusNode = FocusNode(debugLabel: 'password');
  FocusNode _repeatedPasswordFocusNode = FocusNode(debugLabel: 'repassword');
  FocusNode _firstNameFocusNode = FocusNode(debugLabel: 'firstName');
  FocusNode _lastNameFocusNode = FocusNode(debugLabel: 'lastName');
  FocusNode _languageAccentSelectorFocusNode =
      FocusNode(debugLabel: 'languageAccentSelector');
  FocusNode _accentLocalizationSelectorFocusNode =
      FocusNode(debugLabel: 'accentLocalizationSelector');
  FocusNode _genderSelectorFocusNode = FocusNode(debugLabel: 'genderSelector');
  FocusNode _birthdateSelectorFocusNode =
      FocusNode(debugLabel: 'birthdateSelector');

  Timer _debounce;
  int _debounceTime = 2000;

  bool _initialized = false;
  bool _isButtonEnabled = false, _isButtonLoading = false;
  bool _isEmailCheckLoading = false;
  String _lastSearchedEmail;
  bool _isEmailValid = true;
  bool _isPasswordValid = true;
  bool _isRepeatedPasswordValid = true;
  bool _isAccentLocalizationValid = true;
  String _emailErrorMessage;
  String _gender;
  DateTime _birthdate;
  bool _userSelectedBirthdate = false;
  Language _selectedNativeLanguage;
  bool _locationPermissionGranted;
  Location location = Location();
  String _accentLocation;
  LocationData _currentPosition;
  bool _serviceEnabled;
  Completer<GoogleMapController> _mapControllerCompleter = Completer();
  GoogleMapController _mapController;
  LatLng _cameraPosition;
  Set<Marker> _markers = {};
  bool _locationPermissionRequested = false;

  AppState state;

  @override
  void didChangeDependencies() {
    if (!_initialized) {
      _initData();
    }
    super.didChangeDependencies();
  }

  _initData() {
    state = AppStateManager.of(context).state;
    _lastSearchedEmail = state.userInfo.user.email;
    setState(() {
      _emailController = TextEditingController(
        text: state.userInfo.user.email,
      );
    });
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _repeatedPasswordController.addListener(_onRepeatedPasswordChanged);
    _locationPermissionStatus();
    double latitude;
    double longitude;
    if (state.userInfo.user.latitude != null &&
        state.userInfo.user.longitude != null) {
      latitude = double.parse(state.userInfo.user.latitude);
      longitude = double.parse(state.userInfo.user.longitude);
    } else {
      _getCurrentLocation();
      if (_currentPosition != null &&
          _currentPosition.latitude != null &&
          _currentPosition.longitude != null) {
        latitude = _currentPosition.latitude;
        longitude = _currentPosition.longitude;
      }
    }
    if (latitude != null && longitude != null) {
      _setMapLocation(latitude, longitude);
    }

    _selectedNativeLanguage = state.languages.firstWhere(
      (Language language) => language.code == state.userInfo.user.languageCode,
      orElse: () => null,
    );

    if (state.userInfo.user.firstName != null &&
        state.userInfo.user.firstName.isNotEmpty) {
      _firstNameController = TextEditingController(
        text: state.userInfo.user.firstName,
      );
    }

    if (state.userInfo.user.lastName != null &&
        state.userInfo.user.lastName.isNotEmpty) {
      _lastNameController = TextEditingController(
        text: state.userInfo.user.lastName,
      );
    }

    if (state.userInfo.user.genre != null &&
        state.userInfo.user.genre.isNotEmpty) {
      _gender = state.userInfo.user.genre == GENDER_MALE
          ? GENDER_MALE
          : GENDER_FEMALE;
    }

    _birthdate = state.userInfo.user.birthdate;
    _userSelectedBirthdate = !state.userInfo.user.isBirthdateNull;

    setState(() {
      _initialized = true;
    });
  }

  @override
  void dispose() {
    _emailController.removeListener(_onEmailChanged);
    _emailController.dispose();
    _emailFocusNode.dispose();
    _passwordController.removeListener(_onPasswordChanged);
    _passwordController.dispose();
    _passwordFocusNode.dispose();
    _repeatedPasswordController.removeListener(_onRepeatedPasswordChanged);
    _repeatedPasswordController.dispose();
    _repeatedPasswordFocusNode.dispose();
    _firstNameController.dispose();
    _firstNameFocusNode.dispose();
    _lastNameController.dispose();
    _lastNameFocusNode.dispose();
    _languageAccentSelectorFocusNode.dispose();
    _accentLocalizationSelectorFocusNode.dispose();
    _genderSelectorFocusNode.dispose();
    _birthdateSelectorFocusNode.dispose();
    if (_mapController != null) {
      _mapController.dispose();
    }
    super.dispose();
  }

  _onEmailChanged() {
    setState(() {
      _emailErrorMessage = null;
    });
    if (state.userInfo.user.email == _emailController.text) {
      setState(() {
        _isEmailValid = true;
      });
    } else {
      if (_debounce?.isActive ?? false) {
        _debounce.cancel();
      }
      _debounce = Timer(Duration(milliseconds: _debounceTime), _checkEmail);
    }
  }

  _onPasswordChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkPassword);
  }

  _onRepeatedPasswordChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce =
        Timer(Duration(milliseconds: _debounceTime), _checkRepeatedPassword);
  }

  _checkEmail() async {
    if (mounted) {
      if (_emailController.text.isEmpty) {
        _lastSearchedEmail = _emailController.text;
        if (!_formKey.currentState.validate()) {
          setState(() {
            _isEmailValid = false;
          });
        }
      } else {
        if (_lastSearchedEmail != _emailController.text) {
          await _signupCheckEmail();
        }
      }
    }
  }

  _signupCheckEmail() async {
    var localizations = AppLocalizations.of(context);
    if (mounted && !_isEmailCheckLoading) {
      var state = AppStateManager.of(context).state;

      if (_emailController.text == state.userInfo.user.email) {
        setState(() {
          _lastSearchedEmail = _emailController.text;
          _isEmailCheckLoading = false;
        });
        _formKey.currentState.validate();
        return;
      }

      setState(() {
        _lastSearchedEmail = _emailController.text;
        _isEmailCheckLoading = true;
      });
      _formKey.currentState.validate();
      try {
        await signupCheckEmail(
          context,
          email: _emailController.text,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {
          _isEmailCheckLoading = false;
          _isEmailValid = true;
          _emailErrorMessage = null;
        });
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        setState(() {
          _emailErrorMessage = messages;
          _isEmailValid = false;
        });
      } on Exception catch (_) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        if (mounted) {
          setState(() {
            _isEmailCheckLoading = false;
          });
          _formKey.currentState.validate();
        }
      }
    }
  }

  String _onEmailValidate(String email) {
    var localizations = AppLocalizations.of(context);
    if (email == null || email.isEmpty) {
      return localizations.translate('error.shared.emptyEmail');
    } else {
      if (_emailErrorMessage != null && _emailErrorMessage.isNotEmpty) {
        return _emailErrorMessage;
      }
      return null;
    }
  }

  _onEmailSubmit(String email) async {
    await _checkEmail();
    if (_isEmailValid) {
      fieldFocusChange(
        context,
        _emailFocusNode,
        _passwordFocusNode,
      );
    }
  }

  _checkPassword() async {
    if (mounted) {
      bool isPasswordValid =
          _onPasswordValidate(_passwordController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isPasswordValid = isPasswordValid;
      });
    }
  }

  String _onPasswordValidate(String password) {
    var localizations = AppLocalizations.of(context);
    if (password != null && password.isNotEmpty) {
      if (password.length < MIN_PASSWORD_LIMIT) {
        return localizations.translate('error.shared.passwordTooShort');
      }
      if (password.length > MAX_PASSWORD_LIMIT) {
        return localizations.translate('error.shared.passwordTooLong');
      }
    }
    return null;
  }

  _onPasswordSubmit(String password) async {
    await _checkPassword();
    if (_isPasswordValid) {
      fieldFocusChange(
        context,
        _passwordFocusNode,
        _repeatedPasswordFocusNode,
      );
    }
  }

  _checkRepeatedPassword() async {
    if (mounted) {
      bool isRepeatedPasswordValid = _formKey.currentState.validate();
      setState(() {
        _isRepeatedPasswordValid = isRepeatedPasswordValid;
      });
    }
  }

  String _onRepeatedPasswordValidate(String repeatedPassword) {
    var localizations = AppLocalizations.of(context);
    if (repeatedPassword != _passwordController.text) {
      return localizations.translate('error.shared.passwordsDoNotMatch');
    }
    return null;
  }

  _onRepeatedPasswordSubmit(String repeatPassword) async {
    await _checkRepeatedPassword();
    if (_isRepeatedPasswordValid) {
      fieldFocusChange(
        context,
        _repeatedPasswordFocusNode,
        _languageAccentSelectorFocusNode,
      );
    }
  }

  _onFirstNameSubmit(String firstName) async {
    fieldFocusChange(context, _firstNameFocusNode, _lastNameFocusNode);
  }

  _onLastNameSubmit(String lastName) async {
    fieldFocusChange(
        context, _lastNameFocusNode, _languageAccentSelectorFocusNode);
  }

  _locationPermissionStatus() async {
    _locationPermissionRequested = await isLocationPermissionRequested();
    if (_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.isGranted;
    }
    setState(() {});
  }

  _requestMapPermissions() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    await _locationPermissionStatus();
    if (!_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.request().isGranted;
      setLocationPermissionRequested();
      _locationPermissionRequested = true;
    }
    setState(() {});
  }

  _getCurrentLocation() async {
    await _requestMapPermissions();

    if (_locationPermissionGranted == null || !_locationPermissionGranted) {
      return;
    }

    _currentPosition = await location.getLocation();
    if (_currentPosition != null && _mapController == null) {
      _setMapLocation(_currentPosition.latitude, _currentPosition.longitude);
    }
  }

  _setMapLocation(double latitude, double longitude) async {
    final String markerIdVal =
        'marker_id_${latitude.toString()}_${longitude.toString()}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(latitude, longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);
    setState(() {
      _accentLocation = 'Latitude: ${latitude.toString()}, '
          'Longitude: ${longitude.toString()}';
      _cameraPosition = LatLng(latitude, longitude);
      _isAccentLocalizationValid = true;
    });

    _mapController = await _mapControllerCompleter.future;
    _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: _cameraPosition,
          zoom: 12,
        ),
      ),
    );
  }

  _onTapMarker() {
    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _onTapMap(LatLng position) {
    final String markerIdVal =
        'marker_id_${position.latitude}_${position.longitude}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(position.latitude, position.longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);

    setState(() {
      _accentLocation = 'Latitude: ${position.latitude}, '
          'Longitude: ${position.longitude}';
      _cameraPosition = LatLng(position.latitude, position.longitude);
      _isAccentLocalizationValid = true;
    });

    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _editBasicInfo() async {
    setState(() => _isButtonLoading = true);
    var localizations = AppLocalizations.of(context);

    try {
      FocusScope.of(context).requestFocus(FocusNode());

      var userInfo = await editUserInfo(
        context,
        email: _emailController.text,
        password: _passwordController.text,
        repassword: _repeatedPasswordController.text,
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        gender: _gender,
        languageCode: _selectedNativeLanguage.code,
        latitude: _cameraPosition.latitude.toString(),
        longitude: _cameraPosition.longitude.toString(),
        birthdateDay: getDefaultFormattedDayFromDate(_birthdate),
        birthdateMonth: getDefaultFormattedMonthFromDate(_birthdate),
        birthdateYear: getDefaultFormattedYearFromDate(_birthdate),
        interfaceLanguageCode: state.locale.languageCode,
      );

      if (userInfo != null) {
        AppStateManager.of(context).onStateChanged(
          AppState(
            isUserLogged: userInfo != null,
            userInfo: userInfo,
          ),
        );
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('account.basicInfoUpdateMessage'),
            type: SNACKBAR_TYPE_OK,
          ),
        );
      }
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String message;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          case ApiErrorCodeConstants.ERROR_1073:
            setState(() {
              _isAccentLocalizationValid = false;
            });
            if (message == null) {
              message = _error.errorMessage;
            } else {
              message = '$message\n${_error.errorMessage}';
            }
            break;
          default:
            if (message == null) {
              message = _error.errorMessage;
            } else {
              message = '$message\n${_error.errorMessage}';
            }
            break;
        }
      }
      if (message != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on NotConnectedException catch (_) {
      bool _hasConnection = await deviceHasConnection();
      String message = _hasConnection
          ? '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}'
          : '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      setState(() => _isButtonLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceLoginActive;

    _isButtonEnabled = _isEmailValid != null &&
        _isEmailValid &&
        _isPasswordValid != null &&
        _isPasswordValid &&
        _isRepeatedPasswordValid != null &&
        _isRepeatedPasswordValid &&
        _isAccentLocalizationValid != null &&
        _isAccentLocalizationValid &&
        _selectedNativeLanguage != null &&
        _birthdate != null &&
        _userSelectedBirthdate &&
        _cameraPosition != null &&
        _cameraPosition.latitude != null &&
        _cameraPosition.longitude != null;

    _onNativeLanguageMenuOpen() {
      _languageAccentSelectorFocusNode.requestFocus();
      onLanguageMenuOpen(
        context,
        languages: state.languages,
        popularLanguages: state.popularLanguages,
        onChangeLanguage: (Language language) {
          setState(() {
            _selectedNativeLanguage = language;
          });
          Navigator.pop(context);
          fieldFocusChange(
            context,
            _languageAccentSelectorFocusNode,
            _accentLocalizationSelectorFocusNode,
          );
        },
      );
    }

    _onBirthdateSelect() async {
      _birthdateSelectorFocusNode.requestFocus();
      int maximumValidYear = DateTime.now().year - DATE_PICKER_YEARS_13;
      int maxValidMonth = 12;
      int maxValidDay = 31;
      DateTime previousBirthdate;
      if (_birthdate == null) {
        _birthdate = DateTime(maximumValidYear, maxValidMonth, maxValidDay);
      } else {
        previousBirthdate = _birthdate;
      }

      DateTime newBirthdate = await showDatePicker(
        context: context,
        locale: state.locale,
        initialDate: _birthdate,
        firstDate: DateTime(DATE_PICKER_INITIAL_YEAR),
        lastDate: DateTime(maximumValidYear, maxValidMonth, maxValidDay),
      );

      if (newBirthdate != null) {
        setState(() {
          _birthdate = newBirthdate;
          _userSelectedBirthdate = true;
        });
      } else {
        setState(() {
          _birthdate = previousBirthdate;
          _userSelectedBirthdate = previousBirthdate != null;
        });
      }
    }

    InputBorder _textFormFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: themeData.dividerColor,
        width: 1.0,
      ),
    );

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorDarkThemeGrey : colorLightThemeGrey,
    );

    Widget _getContent() => _initialized
        ? ListView(
            children: [
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _isMaintenanceActive
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 15.0),
                              child: MaintenanceWarningMessage(),
                            )
                          : Container(),
                      HeadlineText(
                        text: localizations.translate('shared.username'),
                        textStyle: _headLineTextStyle,
                      ),
                      TextFormField(
                        initialValue: state.userInfo.user.username,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        style: themeData.textTheme.bodyText2.copyWith(
                          fontStyle: FontStyle.normal,
                        ),
                        enabled: false,
                        decoration: InputDecoration(
                          hintMaxLines: 2,
                          errorMaxLines: 3,
                          enabledBorder: _textFormFieldBorder,
                          enabled: false,
                          fillColor: themeIsDark(context)
                              ? colorBlack
                              : colorLightThemeGrey,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.email'),
                        textStyle: _headLineTextStyle,
                      ),
                      Container(
                        child: Stack(
                          children: <Widget>[
                            TextFormField(
                              focusNode: _emailFocusNode,
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.none,
                              style: themeData.textTheme.bodyText2
                                  .copyWith(fontStyle: FontStyle.normal),
                              decoration: InputDecoration(
                                hintMaxLines: 1,
                                errorMaxLines: 3,
                                enabledBorder: _textFormFieldBorder,
                                suffixIcon: _isEmailCheckLoading
                                    ? Container(
                                        height: 10.0,
                                        width: 10.0,
                                        margin: EdgeInsets.all(15.0),
                                        child: CustomCircularProgressIndicator(
                                          isCentered: false,
                                        ),
                                      )
                                    : _isEmailValid == null
                                        ? Container()
                                        : _isEmailValid
                                            ? Icon(
                                                Icons.check,
                                                color: Colors.green,
                                              )
                                            : Icon(
                                                Icons.close,
                                                color: Colors.red,
                                              ),
                              ),
                              validator: _onEmailValidate,
                              onFieldSubmitted: _onEmailSubmit,
                            ),
                            _emailController.text == _lastSearchedEmail &&
                                    !_emailFocusNode.hasFocus
                                ? Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 19.0,
                                      horizontal: 12.0,
                                    ),
                                    child: Text(_lastSearchedEmail),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.password'),
                        textStyle: _headLineTextStyle,
                      ),
                      PasswordField(
                        themeData: themeData.copyWith(
                          inputDecorationTheme: InputDecorationTheme(
                            enabledBorder: _textFormFieldBorder,
                            border: _textFormFieldBorder,
                          ),
                        ),
                        fieldKey: _passwordFieldKey,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        focusNode: _passwordFocusNode,
                        controller: _passwordController,
                        style: themeData.textTheme.bodyText2,
                        validator: _onPasswordValidate,
                        onFieldSubmitted: _onPasswordSubmit,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.repeatPassword'),
                        textStyle: _headLineTextStyle,
                      ),
                      PasswordField(
                        themeData: themeData.copyWith(
                          inputDecorationTheme: InputDecorationTheme(
                            enabledBorder: _textFormFieldBorder,
                            border: _textFormFieldBorder,
                          ),
                        ),
                        fieldKey: _repeatedPasswordFieldKey,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.next,
                        focusNode: _repeatedPasswordFocusNode,
                        controller: _repeatedPasswordController,
                        style: themeData.textTheme.bodyText2,
                        validator: _onRepeatedPasswordValidate,
                        onFieldSubmitted: _onRepeatedPasswordSubmit,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.firstName'),
                        textStyle: _headLineTextStyle,
                      ),
                      TextFormField(
                        focusNode: _firstNameFocusNode,
                        controller: _firstNameController,
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.none,
                        style: themeData.textTheme.bodyText2
                            .copyWith(fontStyle: FontStyle.normal),
                        decoration: InputDecoration(
                          hintMaxLines: 2,
                          errorMaxLines: 3,
                          enabledBorder: _textFormFieldBorder,
                        ),
                        onFieldSubmitted: _onFirstNameSubmit,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.lastName'),
                        textStyle: _headLineTextStyle,
                      ),
                      TextFormField(
                        focusNode: _lastNameFocusNode,
                        controller: _lastNameController,
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.none,
                        style: themeData.textTheme.bodyText2
                            .copyWith(fontStyle: FontStyle.normal),
                        decoration: InputDecoration(
                          hintMaxLines: 2,
                          errorMaxLines: 3,
                          enabledBorder: _textFormFieldBorder,
                        ),
                        onFieldSubmitted: _onLastNameSubmit,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.accent'),
                        textStyle: _headLineTextStyle,
                      ),
                      AccentLocalizationSelector(
                        permissionGranted: _locationPermissionGranted,
                        permissionRequested: _locationPermissionRequested,
                        accentLocation: _accentLocation,
                        onGetCurrentLocation: _getCurrentLocation,
                        mapController: _mapControllerCompleter,
                        cameraPosition: _cameraPosition,
                        markers: _markers,
                        onTapMap: _onTapMap,
                        focusNode: _accentLocalizationSelectorFocusNode,
                        isValid: _isAccentLocalizationValid,
                        parentView: LOCATION_SELECTOR_MESSAGE_ACCOUNT,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.nativeLanguage'),
                        textStyle: _headLineTextStyle,
                      ),
                      LanguageSelector(
                        language: _selectedNativeLanguage,
                        onLanguageMenuOpen: _onNativeLanguageMenuOpen,
                        focusNode: _languageAccentSelectorFocusNode,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.gender.gender'),
                        textStyle: _headLineTextStyle,
                      ),
                      IntrinsicHeight(
                        child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              border: Border.all(
                                color: themeData.dividerColor,
                                width: 1.0,
                              ),
                            ),
                            child: GenderSelector(
                              focusNode: _genderSelectorFocusNode,
                              gender: _gender,
                              onTap: (String newGender) {
                                setState(() {
                                  _gender = newGender;
                                });
                                fieldFocusChange(
                                  context,
                                  _genderSelectorFocusNode,
                                  _birthdateSelectorFocusNode,
                                );
                              },
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      HeadlineText(
                        text: localizations.translate('shared.birthdate'),
                        textStyle: _headLineTextStyle,
                      ),
                      BirthdateSelector(
                        birthdate: _birthdate,
                        onTap: _onBirthdateSelect,
                        focusNode: _birthdateSelectorFocusNode,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      Divider(
                        height: 28,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 15.0, left: 10.0, right: 10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Icon(Icons.info_outlined),
                            ),
                            Expanded(
                              child: Text(
                                localizations
                                    .translate('shared.comboDataWarning'),
                                style: TextStyle(fontSize: 15),
                                textAlign: TextAlign.justify,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        height: 28,
                        thickness: 2,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      MainButton(
                        caption: localizations.translate('shared.save'),
                        onPressed: _editBasicInfo,
                        isLoading: _isButtonLoading,
                        isEnabled: !_isMaintenanceActive &&
                            !_isButtonLoading &&
                            _isButtonEnabled,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        : CustomCircularProgressIndicator();

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: localizations.translate('account.basicInfo'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: _getContent(),
    );
  }
}
