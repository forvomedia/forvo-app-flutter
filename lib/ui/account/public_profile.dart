import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/gender_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/model/user_profile.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/followed_user_api_service.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_detail.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/followed_user_detail.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/followed_users_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/maintenance_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';
import 'package:intl/intl.dart';

class PublicProfileArguments {
  final String username;

  PublicProfileArguments({this.username});
}

class PublicProfile extends StatefulWidget {
  @override
  _PublicProfileState createState() => _PublicProfileState();
}

class _PublicProfileState extends State<PublicProfile> {
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  String _username;
  Future<UserProfile> _userProfile;
  List<Language> _languages;
  bool _following = false;
  var _failed = false;
  var _hasConnection = true;
  var _apiError = false;

  @override
  void didChangeDependencies() {
    _extractArguments();
    _getUserProfile();
    _followingUser();
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args =
        ModalRoute.of(context).settings.arguments as PublicProfileArguments;
    _username = args.username;
  }

  _getUserProfile() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    try {
      if (_username != null && state.locale != null) {
        _userProfile = getUserProfile(
          context,
          username: _username,
          interfaceLanguageCode: state.locale.languageCode,
        );
        _languages = await getUserPronunciationLangs(
          context,
          _username,
          interfaceLanguageCode: state.locale.languageCode,
        );
        if (mounted) {
          setState(() {});
        }
      } else {
        throw Exception();
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      if (mounted) {
        setState(() {});
      }
    }
  }

  _followingUser() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    try {
      if (_username != null && state.locale != null) {
        if (_username != state.userInfo.user.username) {
          var followingLangs = await followingUserLangs(
            context,
            _username,
            interfaceLanguageCode: state.locale.languageCode,
          );
          if (mounted) {
            setState(() {
              _following = followingLangs.isNotEmpty;
            });
          }
        }
      } else {
        throw Exception();
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      if (mounted) {
        setState(() {});
      }
    }
  }

  _followUnfollowUser(Language language) async {
    var localizations = AppLocalizations.of(context);
    String followingAction = _following
        ? PRONUNCIATION_FOLLOWING_USER_ACTION_NO
        : PRONUNCIATION_FOLLOWING_USER_ACTION_YES;

    try {
      Status status = await followUnfollowUser(
        context,
        _username,
        language.code,
        followingAction,
      );
      if (status.statusIsOk) {
        if (_following) {
          _following = false;
        } else {
          _following = true;
        }
        setState(() {});
        showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate(
                _following ? 'shared.followingUser' : 'shared.notFollowingUser',
                params: {
                  'userName': _username,
                },
              ),
              type: SNACKBAR_TYPE_OK,
            ));
      }
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String messages;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          default:
            String errorMessage =
                localizations.translate('error.api.${_error.errorApiCode}');
            if (messages == null) {
              messages = errorMessage;
            } else {
              messages = '$messages\n$errorMessage';
            }
            break;
        }
      }
      if (messages.isNotEmpty) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  Future<void> _onRefresh() async {
    _getUserProfile();
    _followingUser();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_getUserProfile, message: message);
      } else if (_hasConnection) {
        return ServiceError(_getUserProfile);
      } else {
        return NetworkError(_getUserProfile);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;
    final numberFormat = NumberFormat.decimalPattern(state.locale.toString());

    bool _isMaintenanceActive = state.isMaintenanceReadOnlyActive;

    Widget _informationRow(String field, String value) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Container(
            padding: const EdgeInsets.symmetric(
              vertical: 18.0,
              horizontal: 15.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                const Radius.circular(5.0),
              ),
              border: Border.all(
                color: themeData.dividerColor,
                width: 1.0,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  field ?? NOT_AVAILABLE_DATA,
                  style: themeData.textTheme.headline5,
                ),
                Text(
                  value ?? NOT_AVAILABLE_DATA,
                  style: themeData.textTheme.bodyText2,
                ),
              ],
            ),
          ),
        );

    Widget _pronunciationsInformationBox(
            UserProfile userProfile, String field, String value) =>
        Expanded(
          child: InkWell(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  const Radius.circular(10),
                ),
                color: colorBlue,
              ),
              padding:
                  const EdgeInsets.symmetric(vertical: 18.0, horizontal: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    field,
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.headline5.copyWith(
                      color: colorWhite,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    value,
                    style: themeData.textTheme.headline6.copyWith(
                      color: colorWhite,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            onTap: () async {
              if (_username != state.userInfo.user.username) {
                FollowedUserDetailArguments arguments =
                    FollowedUserDetailArguments(
                  userName: userProfile.user.username,
                  followingLanguageCode: '',
                );

                await Navigator.pushNamed(
                  context,
                  RouteConstants.FOLLOWED_USER_DETAIL,
                  arguments: arguments,
                );
                _followingUser();
              } else {
                Navigator.pushNamed(
                    context, RouteConstants.MY_OWN_PRONUNCIATIONS);
              }
            },
          ),
        );

    Widget _totalInformationBox(String field, String value) => Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                const Radius.circular(10),
              ),
              color: themeData.disabledColor,
            ),
            padding:
                const EdgeInsets.symmetric(vertical: 18.0, horizontal: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  field,
                  textAlign: TextAlign.center,
                  style: themeData.textTheme.headline5.copyWith(fontSize: 16),
                ),
                Text(
                  value,
                  style: themeData.textTheme.headline6.copyWith(
                    color: colorLightThemeGrey,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
        );

    Widget _userProfileInformationHeader(UserProfile userProfile) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                localizations.translate(
                  'account.profile.userInfo',
                ),
                style: themeData.textTheme.headline3.copyWith(
                  color: colorLightThemeGrey,
                ),
              ),
            ),
            _username != state.userInfo.user.username
                ? InkWell(
                    child: Row(
                      children: [
                        Icon(
                          ForvoIcons.chat,
                          size: 16,
                          color: colorBlue,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        ),
                        Text(
                          localizations.translate('account.profile.contact'),
                          style: themeData.textTheme.headline3.copyWith(
                            color: colorBlue,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      if (userProfile.user.id != state.userInfo.user.id &&
                          userProfile.allowsUserMessages) {
                        Navigator.pushNamed(
                          context,
                          RouteConstants.CHAT_DETAIL,
                          arguments: ChatConversationDetailArguments(
                            chatConversation: null,
                            userTo: userProfile.user.username,
                          ),
                        );
                      } else {
                        showCustomSnackBar(
                          _scaffoldMessengerKey.currentContext,
                          themedSnackBar(
                            context,
                            localizations.translate(
                              'messages.notAllowedUserMessagesWarning',
                              params: {'userName': userProfile.user.username},
                            ),
                            type: SNACKBAR_TYPE_ERROR,
                          ),
                        );
                      }
                    },
                  )
                : Container(),
          ],
        );

    Widget _userInfoPrivateProfile(UserProfile userProfile) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                const Radius.circular(10),
              ),
              color: themeIsDark(context)
                  ? colorDarkThemeGrey
                  : colorLightThemeLightGrey,
            ),
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Icon(
                    Icons.lock,
                    size: 20,
                    color: themeIsDark(context) ? colorWhite : colorBlack,
                  ),
                ),
                Expanded(
                  child: Text(
                    localizations.translate('account.profile.notPublic'),
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
          ),
        );

    Widget _userInfo(UserProfile userProfile) => Column(
          children: [
            _informationRow(
              localizations.translate('shared.gender.gender'),
              userProfile.user.genre == null || userProfile.user.genre.isEmpty
                  ? NOT_AVAILABLE_DATA
                  : userProfile.user.genre == GENDER_FEMALE
                      ? localizations.translate('shared.gender.female')
                      : localizations.translate('shared.gender.male'),
            ),
            _informationRow(
              localizations.translate('account.profile.accentOrCountry'),
              userProfile.country.name,
            ),
            _informationRow(
              localizations.translate('shared.language'),
              userProfile.user.languageName,
            ),
          ],
        );

    Widget _userProfileInformation(UserProfile userProfile) => Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                bottom: 8.0,
              ),
              child: _userProfileInformationHeader(userProfile),
            ),
            _username != state.userInfo.user.username
                ? userProfile.isPublic
                    ? _userInfo(userProfile)
                    : _userInfoPrivateProfile(userProfile)
                : _userInfo(userProfile),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
                bottom: 18.0,
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    localizations.translate('account.profile.userStats'),
                    style: themeData.textTheme.headline3.copyWith(
                      color: colorLightThemeGrey,
                    ),
                  ),
                ],
              ),
            ),
            IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _pronunciationsInformationBox(
                    userProfile,
                    localizations.translate('shared.pronunciations'),
                    numberFormat
                        .format(userProfile.stats.numberOfPronunciations),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                  ),
                  !userProfile.isPublic &&
                          _username != state.userInfo.user.username
                      ? Container()
                      : _totalInformationBox(
                          localizations.translate('account.profile.addedWords'),
                          numberFormat
                              .format(userProfile.stats.numberOfAddedWords),
                        ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            !userProfile.isPublic && _username != state.userInfo.user.username
                ? Container()
                : IntrinsicHeight(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        _totalInformationBox(
                          localizations.translate('account.profile.votes'),
                          numberFormat.format(userProfile.stats.numberOfVotes),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 10.0),
                        ),
                        _totalInformationBox(
                          localizations.translate('account.profile.visits'),
                          numberFormat.format(userProfile.stats.numberOfVisits),
                        ),
                      ],
                    ),
                  ),
          ],
        );

    Widget _getContent(UserProfile userProfile) => LayoutBuilder(
          builder: (context, constraint) => SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: IntrinsicHeight(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: _username != state.userInfo.user.username
                          ? const EdgeInsets.only(top: 160.0)
                          : const EdgeInsets.only(top: 150.0),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/tutorial-bg.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(10),
                            topRight: const Radius.circular(10),
                          ),
                          color: themeData.backgroundColor,
                        ),
                        padding: const EdgeInsets.all(32),
                        child: _userProfileInformation(userProfile),
                      ),
                    ),
                    Positioned(
                      top: 45.0,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            // ignore: lines_longer_than_80_chars
                            '${localizations.translate('account.profile.user')}:',
                            style: themeData.textTheme.subtitle1.copyWith(
                              color: colorWhite,
                            ),
                          ),
                          Text(
                            _username,
                            style: themeData.textTheme.headline1,
                          ),
                          _username != state.userInfo.user.username
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 4.0),
                                )
                              : Container(),
                          _username != state.userInfo.user.username
                              ? InkWell(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        const Radius.circular(10),
                                      ),
                                      color: colorWhite,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Icon(
                                          ForvoIcons.followUsers,
                                          size: 12,
                                          color: colorBlue,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 8.0, horizontal: 4.0),
                                        ),
                                        Text(
                                          _following
                                              ? localizations.translate(
                                                  // ignore: lines_longer_than_80_chars
                                                  'account.profile.followingUser')
                                              : localizations.translate(
                                                  // ignore: lines_longer_than_80_chars
                                                  'account.profile.notFollowingUser'),
                                          style: themeData.textTheme.subtitle2
                                              .copyWith(
                                            color: colorBlue,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: !_isMaintenanceActive
                                      ? () async {
                                          if (_languages.length > 1) {
                                            await onFollowedUsersMenuOpen(
                                              context,
                                              _username,
                                              _onRefresh,
                                              languages: _languages,
                                            );
                                          } else {
                                            _followUnfollowUser(
                                                _languages.first);
                                          }
                                        }
                                      : () => showMaintenanceSnackBar(
                                          _scaffoldMessengerKey.currentContext,
                                          localizations),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: localizations.translate('account.profile.title'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      bottomNavBarIndexIsActive: _username == state.userInfo.user.username,
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context, _following);
          return _following;
        },
        child: FutureBuilder(
          future: _userProfile,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              return _getContent(snapshot.data);
            } else if (snapshot.hasError) {
              _failed = true;
              if (snapshot.error is ApiException) {
                String messages =
                    (snapshot.error as ApiException).getErrorMessages(context);
                _apiError = true;
                return _buildErrorMessage(message: messages);
              }
              if (snapshot.error is NotConnectedException) {
                _hasConnection = false;
              } else {
                _hasConnection = true;
              }
              return _buildErrorMessage();
            }
            return CustomCircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
