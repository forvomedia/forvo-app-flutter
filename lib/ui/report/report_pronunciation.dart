import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_categorization_information_icon.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';

const String PROBLEM_NOISE = 'noise';
const String PROBLEM_NON_NATIVE = 'non-native';
const String PROBLEM_OTHER = 'other';

class ReportPronunciationArguments {
  final Pronunciation pronunciation;
  final LanguageWordInformation wordInformation;

  ReportPronunciationArguments({
    @required this.pronunciation,
    this.wordInformation,
  });
}

class ReportPronunciation extends StatefulWidget {
  @override
  _ReportPronunciationState createState() => _ReportPronunciationState();
}

class _ReportPronunciationState extends State<ReportPronunciation> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _problemController = TextEditingController();
  FocusNode _problemFocusNode = FocusNode(debugLabel: 'problem');
  Pronunciation _pronunciation;
  LanguageWordInformation _wordInformation;
  String _pronunciationProblem = PROBLEM_NOISE;
  bool _isLoading = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    _extractArguments();
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args = ModalRoute.of(context).settings.arguments
        as ReportPronunciationArguments;
    if (args != null) {
      if (args.pronunciation != null) {
        _pronunciation = args.pronunciation;
      }
      if (args.wordInformation != null) {
        _wordInformation = args.wordInformation;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    _onChanged(String newProblem) {
      setState(() {
        _pronunciationProblem = newProblem;
      });
      if (_pronunciationProblem == PROBLEM_OTHER) {
        _problemFocusNode.requestFocus();
      }
    }

    _onSendReport() async {
      if (_formKey.currentState.validate()) {
        try {
          setState(() {
            _isLoading = true;
          });
          await reportPronunciation(
            context,
            word: _pronunciation.word,
            languageCode: _pronunciation.languageCode,
            pronunciation: _pronunciation.id.toString(),
            pronunciationProblem: _pronunciationProblem,
            problem: _problemController.text,
            interfaceLanguageCode: state.locale.languageCode,
          );

          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('report.confirmationMessage'),
              duration: Duration(seconds: 10),
              type: SNACKBAR_TYPE_OK,
            ),
          );

          Navigator.pop(context, 1);
        } on ApiException catch (error) {
          List<ApiError> errors = error.getErrors();
          String message;
          for (ApiError _error in errors) {
            switch (_error.errorApiCode) {
              default:
                if (message == null) {
                  message = _error.errorMessage;
                } else {
                  message = '$message\n${_error.errorMessage}';
                }
                break;
            }
          }
          if (message.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                message,
                duration: Duration(seconds: 10),
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (_) {
          debugPrint('ERROR ON REPORT PRONUNCIATION');
        } finally {
          if (mounted) {
            setState(() {
              _isLoading = false;
            });
          }
        }
      }
    }

    Widget _getWordSpellings() {
      if (_pronunciation.wordCategorizationGroup != null) {
        List<Widget> widgets = [];

        if (_pronunciation.wordCategorizationGroup.minimumCategories != null &&
            _pronunciation
                .wordCategorizationGroup.minimumCategories.isNotEmpty) {
          for (WordCategory category
              in _pronunciation.wordCategorizationGroup.minimumCategories) {
            for (WordCategorySpelling categorySpelling in category.data) {
              String categoryName =
                  _pronunciation.languageCode == state.locale.languageCode
                      ? categorySpelling.nativeCategoryName
                      : categorySpelling.englishCategoryName;
              widgets.add(
                HtmlTagTextFormatted(
                  text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
                  openTag: '<b>',
                  closeTag: '</b>',
                  boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                  ),
                  normalTextStyle: themeData.textTheme.subtitle1.copyWith(
                    fontSize: 14,
                  ),
                ),
              );
            }
          }
        }

        Widget column = Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        );

        if (_pronunciation.wordCategorizationGroup.showFullInformation) {
          return Row(
            children: [
              Expanded(
                child: column,
              ),
              PronunciationCategorizationInformationIcon(
                categorizationGroup: _pronunciation.wordCategorizationGroup,
                languageCode: _pronunciation.languageCode,
              )
            ],
          );
        }

        return column;
      }

      List<Widget> widgets = [];
      if (_wordInformation != null && _wordInformation.original.isNotEmpty) {
        widgets.add(
          Text(
            _wordInformation.original,
            style: themeData.textTheme.bodyText2,
          ),
        );
      } else {
        widgets.add(
          Text(
            _pronunciation.original,
          ),
        );
      }

      if (_wordInformation != null &&
          _wordInformation.alternativeSpellings != null &&
          _wordInformation.alternativeSpellings.isNotEmpty) {
        for (WordSpelling spelling in _wordInformation.alternativeSpellings) {
          if (spelling.spelling.isNotEmpty) {
            widgets.add(
              Text(
                spelling.spelling,
              ),
            );
          }
        }
      }

      return Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets,
            ),
          ),
        ],
      );
    }

    String _getLanguageName() {
      String language = _pronunciation.languageName;
      if (_pronunciation.languageAccentName != null &&
          _pronunciation.languageAccentName.isNotEmpty) {
        language = '$language (${_pronunciation.languageAccentName})';
      }
      return language;
    }

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorDarkThemeGrey : colorLightThemeGrey,
    );

    Widget _getContent() => Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Text(
                        localizations.translate('report.title'),
                        style: themeData.textTheme.headline6,
                      ),
                    ),
                    Text(
                      localizations.translate('report.subtitle'),
                      style: themeData.textTheme.bodyText2,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                    ),
                    HeadlineText(
                      text: localizations.translate('report.fields.word'),
                      textStyle: _headLineTextStyle,
                    ),
                    _getWordSpellings(),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.language'),
                      textStyle: _headLineTextStyle,
                    ),
                    Text(
                      _getLanguageName(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    HeadlineText(
                      text:
                          localizations.translate('report.fields.pronouncedBy'),
                      textStyle: _headLineTextStyle,
                    ),
                    Text(
                      _pronunciation.username,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    HeadlineText(
                      text: localizations.translate('report.fields.problem'),
                      textStyle: _headLineTextStyle,
                    ),
                    Row(
                      children: <Widget>[
                        Radio<String>(
                          value: PROBLEM_NOISE,
                          groupValue: _pronunciationProblem,
                          onChanged: _onChanged,
                        ),
                        Expanded(
                          child: GestureDetector(
                            child: Text(
                              localizations.translate('report.problems.noisy'),
                            ),
                            onTap: () => _onChanged(PROBLEM_NOISE),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    Row(
                      children: <Widget>[
                        Radio<String>(
                          value: PROBLEM_NON_NATIVE,
                          groupValue: _pronunciationProblem,
                          onChanged: _onChanged,
                        ),
                        Expanded(
                          child: GestureDetector(
                            child: Text(
                              localizations
                                  .translate('report.problems.nonNative'),
                            ),
                            onTap: () => _onChanged(PROBLEM_NON_NATIVE),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    Row(
                      children: <Widget>[
                        Radio<String>(
                          value: PROBLEM_OTHER,
                          groupValue: _pronunciationProblem,
                          onChanged: _onChanged,
                        ),
                        Expanded(
                          child: GestureDetector(
                            child: Text(
                              localizations.translate('report.problems.other'),
                            ),
                            onTap: () => _onChanged(PROBLEM_OTHER),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                    ),
                    _pronunciationProblem == PROBLEM_OTHER
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 16.0),
                            child: TextFormField(
                              controller: _problemController,
                              focusNode: _problemFocusNode,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.done,
                              textCapitalization: TextCapitalization.sentences,
                              style: themeData.textTheme.bodyText2.copyWith(
                                fontSize: 16,
                              ),
                              maxLines: 5,
                              maxLength: 1000,
                              validator: (String problem) {
                                if (_pronunciationProblem == PROBLEM_OTHER &&
                                    (problem == null || problem.isEmpty)) {
                                  return localizations
                                      .translate('error.shared.requiredField');
                                }
                                return null;
                              },
                            ),
                          )
                        : Container(),
                    MainButton(
                      caption: localizations.translate('report.sendReport'),
                      onPressed: _onSendReport,
                      isLoading: _isLoading,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHideActions: true,
      bottomNavBarIndexIsActive: false,
      body: _getContent(),
    );
  }
}
