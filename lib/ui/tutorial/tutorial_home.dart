import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';

class TutorialHome extends StatefulWidget {
  @override
  State<TutorialHome> createState() => _TutorialHomeState();
}

class _TutorialHomeState extends State<TutorialHome>
    with SingleTickerProviderStateMixin {
  bool _isTutorialButtonLoading = false;
  bool _isSkipButtonLoading = false;
  bool _isScrollable = false;
  ScrollController _scrollController;
  AnimationController _animationController;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _initValues();
  }

  _initValues() async {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);

    Future.delayed(
        const Duration(milliseconds: 500), _showScrollableBottomIcon);
  }

  _scrollListener() {
    _showScrollableBottomIcon();
  }

  _showScrollableBottomIcon() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        _isScrollable = false;
      });
    } else {
      setState(() {
        _isScrollable = true;
      });
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    skipTutorial() async {
      setState(() {
        _isSkipButtonLoading = true;
      });
      await setTutorialChecked();
      await requestAppTrackinPermission();

      Map eventValues = {
        APPSFLYER_AF_SUCCESS: APPSFLYER_NO,
        APPSFLYER_TUTORIAL_ID: APPSFLYER_TUTORIAL_ID,
        APPSFLYER_CONTENT: APPSFLYER_CONTENT,
      };
      appsflyerEventLog(APPSFLYER_AF_TUTORIAL_COMPLETION, eventValues);

      Navigator.pushNamedAndRemoveUntil(
          context, RouteConstants.START, (route) => false);
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHidden: true,
      bottomNavBarHidden: true,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              width: getDeviceWidth(context),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/tutorial-bg.png'),
                      fit: BoxFit.fill)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: SvgPicture.asset('assets/images/logo.svg'),
                        ),
                      ],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                      child: Text(
                        localizations.translate('tutorial.title'),
                        textAlign: TextAlign.center,
                        style: themeData.textTheme.headline6,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                controller: _scrollController,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    left: 10.0,
                                    right: 10.0,
                                    bottom: 10.0,
                                  ),
                                  child: Text(
                                    localizations
                                        .translate('tutorial.description'),
                                    textAlign: TextAlign.justify,
                                    style:
                                        themeData.textTheme.bodyText1.copyWith(
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            _isScrollable
                                ? FadeTransition(
                                    opacity: _animationController,
                                    child: Icon(
                                      Icons.arrow_drop_down,
                                      color: colorBlack,
                                    ),
                                  )
                                : Container(
                                    height: 25,
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: getDeviceHeight(context) * 0.2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SecondaryButton(
                    caption: localizations.translate('tutorial.viewTutorial'),
                    isLoading: _isTutorialButtonLoading,
                    isEnabled: !_isTutorialButtonLoading,
                    onPressed: () {
                      setState(() {
                        _isTutorialButtonLoading = true;
                      });
                      Navigator.of(context).pushNamed(
                        RouteConstants.TUTORIAL_STEPS,
                      );
                    }),
                SecondaryButton(
                  caption: localizations.translate('tutorial.skip'),
                  isLoading: _isSkipButtonLoading,
                  isEnabled: !_isSkipButtonLoading,
                  buttonTextColor: themeData.textTheme.headline5.color,
                  onPressed: skipTutorial,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
