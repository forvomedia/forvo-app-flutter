import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';

class TutorialSteps extends StatefulWidget {
  @override
  State<TutorialSteps> createState() => _TutorialStepsState();
}

class _TutorialStepsState extends State<TutorialSteps>
    with SingleTickerProviderStateMixin {
  bool _tutorialChecked;
  String _tutorialCompleted = APPSFLYER_NO;
  bool _isScrollable = false;
  ScrollController _scrollController;
  AnimationController _animationController;
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _initValues();
  }

  _initValues() async {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);

    _tutorialChecked = await isTutorialChecked();

    Future.delayed(
        const Duration(milliseconds: 500), _showScrollableBottomIcon);
  }

  _scrollListener() {
    _showScrollableBottomIcon();
  }

  _showScrollableBottomIcon() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        _isScrollable = false;
      });
    } else {
      setState(() {
        _isScrollable = true;
      });
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    skipTutorial() async {
      setTutorialChecked();
      Map eventValues = {
        APPSFLYER_AF_SUCCESS: _tutorialCompleted,
        APPSFLYER_AF_TUTORIAL_ID: APPSFLYER_TUTORIAL_ID,
        APPSFLYER_AF_CONTENT: APPSFLYER_CONTENT,
      };
      await requestAppTrackinPermission();
      appsflyerEventLog(APPSFLYER_AF_TUTORIAL_COMPLETION, eventValues);
      Navigator.pushNamedAndRemoveUntil(
          context, RouteConstants.START, (route) => false);
    }

    Widget stepPage(IconData icon, String title, String body) => Column(
          children: <Widget>[
            Icon(
              icon,
              color: colorWhite,
              size: 100,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: themeData.textTheme.headline6,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        controller: _scrollController,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: 10.0,
                            right: 10.0,
                            bottom: 10,
                          ),
                          child: Text(
                            body,
                            textAlign: TextAlign.justify,
                            style: themeData.textTheme.bodyText1.copyWith(
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    _isScrollable
                        ? FadeTransition(
                            opacity: _animationController,
                            child: Icon(
                              Icons.arrow_drop_down,
                              color: colorBlack,
                            ),
                          )
                        : Container(
                            height: 25,
                          ),
                  ],
                ),
              ),
            ),
          ],
        );

    Widget recordWordPage() => stepPage(
          ForvoIcons.mic,
          localizations.translate('tutorial.steps.recordWord.title'),
          localizations.translate('tutorial.steps.recordWord.description'),
        );

    Widget addWordPage() => stepPage(
          ForvoIcons.addWord,
          localizations.translate('tutorial.steps.addWord.title'),
          localizations.translate('tutorial.steps.addWord.description'),
        );

    Widget messagesPage() => stepPage(
          ForvoIcons.chat,
          localizations.translate('tutorial.steps.messages.title'),
          localizations.translate('tutorial.steps.messages.description'),
        );

    Widget favouritesPage() => stepPage(
          ForvoIcons.favorites,
          localizations.translate('tutorial.steps.favorites.title'),
          localizations.translate('tutorial.steps.favorites.description'),
        );

    Widget notificationsPage() => stepPage(
          ForvoIcons.notifications,
          localizations.translate('tutorial.steps.notifications.title'),
          localizations.translate('tutorial.steps.notifications.description'),
        );

    Widget myAccountPage() => stepPage(
          ForvoIcons.user,
          localizations.translate('tutorial.steps.myAccount.title'),
          localizations.translate('tutorial.steps.myAccount.description'),
        );

    var pages = <Widget>[
      recordWordPage(),
      addWordPage(),
      messagesPage(),
      favouritesPage(),
      notificationsPage(),
      myAccountPage(),
    ];

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHidden: true,
      bottomNavBarHidden: true,
      body: DefaultTabController(
        length: pages.length,
        child: Builder(
          builder: (BuildContext context) => Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  width: getDeviceWidth(context),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/tutorial-bg.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: SvgPicture.asset('assets/images/logo.svg'),
                            ),
                          ],
                        ),
                        Expanded(
                          child: TabBarView(
                            children: pages,
                            physics: NeverScrollableScrollPhysics(),
                          ),
                        ),
                        isLastStep(DefaultTabController.of(context)) &&
                                !_tutorialChecked
                            ? Padding(
                                padding: const EdgeInsets.only(bottom: 40.0),
                                child: InkWell(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        const Radius.circular(10),
                                      ),
                                      color: colorWhite,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10.0, horizontal: 10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          localizations
                                              .translate('tutorial.startApp'),
                                          style: themeData.textTheme.subtitle2
                                              .copyWith(
                                            color: colorBlue,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    _tutorialCompleted = APPSFLYER_YES;
                                    skipTutorial();
                                  },
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: getDeviceHeight(context) * 0.2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SecondaryButton(
                          caption: isFirstStep(DefaultTabController.of(context))
                              ? localizations.translate('tutorial.skip')
                              : localizations.translate('tutorial.previous'),
                          onPressed: () {
                            TabController controller =
                                DefaultTabController.of(context);
                            if (!controller.indexIsChanging) {
                              if (isFirstStep(controller)) {
                                skipTutorial();
                              } else {
                                controller.animateTo(controller.index - 1);
                              }
                            }
                            setState(() {});
                            Future.delayed(const Duration(milliseconds: 500),
                                _showScrollableBottomIcon);
                          },
                        ),
                        TabPageSelector(
                          indicatorSize: 14,
                        ),
                        isLastStep(DefaultTabController.of(context)) &&
                                !_tutorialChecked
                            ? Container()
                            : SecondaryButton(
                                caption: isLastStep(
                                        DefaultTabController.of(context))
                                    ? localizations.translate('tutorial.end')
                                    : localizations.translate('tutorial.next'),
                                onPressed: () {
                                  TabController controller =
                                      DefaultTabController.of(context);
                                  if (!controller.indexIsChanging) {
                                    if (isLastStep(controller)) {
                                      skipTutorial();
                                    } else {
                                      controller
                                          .animateTo(controller.index + 1);
                                    }
                                  }
                                  setState(() {});
                                  Future.delayed(
                                      const Duration(milliseconds: 500),
                                      _showScrollableBottomIcon);
                                },
                              ),
                      ],
                    ),
                    isFirstStep(DefaultTabController.of(context)) ||
                            isLastStep(DefaultTabController.of(context))
                        ? Container()
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SecondaryButton(
                                caption:
                                    localizations.translate('tutorial.skip'),
                                onPressed: skipTutorial,
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool isLastStep(TabController controller) =>
      controller.index == controller.length - 1;

  bool isFirstStep(TabController controller) => controller.index == 0;
}
