import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/media_query_util.dar.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _startTime();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: Center(
          child: SvgPicture.asset(
            isDarkThemeActive(context)
                ? 'assets/images/splash-logo-dark.svg'
                : 'assets/images/splash-logo-light.svg',
            fit: BoxFit.fitWidth,
          ),
        ),
      );

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  _startTime() {
    var _duration = Duration(seconds: 4);
    var timer = Timer(_duration, _navigationPage);

    setState(() => _timer = timer);
  }

  _navigationPage() async {
    bool tutorialChecked = await isTutorialChecked();
    if (!tutorialChecked) {
      Navigator.of(context).pushReplacementNamed(RouteConstants.TUTORIAL_HOME);
    } else {
      Navigator.of(context).pushReplacementNamed(RouteConstants.START);
    }
  }
}
