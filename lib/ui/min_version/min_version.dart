import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/layout_constants.dart';
import 'package:forvo/model/version.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:url_launcher/url_launcher.dart';

class MinVersionArguments {
  final Version version;

  MinVersionArguments({this.version});
}

class MinVersion extends StatefulWidget {
  @override
  _MinVersionState createState() => _MinVersionState();
}

class _MinVersionState extends State<MinVersion> {
  Version _version;
  bool _isForceLogoutChecked = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    _extractArguments();
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args = ModalRoute.of(context).settings.arguments as MinVersionArguments;
    _version = args.version;
  }

  _goToStore() async {
    var localizations = AppLocalizations.of(context);
    try {
      var url = deviceIsAndroid() ? _version.androidUrl : _version.iosUrl;
      await launch(url);
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('minVersionError'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  _checkForceLogout() async {
    _isForceLogoutChecked = true;
    if (_version.forceLogout) {
      bool _isUserLogged = await isUserLogged();
      if (_isUserLogged) {
        forceLogoutUserSession();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    double _logoHeight = 180.0;
    if (getDeviceHeight(context) < LayoutConstants.LAYOUT_TABLET_BREAKPOINT) {
      _logoHeight = 130.0;
    }

    if (_version != null && !_isForceLogoutChecked) {
      _checkForceLogout();
    }

    return WillPopScope(
      onWillPop: () async => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            localizations.translate('shared.appCloseWarning'),
            style: themeData.textTheme.bodyText2,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                localizations.translate('shared.close'),
              ),
              onPressed: () => Navigator.of(context).pop(true),
            ),
            ElevatedButton(
              child: Text(
                localizations.translate('shared.cancel'),
              ),
              onPressed: () => Navigator.of(context).pop(false),
            ),
          ],
        ),
      ),
      child: CustomScaffold(
        scaffoldMessengerKey: _scaffoldMessengerKey,
        headerAppBarHidden: true,
        bottomNavBarHidden: true,
        scaffoldBackgroundColor: colorBlue,
        body: Container(
          color: themeData.backgroundColor,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: ListView(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/user-bg.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: Container(
                        width: getDeviceWidth(context),
                        height: getDeviceHeight(context) * 0.15,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SvgPicture.asset('assets/images/logo.svg'),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(20.0),
                            child: Icon(
                              Icons.download_rounded,
                              size: _logoHeight,
                            ),
                          ),
                          ListTile(
                            title: Text(
                              localizations.translate('minVersion.title'),
                              style: themeData.textTheme.headline6,
                              textAlign: TextAlign.center,
                            ),
                            subtitle: Padding(
                              padding: EdgeInsets.only(top: 25.0),
                              child: Text(
                                localizations.translate('minVersion.subtitle'),
                                style: themeData.textTheme.bodyText2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: MainButton(
                  caption: localizations.translate('minVersion.button'),
                  onPressed: _version != null ? _goToStore : null,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
