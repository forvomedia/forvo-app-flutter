import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:forvo/common/notifiers/theme_changer.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/service/social_network/apple/apple_service.dart';
import 'package:forvo/service/social_network/facebook/facebook_service.dart';
import 'package:forvo/service/social_network/google/google_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/registration/password_reminder.dart';
import 'package:forvo/ui/registration/widgets/apple_button.dart';
import 'package:forvo/ui/registration/widgets/facebook_button.dart';
import 'package:forvo/ui/registration/widgets/google_button.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text_form_field/password_field.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/crypto_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/messages_util.dart';
import 'package:forvo/util/notification_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      GlobalKey<FormFieldState<String>>();

  bool _loginButtonLoading = false;
  bool _loading = false;
  String _loginChannel = APPSFLYER_AF_LOGIN_DEFAULT;

  void _loginSocialNetWork(String socialNetwork) async {
    var localizations = AppLocalizations.of(context);
    try {
      setState(() {
        _loading = true;
      });

      String response;
      switch (socialNetwork) {
        case SocialNetworkConstants.FACEBOOK:
          _loginChannel = SocialNetworkConstants.FACEBOOK;
          response = await loginFacebook(context);
          break;
        case SocialNetworkConstants.GOOGLE:
          _loginChannel = SocialNetworkConstants.GOOGLE;
          response = await loginGoogle(context);
          break;
        case SocialNetworkConstants.APPLE:
          _loginChannel = SocialNetworkConstants.APPLE;
          response = await loginApple(context);
          break;
        default:
          response = SocialNetworkConstants.REQUEST_ERROR;
          break;
      }

      if (response != null) {
        switch (response) {
          case SocialNetworkConstants.REQUEST_OK:
            _postLogin();
            break;
          case SocialNetworkConstants.REQUEST_ERROR:
            break;
          case SocialNetworkConstants.REQUEST_CANCELLED:
            break;
        }
      }
    } on ApiException catch (error) {
      String messages = error.getErrorMessages(context);
      if (messages != null) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        deleteUserSession();
      }
    } on Exception catch (_) {
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      deleteUserSession();
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }

  void _submit() {
    if (_formKey.currentState.validate()) {
      _login(_userController.text, _passwordController.text);
    }
  }

  void _postLogin() async {
    Map eventValues = {
      APPSFLYER_AF_CHANNEL: _loginChannel,
    };
    appsflyerEventLog(APPSFLYER_AF_LOGIN, eventValues);

    String languageCode = await getPreferredLanguageCode();
    AppStateManager.of(context).onLocaleChanged(languageCode);

    ThemeChanger _themeChanger =
        Provider.of<ThemeChanger>(context, listen: false);
    ThemeData _lightTheme = await getUserLightTheme();
    ThemeData _darkTheme = await getUserDarkTheme();
    _themeChanger.setTheme(_lightTheme, _darkTheme);

    await subscribePushNotifications(context, languageCode);

    Navigator.of(context).pushNamedAndRemoveUntil(
      RouteConstants.HOME,
      (Route<dynamic> route) => false,
    );
  }

  void _login(String usernameEmail, String password) async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    String languageCode = state.locale.languageCode;
    String md5Password = generateMd5(password);
    bool _isUserLogged = false;

    try {
      setState(() => _loginButtonLoading = true);
      FocusScope.of(context).requestFocus(FocusNode());

      var userInfo = await login(
        context,
        usernameEmail,
        password,
        languageCode,
      );
      userInfo.user.setMd5Password(md5Password);
      _isUserLogged = userInfo != null;

      AppStateManager.of(context).onStateChanged(
        AppState(
          isUserLogged: _isUserLogged,
          isSocialNetworkLogin: false,
          userInfo: userInfo,
        ),
      );
      if (_isUserLogged) {
        getMessagesAfterLogin(
          userInfo.user.username,
          userInfo.user.md5Password,
        );
      }
      _postLogin();
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String message;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          case ApiErrorCodeConstants.ERROR_1059:
            var userInfo = await login(
              context,
              usernameEmail,
              password,
              languageCode,
            );
            userInfo.user.setMd5Password(md5Password);
            _isUserLogged = userInfo != null;

            AppStateManager.of(context).onStateChanged(
              AppState(
                isUserLogged: _isUserLogged,
                isSocialNetworkLogin: false,
                userInfo: userInfo,
              ),
            );
            if (_isUserLogged) {
              getMessagesAfterLogin(
                userInfo.user.username,
                userInfo.user.md5Password,
              );
            }
            _postLogin();
            break;
          default:
            String translatedErrorMessage =
                _error.getTranslatedErrorMessage(context);
            if (message == null) {
              message = translatedErrorMessage;
            } else {
              message = '$message\n$translatedErrorMessage';
            }
            break;
        }
      }
      if (message != null) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        deleteUserSession();
      }
    } on NotConnectedException catch (_) {
      bool _hasConnection = await deviceHasConnection();
      String message = _hasConnection
          ? '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}'
          : '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';

      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      deleteUserSession();
    } on Exception catch (_) {
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      deleteUserSession();
    } finally {
      setState(() => _loginButtonLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);

    final FocusNode _passwordFocusNode = FocusNode();

    InputBorder _inputBorder = UnderlineInputBorder(
      borderSide: BorderSide(color: colorBlue),
    );
    InputBorder _focusedInputBorder = UnderlineInputBorder(
      borderSide: BorderSide(color: colorBlue),
    );

    var themeData = Theme.of(context).copyWith(
      inputDecorationTheme: InputDecorationTheme(
        filled: false,
        border: _inputBorder,
        focusedBorder: _focusedInputBorder,
      ),
    );

    bool _deviceIsIOS = !deviceIsAndroid();
    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceLoginActive;

    double socialNetworkIconSize = 18;
    double socialNetworkFontSize = 13;
    if (_deviceIsIOS) {
      socialNetworkIconSize = 18;
      socialNetworkFontSize = 18;
    } else {
      socialNetworkIconSize = 18;
      socialNetworkFontSize = 13;
      if (useSmallMobileLayout(context)) {
        socialNetworkIconSize = 16;
        socialNetworkFontSize = 12;
      }
    }

    TextAlign socialNetworkButtonTextAlign = TextAlign.center;
    double socialNetworkButtonTextContainerWidthPercentage;
    double socialNetworkButtonTextContainerVerticalPadding;
    if (_deviceIsIOS) {
      socialNetworkButtonTextAlign = TextAlign.start;
      socialNetworkButtonTextContainerWidthPercentage = 0.50;
      socialNetworkButtonTextContainerVerticalPadding = 14.0;
    } else {
      socialNetworkButtonTextContainerWidthPercentage = 0.24;
      socialNetworkButtonTextContainerVerticalPadding = 10.0;
    }

    Widget _getGoogleButton() => GoogleButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('login.googleButton'),
          isTextInOneLine: _deviceIsIOS,
          textContainerWidthSize: getDeviceWidth(context) *
              socialNetworkButtonTextContainerWidthPercentage,
          textContainerVerticalPadding:
              socialNetworkButtonTextContainerVerticalPadding,
          textAlign: socialNetworkButtonTextAlign,
          iconSize: socialNetworkIconSize,
          fontSize: socialNetworkFontSize,
          onPressed: () async =>
              _loginSocialNetWork(SocialNetworkConstants.GOOGLE),
        );

    Widget _getFacebookButton() => FacebookButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('login.facebookButton'),
          isTextInOneLine: _deviceIsIOS,
          textContainerWidthSize: getDeviceWidth(context) *
              socialNetworkButtonTextContainerWidthPercentage,
          textContainerVerticalPadding:
              socialNetworkButtonTextContainerVerticalPadding,
          textAlign: socialNetworkButtonTextAlign,
          iconSize: socialNetworkIconSize,
          fontSize: socialNetworkFontSize,
          onPressed: () async =>
              _loginSocialNetWork(SocialNetworkConstants.FACEBOOK),
        );

    Widget _getAppleButton() => AppleButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('login.appleButton'),
          onPressed: () async =>
              _loginSocialNetWork(SocialNetworkConstants.APPLE),
        );

    Widget _getListView() => ListView(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SvgPicture.asset('assets/images/logo.svg'),
                      SecondaryButton(
                        caption: localizations.translate('signup.button'),
                        buttonTextColor: colorWhite,
                        onPressed: () => Navigator.pushNamedAndRemoveUntil(
                          context,
                          RouteConstants.SIGN_UP,
                          ModalRoute.withName(RouteConstants.START),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsetsDirectional.only(top: 25, bottom: 15),
                    child: Text(
                      localizations.translate('shared.login'),
                      style: themeData.textTheme.bodyText1
                          .copyWith(fontWeight: FontWeight.bold, fontSize: 28),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: themeData.dividerColor),
                      borderRadius: BorderRadius.circular(10),
                      color: isDarkThemeActive(context)
                          ? themeData.dividerColor
                          : themeData.scaffoldBackgroundColor,
                      boxShadow: [
                        BoxShadow(
                          color: themeData.dividerColor,
                          offset: Offset(1.0, 1.0),
                        )
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            _isMaintenanceActive
                                ? Padding(
                                    padding:
                                        const EdgeInsets.only(bottom: 20.0),
                                    child: MaintenanceWarningMessage(),
                                  )
                                : Container(),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                localizations.translate('login.subtitle'),
                                style: themeData.textTheme.bodyText2
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: TextFormField(
                                controller: _userController,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                style: themeData.textTheme.bodyText2
                                    .copyWith(fontStyle: FontStyle.normal),
                                decoration: InputDecoration(
                                  border: _inputBorder,
                                  focusedBorder: _focusedInputBorder,
                                  hintMaxLines: 2,
                                  hintText: localizations
                                      .translate('login.emailUserField'),
                                ),
                                validator: (String value) {
                                  if (value == null || value.isEmpty) {
                                    return localizations.translate(
                                        'error.shared.emptyUsernameEmail');
                                  } else {
                                    return null;
                                  }
                                },
                                onEditingComplete: () {
                                  FocusScope.of(context)
                                      .requestFocus(_passwordFocusNode);
                                },
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: PasswordField(
                                themeData: themeData,
                                fieldKey: _passwordFieldKey,
                                focusNode: _passwordFocusNode,
                                controller: _passwordController,
                                hintText:
                                    localizations.translate('shared.password'),
                                style: themeData.textTheme.bodyText2,
                                validator: (String value) {
                                  if (value == null || value.isEmpty) {
                                    return localizations.translate(
                                        'error.shared.emptyPassword');
                                  } else {
                                    return null;
                                  }
                                },
                                onEditingComplete: _submit,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 20.0),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: MainButton(
                                caption:
                                    localizations.translate('shared.login'),
                                isLoading: _loginButtonLoading,
                                isEnabled: !_isMaintenanceActive,
                                onPressed: _submit,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 20.0),
                  ),
                  _deviceIsIOS
                      ? Column(
                          children: [
                            _getGoogleButton(),
                            Padding(padding: const EdgeInsets.all(4)),
                            _getFacebookButton(),
                            Padding(padding: const EdgeInsets.all(4)),
                            _getAppleButton(),
                          ],
                        )
                      : IntrinsicHeight(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Expanded(
                                child: _getGoogleButton(),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4),
                              ),
                              Expanded(
                                child: _getFacebookButton(),
                              )
                            ],
                          ),
                        ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: SecondaryButton(
                      caption: localizations.translate('shared.forgotPassword'),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PasswordReminder(),
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          ],
        );

    return SafeArea(
      child: Scaffold(
        backgroundColor: colorBlue,
        body: Stack(
          children: <Widget>[
            PositionedDirectional(
              child: Container(
                height: getDeviceHeight(context),
                color: themeData.backgroundColor,
                alignment: Alignment.topCenter,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/login-bg.png'),
                        fit: BoxFit.fill),
                  ),
                  child: Container(
                    width: getDeviceWidth(context),
                    height: getDeviceHeight(context) * 0.35,
                  ),
                ),
              ),
            ),
            PositionedDirectional(
              child: _getListView(),
            ),
            _loading
                ? PositionedDirectional(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 10,
                          color: Colors.transparent,
                        ),
                        color: Colors.black.withOpacity(0.5),
                      ),
                      child: CustomCircularProgressIndicator(
                        valueColor: colorWhite,
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
