import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/social_network_user_info.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/registration/widgets/accent_location_selector.dart';
import 'package:forvo/ui/registration/widgets/birthdate_selector.dart';
import 'package:forvo/ui/registration/widgets/gender_selector.dart';
import 'package:forvo/ui/registration/widgets/language_selector.dart';
import 'package:forvo/ui/registration/widgets/terms_privacy_checkbox.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/menu/language_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/form_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;
import 'package:url_launcher/url_launcher.dart';

class SocialNetworkCompleteSignUp extends StatefulWidget {
  final String socialNetwork;
  final SocialNetworkUserInfo userInfo;

  SocialNetworkCompleteSignUp({
    this.socialNetwork,
    this.userInfo,
  });

  @override
  _SocialNetworkCompleteSignUpState createState() =>
      _SocialNetworkCompleteSignUpState();
}

class _SocialNetworkCompleteSignUpState
    extends State<SocialNetworkCompleteSignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  FocusNode _usernameFocusNode = FocusNode(debugLabel: 'username');
  FocusNode _languageAccentSelectorFocusNode =
      FocusNode(debugLabel: 'languageAccentSelector');
  FocusNode _accentLocalizationSelectorFocusNode =
      FocusNode(debugLabel: 'accentLocalizationSelector');
  FocusNode _firstNameFocusNode = FocusNode(debugLabel: 'firstName');
  FocusNode _lastNameFocusNode = FocusNode(debugLabel: 'lastName');
  FocusNode _genderSelectorFocusNode = FocusNode(debugLabel: 'genderSelector');
  FocusNode _rolSelectorFocusNode = FocusNode(debugLabel: 'rolSelector');
  FocusNode _birthdateSelectorFocusNode =
      FocusNode(debugLabel: 'birthdateSelector');

  Timer _debounce;
  int _debounceTime = 2000;

  bool _checkNullValues = false;

  bool _isButtonEnabled = false, _isButtonLoading = false;
  bool _isUsernameCheckLoading = false;
  String _lastSearchedUsername;
  bool _isUsernameValid;
  bool _isFirstNameValid;
  bool _isLastNameValid;
  bool _isAccentLocalizationValid = true;
  String _usernameErrorMessage;
  String _gender;
  DateTime _birthdate;
  bool _userSelectedBirthdate = false;
  Language _selectedNativeLanguage;
  bool _locationPermissionGranted;
  bool _locationPermissionRequested = false;
  Location location = Location();
  String _accentLocation;
  LocationData _currentPosition;
  bool _serviceEnabled;
  TapGestureRecognizer _privacyTapRecognizer;
  TapGestureRecognizer _termsTapRecognizer;
  bool _acceptTerms;
  Completer<GoogleMapController> _mapControllerCompleter = Completer();
  GoogleMapController _mapController;
  LatLng _cameraPosition =
      LatLng(LOCATION_DEFAULT_MAP_LATITUDE, LOCATION_DEFAULT_MAP_LONGITUDE);
  Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    _acceptTerms = false;
    _getCurrentLocation();
    _usernameController =
        TextEditingController(text: widget.userInfo.email.split('@').first);
    _usernameController.addListener(_onUsernameChanged);
    if (widget.userInfo.firstName.isNotEmpty) {
      _firstNameController =
          TextEditingController(text: widget.userInfo.firstName);
    }
    _firstNameController.addListener(_onFirstNameChanged);
    if (widget.userInfo.lastName.isNotEmpty) {
      _lastNameController =
          TextEditingController(text: widget.userInfo.lastName);
    }
    _lastNameController.addListener(_onLastNameChanged);
    _privacyTapRecognizer = TapGestureRecognizer()
      ..onTap = () => _launchURL(PRIVACY_POLICY_URL);
    _termsTapRecognizer = TapGestureRecognizer()
      ..onTap = () => _launchURL(TERMS_OF_USE_URL);
    _locationPermissionStatus();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _usernameController.removeListener(_onUsernameChanged);
    _usernameController.dispose();
    _usernameFocusNode.dispose();
    _languageAccentSelectorFocusNode.dispose();
    _accentLocalizationSelectorFocusNode.dispose();
    _firstNameController.removeListener(_onFirstNameChanged);
    _firstNameController.dispose();
    _firstNameFocusNode.dispose();
    _lastNameController.removeListener(_onLastNameChanged);
    _lastNameController.dispose();
    _lastNameFocusNode.dispose();
    _genderSelectorFocusNode.dispose();
    _rolSelectorFocusNode.dispose();
    _birthdateSelectorFocusNode.dispose();
    _privacyTapRecognizer.dispose();
    _termsTapRecognizer.dispose();
    if (_mapController != null) {
      _mapController.dispose();
    }
    super.dispose();
  }

  _onUsernameChanged() {
    setState(() {
      _usernameErrorMessage = null;
    });
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkUsername);
  }

  _onFirstNameChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkFirstName);
  }

  _onLastNameChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkLastName);
  }

  _checkUsername() async {
    if (mounted) {
      if (_usernameController.text.isEmpty) {
        _lastSearchedUsername = _usernameController.text;
        if (!_formKey.currentState.validate()) {
          setState(() {
            _isUsernameValid = false;
          });
        }
      } else {
        if (_lastSearchedUsername == null ||
            _lastSearchedUsername != _usernameController.text) {
          await _signupCheckUsername();
        }
      }
    }
  }

  _signupCheckUsername() async {
    if (mounted && !_isUsernameCheckLoading) {
      var state = AppStateManager.of(context).state;
      setState(() {
        _lastSearchedUsername = _usernameController.text;
        _isUsernameCheckLoading = true;
      });
      _formKey.currentState.validate();
      try {
        await signupCheckUsername(
          context,
          username: _usernameController.text,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {
          _isUsernameCheckLoading = false;
          _isUsernameValid = true;
          _usernameErrorMessage = null;
        });
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        setState(() {
          _usernameErrorMessage = messages;
          _isUsernameValid = false;
        });
      } on Exception catch (_) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            AppLocalizations.of(context).translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        if (mounted) {
          setState(() {
            _isUsernameCheckLoading = false;
          });
          _formKey.currentState.validate();
        }
      }
    }
  }

  String _onUsernameValidate(String username) {
    var localizations = AppLocalizations.of(context);
    if (username == null || username.isEmpty) {
      return localizations.translate('error.signup.emptyUsername');
    } else {
      if (_usernameErrorMessage != null && _usernameErrorMessage.isNotEmpty) {
        return _usernameErrorMessage;
      }
      return null;
    }
  }

  _onUsernameSubmit(String username) async {
    await _checkUsername();
    if (_isUsernameValid) {
      fieldFocusChange(
        context,
        _usernameFocusNode,
        _languageAccentSelectorFocusNode,
      );
    }
  }

  _checkFirstName() async {
    if (mounted) {
      bool isFirstNameValid =
          _onFirstNameValidate(_firstNameController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isFirstNameValid = isFirstNameValid;
      });
    }
  }

  String _onFirstNameValidate(String firstName) {
    var localizations = AppLocalizations.of(context);
    if (firstName == null || firstName.isEmpty) {
      return localizations.translate('error.shared.emptyFirstName');
    } else {
      return null;
    }
  }

  _onFirstNameSubmit(String firstName) async {
    await _checkFirstName();
    if (_isFirstNameValid) {
      fieldFocusChange(context, _firstNameFocusNode, _lastNameFocusNode);
    }
  }

  _checkLastName() async {
    if (mounted) {
      bool isLastNameValid =
          _onLastNameValidate(_lastNameController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isLastNameValid = isLastNameValid;
      });
    }
  }

  String _onLastNameValidate(String lastName) {
    var localizations = AppLocalizations.of(context);
    if (lastName == null || lastName.isEmpty) {
      return localizations.translate('error.shared.emptyLastName');
    } else {
      return null;
    }
  }

  _onLastNameSubmit(String lastName) async {
    await _checkLastName();
    if (_isLastNameValid) {
      fieldFocusChange(
          context, _lastNameFocusNode, _languageAccentSelectorFocusNode);
    }
  }

  _locationPermissionStatus() async {
    _locationPermissionRequested = await isLocationPermissionRequested();
    if (_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.isGranted;
    }
    setState(() {});
  }

  _getCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    await _locationPermissionStatus();

    if (!_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.request().isGranted;
      setLocationPermissionRequested();
      _locationPermissionRequested = true;
    }
    setState(() {});

    if (_locationPermissionGranted == null || !_locationPermissionGranted) {
      return;
    }

    _currentPosition = await location.getLocation();

    final String markerIdVal =
        'marker_id_${_currentPosition.latitude}_${_currentPosition.longitude}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);
    setState(() {
      _accentLocation = 'Latitude: ${_currentPosition.latitude}, '
          'Longitude: ${_currentPosition.longitude}';
      _cameraPosition =
          LatLng(_currentPosition.latitude, _currentPosition.longitude);
      _isAccentLocalizationValid = true;
    });

    _mapController = await _mapControllerCompleter.future;
    _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: _cameraPosition,
          zoom: 12,
        ),
      ),
    );
  }

  _onTapMarker() {
    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _onTapMap(LatLng position) {
    final String markerIdVal =
        'marker_id_${position.latitude}_${position.longitude}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(position.latitude, position.longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);

    setState(() {
      _accentLocation = 'Latitude: ${position.latitude}, '
          'Longitude: ${position.longitude}';
      _cameraPosition = LatLng(position.latitude, position.longitude);
      _isAccentLocalizationValid = true;
    });

    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _checkFormParams() {
    _checkUsername();
    FocusNode firstNullValueFocusNode;
    firstNullValueFocusNode = _birthdate == null || !_userSelectedBirthdate
        ? _birthdateSelectorFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode =
        _gender == null ? _genderSelectorFocusNode : firstNullValueFocusNode;
    if (_markers.isEmpty) {
      _isAccentLocalizationValid = false;
      firstNullValueFocusNode = _accentLocalizationSelectorFocusNode;
    }
    firstNullValueFocusNode = _selectedNativeLanguage == null
        ? _languageAccentSelectorFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isLastNameValid == null || !_isLastNameValid
        ? _lastNameFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isFirstNameValid == null || !_isFirstNameValid
        ? _firstNameFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isUsernameValid == null || !_isUsernameValid
        ? _usernameFocusNode
        : firstNullValueFocusNode;
    if (firstNullValueFocusNode != null) {
      fieldFocusChange(
          context, firstNullValueFocusNode, firstNullValueFocusNode);
    }
  }

  _socialNetworkSignup() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    String languageCode = state.locale.languageCode;

    setState(() => _isButtonLoading = true);

    try {
      String _ip = await getIP();
      await socialNetworkSignup(
        context,
        socialNetwork: widget.socialNetwork,
        socialNetworkId: widget.userInfo.id,
        username: _usernameController.text,
        email: widget.userInfo.email,
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        gender: _gender,
        languageCode: _selectedNativeLanguage.code,
        latitude: _cameraPosition.latitude.toString(),
        longitude: _cameraPosition.longitude.toString(),
        ip: _ip,
        birthdateDay: getDefaultFormattedDayFromDate(_birthdate),
        birthdateMonth: getDefaultFormattedMonthFromDate(_birthdate),
        birthdateYear: getDefaultFormattedYearFromDate(_birthdate),
        interfaceLanguageCode: languageCode,
      );

      Map eventValues = {
        APPSFLYER_AF_REGISTRATION_METHOD: 'email, ${widget.socialNetwork}',
      };
      appsflyerEventLog(APPSFLYER_AF_COMPLETE_REGISTRATION, eventValues);

      Navigator.of(context).pop(SocialNetworkConstants.REQUEST_OK);
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String message;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          case ApiErrorCodeConstants.ERROR_1073:
            setState(() {
              _isAccentLocalizationValid = false;
            });
            if (message == null) {
              message = _error.errorMessage;
            } else {
              message = '$message\n${_error.errorMessage}';
            }
            break;
          default:
            if (message == null) {
              message = _error.errorMessage;
            } else {
              message = '$message\n${_error.errorMessage}';
            }
            break;
        }
      }
    } on NotConnectedException catch (_) {
      bool _hasConnection = await deviceHasConnection();
      String message = _hasConnection
          ? '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}'
          : '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';

      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  void _launchURL(url) async {
    if (!await launch(url, forceWebView: true)) {
      var localizations = AppLocalizations.of(context);
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    _isButtonEnabled = _isUsernameValid != null &&
        _isUsernameValid &&
        _isFirstNameValid != null &&
        _isFirstNameValid &&
        _isLastNameValid != null &&
        _isLastNameValid &&
        _isAccentLocalizationValid != null &&
        _isAccentLocalizationValid &&
        _selectedNativeLanguage != null &&
        _gender != null &&
        _birthdate != null &&
        _userSelectedBirthdate &&
        _cameraPosition != null &&
        _cameraPosition.latitude != null &&
        _cameraPosition.latitude != LOCATION_DEFAULT_MAP_LATITUDE &&
        _cameraPosition.longitude != null &&
        _cameraPosition.longitude != LOCATION_DEFAULT_MAP_LONGITUDE &&
        _acceptTerms;

    _onNativeLanguageMenuOpen() {
      _languageAccentSelectorFocusNode.requestFocus();
      onLanguageMenuOpen(
        context,
        languages: state.languages,
        popularLanguages: state.popularLanguages,
        onChangeLanguage: (Language language) {
          setState(() {
            _selectedNativeLanguage = language;
          });
          Navigator.pop(context);
          fieldFocusChange(
            context,
            _languageAccentSelectorFocusNode,
            _accentLocalizationSelectorFocusNode,
          );
        },
      );
    }

    _onBirthdateSelect() async {
      _birthdateSelectorFocusNode.requestFocus();
      int maximumValidYear = DateTime.now().year - DATE_PICKER_YEARS_13;
      int maxValidMonth = 12;
      int maxValidDay = 31;
      DateTime previousBirthdate;
      if (_birthdate == null) {
        _birthdate = DateTime(maximumValidYear, maxValidMonth, maxValidDay);
      } else {
        previousBirthdate = _birthdate;
      }

      DateTime newBirthdate = await showDatePicker(
        context: context,
        locale: state.locale,
        initialDate: _birthdate,
        firstDate: DateTime(DATE_PICKER_INITIAL_YEAR),
        lastDate: DateTime(maximumValidYear, maxValidMonth, maxValidDay),
      );

      if (newBirthdate != null) {
        setState(() {
          _birthdate = newBirthdate;
          _userSelectedBirthdate = true;
        });
      } else {
        setState(() {
          _birthdate = previousBirthdate;
          _userSelectedBirthdate = previousBirthdate != null;
        });
      }
    }

    InputBorder _textFormFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: themeData.dividerColor,
        width: 1.0,
      ),
    );

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorWhite : colorLightThemeGrey,
    );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHidden: true,
      bottomNavBarHidden: true,
      scaffoldBackgroundColor: colorBlue,
      body: ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/user-bg.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Container(
              width: getDeviceWidth(context),
              height: getDeviceHeight(context) * 0.15,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/images/logo.svg'),
                  SecondaryButton(
                    caption: localizations.translate('shared.login'),
                    buttonTextColor: colorWhite,
                    onPressed: () => Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.LOGIN,
                      ModalRoute.withName(RouteConstants.START),
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            color: themeData.backgroundColor,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsetsDirectional.only(top: 35, bottom: 10),
                    child: Center(
                      child: Text(
                        localizations.translate('signup.button'),
                        style: themeData.textTheme.headline6,
                      ),
                    ),
                  ),
                  Divider(
                    height: 28,
                    thickness: 2,
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.username'),
                    textStyle: _headLineTextStyle,
                  ),
                  TextFormField(
                    controller: _usernameController,
                    autofocus: true,
                    focusNode: _usernameFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    style: themeData.textTheme.bodyText2.copyWith(
                      fontStyle: FontStyle.normal,
                    ),
                    decoration: InputDecoration(
                      hintMaxLines: 2,
                      errorMaxLines: 3,
                      enabledBorder: _textFormFieldBorder,
                      suffixIcon: _isUsernameCheckLoading
                          ? Container(
                              height: 10.0,
                              width: 10.0,
                              margin: EdgeInsets.all(15.0),
                              child: CustomCircularProgressIndicator(
                                isCentered: false,
                              ),
                            )
                          : _isUsernameValid == null
                              ? Container()
                              : _isUsernameValid
                                  ? Icon(
                                      Icons.check,
                                      color: Colors.green,
                                    )
                                  : Icon(
                                      Icons.close,
                                      color: Colors.red,
                                    ),
                    ),
                    validator: _onUsernameValidate,
                    onFieldSubmitted: _onUsernameSubmit,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.firstName'),
                    textStyle: _headLineTextStyle,
                  ),
                  TextFormField(
                    focusNode: _firstNameFocusNode,
                    controller: _firstNameController,
                    keyboardType: TextInputType.name,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.none,
                    style: themeData.textTheme.bodyText2
                        .copyWith(fontStyle: FontStyle.normal),
                    decoration: InputDecoration(
                      hintMaxLines: 2,
                      errorMaxLines: 3,
                      enabledBorder: _textFormFieldBorder,
                    ),
                    validator: _onFirstNameValidate,
                    onFieldSubmitted: _onFirstNameSubmit,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.lastName'),
                    textStyle: _headLineTextStyle,
                  ),
                  TextFormField(
                    focusNode: _lastNameFocusNode,
                    controller: _lastNameController,
                    keyboardType: TextInputType.name,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.none,
                    style: themeData.textTheme.bodyText2
                        .copyWith(fontStyle: FontStyle.normal),
                    decoration: InputDecoration(
                      hintMaxLines: 2,
                      errorMaxLines: 3,
                      enabledBorder: _textFormFieldBorder,
                    ),
                    validator: _onLastNameValidate,
                    onFieldSubmitted: _onLastNameSubmit,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.nativeLanguage'),
                    textStyle: _headLineTextStyle,
                  ),
                  LanguageSelector(
                    language: _selectedNativeLanguage,
                    onLanguageMenuOpen: _onNativeLanguageMenuOpen,
                    focusNode: _languageAccentSelectorFocusNode,
                    checkValue: _checkNullValues,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.accent'),
                    textStyle: _headLineTextStyle,
                  ),
                  AccentLocalizationSelector(
                    permissionGranted: _locationPermissionGranted,
                    permissionRequested: _locationPermissionRequested,
                    accentLocation: _accentLocation,
                    onGetCurrentLocation: _getCurrentLocation,
                    mapController: _mapControllerCompleter,
                    cameraPosition: _cameraPosition,
                    markers: _markers,
                    onTapMap: _onTapMap,
                    focusNode: _accentLocalizationSelectorFocusNode,
                    isValid: _isAccentLocalizationValid,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.gender.gender'),
                    textStyle: _headLineTextStyle,
                  ),
                  IntrinsicHeight(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          const Radius.circular(5.0),
                        ),
                        border: Border.all(
                          color: themeData.dividerColor,
                          width: 1.0,
                        ),
                      ),
                      child: GenderSelector(
                        focusNode: _genderSelectorFocusNode,
                        gender: _gender,
                        onTap: (String newGender) {
                          setState(() {
                            _gender = newGender;
                          });
                          fieldFocusChange(
                            context,
                            _genderSelectorFocusNode,
                            _rolSelectorFocusNode,
                          );
                        },
                        checkValue: _checkNullValues,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  HeadlineText(
                    text: localizations.translate('shared.birthdate'),
                    textStyle: _headLineTextStyle,
                  ),
                  BirthdateSelector(
                    birthdate: _birthdate,
                    onTap: _onBirthdateSelect,
                    focusNode: _birthdateSelectorFocusNode,
                    checkValue: _checkNullValues,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  Divider(
                    height: 28,
                    thickness: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 15.0, left: 10.0, right: 10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.info_outlined),
                        ),
                        Expanded(
                          child: Text(
                            localizations.translate('shared.comboDataWarning'),
                            style: TextStyle(fontSize: 15),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  TermsPrivacyCheckbox(
                    acceptTerms: _acceptTerms,
                    onChanged: (bool value) {
                      if (value) {
                        _checkNullValues = true;
                        _checkFormParams();
                      }
                      setState(() => _acceptTerms = value);
                    },
                    termsTapRecognizer: _termsTapRecognizer,
                    privacyTapRecognizer: _privacyTapRecognizer,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  Divider(
                    height: 28,
                    thickness: 2,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                  MainButton(
                    caption: localizations.translate('signup.button'),
                    onPressed: _socialNetworkSignup,
                    isLoading: _isButtonLoading,
                    isEnabled: !_isUsernameCheckLoading &&
                        !_isButtonLoading &&
                        _isButtonEnabled,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
