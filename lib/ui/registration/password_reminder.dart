import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';

class PasswordReminder extends StatefulWidget {
  @override
  _PasswordReminderState createState() => _PasswordReminderState();
}

class _PasswordReminderState extends State<PasswordReminder> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  Timer _debounce;
  int _debounceTime = 2000;

  bool _isEmailValid;
  bool _isButtonLoading = false;
  bool _isButtonEnabled = false;
  String _lastSearchedEmail;

  @override
  void initState() {
    super.initState();
    _emailController.addListener(_onEmailChanged);
  }

  @override
  void dispose() {
    _emailController.removeListener(_onEmailChanged);
    _emailController.dispose();
    _emailFocusNode.dispose();
    super.dispose();
  }

  _onEmailChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkEmail);
  }

  _checkEmail() async {
    if (mounted) {
      if (_emailController.text.isEmpty) {
        _lastSearchedEmail = _emailController.text;
        bool isValid = _formKey.currentState.validate();
        setState(() {
          _isEmailValid = isValid;
        });
      } else {
        if (_lastSearchedEmail == null ||
            _lastSearchedEmail != _emailController.text) {
          setState(() {
            _lastSearchedEmail = _emailController.text;
          });
          bool isValid = _formKey.currentState.validate();
          setState(() {
            _isEmailValid = isValid;
          });
        }
      }
    }
  }

  String _onEmailValidate(String email) {
    var localizations = AppLocalizations.of(context);
    if (email == null || email.isEmpty) {
      return localizations.translate('error.shared.emptyEmail');
    }
    return null;
  }

  _onEmailSubmit(String email) async {
    await _checkEmail();
  }

  void _submit() {
    if (_formKey.currentState.validate()) {
      _reminder(_emailController.text);
    }
  }

  void _reminder(String usernameEmail) async {
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    try {
      setState(() => _isButtonLoading = true);
      FocusScope.of(context).requestFocus(FocusNode());

      Status status = await passwordReminder(
        context,
        email: usernameEmail,
        interfaceLanguageCode: state.locale.languageCode,
      );

      if (status.statusIsOk) {
        showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('passwordReminder.responseOK'),
              type: SNACKBAR_TYPE_OK,
            ));
      }
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String message;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          default:
            if (message == null) {
              message = _error.errorMessage;
            } else {
              message = '$message\n${_error.errorMessage}';
            }
            break;
        }
      }
      if (message != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on NotConnectedException catch (_) {
      bool _hasConnection = await deviceHasConnection();
      String message = _hasConnection
          ? '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}'
          : '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      setState(() => _isButtonLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    _isButtonEnabled = _isEmailValid != null && _isEmailValid;

    InputBorder _textFormFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: themeData.dividerColor,
        width: 1.0,
      ),
    );

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorWhite : colorLightThemeGrey,
    );

    Widget _getListView() => Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/user-bg.png'),
                      fit: BoxFit.fill)),
              child: Container(
                width: getDeviceWidth(context),
                height: getDeviceHeight(context) * 0.2,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SvgPicture.asset('assets/images/logo.svg'),
                    SecondaryButton(
                      caption: localizations.translate('shared.login'),
                      buttonTextColor: colorWhite,
                      onPressed: () => Navigator.pushNamedAndRemoveUntil(
                          context,
                          RouteConstants.LOGIN,
                          ModalRoute.withName(RouteConstants.START)),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                color: themeData.backgroundColor,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      children: [
                        Padding(
                          padding: const EdgeInsetsDirectional.only(
                            top: 45,
                            bottom: 15,
                          ),
                          child: Center(
                            child: Text(
                              localizations.translate('shared.forgotPassword'),
                              style: themeData.textTheme.headline6
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Text(
                            localizations
                                .translate('passwordReminder.subtitle'),
                            textAlign: TextAlign.center,
                            style: themeData.textTheme.headline5,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                        ),
                        HeadlineText(
                          text: localizations.translate('shared.email'),
                          textStyle: _headLineTextStyle,
                        ),
                        TextFormField(
                          focusNode: _emailFocusNode,
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          textCapitalization: TextCapitalization.none,
                          style: themeData.textTheme.bodyText2
                              .copyWith(fontStyle: FontStyle.normal),
                          decoration: InputDecoration(
                            hintMaxLines: 2,
                            errorMaxLines: 3,
                            enabledBorder: _textFormFieldBorder,
                          ),
                          validator: _onEmailValidate,
                          onFieldSubmitted: _onEmailSubmit,
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 20.0),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: MainButton(
                            caption: localizations.translate('shared.send'),
                            isLoading: _isButtonLoading,
                            isEnabled: !_isButtonLoading && _isButtonEnabled,
                            onPressed: _submit,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHidden: true,
      bottomNavBarHidden: true,
      scaffoldBackgroundColor: colorBlue,
      body: _getListView(),
    );
  }
}
