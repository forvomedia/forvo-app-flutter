import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:forvo/common/notifiers/theme_changer.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/service/social_network/apple/apple_service.dart';
import 'package:forvo/service/social_network/facebook/facebook_service.dart';
import 'package:forvo/service/social_network/google/google_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/registration/widgets/accent_location_selector.dart';
import 'package:forvo/ui/registration/widgets/apple_button.dart';
import 'package:forvo/ui/registration/widgets/birthdate_selector.dart';
import 'package:forvo/ui/registration/widgets/facebook_button.dart';
import 'package:forvo/ui/registration/widgets/gender_selector.dart';
import 'package:forvo/ui/registration/widgets/google_button.dart';
import 'package:forvo/ui/registration/widgets/language_selector.dart';
import 'package:forvo/ui/registration/widgets/terms_privacy_checkbox.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/menu/language_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text_form_field/password_field.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/form_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/notification_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      GlobalKey<FormFieldState<String>>();
  final TextEditingController _repeatedPasswordController =
      TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final GlobalKey<FormFieldState<String>> _repeatedPasswordFieldKey =
      GlobalKey<FormFieldState<String>>();

  FocusNode _usernameFocusNode = FocusNode(debugLabel: 'username');
  FocusNode _emailFocusNode = FocusNode(debugLabel: 'email');
  FocusNode _passwordFocusNode = FocusNode(debugLabel: 'password');
  FocusNode _repeatedPasswordFocusNode = FocusNode(debugLabel: 'repassword');
  FocusNode _firstNameFocusNode = FocusNode(debugLabel: 'firstName');
  FocusNode _lastNameFocusNode = FocusNode(debugLabel: 'lastName');
  FocusNode _languageAccentSelectorFocusNode =
      FocusNode(debugLabel: 'languageAccentSelector');
  FocusNode _accentLocalizationSelectorFocusNode =
      FocusNode(debugLabel: 'accentLocalizationSelector');
  FocusNode _genderSelectorFocusNode = FocusNode(debugLabel: 'genderSelector');
  FocusNode _birthdateSelectorFocusNode =
      FocusNode(debugLabel: 'birthdateSelector');

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  Timer _debounce;
  int _debounceTime = 2000;

  bool _checkNullValues = false;

  bool _isSocialNetworkCheckLoading = false;
  bool _isButtonEnabled = false, _isButtonLoading = false;
  bool _isUsernameCheckLoading = false, _isEmailCheckLoading = false;
  String _lastSearchedUsername, _lastSearchedEmail;
  bool _isUsernameValid,
      _isEmailValid,
      _isPasswordValid,
      _isRepeatedPasswordValid,
      _isFirstNameValid,
      _isLastNameValid;
  bool _isAccentLocalizationValid = true;
  String _usernameErrorMessage, _emailErrorMessage;
  String _gender;
  DateTime _birthdate;
  bool _userSelectedBirthdate = false;
  Language _selectedNativeLanguage;
  bool _locationPermissionGranted;
  bool _locationPermissionRequested = false;
  Location location = Location();
  String _accentLocation;
  LocationData _currentPosition;
  bool _serviceEnabled;
  TapGestureRecognizer _privacyTapRecognizer;
  TapGestureRecognizer _termsTapRecognizer;
  bool _acceptTerms;
  Completer<GoogleMapController> _mapControllerCompleter = Completer();
  GoogleMapController _mapController;
  LatLng _cameraPosition =
      LatLng(LOCATION_DEFAULT_MAP_LATITUDE, LOCATION_DEFAULT_MAP_LONGITUDE);
  Set<Marker> _markers = {};

  @override
  void initState() {
    super.initState();
    _acceptTerms = false;
    _usernameController.addListener(_onUsernameChanged);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _repeatedPasswordController.addListener(_onRepeatedPasswordChanged);
    _firstNameController.addListener(_onFirstNameChanged);
    _lastNameController.addListener(_onLastNameChanged);
    _privacyTapRecognizer = TapGestureRecognizer()
      ..onTap = () => _launchURL(PRIVACY_POLICY_URL);
    _termsTapRecognizer = TapGestureRecognizer()
      ..onTap = () => _launchURL(TERMS_OF_USE_URL);
    _locationPermissionStatus();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _locationPermissionStatus();
  }

  @override
  void dispose() {
    _usernameController.removeListener(_onUsernameChanged);
    _usernameController.dispose();
    _usernameFocusNode.dispose();
    _emailController.removeListener(_onEmailChanged);
    _emailController.dispose();
    _emailFocusNode.dispose();
    _passwordController.removeListener(_onPasswordChanged);
    _passwordController.dispose();
    _passwordFocusNode.dispose();
    _repeatedPasswordController.removeListener(_onRepeatedPasswordChanged);
    _repeatedPasswordController.dispose();
    _repeatedPasswordFocusNode.dispose();
    _firstNameController.removeListener(_onFirstNameChanged);
    _firstNameController.dispose();
    _firstNameFocusNode.dispose();
    _lastNameController.removeListener(_onLastNameChanged);
    _lastNameController.dispose();
    _lastNameFocusNode.dispose();
    _languageAccentSelectorFocusNode.dispose();
    _accentLocalizationSelectorFocusNode.dispose();
    _genderSelectorFocusNode.dispose();
    _birthdateSelectorFocusNode.dispose();
    _privacyTapRecognizer.dispose();
    _termsTapRecognizer.dispose();
    if (_mapController != null) {
      _mapController.dispose();
    }
    super.dispose();
  }

  _onUsernameChanged() {
    setState(() {
      _usernameErrorMessage = null;
    });
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkUsername);
  }

  _onEmailChanged() {
    setState(() {
      _emailErrorMessage = null;
    });
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkEmail);
  }

  _onPasswordChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkPassword);
  }

  _onRepeatedPasswordChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce =
        Timer(Duration(milliseconds: _debounceTime), _checkRepeatedPassword);
  }

  _onFirstNameChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkFirstName);
  }

  _onLastNameChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), _checkLastName);
  }

  _checkUsername() async {
    if (mounted) {
      if (_usernameController.text.isEmpty) {
        _lastSearchedUsername = _usernameController.text;
        if (!_formKey.currentState.validate()) {
          setState(() {
            _isUsernameValid = false;
          });
        }
      } else {
        if (_lastSearchedUsername == null ||
            _lastSearchedUsername != _usernameController.text) {
          await _signupCheckUsername();
        }
      }
    }
  }

  _signupCheckUsername() async {
    if (mounted && !_isUsernameCheckLoading) {
      var state = AppStateManager.of(context).state;
      setState(() {
        _lastSearchedUsername = _usernameController.text;
        _isUsernameCheckLoading = true;
      });
      _formKey.currentState.validate();
      try {
        await signupCheckUsername(
          context,
          username: _usernameController.text,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {
          _isUsernameCheckLoading = false;
          _isUsernameValid = true;
          _usernameErrorMessage = null;
        });
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        setState(() {
          _usernameErrorMessage = messages;
          _isUsernameValid = false;
        });
      } on Exception catch (_) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            AppLocalizations.of(context).translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        if (mounted) {
          setState(() {
            _isUsernameCheckLoading = false;
          });
          _formKey.currentState.validate();
        }
      }
    }
  }

  String _onUsernameValidate(String username) {
    var localizations = AppLocalizations.of(context);
    if (username == null || username.isEmpty) {
      return localizations.translate('error.signup.emptyUsername');
    } else {
      if (_usernameErrorMessage != null && _usernameErrorMessage.isNotEmpty) {
        return _usernameErrorMessage;
      }
      return null;
    }
  }

  _onUsernameSubmit(String username) async {
    await _checkUsername();
    if (_isUsernameValid) {
      fieldFocusChange(
        context,
        _usernameFocusNode,
        _emailFocusNode,
      );
    }
  }

  _checkEmail() async {
    if (mounted) {
      if (_emailController.text.isEmpty) {
        _lastSearchedEmail = _emailController.text;
        if (!_formKey.currentState.validate()) {
          setState(() {
            _isEmailValid = false;
          });
        }
      } else {
        if (_lastSearchedEmail == null ||
            _lastSearchedEmail != _emailController.text) {
          await _signupCheckEmail();
        }
      }
    }
  }

  _signupCheckEmail() async {
    if (mounted && !_isEmailCheckLoading) {
      var state = AppStateManager.of(context).state;
      var localizations = AppLocalizations.of(context);
      setState(() {
        _lastSearchedEmail = _emailController.text;
        _isEmailCheckLoading = true;
      });
      _formKey.currentState.validate();
      try {
        await signupCheckEmail(
          context,
          email: _emailController.text,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {
          _isEmailCheckLoading = false;
          _isEmailValid = true;
          _emailErrorMessage = null;
        });
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        setState(() {
          _emailErrorMessage = messages;
          _isEmailValid = false;
        });
      } on Exception catch (_) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        if (mounted) {
          setState(() {
            _isEmailCheckLoading = false;
          });
          _formKey.currentState.validate();
        }
      }
    }
  }

  String _onEmailValidate(String email) {
    var localizations = AppLocalizations.of(context);
    if (email == null || email.isEmpty) {
      return localizations.translate('error.shared.emptyEmail');
    } else {
      if (_emailErrorMessage != null && _emailErrorMessage.isNotEmpty) {
        return _emailErrorMessage;
      }
      return null;
    }
  }

  _onEmailSubmit(String email) async {
    await _checkEmail();
    if (_isEmailValid) {
      fieldFocusChange(
        context,
        _emailFocusNode,
        _passwordFocusNode,
      );
    }
  }

  _checkPassword() async {
    if (mounted) {
      bool isPasswordValid =
          _onPasswordValidate(_passwordController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isPasswordValid = isPasswordValid;
      });
    }
  }

  String _onPasswordValidate(String password) {
    var localizations = AppLocalizations.of(context);
    if (password == null || password.isEmpty) {
      return localizations.translate('error.shared.emptyPassword');
    } else {
      if (password.length < MIN_PASSWORD_LIMIT) {
        return localizations.translate('error.shared.passwordTooShort');
      }
      if (password.length > MAX_PASSWORD_LIMIT) {
        return localizations.translate('error.shared.passwordTooLong');
      }
      if (password == _emailController.text) {
        return localizations.translate('error.shared.passwordEmail');
      }
      return null;
    }
  }

  _onPasswordSubmit(String password) async {
    await _checkPassword();
    if (_isPasswordValid) {
      fieldFocusChange(
        context,
        _passwordFocusNode,
        _repeatedPasswordFocusNode,
      );
    }
  }

  _checkRepeatedPassword() async {
    if (mounted) {
      bool isRepeatedPasswordValid = _formKey.currentState.validate();
      setState(() {
        _isRepeatedPasswordValid = isRepeatedPasswordValid;
      });
    }
  }

  String _onRepeatedPasswordValidate(String repeatedPassword) {
    var localizations = AppLocalizations.of(context);
    if (repeatedPassword == null || repeatedPassword.isEmpty) {
      return localizations.translate('error.shared.emptyPassword');
    } else if (repeatedPassword != _passwordController.text) {
      return localizations.translate('error.shared.passwordsDoNotMatch');
    } else {
      return null;
    }
  }

  _onRepeatedPasswordSubmit(String repeatPassword) async {
    await _checkRepeatedPassword();
    if (_isRepeatedPasswordValid) {
      fieldFocusChange(
        context,
        _repeatedPasswordFocusNode,
        _firstNameFocusNode,
      );
    }
  }

  _checkFirstName() async {
    if (mounted) {
      bool isFirstNameValid =
          _onFirstNameValidate(_firstNameController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isFirstNameValid = isFirstNameValid;
      });
    }
  }

  String _onFirstNameValidate(String firstName) {
    var localizations = AppLocalizations.of(context);
    if (firstName == null || firstName.isEmpty) {
      return localizations.translate('error.shared.emptyFirstName');
    } else {
      return null;
    }
  }

  _onFirstNameSubmit(String firstName) async {
    await _checkFirstName();
    if (_isFirstNameValid) {
      fieldFocusChange(context, _firstNameFocusNode, _lastNameFocusNode);
    }
  }

  _checkLastName() async {
    if (mounted) {
      bool isLastNameValid =
          _onLastNameValidate(_lastNameController.text) == null;
      _formKey.currentState.validate();
      setState(() {
        _isLastNameValid = isLastNameValid;
      });
    }
  }

  String _onLastNameValidate(String lastName) {
    var localizations = AppLocalizations.of(context);
    if (lastName == null || lastName.isEmpty) {
      return localizations.translate('error.shared.emptyLastName');
    } else {
      return null;
    }
  }

  _onLastNameSubmit(String lastName) async {
    await _checkLastName();
    if (_isLastNameValid) {
      fieldFocusChange(
          context, _lastNameFocusNode, _languageAccentSelectorFocusNode);
    }
  }

  _socialNetWorkSignup(String socialNetwork) async {
    var localizations = AppLocalizations.of(context);
    try {
      setState(() {
        _isSocialNetworkCheckLoading = true;
      });

      String response;
      switch (socialNetwork) {
        case SocialNetworkConstants.FACEBOOK:
          response = await loginFacebook(context);
          break;
        case SocialNetworkConstants.GOOGLE:
          response = await loginGoogle(context);
          break;
        case SocialNetworkConstants.APPLE:
          response = await loginApple(context);
          break;
        default:
          response = SocialNetworkConstants.REQUEST_ERROR;
          break;
      }

      if (response != null) {
        switch (response) {
          case SocialNetworkConstants.REQUEST_OK:
            _postSocialNetworkSignUp();
            break;
          case SocialNetworkConstants.REQUEST_ERROR:
            break;
          case SocialNetworkConstants.REQUEST_CANCELLED:
            break;
        }
      }
    } on ApiException catch (error) {
      String messages = error.getErrorMessages(context);
      if (messages != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        deleteUserSession();
      }
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      deleteUserSession();
    } finally {
      setState(() {
        _isSocialNetworkCheckLoading = false;
      });
    }
  }

  _postSocialNetworkSignUp() async {
    String languageCode = await getPreferredLanguageCode();
    AppStateManager.of(context).onLocaleChanged(languageCode);

    ThemeChanger _themeChanger =
        Provider.of<ThemeChanger>(context, listen: false);
    ThemeData _lightTheme = await getUserLightTheme();
    ThemeData _darkTheme = await getUserDarkTheme();
    _themeChanger.setTheme(_lightTheme, _darkTheme);

    await subscribePushNotifications(context, languageCode);

    Navigator.of(context).pushNamedAndRemoveUntil(
      RouteConstants.HOME,
      (Route<dynamic> route) => false,
    );
  }

  _locationPermissionStatus() async {
    _locationPermissionRequested = await isLocationPermissionRequested();
    if (_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.isGranted;
    }
    setState(() {});
  }

  _getCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    await _locationPermissionStatus();

    if (!_locationPermissionRequested) {
      _locationPermissionGranted =
          await permission_handler.Permission.location.request().isGranted;
      setLocationPermissionRequested();
      setState(() {
        _locationPermissionRequested = true;
      });
    }

    if (_locationPermissionGranted == null || !_locationPermissionGranted) {
      return;
    }

    _currentPosition = await location.getLocation();

    final String markerIdVal =
        'marker_id_${_currentPosition.latitude}_${_currentPosition.longitude}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);
    setState(() {
      _accentLocation = 'Latitude: ${_currentPosition.latitude}, '
          'Longitude: ${_currentPosition.longitude}';
      _cameraPosition =
          LatLng(_currentPosition.latitude, _currentPosition.longitude);
      _isAccentLocalizationValid = true;
    });

    _mapController = await _mapControllerCompleter.future;
    _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: _cameraPosition,
          zoom: 12,
        ),
      ),
    );
  }

  _onTapMarker() {
    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _onTapMap(LatLng position) {
    final String markerIdVal =
        'marker_id_${position.latitude}_${position.longitude}';
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(position.latitude, position.longitude),
      onTap: _onTapMarker,
    );

    _markers.clear();
    _markers.add(marker);

    setState(() {
      _accentLocation = 'Latitude: ${position.latitude}, '
          'Longitude: ${position.longitude}';
      _cameraPosition = LatLng(position.latitude, position.longitude);
      _isAccentLocalizationValid = true;
    });

    fieldFocusChange(
      context,
      _accentLocalizationSelectorFocusNode,
      _genderSelectorFocusNode,
    );
  }

  _checkFormParams() {
    _checkUsername();
    FocusNode firstNullValueFocusNode;
    firstNullValueFocusNode = _birthdate == null || !_userSelectedBirthdate
        ? _birthdateSelectorFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode =
        _gender == null ? _genderSelectorFocusNode : firstNullValueFocusNode;
    if (_markers.isEmpty) {
      _isAccentLocalizationValid = false;
      firstNullValueFocusNode = _accentLocalizationSelectorFocusNode;
    }
    firstNullValueFocusNode = _selectedNativeLanguage == null
        ? _languageAccentSelectorFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isLastNameValid == null || !_isLastNameValid
        ? _lastNameFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isFirstNameValid == null || !_isFirstNameValid
        ? _firstNameFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode =
        _isRepeatedPasswordValid == null || !_isRepeatedPasswordValid
            ? _repeatedPasswordFocusNode
            : firstNullValueFocusNode;
    firstNullValueFocusNode = _isPasswordValid == null || !_isPasswordValid
        ? _passwordFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isEmailValid == null || !_isEmailValid
        ? _emailFocusNode
        : firstNullValueFocusNode;
    firstNullValueFocusNode = _isUsernameValid == null || !_isUsernameValid
        ? _usernameFocusNode
        : firstNullValueFocusNode;
    if (firstNullValueFocusNode != null) {
      fieldFocusChange(
          context, firstNullValueFocusNode, firstNullValueFocusNode);
    }
  }

  _signup() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    String languageCode = state.locale.languageCode;

    setState(() => _isButtonLoading = true);

    try {
      String _ip = await getIP();
      await signup(
        context,
        username: _usernameController.text,
        email: _emailController.text,
        password: _passwordController.text,
        repassword: _repeatedPasswordController.text,
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        gender: _gender,
        languageCode: _selectedNativeLanguage.code,
        latitude: _cameraPosition.latitude.toString(),
        longitude: _cameraPosition.longitude.toString(),
        ip: _ip,
        birthdateDay: getDefaultFormattedDayFromDate(_birthdate),
        birthdateMonth: getDefaultFormattedMonthFromDate(_birthdate),
        birthdateYear: getDefaultFormattedYearFromDate(_birthdate),
        interfaceLanguageCode: languageCode,
      );

      Map eventValues = {
        APPSFLYER_AF_REGISTRATION_METHOD: APPSFLYER_REGISTRATION_METHOD_DEFAULT,
      };
      appsflyerEventLog(APPSFLYER_AF_COMPLETE_REGISTRATION, eventValues);

      Navigator.pushNamedAndRemoveUntil(
        context,
        RouteConstants.HOME,
        (route) => false,
      );

      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          localizations.translate('signup.activationEmailSent'),
          duration: Duration(seconds: 10),
          type: SNACKBAR_TYPE_OK,
        ),
      );
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String messages;
      for (ApiError _error in errors) {
        switch (_error.errorApiCode) {
          case ApiErrorCodeConstants.ERROR_1073:
            setState(() {
              _isAccentLocalizationValid = false;
            });
            String errorMessage =
                localizations.translate('error.api.${_error.errorApiCode}');
            if (messages == null) {
              messages = errorMessage;
            } else {
              messages = '$messages\n$errorMessage';
            }
            break;
          default:
            String errorMessage =
                localizations.translate('error.api.${_error.errorApiCode}');
            if (messages == null) {
              messages = errorMessage;
            } else {
              messages = '$messages\n$errorMessage';
            }
            break;
        }
      }
      if (messages != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        deleteUserSession();
      }
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      setState(() => _isButtonLoading = false);
    }
  }

  void _launchURL(url) async {
    if (!await launch(url, forceWebView: true)) {
      var localizations = AppLocalizations.of(context);
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    _isButtonEnabled = _isUsernameValid != null &&
        _isUsernameValid &&
        _isEmailValid != null &&
        _isEmailValid &&
        _isPasswordValid != null &&
        _isPasswordValid &&
        _isRepeatedPasswordValid != null &&
        _isRepeatedPasswordValid &&
        _isFirstNameValid != null &&
        _isFirstNameValid &&
        _isLastNameValid != null &&
        _isLastNameValid &&
        _selectedNativeLanguage != null &&
        _isAccentLocalizationValid != null &&
        _isAccentLocalizationValid &&
        _gender != null &&
        _birthdate != null &&
        _userSelectedBirthdate &&
        _cameraPosition != null &&
        _cameraPosition.latitude != null &&
        _cameraPosition.latitude != LOCATION_DEFAULT_MAP_LATITUDE &&
        _cameraPosition.longitude != null &&
        _cameraPosition.longitude != LOCATION_DEFAULT_MAP_LONGITUDE &&
        _acceptTerms;

    _onNativeLanguageMenuOpen() {
      _languageAccentSelectorFocusNode.requestFocus();
      onLanguageMenuOpen(
        context,
        languages: state.languages,
        popularLanguages: state.popularLanguages,
        onChangeLanguage: (Language language) {
          setState(() {
            _selectedNativeLanguage = language;
          });
          Navigator.pop(context);
          fieldFocusChange(
            context,
            _languageAccentSelectorFocusNode,
            _accentLocalizationSelectorFocusNode,
          );
        },
      );
    }

    _onBirthdateSelect() async {
      _birthdateSelectorFocusNode.requestFocus();
      int maximumValidYear = DateTime.now().year - DATE_PICKER_YEARS_13;
      int maxValidMonth = 12;
      int maxValidDay = 31;
      DateTime previousBirthdate;
      if (_birthdate == null) {
        _birthdate = DateTime(maximumValidYear, maxValidMonth, maxValidDay);
      } else {
        previousBirthdate = _birthdate;
      }

      DateTime newBirthdate = await showDatePicker(
        context: context,
        locale: state.locale,
        initialDate: _birthdate,
        firstDate: DateTime(DATE_PICKER_INITIAL_YEAR),
        lastDate: DateTime(maximumValidYear, maxValidMonth, maxValidDay),
      );

      if (newBirthdate != null) {
        setState(() {
          _birthdate = newBirthdate;
          _userSelectedBirthdate = true;
        });
      } else {
        setState(() {
          _birthdate = previousBirthdate;
          _userSelectedBirthdate = previousBirthdate != null;
        });
      }
    }

    InputBorder _textFormFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: themeData.dividerColor,
        width: 1.0,
      ),
    );

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorWhite : colorLightThemeGrey,
    );

    bool _deviceIsIOS = !deviceIsAndroid();
    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceSignupActive;

    double socialNetworkIconSize = 18;
    double socialNetworkFontSize = 13;
    if (_deviceIsIOS) {
      socialNetworkIconSize = 18;
      socialNetworkFontSize = 18;
    } else {
      socialNetworkIconSize = 18;
      socialNetworkFontSize = 13;
      if (useSmallMobileLayout(context)) {
        socialNetworkIconSize = 16;
        socialNetworkFontSize = 12;
      }
    }

    TextAlign socialNetworkButtonTextAlign = TextAlign.center;
    double socialNetworkButtonTextContainerWidthPercentage;
    double socialNetworkButtonTextContainerVerticalPadding;
    if (_deviceIsIOS) {
      socialNetworkButtonTextAlign = TextAlign.start;
      socialNetworkButtonTextContainerWidthPercentage = 0.50;
      socialNetworkButtonTextContainerVerticalPadding = 14.0;
    } else {
      socialNetworkButtonTextContainerWidthPercentage = 0.24;
      socialNetworkButtonTextContainerVerticalPadding = 10.0;
    }

    Widget _getGoogleButton() => GoogleButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('signup.googleButton'),
          isTextInOneLine: _deviceIsIOS,
          textContainerWidthSize: getDeviceWidth(context) *
              socialNetworkButtonTextContainerWidthPercentage,
          textContainerVerticalPadding:
              socialNetworkButtonTextContainerVerticalPadding,
          textAlign: socialNetworkButtonTextAlign,
          iconSize: socialNetworkIconSize,
          fontSize: socialNetworkFontSize,
          onPressed: () async =>
              _socialNetWorkSignup(SocialNetworkConstants.GOOGLE),
        );

    Widget _getFacebookButton() => FacebookButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('signup.facebookButton'),
          isTextInOneLine: _deviceIsIOS,
          textContainerWidthSize: getDeviceWidth(context) *
              socialNetworkButtonTextContainerWidthPercentage,
          textContainerVerticalPadding:
              socialNetworkButtonTextContainerVerticalPadding,
          textAlign: socialNetworkButtonTextAlign,
          iconSize: socialNetworkIconSize,
          fontSize: socialNetworkFontSize,
          onPressed: () async =>
              _socialNetWorkSignup(SocialNetworkConstants.FACEBOOK),
        );

    Widget _getAppleButton() => AppleButton(
          isEnabled: !_isMaintenanceActive,
          text: localizations.translate('signup.appleButton'),
          onPressed: () async =>
              _socialNetWorkSignup(SocialNetworkConstants.APPLE),
        );

    Widget _getContent() => ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/user-bg.png'),
                      fit: BoxFit.fill)),
              child: Container(
                width: getDeviceWidth(context),
                height: getDeviceHeight(context) * 0.15,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SvgPicture.asset('assets/images/logo.svg'),
                    SecondaryButton(
                      caption: localizations.translate('shared.login'),
                      buttonTextColor: colorWhite,
                      onPressed: () => Navigator.pushNamedAndRemoveUntil(
                        context,
                        RouteConstants.LOGIN,
                        ModalRoute.withName(RouteConstants.START),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: themeData.backgroundColor,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsetsDirectional.only(top: 35, bottom: 10),
                      child: Center(
                        child: Text(
                          localizations.translate('signup.button'),
                          style: themeData.textTheme.headline6,
                        ),
                      ),
                    ),
                    _isMaintenanceActive
                        ? MaintenanceWarningMessage()
                        : Container(),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20.0),
                    ),
                    _deviceIsIOS
                        ? Column(
                            children: [
                              _getGoogleButton(),
                              Padding(padding: const EdgeInsets.all(4)),
                              _getFacebookButton(),
                              Padding(padding: const EdgeInsets.all(4)),
                              _getAppleButton(),
                            ],
                          )
                        : IntrinsicHeight(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Expanded(
                                  child: _getGoogleButton(),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(4),
                                ),
                                Expanded(
                                  child: _getFacebookButton(),
                                )
                              ],
                            ),
                          ),
                    Divider(
                      height: 28,
                      thickness: 2,
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.username'),
                      textStyle: _headLineTextStyle,
                    ),
                    TextFormField(
                      controller: _usernameController,
                      focusNode: _usernameFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      style: themeData.textTheme.bodyText2.copyWith(
                        fontStyle: FontStyle.normal,
                      ),
                      decoration: InputDecoration(
                        hintMaxLines: 2,
                        errorMaxLines: 3,
                        enabledBorder: _textFormFieldBorder,
                        suffixIcon: _isUsernameCheckLoading
                            ? Container(
                                height: 10.0,
                                width: 10.0,
                                margin: EdgeInsets.all(15.0),
                                child: CustomCircularProgressIndicator(
                                  isCentered: false,
                                ),
                              )
                            : _isUsernameValid == null
                                ? Container()
                                : _isUsernameValid
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      )
                                    : Icon(
                                        Icons.close,
                                        color: Colors.red,
                                      ),
                      ),
                      validator: _onUsernameValidate,
                      onFieldSubmitted: _onUsernameSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.email'),
                      textStyle: _headLineTextStyle,
                    ),
                    TextFormField(
                      focusNode: _emailFocusNode,
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                      textCapitalization: TextCapitalization.none,
                      style: themeData.textTheme.bodyText2
                          .copyWith(fontStyle: FontStyle.normal),
                      decoration: InputDecoration(
                        hintMaxLines: 2,
                        errorMaxLines: 3,
                        enabledBorder: _textFormFieldBorder,
                        suffixIcon: _isEmailCheckLoading
                            ? Container(
                                height: 10.0,
                                width: 10.0,
                                margin: EdgeInsets.all(15.0),
                                child: CustomCircularProgressIndicator(
                                  isCentered: false,
                                ),
                              )
                            : _isEmailValid == null
                                ? Container()
                                : _isEmailValid
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      )
                                    : Icon(
                                        Icons.close,
                                        color: Colors.red,
                                      ),
                      ),
                      validator: _onEmailValidate,
                      onFieldSubmitted: _onEmailSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.password'),
                      textStyle: _headLineTextStyle,
                    ),
                    PasswordField(
                      themeData: themeData.copyWith(
                        inputDecorationTheme: InputDecorationTheme(
                          enabledBorder: _textFormFieldBorder,
                          border: _textFormFieldBorder,
                        ),
                      ),
                      fieldKey: _passwordFieldKey,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      focusNode: _passwordFocusNode,
                      controller: _passwordController,
                      style: themeData.textTheme.bodyText2,
                      validator: _onPasswordValidate,
                      onFieldSubmitted: _onPasswordSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.repeatPassword'),
                      textStyle: _headLineTextStyle,
                    ),
                    PasswordField(
                      themeData: themeData.copyWith(
                        inputDecorationTheme: InputDecorationTheme(
                          enabledBorder: _textFormFieldBorder,
                          border: _textFormFieldBorder,
                        ),
                      ),
                      fieldKey: _repeatedPasswordFieldKey,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      focusNode: _repeatedPasswordFocusNode,
                      controller: _repeatedPasswordController,
                      style: themeData.textTheme.bodyText2,
                      validator: _onRepeatedPasswordValidate,
                      onFieldSubmitted: _onRepeatedPasswordSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.firstName'),
                      textStyle: _headLineTextStyle,
                    ),
                    TextFormField(
                      focusNode: _firstNameFocusNode,
                      controller: _firstNameController,
                      keyboardType: TextInputType.name,
                      textInputAction: TextInputAction.next,
                      textCapitalization: TextCapitalization.none,
                      style: themeData.textTheme.bodyText2
                          .copyWith(fontStyle: FontStyle.normal),
                      decoration: InputDecoration(
                        hintMaxLines: 2,
                        errorMaxLines: 3,
                        enabledBorder: _textFormFieldBorder,
                      ),
                      validator: _onFirstNameValidate,
                      onFieldSubmitted: _onFirstNameSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.lastName'),
                      textStyle: _headLineTextStyle,
                    ),
                    TextFormField(
                      focusNode: _lastNameFocusNode,
                      controller: _lastNameController,
                      keyboardType: TextInputType.name,
                      textInputAction: TextInputAction.next,
                      textCapitalization: TextCapitalization.none,
                      style: themeData.textTheme.bodyText2
                          .copyWith(fontStyle: FontStyle.normal),
                      decoration: InputDecoration(
                        hintMaxLines: 2,
                        errorMaxLines: 3,
                        enabledBorder: _textFormFieldBorder,
                      ),
                      validator: _onLastNameValidate,
                      onFieldSubmitted: _onLastNameSubmit,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.nativeLanguage'),
                      textStyle: _headLineTextStyle,
                    ),
                    LanguageSelector(
                      language: _selectedNativeLanguage,
                      onLanguageMenuOpen: _onNativeLanguageMenuOpen,
                      focusNode: _languageAccentSelectorFocusNode,
                      checkValue: _checkNullValues,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.accent'),
                      textStyle: _headLineTextStyle,
                    ),
                    AccentLocalizationSelector(
                      permissionGranted: _locationPermissionGranted,
                      permissionRequested: _locationPermissionRequested,
                      accentLocation: _accentLocation,
                      onGetCurrentLocation: _getCurrentLocation,
                      mapController: _mapControllerCompleter,
                      cameraPosition: _cameraPosition,
                      markers: _markers,
                      onTapMap: _onTapMap,
                      focusNode: _accentLocalizationSelectorFocusNode,
                      isValid: _isAccentLocalizationValid,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.gender.gender'),
                      textStyle: _headLineTextStyle,
                    ),
                    IntrinsicHeight(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            const Radius.circular(5.0),
                          ),
                          border: Border.all(
                            color: themeData.dividerColor,
                            width: 1.0,
                          ),
                        ),
                        child: GenderSelector(
                          focusNode: _genderSelectorFocusNode,
                          gender: _gender,
                          onTap: (String newGender) {
                            setState(() {
                              _gender = newGender;
                            });
                            fieldFocusChange(
                              context,
                              _genderSelectorFocusNode,
                              _birthdateSelectorFocusNode,
                            );
                          },
                          checkValue: _checkNullValues,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    HeadlineText(
                      text: localizations.translate('shared.birthdate'),
                      textStyle: _headLineTextStyle,
                    ),
                    BirthdateSelector(
                      birthdate: _birthdate,
                      onTap: _onBirthdateSelect,
                      focusNode: _birthdateSelectorFocusNode,
                      checkValue: _checkNullValues,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    Divider(
                      height: 28,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0, left: 10.0, right: 10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Icon(Icons.info_outlined),
                          ),
                          Expanded(
                            child: Text(
                              localizations
                                  .translate('shared.comboDataWarning'),
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    Divider(
                      height: 28,
                      thickness: 2,
                    ),
                    TermsPrivacyCheckbox(
                      acceptTerms: _acceptTerms,
                      onChanged: (bool value) {
                        if (value) {
                          _checkNullValues = true;
                          _checkFormParams();
                        }
                        setState(() => _acceptTerms = value);
                      },
                      termsTapRecognizer: _termsTapRecognizer,
                      privacyTapRecognizer: _privacyTapRecognizer,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    Divider(
                      height: 28,
                      thickness: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                    MainButton(
                      caption: localizations.translate('signup.button'),
                      onPressed: _signup,
                      isLoading: _isButtonLoading,
                      isEnabled: !_isMaintenanceActive &&
                          !_isUsernameCheckLoading &&
                          !_isButtonLoading &&
                          _isButtonEnabled,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHidden: true,
      bottomNavBarHidden: true,
      scaffoldBackgroundColor: colorBlue,
      body: Stack(
        children: [
          Positioned(
            child: _getContent(),
          ),
          _isSocialNetworkCheckLoading
              ? PositionedDirectional(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 10, color: Colors.transparent),
                      color: Colors.black.withOpacity(0.5),
                    ),
                    child: CustomCircularProgressIndicator(
                      valueColor: colorWhite,
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
