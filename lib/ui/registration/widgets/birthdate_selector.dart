import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/date_util.dart';

class BirthdateSelector extends StatelessWidget {
  final DateTime birthdate;
  final Function() onTap;
  final FocusNode focusNode;
  final bool checkValue;
  final bool showMessage;

  BirthdateSelector({
    @required this.birthdate,
    @required this.onTap,
    this.focusNode,
    this.checkValue,
    this.showMessage,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              const Radius.circular(5.0),
            ),
            border: Border.all(
              color: (checkValue != null && checkValue) ||
                      (focusNode != null && focusNode.hasFocus)
                  ? (birthdate == null ? Colors.red : colorBlue)
                  : themeData.dividerColor,
              width: 1.0,
            ),
          ),
          child: InkWell(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    birthdate == null
                        ? localizations
                            .translate('shared.birthdateSelectHintText')
                        : getDefaultFormattedDate(birthdate),
                  ),
                ],
              ),
            ),
            onTap: onTap,
          ),
        ),
        showMessage != null && showMessage
            ? Padding(
                padding:
                    const EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Icon(Icons.info_outlined),
                    ),
                    Expanded(
                      child: Text(
                        localizations.translate('shared.birthdateWarning'),
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              )
            : Container(),
      ],
    );
  }
}
