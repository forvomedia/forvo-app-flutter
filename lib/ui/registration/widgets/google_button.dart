import 'package:flutter/material.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class GoogleButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final double textContainerWidthSize;
  final double textContainerVerticalPadding;
  final TextAlign textAlign;
  final double iconSize;
  final double fontSize;
  final bool isTextInOneLine;
  final bool isEnabled;

  GoogleButton({
    @required this.onPressed,
    @required this.text,
    @required this.textContainerWidthSize,
    @required this.textContainerVerticalPadding,
    @required this.textAlign,
    @required this.iconSize,
    @required this.fontSize,
    this.isTextInOneLine,
    this.isEnabled = true,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);

    double disabledButtonTextOpacity = 0.6;
    Widget _textWidget = Text(
      text,
      maxLines: 3,
      textAlign: textAlign,
      style: themeData.textTheme.button.copyWith(
        color: isEnabled
            ? colorBlack
            : themeIsDark(context)
                ? colorDarkThemeGrey.withOpacity(disabledButtonTextOpacity)
                : colorLightBlack.withOpacity(disabledButtonTextOpacity),
        fontSize: fontSize,
      ),
    );

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: colorWhite,
        onSurface:
            themeIsDark(context) ? colorLightThemeGrey : colorDarkThemeGrey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            ForvoIcons.google,
            color: colorBlack,
            size: iconSize,
          ),
          isTextInOneLine
              ? Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 6.0,
                    vertical: textContainerVerticalPadding,
                  ),
                  child: _textWidget,
                )
              : Container(
                  padding: EdgeInsetsDirectional.only(
                    start: 10,
                    top: textContainerVerticalPadding,
                    bottom: textContainerVerticalPadding,
                  ),
                  width: textContainerWidthSize,
                  child: _textWidget,
                )
        ],
      ),
      onPressed: isEnabled ? onPressed : null,
    );
  }
}
