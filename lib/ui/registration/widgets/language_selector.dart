import 'package:flutter/material.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/theme_util.dart';

class LanguageSelector extends StatelessWidget {
  final Language language;
  final Function onLanguageMenuOpen;
  final FocusNode focusNode;
  final bool checkValue;

  LanguageSelector(
      {@required this.language,
      @required this.onLanguageMenuOpen,
      this.focusNode,
      this.checkValue});

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    return InkWell(
      focusNode: focusNode,
      onTap: onLanguageMenuOpen,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            const Radius.circular(5.0),
          ),
          border: Border.all(
            color: (checkValue != null && checkValue) ||
                    (focusNode != null && focusNode.hasFocus)
                ? (language == null ? Colors.red : colorBlue)
                : themeData.dividerColor,
            width: 1.0,
          ),
          color: themeIsDark(context) ? colorDarkThemeLightGrey : colorWhite,
        ),
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 10.0),
                child: language != null
                    ? Text(
                        getLanguageNameAndCode(language),
                      )
                    : Container(
                        child: Text(''),
                      ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: onLanguageMenuOpen,
            )
          ],
        ),
      ),
    );
  }
}
