import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/ui/colors.dart';

class TermsPrivacyCheckbox extends StatelessWidget {
  final bool acceptTerms;
  final Function(bool) onChanged;
  final TapGestureRecognizer termsTapRecognizer;
  final TapGestureRecognizer privacyTapRecognizer;

  TermsPrivacyCheckbox({
    @required this.acceptTerms,
    @required this.onChanged,
    @required this.termsTapRecognizer,
    @required this.privacyTapRecognizer,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Checkbox(
          value: acceptTerms,
          onChanged: onChanged,
          activeColor: colorBlue,
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 8,
            ),
            child: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                text: localizations
                    .translate('signup.termsAndPrivacy.initialText'),
                style: themeData.textTheme.bodyText2,
                children: <TextSpan>[
                  TextSpan(
                    text: localizations.translate('shared.about.termsOfUse'),
                    style: themeData.textTheme.headline3,
                    recognizer: termsTapRecognizer,
                  ),
                  TextSpan(
                    text: localizations.translate(
                      'signup.termsAndPrivacy.connectionText',
                    ),
                    style: themeData.textTheme.bodyText2,
                  ),
                  TextSpan(
                    text: localizations.translate(
                      'shared.about.privacyPolicy',
                    ),
                    style: themeData.textTheme.headline3,
                    recognizer: privacyTapRecognizer,
                  ),
                  TextSpan(
                    text: localizations
                        .translate('signup.termsAndPrivacy.finalText'),
                    style: themeData.textTheme.bodyText2,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
