import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/gender_constants.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class GenderSelector extends StatelessWidget {
  final String gender;
  final Function(String) onTap;
  final FocusNode focusNode;
  final bool checkValue;

  GenderSelector({
    @required this.gender,
    @required this.onTap,
    this.focusNode,
    this.checkValue,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          const Radius.circular(5.0),
        ),
        border: Border.all(
          color: (checkValue != null && checkValue) ||
                  (focusNode != null && focusNode.hasFocus)
              ? (gender == null ? Colors.red : colorBlue)
              : themeData.dividerColor,
          width: 1.0,
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: InkWell(
              focusNode: focusNode,
              onTap: () {
                onTap(GENDER_MALE);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(5.0),
                    bottomLeft: const Radius.circular(5.0),
                  ),
                  color: gender != null && gender == GENDER_MALE
                      ? colorBlue
                      : themeIsDark(context)
                          ? colorDarkThemeLightGrey
                          : colorWhite,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Text(
                    localizations.translate('shared.gender.male'),
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.bodyText2.copyWith(
                      color: themeIsDark(context)
                          ? colorWhite
                          : gender != null && gender == GENDER_MALE
                              ? colorWhite
                              : colorBlack,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              focusNode: focusNode,
              onTap: () {
                onTap(GENDER_FEMALE);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: const Radius.circular(5.0),
                    bottomRight: const Radius.circular(5.0),
                  ),
                  color: gender != null && gender == GENDER_FEMALE
                      ? colorBlue
                      : themeIsDark(context)
                          ? colorDarkThemeLightGrey
                          : colorWhite,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Text(
                    localizations.translate('shared.gender.female'),
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.bodyText2.copyWith(
                      color: themeIsDark(context)
                          ? colorWhite
                          : gender != null && gender == GENDER_FEMALE
                              ? colorWhite
                              : colorBlack,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
