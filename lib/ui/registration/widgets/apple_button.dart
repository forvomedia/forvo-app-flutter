import 'package:flutter/material.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AppleButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final bool isEnabled;

  AppleButton({
    @required this.onPressed,
    @required this.text,
    this.isEnabled = true,
  });

  @override
  Widget build(BuildContext context) => SignInWithAppleButton(
        text: text,
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        iconAlignment: IconAlignment.center,
        onPressed: isEnabled ? onPressed : null,
        style: isEnabled
            ? SignInWithAppleButtonStyle.black
            : SignInWithAppleButtonStyle.white,
      );
}
