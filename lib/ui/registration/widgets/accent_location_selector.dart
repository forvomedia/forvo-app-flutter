import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AccentLocalizationSelector extends StatelessWidget {
  final bool permissionGranted;
  final bool permissionRequested;
  final String accentLocation;
  final Function onGetCurrentLocation;
  final LatLng cameraPosition;
  final Completer<GoogleMapController> mapController;
  final Set<Marker> markers;
  final Function(LatLng) onTapMap;
  final FocusNode focusNode;
  final bool isValid;
  final String parentView;

  AccentLocalizationSelector({
    @required this.permissionGranted,
    @required this.permissionRequested,
    @required this.accentLocation,
    @required this.onGetCurrentLocation,
    this.cameraPosition,
    this.mapController,
    this.markers,
    this.onTapMap,
    this.focusNode,
    this.isValid,
    this.parentView,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;
    bool isUserContributor = state?.userInfo?.user?.isContributor ?? false;
    String accentLocationPermissionGranted;
    String accentLocationPermissionDenied;
    if (isUserContributor &&
        parentView != null &&
        parentView == LOCATION_SELECTOR_MESSAGE_ACCOUNT) {
      accentLocationPermissionGranted =
          'account.accentLocationPermissionGranted';
      accentLocationPermissionDenied = 'account.accentLocationPermissionDenied';
    } else {
      accentLocationPermissionGranted =
          'signup.accentLocationPermissionGranted';
      accentLocationPermissionDenied = 'signup.accentLocationPermissionDenied';
    }
    return InkWell(
      focusNode: focusNode,
      onTap: onGetCurrentLocation,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            const Radius.circular(5.0),
          ),
          border: Border.all(
            color: (isValid != null && !isValid)
                ? Colors.red
                : focusNode != null && focusNode.hasFocus
                    ? ((permissionGranted == null ||
                            !permissionGranted ||
                            cameraPosition == null ||
                            (cameraPosition != null &&
                                cameraPosition.latitude ==
                                    LOCATION_DEFAULT_MAP_LATITUDE &&
                                cameraPosition.longitude ==
                                    LOCATION_DEFAULT_MAP_LONGITUDE))
                        ? Colors.red
                        : colorBlue)
                    : themeData.dividerColor,
            width: 1.0,
          ),
          color: themeIsDark(context) ? colorDarkThemeLightGrey : colorWhite,
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 10.0),
                    child: permissionRequested != null &&
                            permissionRequested &&
                            permissionGranted != null &&
                            !permissionGranted
                        ? Container(
                            child: Text(
                              localizations
                                  .translate(accentLocationPermissionDenied),
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.justify,
                            ),
                          )
                        : Container(
                            child: Text(
                              localizations
                                  .translate(accentLocationPermissionGranted),
                              style: TextStyle(fontSize: 15),
                              textAlign: TextAlign.justify,
                            ),
                          ),
                  ),
                ),
                IconButton(
                  icon: Icon(permissionGranted != null && permissionGranted
                      ? Icons.my_location
                      : permissionRequested != null && permissionRequested
                          ? Icons.perm_device_information
                          : Icons.perm_device_information),
                  onPressed: onGetCurrentLocation,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 2.5,
                    width: MediaQuery.of(context).size.width,
                    child: GoogleMap(
                      initialCameraPosition: CameraPosition(
                          target: cameraPosition == null ||
                                  (cameraPosition != null &&
                                      cameraPosition.latitude == null &&
                                      cameraPosition.longitude == null)
                              ? LatLng(LOCATION_DEFAULT_MAP_LATITUDE,
                                  LOCATION_DEFAULT_MAP_LONGITUDE)
                              : cameraPosition,
                          zoom: accentLocation != null ? 15 : 0),
                      mapType: MapType.normal,
                      markers: markers,
                      onMapCreated: (GoogleMapController controller) {
                        mapController.complete(controller);
                      },
                      myLocationEnabled: permissionGranted ?? false,
                      onTap: onTapMap,
                      gestureRecognizers: {
                        Factory<OneSequenceGestureRecognizer>(
                          () => EagerGestureRecognizer(),
                        ),
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 6.0),
                    child: Text(
                      localizations.translate(
                        'signup.location',
                        params: cameraPosition == null ||
                                (cameraPosition != null &&
                                    cameraPosition.latitude == null &&
                                    cameraPosition.longitude == null)
                            ? {
                                'latitude': '-',
                                'longitude': '-',
                              }
                            : {
                                'latitude': cameraPosition.latitude,
                                'longitude': cameraPosition.longitude,
                              },
                      ),
                      style: themeData.textTheme.bodyText2.copyWith(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  !isValid
                      ? Padding(
                          padding: const EdgeInsets.only(top: 12.0, left: 6.0),
                          child: Text(
                            localizations.translate('error.api.1073'),
                            style: themeData.textTheme.bodyText2.copyWith(
                              fontSize: 16,
                              color: Colors.red,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
