import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:forvo/common/notifiers/theme_changer.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/service/api/device_api_service.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/notifiers/chat_connection_change_notifier.dart';
import 'package:forvo/ui/notifiers/locale_change_notifier.dart';
import 'package:forvo/ui/splash.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/app_version_util.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/crypto_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/messages_util.dart';
import 'package:forvo/util/notification_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:provider/provider.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  FirebaseMessaging _firebaseMessaging;
  String _firebaseToken;
  AppState _state;
  bool _isUserLogged;
  String _languageCode;

  @override
  void initState() {
    _setOrientation();
    _loadAppState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_state == null) {
      return Container();
    }

    final theme = Provider.of<ThemeChanger>(context);
    return MaterialApp(
      scaffoldMessengerKey: appGlobals.scaffoldMessengerKey,
      builder: (context, child) {
        _configureFirebaseNotifications();
        var options = MediaQuery.of(context);

        themeIsDark(context)
            ? SystemChrome.setSystemUIOverlayStyle(
                SystemUiOverlayStyle(
                  // Only honored in Android M and above
                  statusBarColor: Colors.black,
                  // Only honored in Android M and above
                  statusBarIconBrightness: Brightness.light,
                  // Only honored in iOS
                  statusBarBrightness: Brightness.dark,
                ),
              )
            : SystemChrome.setSystemUIOverlayStyle(
                SystemUiOverlayStyle(
                  // Only honored in Android M and above
                  statusBarColor: Colors.grey,
                  // Only honored in Android M and above
                  statusBarIconBrightness: Brightness.light,
                  // Only honored in iOS
                  statusBarBrightness: Brightness.light,
                ),
              );

        return MediaQuery(
          data: options,
          child: AppStateManager(
            state: _state,
            onStateChanged: _onStateChanged,
            onLocaleChanged: _onLocaleChanged,
            child: Scaffold(
              key: appGlobals.scaffoldKey,
              body: Center(
                child: child,
              ),
            ),
          ),
        );
      },
      theme: theme.getLightTheme(),
      darkTheme: theme.getDarkTheme(),
      home: SplashScreen(),
      routes: getRoutes(context),
      localizationsDelegates: [
        AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        DefaultMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        DefaultWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate,
      ],
      supportedLocales: LanguageConstants.SUPPORTED_LANGUAGE_CODES.map(
        (lang) => Locale(
          lang,
          getCountryCodeFromLanguage(lang),
        ),
      ),
      locale: _state.locale,
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context).translate('app.title'),
      navigatorKey: appGlobals.navigatorKey,
    );
  }

  void _onLocaleChanged(String newLanguageCode) async {
    var newLocale = Locale(
      newLanguageCode,
      getCountryCodeFromLanguage(newLanguageCode),
    );
    saveUserLanguageCode(newLanguageCode);
    _onStateChanged(AppState(locale: newLocale));
    _state.localeNotifier.locale = newLocale;
  }

  void _onStateChanged(AppState state) {
    _state.merge(state);
    if (mounted) {
      setState(() {});
    }
  }

  void _setOrientation() async {
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  void _deleteUserSession(Locale locale) {
    bool _isUserLogged = false;
    _onStateChanged(
      AppState(
        locale: locale,
        localeNotifier: LocaleChangeNotifier(),
        chatConnectionChangeNotifier: ChatConnectionChangeNotifier(),
        isUserLogged: _isUserLogged,
        isSocialNetworkLogin: _isUserLogged,
        userInfo: null,
      ),
    );
    deleteUserSession();
  }

  void _loadAppState() async {
    var data = await Future.wait([
      isUserLogged(),
      getPreferredLanguageCode(),
    ]);
    bool _isSocialNetworkLogin = false;
    _isUserLogged = data[0];
    _languageCode = data[1];
    var locale = Locale(
      _languageCode,
      getCountryCodeFromLanguage(_languageCode),
    );

    setState(() {
      _state = AppState(
        locale: locale,
        localeNotifier: LocaleChangeNotifier(),
        chatConnectionChangeNotifier: ChatConnectionChangeNotifier(),
        isUserLogged: _isUserLogged,
        isSocialNetworkLogin: _isSocialNetworkLogin,
      );
    });

    if (_isUserLogged) {
      var userData = await Future.wait([
        getUserLoginCredential(),
        getUserEncryptedPassword(),
        getUserSocialNetwork(),
        getUserSocialNetworkId(),
      ]);

      String userLoginCredential = userData[0];
      String encryptedPassword = userData[1];
      String socialNetwork = userData[2];
      String socialNetworkId = userData[3];

      UserInfo userInfo;
      if ((userLoginCredential != null &&
              userLoginCredential.isNotEmpty &&
              encryptedPassword != null &&
              encryptedPassword.isNotEmpty) ||
          (userLoginCredential != null &&
              userLoginCredential.isNotEmpty &&
              socialNetwork != null &&
              socialNetwork.isNotEmpty &&
              socialNetworkId != null &&
              socialNetworkId.isNotEmpty)) {
        try {
          _isSocialNetworkLogin = false;
          if (encryptedPassword != null) {
            String decryptedPassword = decryptString(encryptedPassword);
            String md5Password = generateMd5(decryptedPassword);
            userInfo = await login(
              context,
              userLoginCredential,
              decryptedPassword,
              _languageCode,
              manualLogin: false,
            );
            userInfo.user.setMd5Password(md5Password);
          } else {
            userInfo = await socialNetworkLogin(
              context,
              socialNetwork,
              socialNetworkId,
              userLoginCredential,
              _languageCode,
              manualLogin: false,
            );
            _isSocialNetworkLogin = userInfo != null;
          }

          _isUserLogged = userInfo != null;
          _onStateChanged(
            AppState(
              isUserLogged: _isUserLogged,
              isSocialNetworkLogin: _isSocialNetworkLogin,
              userInfo: userInfo,
            ),
          );
          if (_isUserLogged) {
            getMessagesAfterLogin(
              userInfo.user.username,
              userInfo.user.md5Password,
            );
          }
        } on ApiException catch (error) {
          List<ApiError> errors = error.getErrors();
          String message;
          for (ApiError _error in errors) {
            switch (_error.errorApiCode) {
              case ApiErrorCodeConstants.ERROR_1059:
                _isSocialNetworkLogin = false;
                if (encryptedPassword != null) {
                  String decryptedPassword = decryptString(encryptedPassword);
                  String md5Password = generateMd5(decryptedPassword);
                  userInfo = await login(
                    context,
                    userLoginCredential,
                    decryptedPassword,
                    _languageCode,
                    manualLogin: false,
                  );
                  userInfo.user.setMd5Password(md5Password);
                } else {
                  userInfo = await socialNetworkLogin(
                    context,
                    socialNetwork,
                    socialNetworkId,
                    userLoginCredential,
                    _languageCode,
                    manualLogin: false,
                  );
                  _isSocialNetworkLogin = userInfo != null;
                }
                _isUserLogged = userInfo != null;
                _onStateChanged(
                  AppState(
                    isUserLogged: _isUserLogged,
                    isSocialNetworkLogin: _isSocialNetworkLogin,
                    userInfo: userInfo,
                  ),
                );
                if (_isUserLogged) {
                  getMessagesAfterLogin(
                    userInfo.user.username,
                    userInfo.user.md5Password,
                  );
                }
                break;
              default:
                if (message == null) {
                  message = _error.errorMessage;
                } else {
                  message = '$message\n${_error.errorMessage}';
                }
                break;
            }
          }
          if (message != null) {
            debugPrint(message);
            _deleteUserSession(locale);
          }
        } on Exception catch (_) {
          _deleteUserSession(locale);
        }
      } else {
        _deleteUserSession(locale);
      }
    }

    _state.localeNotifier.locale = locale;
    if (_isUserLogged) {
      _postLogin(locale.languageCode);
    }
    _backgroundTasks(context);
  }

  _postLogin(String languageCode) async {
    if (_firebaseToken != null && _firebaseToken.isNotEmpty) {
      String _previousToken = await getUserPushNotificationToken();
      if (_previousToken != _firebaseToken) {
        subscribePushNotifications(context, languageCode);
      }
    }
  }

  _backgroundTasks(BuildContext context) async {
    getLanguagesAndPopularLanguages(
      context,
      _state,
      _languageCode,
      callingFromApp: true,
    );
    _loadVersionPeriodicChecking(context);
    _checkNeedToUpdateDeviceInfo(context);
  }

  _loadVersionPeriodicChecking(BuildContext context) {
    Timer.periodic(
      Duration(minutes: APP_VERSION_CHECK_TIMER_MINUTES),
      (timer) => checkVersionValidity(context),
    );
  }

  _checkNeedToUpdateDeviceInfo(BuildContext context) async {
    bool _forceUpdateDeviceInfo = false;

    // CHECK APP VERSION
    String _appVersion = await getSavedAppVersion();
    String _currentAppVersion = await getAppVersion();
    if (_appVersion != null) {
      if (_currentAppVersion != null && _appVersion != _currentAppVersion) {
        _forceUpdateDeviceInfo = true;
        saveAppVersion(_currentAppVersion);
      }
    } else {
      if (_currentAppVersion != null) {
        saveAppVersion(_currentAppVersion);
      }
    }

    // CHECK OS VERSION
    String _osVersion = await getSavedOsVersion();
    String _currentOsVersion = await getDeviceOSVersion();
    if (_osVersion != null) {
      if (_currentOsVersion != null && _osVersion != _currentOsVersion) {
        _forceUpdateDeviceInfo = true;
        saveOsVersion(_currentOsVersion);
        appsflyerEventLog(APPSFLYER_AF_UPDATE, {});
      }
    } else {
      if (_currentOsVersion != null) {
        saveOsVersion(_currentOsVersion);
      }
    }

    if (_forceUpdateDeviceInfo && _isUserLogged) {
      updateDeviceInfo(context);
    }
  }

  _configureFirebaseNotifications() async {
    if (_firebaseMessaging != null) {
      return;
    }
    _firebaseMessaging = FirebaseMessaging.instance;
    _firebaseToken = await _firebaseMessaging.getToken();

    await configureFirebaseNotifications(_firebaseMessaging);
  }
}
