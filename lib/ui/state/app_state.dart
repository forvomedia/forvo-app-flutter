import 'package:flutter/material.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/translation_language_group.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/ui/notifiers/chat_connection_change_notifier.dart';
import 'package:forvo/ui/notifiers/locale_change_notifier.dart';

class AppState {
  Locale locale;
  LocaleChangeNotifier localeNotifier;
  ChatConnectionChangeNotifier chatConnectionChangeNotifier;
  bool isUserLogged;
  bool isSocialNetworkLogin;
  UserInfo userInfo;
  List<Language> languages;
  List<Language> popularLanguages;
  List<TranslationLanguageGroup> translationLanguageGroups;
  bool maintenanceReadOnly;
  bool maintenanceAddWord;
  bool maintenancePronounce;
  bool maintenanceLogin;
  bool maintenanceSignup;
  bool maintenanceMessages;

  AppState({
    this.locale,
    this.localeNotifier,
    this.chatConnectionChangeNotifier,
    this.isUserLogged,
    this.isSocialNetworkLogin,
    this.userInfo,
    this.languages,
    this.popularLanguages,
    this.translationLanguageGroups,
    this.maintenanceReadOnly,
    this.maintenanceAddWord,
    this.maintenancePronounce,
    this.maintenanceLogin,
    this.maintenanceSignup,
    this.maintenanceMessages,
  });

  bool get isMaintenanceReadOnlyActive =>
      maintenanceReadOnly != null && maintenanceReadOnly == true;

  bool get isMaintenanceAddWordActive =>
      maintenanceAddWord != null && maintenanceAddWord == true;

  bool get isMaintenancePronounceActive =>
      maintenancePronounce != null && maintenancePronounce == true;

  bool get isMaintenanceLoginActive =>
      maintenanceLogin != null && maintenanceLogin == true;

  bool get isMaintenanceSignupActive =>
      maintenanceSignup != null && maintenanceSignup == true;

  bool get isMaintenanceMessagesActive =>
      maintenanceMessages != null && maintenanceMessages == true;

  void merge(AppState state) {
    locale = state.locale ?? locale;
    localeNotifier = state.localeNotifier ?? localeNotifier;
    chatConnectionChangeNotifier =
        state.chatConnectionChangeNotifier ?? chatConnectionChangeNotifier;
    isUserLogged = state.isUserLogged ?? isUserLogged;
    isSocialNetworkLogin = state.isSocialNetworkLogin ?? isSocialNetworkLogin;
    userInfo = state.userInfo ?? userInfo;
    languages = state.languages ?? languages;
    popularLanguages = state.popularLanguages ?? popularLanguages;
    translationLanguageGroups =
        state.translationLanguageGroups ?? translationLanguageGroups;
    maintenanceReadOnly = state.maintenanceReadOnly ?? maintenanceReadOnly;
    maintenanceAddWord = state.maintenanceAddWord ?? maintenanceAddWord;
    maintenancePronounce = state.maintenancePronounce ?? maintenancePronounce;
    maintenanceLogin = state.maintenanceLogin ?? maintenanceLogin;
    maintenanceSignup = state.maintenanceSignup ?? maintenanceSignup;
    maintenanceMessages = state.maintenanceMessages ?? maintenanceMessages;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          locale == other.locale &&
          localeNotifier == other.localeNotifier &&
          chatConnectionChangeNotifier == other.chatConnectionChangeNotifier &&
          isUserLogged == other.isUserLogged &&
          isSocialNetworkLogin == other.isSocialNetworkLogin &&
          userInfo == other.userInfo &&
          languages == other.languages &&
          popularLanguages == other.popularLanguages &&
          translationLanguageGroups == other.translationLanguageGroups &&
          maintenanceReadOnly == other.maintenanceReadOnly &&
          maintenanceAddWord == other.maintenanceAddWord &&
          maintenancePronounce == other.maintenancePronounce &&
          maintenanceLogin == other.maintenanceLogin &&
          maintenanceSignup == other.maintenanceSignup &&
          maintenanceMessages == other.maintenanceMessages;

  @override
  int get hashCode =>
      locale.hashCode ^
      localeNotifier.hashCode ^
      chatConnectionChangeNotifier.hashCode ^
      isUserLogged.hashCode ^
      isSocialNetworkLogin.hashCode ^
      userInfo.hashCode ^
      languages.hashCode ^
      popularLanguages.hashCode ^
      translationLanguageGroups.hashCode ^
      maintenanceReadOnly.hashCode ^
      maintenanceAddWord.hashCode ^
      maintenancePronounce.hashCode ^
      maintenanceLogin.hashCode ^
      maintenanceSignup.hashCode ^
      maintenanceMessages.hashCode;
}
