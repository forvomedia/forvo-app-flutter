import 'package:flutter/material.dart';
import 'package:forvo/ui/state/app_state.dart';

class AppStateManager extends InheritedWidget {
  AppStateManager({
    @required this.state,
    @required this.onLocaleChanged,
    @required this.onStateChanged,
    Key key,
    Widget child,
  })  : assert(state != null),
        super(key: key, child: child);

  final AppState state;
  final ValueChanged<String> onLocaleChanged;
  final ValueChanged<AppState> onStateChanged;

  static AppStateManager of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<AppStateManager>();

  @override
  bool updateShouldNotify(AppStateManager oldWidget) =>
      state != oldWidget.state;
}
