import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/theme_util.dart';

class HeaderAppIconItem extends StatelessWidget {
  final Icon icon;
  final Icon activeIcon;
  final String text;
  final Function onTap;
  final bool active;

  HeaderAppIconItem({
    @required this.icon,
    @required this.active,
    @required this.text,
    @required this.onTap,
    this.activeIcon,
  });

  @override
  Widget build(BuildContext context) {
    double textFontSize = useSmallMobileLayout(context) ? 12 : 14;
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          active ? activeIcon : icon,
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              text,
              style: active
                  ? TextStyle(fontSize: textFontSize, color: colorBlue)
                  : TextStyle(
                      fontSize: textFontSize,
                      color: themeIsDark(context) ? colorWhite : colorBlack,
                    ),
            ),
          )
        ],
      ),
    );
  }
}
