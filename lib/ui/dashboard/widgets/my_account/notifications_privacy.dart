import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/user_profile.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/switch/rigth_switch.dart';
import 'package:forvo/ui/widgets/switch/rigth_switch_extended.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/notification_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class NotificationsAndPrivacy extends StatefulWidget {
  @override
  _NotificationsAndPrivacyState createState() =>
      _NotificationsAndPrivacyState();
}

class _NotificationsAndPrivacyState extends State<NotificationsAndPrivacy> {
  Future<UserProfile> _userProfile;
  bool _initialized = false;
  bool _isButtonLoading = false;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _changed = false;
  bool _initialPublicProfile, _publicProfile = false;
  bool _initialMessagesFromForvo, _messagesFromForvo = false;
  bool _initialNotificationPronWords, _notificationPronWords = false;
  bool _initialNotificationVotes, _notificationVotes = false;
  bool _initialMessagesFromUsers, _messagesFromUsers = false;
  bool _initialEmailNotifyMessages, _emailNotifyMessages = false;
  bool _initialPushNotifications, _pushNotifications = false;
  bool _currentAuthorizationStatus = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    _getUserProfile();
    super.didChangeDependencies();
  }

  _getUserProfile() async {
    var state = AppStateManager.of(context).state;
    _currentAuthorizationStatus =
        await configureFirebaseNotifications(FirebaseMessaging.instance);

    if (!_currentAuthorizationStatus) {
      await subscribePushNotifications(context, state.locale.languageCode);
    }
    bool pushNotificationsStatus = await isPushNotificationSettingSelected();
    pushNotificationsStatus ??= false;

    try {
      if (state.userInfo != null && state.locale != null) {
        _userProfile = getUserProfile(
          context,
          username: state.userInfo.user.username,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {
          _initialPushNotifications = pushNotificationsStatus;
          _pushNotifications = pushNotificationsStatus;
        });
      } else {
        throw Exception();
      }
    } on Exception catch (_) {
      var localizations = AppLocalizations.of(context);
      bool _deviceHasConnection = await deviceHasConnection();
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      setState(() {});
    }
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_getUserProfile, message: message);
      } else if (_hasConnection) {
        return ServiceError(_getUserProfile);
      } else {
        return NetworkError(_getUserProfile);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceLoginActive;

    _updateNotificationSettings(
      bool publicProfile,
      bool messagesFromForvo,
      bool notificationPronWords,
      bool notificationVotes,
      bool messagesFromUsers,
      bool emailNotifyMessages,
      bool pushNotifications,
    ) async {
      setState(() {
        _isButtonLoading = true;
      });
      try {
        String languageCode = state.locale.languageCode;

        bool status = true;
        if (_initialPublicProfile != publicProfile ||
            _initialMessagesFromForvo != messagesFromForvo ||
            _initialNotificationPronWords != notificationPronWords ||
            _initialNotificationVotes != notificationVotes ||
            _initialMessagesFromUsers != messagesFromUsers ||
            _initialEmailNotifyMessages != emailNotifyMessages) {
          status = await updateNotificationSettings(
            context,
            publicProfile: publicProfile,
            messagesFromForvo: messagesFromForvo,
            notificationPronWords: notificationPronWords,
            notificationVotes: notificationVotes,
            messagesFromUsers: messagesFromUsers,
            emailNotifyMessages: emailNotifyMessages,
          );
        }

        if (status) {
          if (_initialPushNotifications != pushNotifications) {
            await saveUserPushNotificationStatus(value: pushNotifications);
            await subscribePushNotifications(context, languageCode);
          }

          setState(() {
            _initialPublicProfile = publicProfile;
            _initialMessagesFromForvo = messagesFromForvo;
            _initialNotificationPronWords = notificationPronWords;
            _initialNotificationVotes = notificationVotes;
            _initialMessagesFromUsers = messagesFromUsers;
            _initialEmailNotifyMessages = emailNotifyMessages;
            _initialPushNotifications = pushNotifications;
            _changed = false;
          });

          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate(
                'account.notificationsAndPrivacy.success',
              ),
              type: SNACKBAR_TYPE_OK,
            ),
          );
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String message;
        for (ApiError _error in errors) {
          switch (_error.errorApiCode) {
            default:
              if (message == null) {
                message = _error.errorMessage;
              } else {
                message = '$message\n${_error.errorMessage}';
              }
              break;
          }
        }
        if (message.isNotEmpty) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        setState(() {
          _isButtonLoading = false;
        });
      }
    }

    Widget _getContent(UserProfile userProfile) {
      if (!_initialized) {
        _initialPublicProfile = _publicProfile = userProfile.isPublic;
        _initialMessagesFromForvo =
            _messagesFromForvo = userProfile.notify.forvo;
        _initialNotificationPronWords =
            _notificationPronWords = userProfile.notify.notify;
        _initialNotificationVotes =
            _notificationVotes = userProfile.notify.votes;
        _initialMessagesFromUsers =
            _messagesFromUsers = userProfile.notify.allowMessages;
        _initialEmailNotifyMessages =
            _emailNotifyMessages = userProfile.notify.allowMailMessages;
        _initialized = true;
      }

      return SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
        child: Column(
          children: <Widget>[
            _isMaintenanceActive ? Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: MaintenanceWarningMessage(),
            ) : Container(),
            RightSwitch(
              title: localizations.translate(
                  'account.notificationsAndPrivacy.profileAndActivity.title'),
              subtitle: localizations.translate(
                  // ignore: lines_longer_than_80_chars
                  'account.notificationsAndPrivacy.profileAndActivity.subtitle'),
              onChange: (bool value) {
                setState(() {
                  _publicProfile = value;
                  _changed = value != _initialPublicProfile;
                });
              },
              value: _publicProfile,
            ),
            RightSwitch(
              title: localizations
                  .translate('account.notificationsAndPrivacy.forvo.title'),
              subtitle: localizations
                  .translate('account.notificationsAndPrivacy.forvo.subtitle'),
              onChange: (bool value) {
                setState(() {
                  _messagesFromForvo = value;
                  _changed = value != _initialMessagesFromForvo;
                });
              },
              value: _messagesFromForvo,
            ),
            RightSwitch(
              title: localizations
                  .translate('account.notificationsAndPrivacy.notify.title'),
              subtitle: localizations
                  .translate('account.notificationsAndPrivacy.notify.subtitle'),
              onChange: (bool value) {
                setState(() {
                  _notificationPronWords = value;
                  _changed = value != _initialNotificationPronWords;
                });
              },
              value: _notificationPronWords,
            ),
            RightSwitch(
              title: localizations
                  .translate('account.notificationsAndPrivacy.votes.title'),
              subtitle: localizations
                  .translate('account.notificationsAndPrivacy.votes.subtitle'),
              onChange: (bool value) {
                setState(() {
                  _notificationVotes = value;
                  _changed = value != _initialNotificationVotes;
                });
              },
              value: _notificationVotes,
            ),
            RightSwitchExtended(
              title: localizations
                  .translate('account.notificationsAndPrivacy.messages.title'),
              subtitle: localizations.translate(
                  'account.notificationsAndPrivacy.messages.subtitle'),
              onChange: (bool value) {
                setState(() {
                  _messagesFromUsers = value;
                  _changed = value != _initialMessagesFromUsers;
                });
              },
              value: _messagesFromUsers,
              subtitleExtended: localizations.translate(
                  'account.notificationsAndPrivacy.messages.subtitleExtended'),
              onChangeExtended: _messagesFromUsers
                  ? (bool value) {
                      setState(() {
                        _emailNotifyMessages = value;
                        _changed = value != _initialEmailNotifyMessages;
                      });
                    }
                  : null,
              valueExtended: _emailNotifyMessages,
            ),
            RightSwitch(
              title: localizations.translate(
                  'account.notificationsAndPrivacy.pushNotifications.title'),
              subtitle: null,
              onChange: (bool value) async {
                _currentAuthorizationStatus =
                    await configureFirebaseNotifications(
                        FirebaseMessaging.instance);
                if (!_currentAuthorizationStatus) {
                  value = false;
                  showCustomSnackBar(
                    _scaffoldMessengerKey.currentContext,
                    themedSnackBar(
                      context,
                      localizations.translate(
                        'error.notifications.permissionNotGranted',
                      ),
                      duration: Duration(seconds: 10),
                      type: SNACKBAR_TYPE_ERROR,
                    ),
                  );
                }

                setState(() {
                  _pushNotifications = value;
                  _changed = value != _initialPushNotifications;
                });
              },
              value: _pushNotifications,
            ),
            MainButton(
              caption: localizations.translate('shared.save'),
              isLoading: _isButtonLoading,
              isEnabled: !_isMaintenanceActive && !_isButtonLoading && _changed,
              onPressed: () {
                _updateNotificationSettings(
                  _publicProfile,
                  _messagesFromForvo,
                  _notificationPronWords,
                  _notificationVotes,
                  _messagesFromUsers,
                  _emailNotifyMessages,
                  _pushNotifications,
                );
              },
            )
          ],
        ),
      );
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle:
          localizations.translate('account.notificationsAndPrivacy.title'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: FutureBuilder(
        future: _userProfile,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            return _getContent(snapshot.data);
          } else if (snapshot.hasError) {
            _failed = true;
            if (snapshot.error is ApiException) {
              String messages =
                  (snapshot.error as ApiException).getErrorMessages(context);
              _apiError = true;
              return _buildErrorMessage(message: messages);
            }
            if (snapshot.error is NotConnectedException) {
              _hasConnection = false;
            } else {
              _hasConnection = true;
            }
            return _buildErrorMessage();
          }
          return CustomCircularProgressIndicator();
        },
      ),
    );
  }
}
