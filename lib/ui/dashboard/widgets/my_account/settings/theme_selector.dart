import 'package:flutter/material.dart';
import 'package:forvo/common/notifiers/theme_changer.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/themes/dark_theme.dart';
import 'package:forvo/ui/themes/light_theme.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:provider/provider.dart';

const String THEME_DARK = '0';
const String THEME_LIGHT = '1';
const String THEME_SYSTEM = '2';

class ThemeSelector extends StatefulWidget {
  @override
  State createState() => ThemeSelectorState();
}

class ThemeSelectorState extends State<ThemeSelector> {
  String _radioVal = THEME_DARK;
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
  GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _initValues();
  }

  _initValues() async {
    String _theme = await getUserThemeOption();
    setState(() {
      if (_theme != null) {
        _radioVal = _theme;
      } else {
        _radioVal = THEME_SYSTEM;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    ThemeChanger _themeChanger = Provider.of<ThemeChanger>(context);

    _onChanged(String newTheme) async {
      switch (newTheme) {
        case THEME_DARK:
          await saveUserThemeOption(newTheme);
          _themeChanger.setTheme(darkTheme, darkTheme);
          break;
        case THEME_LIGHT:
          await saveUserThemeOption(newTheme);
          _themeChanger.setTheme(lightTheme, lightTheme);
          break;
        default:
          await deleteUserThemeOption();
          _themeChanger.setTheme(lightTheme, darkTheme);
          break;
      }
      setState(() {
        _radioVal = newTheme;
      });
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle:
          localizations.translate('settings.themeSelector.title'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Container(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
            ),
            Row(
              children: <Widget>[
                Radio<String>(
                  value: THEME_DARK,
                  groupValue: _radioVal,
                  onChanged: _onChanged,
                ),
                Expanded(
                  child: Container(
                    child: GestureDetector(
                      child: Text(
                        localizations
                            .translate('settings.themeSelector.darkTheme'),
                      ),
                      onTap: () => _onChanged(THEME_DARK),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
            ),
            Row(
              children: <Widget>[
                Radio<String>(
                  value: THEME_LIGHT,
                  groupValue: _radioVal,
                  onChanged: _onChanged,
                ),
                Expanded(
                  child: GestureDetector(
                    child: Text(
                      localizations
                          .translate('settings.themeSelector.lightTheme'),
                    ),
                    onTap: () => _onChanged(THEME_LIGHT),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
            ),
            Row(
              children: <Widget>[
                Radio<String>(
                  value: THEME_SYSTEM,
                  groupValue: _radioVal,
                  onChanged: _onChanged,
                ),
                Expanded(
                  child: GestureDetector(
                    child: Text(
                      localizations
                          .translate('settings.themeSelector.systemDefault'),
                    ),
                    onTap: () => _onChanged(THEME_SYSTEM),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
