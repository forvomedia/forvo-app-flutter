import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/notification_util.dart';

class InterfaceLanguage extends StatefulWidget {
  @override
  State createState() => InterfaceLanguageState();
}

class InterfaceLanguageState extends State<InterfaceLanguage> {
  String _radioVal;
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _initValues();
  }

  _initValues() async {
    String languageCode = AppStateManager.of(context).state.locale.languageCode;
    setState(() {
      if (languageCode != null) {
        _radioVal = languageCode;
      } else {
        _radioVal = LanguageConstants.LANGUAGE_CODE_EN;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);

    _onChanged(String newLanguageCode) async {
      AppStateManager.of(context).onLocaleChanged(newLanguageCode);
      setState(() {
        _radioVal = newLanguageCode;
      });

      if (state.isUserLogged) {
        var data = await Future.wait([
          getUserInfo(_scaffoldMessengerKey.currentContext, newLanguageCode),
          subscribePushNotifications(
              _scaffoldMessengerKey.currentContext, newLanguageCode),
        ]);

        UserInfo userInfo = data[0] as UserInfo;
        state.merge(
          AppState(
            userInfo: userInfo,
          ),
        );
      }

      await getLanguagesAndPopularLanguages(
          _scaffoldMessengerKey.currentContext, state, newLanguageCode);
    }

    Widget _getLanguageRow(String languageCode) => Padding(
          padding: const EdgeInsets.only(top: 16),
          child: Row(
            children: [
              Radio<String>(
                value: languageCode,
                groupValue: _radioVal,
                onChanged: _onChanged,
              ),
              Expanded(
                child: Container(
                  child: GestureDetector(
                    child: Text(
                      localizations.translate('language.names.$languageCode'),
                    ),
                    onTap: () => _onChanged(languageCode),
                  ),
                ),
              ),
            ],
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: localizations.translate('shared.changeLanguage'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Container(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: LanguageConstants.SUPPORTED_LANGUAGE_CODES
              .map(_getLanguageRow)
              .toList(),
        ),
      ),
    );
  }
}
