import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/app_review_util.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  String _version;

  @override
  void initState() {
    _initData();
    super.initState();
  }

  _initData() async {
    String version = await getAppVersion();
    setState(() {
      _version = version;
    });
  }

  void _launchURL(url) async {
    if (!await launch(url, forceWebView: true)) {
      var localizations = AppLocalizations.of(context);
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          localizations.translate('error.default'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: localizations.translate('shared.about.about'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  ListTile(
                    title: Text(
                      localizations.translate('tutorial.viewTutorial'),
                      style: themeData.textTheme.bodyText2,
                    ),
                    onTap: () => Navigator.pushNamed(
                        context, RouteConstants.TUTORIAL_HOME),
                  ),
                  ListTile(
                    title: Text(
                      localizations.translate('shared.about.termsOfUse'),
                      style: themeData.textTheme.bodyText2,
                    ),
                    onTap: () => _launchURL(TERMS_OF_USE_URL),
                  ),
                  ListTile(
                    title: Text(
                      localizations.translate('shared.about.privacyPolicy'),
                      style: themeData.textTheme.bodyText2,
                    ),
                    onTap: () => _launchURL(PRIVACY_POLICY_URL),
                  ),
                  ListTile(
                    title: Text(
                      localizations.translate('shared.about.appReview'),
                      style: themeData.textTheme.bodyText2,
                    ),
                    onTap: () {
                      showAlertDialog(_scaffoldMessengerKey.currentContext);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: InkWell(
                child: Text(
                  localizations.translate('shared.about.deleteAccount'),
                  style: themeData.textTheme.bodyText2.copyWith(
                    fontSize: 16,
                    color: colorRedBad,
                  ),
                ),
                onTap: () =>
                    Navigator.pushNamed(context, RouteConstants.DELETE_ACCOUNT),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                localizations
                    .translate('app.aboutName', params: {'version': _version}),
                style: themeData.textTheme.subtitle1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
