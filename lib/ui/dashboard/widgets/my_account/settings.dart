import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';

class Settings extends StatelessWidget {
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: localizations.translate('shared.settings'),
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: ListView(
            children: [
              ListTile(
                title: Text(
                  localizations.translate('shared.changeLanguage'),
                  style: themeData.textTheme.bodyText2,
                ),
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteConstants.INTERFACE_LANGUAGE,
                ),
              ),
              ListTile(
                title: Text(
                  localizations.translate('settings.themeSelector.title'),
                  style: themeData.textTheme.bodyText2,
                ),
                onTap: () =>
                    Navigator.pushNamed(context, RouteConstants.THEME_SELECTOR),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
