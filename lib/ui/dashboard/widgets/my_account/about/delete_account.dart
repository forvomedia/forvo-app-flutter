import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/ui/widgets/common/headline_text.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';

class DeleteAccount extends StatefulWidget {
  @override
  _DeleteAccountState createState() => _DeleteAccountState();
}

class _DeleteAccountState extends State<DeleteAccount> {
  bool _isButtonLoading = false;
  var _textFieldController = TextEditingController();
  FocusNode _textEditingFocusNode = FocusNode(debugLabel: 'texEditing');

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  _deleteAccount() async {
    var localizations = AppLocalizations.of(context);

    try {
      setState(() {
        _isButtonLoading = true;
      });

      var accountDeleted =
          await deleteAccount(context, reason: _textFieldController.text);
      if (accountDeleted) {
        Navigator.of(context).pushNamedAndRemoveUntil(
            RouteConstants.HOME, (Route<dynamic> route) => false);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('deleteAccount.success'),
            type: SNACKBAR_TYPE_OK,
          ),
        );
      }
    } on ApiException catch (error) {
      String messages = error.getErrorMessages(context);
      if (messages != null) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } finally {
      setState(() {
        _isButtonLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceAddWordActive;

    InputBorder _textFormFieldBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: themeData.dividerColor,
        width: 1.0,
      ),
    );

    TextStyle _headLineTextStyle = themeData.textTheme.bodyText2.copyWith(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: themeIsDark(context) ? colorDarkThemeGrey : colorLightThemeGrey,
    );

    Widget _getContent() => SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 20.0,
          ),
          child: Container(
            width: getDeviceWidth(context),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _isMaintenanceActive
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 20.0),
                              child: MaintenanceWarningMessage(),
                            )
                          : Container(),
                      Text(
                        localizations.translate('deleteAccount.title'),
                        style: themeData.textTheme.headline6,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                      ),
                      Text(
                        localizations.translate('deleteAccount.subtitle'),
                        style: themeData.textTheme.subtitle1,
                      ),
                    ],
                  ),
                ),
                HeadlineText(
                  text: localizations.translate('shared.username'),
                  textStyle: _headLineTextStyle,
                ),
                TextFormField(
                  initialValue: state.userInfo.user.username,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  style: themeData.textTheme.bodyText2.copyWith(
                    fontStyle: FontStyle.normal,
                  ),
                  enabled: false,
                  decoration: InputDecoration(
                    hintMaxLines: 2,
                    errorMaxLines: 3,
                    enabledBorder: _textFormFieldBorder,
                    enabled: false,
                    fillColor:
                        themeIsDark(context) ? colorBlack : colorLightThemeGrey,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                ),
                HeadlineText(
                  text: localizations.translate('deleteAccount.reasonTitle'),
                  textStyle: _headLineTextStyle,
                ),
                TextFormField(
                  controller: _textFieldController,
                  focusNode: _textEditingFocusNode,
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,
                  minLines: 3,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                ),
                MainButton(
                  caption:
                      localizations.translate('deleteAccount.button.delete'),
                  onPressed: _deleteAccount,
                  isLoading: _isButtonLoading,
                  isEnabled: !_isMaintenanceActive && !_isButtonLoading,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                ),
                SecondaryButton(
                  caption: localizations.translate('shared.cancel'),
                  onPressed: () => Navigator.pop(context),
                  isEnabled: !_isMaintenanceActive && !_isButtonLoading,
                ),
              ],
            ),
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarHideActions: true,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: _getContent(),
    );
  }
}
