import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_words.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class MyRequestedPronunciations extends StatefulWidget {
  @override
  _MyRequestedPronunciationsState createState() =>
      _MyRequestedPronunciationsState();
}

class _MyRequestedPronunciationsState extends State<MyRequestedPronunciations> {
  ScrollController _scrollController = ScrollController();
  Map<String, LanguageWithWords> _requestedWords = {};
  List<Language> _languages;
  int _selectedLanguageIndex = 0;
  int _page = 1;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _isLoading = false;
  bool _isLanguageListInitialized = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  void _fetchRequestedWords() async {
    setState(() {
      _isLoading = true;
    });
    try {
      var state = AppStateManager.of(context).state;
      LanguageWithWords words;
      if (state.userInfo != null && state.locale != null) {
        String langCode;
        if (_languages != null && _languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;

          if (_requestedWords != null &&
              _requestedWords.isNotEmpty &&
              _requestedWords[langCode] != null &&
              _requestedWords[langCode].items.isNotEmpty) {
            words = _requestedWords[langCode];
          } else {
            var data = await Future.wait([
              getUserRequestedWords(
                context,
                username: state.userInfo.user.username,
                languageCode: langCode,
                interfaceLanguageCode: state.locale.languageCode,
                page: _page,
              ),
            ]);
            words = data[0];
          }
        } else {
          _languages = await getUserRequestedWordsLangs(
            context,
            state.userInfo.user.username,
            interfaceLanguageCode: state.locale.languageCode,
          );
          if (_languages.isNotEmpty) {
            words = await getUserRequestedWords(
              context,
              username: state.userInfo.user.username,
              languageCode: _languages[_selectedLanguageIndex].code,
              interfaceLanguageCode: state.locale.languageCode,
              page: _page,
            );
          } else {
            words = null;
          }
        }
        if (_languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;
          if (_requestedWords == null || _requestedWords.isEmpty) {
            for (Language language in _languages) {
              _requestedWords[language.code] = null;
            }
          }
          _requestedWords[langCode] = words;
        }
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isLoading = false;
          _apiError = false;
          _isLanguageListInitialized = true;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _isLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isLoading) {
      _fetchRequestedWords();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (!_isLoading) {
        _fetchRequestedWords();
      }
    });
  }

  Future<void> _onRefresh() async {
    _fetchRequestedWords();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    Widget _buildList() {
      String languageCode;
      if (_languages.isNotEmpty) {
        languageCode = _languages[_selectedLanguageIndex].code;
      }

      return !_isLoading &&
              (_languages.isEmpty ||
                  _requestedWords[languageCode].items.isEmpty)
          ? Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    localizations.translate(
                        'account.myRequestedPronunciations.emptyMessage'),
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.subtitle1,
                  ),
                ),
              ],
            )
          : _isLoading
              ? CustomCircularProgressIndicator()
              : ListView.builder(
                  controller: _scrollController,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                  ),
                  itemCount: _requestedWords[languageCode].items.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = _requestedWords[languageCode].items[index];
                    return ListTile(
                      contentPadding: const EdgeInsets.only(top: 4.0),
                      leading: item.standardPronunciation != null
                          ? PlayButton(
                              id: item.id,
                              url: item.standardPronunciation.audioRealMp3,
                              folderPath: item.languageCode,
                            )
                          : Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                    'assets/images/record.png',
                                  ),
                                ),
                              ),
                            ),
                      title: Text(
                        item.original,
                        style: themeData.textTheme.bodyText2,
                      ),
                      onTap: () => print(item.id),
                    );
                  },
                );
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _isLanguageListInitialized && _languages.isNotEmpty
              ? LanguageButtonBar(
                  languages: _languages,
                  onPressed: (index) {
                    setState(() {
                      _selectedLanguageIndex = index;
                    });
                    _fetchRequestedWords();
                  },
                  selectedIndex: _selectedLanguageIndex,
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  localizations
                      .translate('account.myRequestedPronunciations.title'),
                  style: themeData.textTheme.headline6,
                ),
                Divider(color: themeData.dividerColor),
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: _isLoading && !_isLanguageListInitialized
                  ? CustomCircularProgressIndicator()
                  : _failed
                      ? _buildErrorMessage()
                      : _buildList(),
            ),
          ),
        ],
      ),
    );
  }
}
