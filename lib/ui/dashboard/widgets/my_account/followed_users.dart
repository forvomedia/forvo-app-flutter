import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/followed_user.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_followed_users.dart';
import 'package:forvo/service/api/followed_user_api_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/followed_user_detail.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class FollowedUsers extends StatefulWidget {
  @override
  _FollowedUsersState createState() => _FollowedUsersState();
}

class _FollowedUsersState extends State<FollowedUsers> {
  Future<List<LanguageWithFollowedUsers>> _followedUsers;
  int _selectedLanguageIndex = 0;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  dynamic _hasChanged = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _fetchFollowedUsersData();
  }

  _fetchFollowedUsersData() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    try {
      if (state.userInfo != null && state.locale != null) {
        _followedUsers = getUserFollowedUsers(
          context,
          state.userInfo.user.username,
          interfaceLanguageCode: state.locale.languageCode,
        );
        setState(() {});
      } else {
        throw Exception();
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  Future<void> _onRefresh() async {
    await _fetchFollowedUsersData();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) => CustomScaffold(
        scaffoldMessengerKey: _scaffoldMessengerKey,
        bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
        body: getBody(),
      );

  Widget getBody() => FutureBuilder(
      future: _followedUsers,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          return createFollowedUsersView(context, snapshot.data);
        } else if (snapshot.hasError) {
          _failed = true;
          if (snapshot.error is ApiException) {
            String messages =
                (snapshot.error as ApiException).getErrorMessages(context);
            _apiError = true;
            return _buildErrorMessage(message: messages);
          }
          if (snapshot.error is NotConnectedException) {
            _hasConnection = false;
          } else {
            _hasConnection = true;
          }
          return _buildErrorMessage();
        }
        return CustomCircularProgressIndicator();
      });

  List<Language> getLanguages(List<LanguageWithFollowedUsers> followedUsers) {
    var languages =
        followedUsers.map((followed) => followed.language).toSet().toList();
    languages.sort((a, b) => a.name.compareTo(b.name));
    return languages;
  }

  Container createFollowedUsersView(
      BuildContext context, List<LanguageWithFollowedUsers> followedUsers) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    var languages = getLanguages(followedUsers);
    if (_selectedLanguageIndex > languages.length - 1) {
      _selectedLanguageIndex = 0;
    }

    var followedUsersAndLanguage = followedUsers
        .where((user) => user.language == languages[_selectedLanguageIndex]);

    List<FollowedUser> followedUsersByLanguage = [];
    if (followedUsersAndLanguage.isNotEmpty) {
      followedUsersByLanguage = followedUsersAndLanguage.first.items;
    }

    return Container(
      width: getDeviceWidth(context),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          languages.isNotEmpty
              ? LanguageButtonBar(
                  languages: languages,
                  onPressed: (index) {
                    setState(() => _selectedLanguageIndex = index);
                  },
                  selectedIndex: _selectedLanguageIndex,
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  localizations.translate('account.myFollowedUsers.title'),
                  style: themeData.textTheme.headline6,
                ),
                Divider(color: themeData.dividerColor),
              ],
            ),
          ),
          Expanded(
            child: followedUsersByLanguage.isEmpty
                ? Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(
                          localizations.translate(
                              'account.myFollowedUsers.emptyMessage'),
                          textAlign: TextAlign.center,
                          style: themeData.textTheme.subtitle1,
                        ),
                      ),
                    ],
                  )
                : RefreshIndicator(
                    onRefresh: () async {
                      _fetchFollowedUsersData();
                    },
                    child: ListView(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 22.0,
                      ),
                      children: ListTile.divideTiles(
                        color: themeData.dividerColor,
                        context: context,
                        tiles: getFollowedUsersTiles(
                          followedUsersByLanguage,
                          themeData,
                          localizations,
                        ),
                      ).toList(),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  Iterable<Widget> getFollowedUsersTiles(Iterable<FollowedUser> followedUsers,
          ThemeData themeData, AppLocalizations localizations) =>
      followedUsers.map(
        (item) => ListTile(
          contentPadding: const EdgeInsets.only(top: 4.0),
          title: Text(
            item.name,
            style: themeData.textTheme.bodyText2,
          ),
          subtitle: Text(
            localizations.translate('shared.pronunciationsNumber',
                params: {'pronunciations': '${item.pronunciationsNumber}'}),
            style: themeData.textTheme.subtitle1,
          ),
          trailing: Icon(Icons.navigate_next),
          onTap: () async {
            FollowedUserDetailArguments arguments = FollowedUserDetailArguments(
              userName: item.name,
              followingLanguageCode: item.followingLangCode,
            );
            _hasChanged = await Navigator.pushNamed(
              context,
              RouteConstants.FOLLOWED_USER_DETAIL,
              arguments: arguments,
            );
            if (_hasChanged != null && _hasChanged) {
              _fetchFollowedUsersData();
            }
          },
        ),
      );
}
