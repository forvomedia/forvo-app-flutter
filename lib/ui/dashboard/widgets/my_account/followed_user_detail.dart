import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/followed_user_api_service.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/account/public_profile.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_detail.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_icon_item.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/followed_users_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/maintenance_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class FollowedUserDetailArguments {
  final String userName;
  final String followingLanguageCode;

  FollowedUserDetailArguments({this.userName, this.followingLanguageCode});
}

class FollowedUserDetail extends StatefulWidget {
  FollowedUserDetail();

  @override
  _FollowedUserDetailState createState() => _FollowedUserDetailState();
}

class _FollowedUserDetailState extends State<FollowedUserDetail> {
  final TextEditingController _filterController = TextEditingController();
  FocusNode _textEditingFocusNode = FocusNode(debugLabel: 'texEditing');
  ScrollController _scrollController = ScrollController();
  String _followedUserName = '';
  String _followedUserLanguageCode = '';
  bool _followedUserAcceptUserMessages = true;
  Map<String, LanguageWithPronunciations> _pronunciations = {};
  LanguageWithPronunciations _searchTermPronunciations;
  List<Language> _languages;
  List<Language> _following;
  String _filterText = '';
  Map _pages = {};
  int _page = 1;
  int _selectedLanguageIndex = 0;
  String _selectedLanguageCode;
  bool _initialized = false;
  bool _initializedParameters = false;
  bool _isLoading = false;
  bool _isPaginationLoading = false;
  bool _isLanguageListInitialized = false;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _hasChanged = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_isPaginationLoading &&
            _pages[_languages[_selectedLanguageIndex].code] + 1 <=
                _pronunciations[_languages[_selectedLanguageIndex].code]
                    .pages &&
            _filterText.isEmpty) {
          _pages[_languages[_selectedLanguageIndex].code] += 1;
          _fetchFollowedUserPronunciationsNewPageData();
        }
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _extractArguments();
    if (!_isLoading && !_initialized && _initializedParameters) {
      _fetchFollowedUserPronunciationsData();
      _fetchFollowedUser();
    }
  }

  _extractArguments() {
    var args = ModalRoute.of(context).settings.arguments
        as FollowedUserDetailArguments;
    if (args != null) {
      _followedUserName = args.userName;
      _followedUserLanguageCode = args.followingLanguageCode;

      setState(() {
        _initializedParameters = true;
      });
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _fetchFollowedUserPronunciationsData() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });

      var state = AppStateManager.of(context).state;
      try {
        if (state.userInfo != null && state.locale != null) {
          if (_languages != null && _languages.isNotEmpty) {
            _selectedLanguageCode = _languages[_selectedLanguageIndex].code;

            if (_pronunciations == null ||
                _pronunciations.isEmpty ||
                _pronunciations[_selectedLanguageCode] == null ||
                _pronunciations[_selectedLanguageCode].items.isEmpty) {
              var data = await Future.wait([
                getUserPronunciations(
                  context,
                  username: _followedUserName,
                  languageCode: _selectedLanguageCode,
                  interfaceLanguageCode: state.locale.languageCode,
                  page: _pages[_languages[_selectedLanguageIndex].code],
                ),
              ]);
              _pronunciations[_selectedLanguageCode] = data[0];
            }
          } else {
            if (_followedUserLanguageCode != null &&
                _followedUserLanguageCode.isNotEmpty) {
              _selectedLanguageCode = _followedUserLanguageCode;

              var data = await Future.wait([
                getUserPronunciationLangs(
                  context,
                  _followedUserName,
                  interfaceLanguageCode: state.locale.languageCode,
                ),
                getUserPronunciations(
                  context,
                  username: _followedUserName,
                  languageCode: _selectedLanguageCode,
                  interfaceLanguageCode: state.locale.languageCode,
                  page: 1,
                ),
              ]);
              _languages = getLanguages(data[0]);
              _pronunciations[_selectedLanguageCode] = data[1];
            } else {
              _languages = await getUserPronunciationLangs(
                context,
                _followedUserName,
                interfaceLanguageCode: state.locale.languageCode,
              );
              if (_languages.isNotEmpty) {
                _selectedLanguageCode = _languages[_selectedLanguageIndex].code;
                _pronunciations[_selectedLanguageCode] =
                    await getUserPronunciations(
                  context,
                  username: _followedUserName,
                  languageCode: _selectedLanguageCode,
                  interfaceLanguageCode: state.locale.languageCode,
                  page: 1,
                );
              }
            }
          }
          if (_pages == null || _pages.isEmpty) {
            for (var value in _languages) {
              _pages[value.code] = 1;
            }
          }
          if (_pronunciations == null || _pronunciations.isEmpty) {
            for (Language language in _languages) {
              _pronunciations[language.code] = null;
            }
          } else {
            _followedUserAcceptUserMessages =
                _pronunciations[_selectedLanguageCode]
                    .items
                    .first
                    .pronouncerAllowsUserMessages;
          }
          if (mounted) {
            setState(() {
              _isLoading = false;
              _failed = false;
              _hasConnection = true;
              _isLanguageListInitialized = true;
            });
          }
        } else {
          var localizations = AppLocalizations.of(context);
          if (mounted) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                localizations.translate('error.default'),
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        var localizations = AppLocalizations.of(context);
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        if (mounted) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          setState(() {
            _isLoading = false;
            _failed = true;
            _hasConnection = _deviceHasConnection;
          });
        }
      }
    }
  }

  _fetchFollowedUserPronunciationsNewPageData() async {
    if (mounted) {
      setState(() {
        _isPaginationLoading = true;
      });
      var state = AppStateManager.of(context).state;
      try {
        LanguageWithPronunciations pronunciations;
        _selectedLanguageCode = _languages[_selectedLanguageIndex].code;
        var data = await Future.wait([
          getUserPronunciations(
            context,
            username: _followedUserName,
            languageCode: _selectedLanguageCode,
            interfaceLanguageCode: state.locale.languageCode,
            page: _pages[_languages[_selectedLanguageIndex].code],
          ),
        ]);
        pronunciations = data[0];

        _pronunciations[_selectedLanguageCode]
            .items
            .addAll(pronunciations.items);
        if (mounted) {
          setState(() {
            _failed = false;
            _hasConnection = true;
            _isPaginationLoading = false;
            _apiError = false;
            _isLanguageListInitialized = true;
          });
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        var localizations = AppLocalizations.of(context);
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        if (mounted) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          setState(() {
            _pages[_languages[_selectedLanguageIndex].code] -= 1;
            _isPaginationLoading = false;
            _failed = true;
            _hasConnection = _deviceHasConnection;
          });
        }
      }
    }
  }

  _fetchFollowedUserPronunciationsTermData(String value) async {
    var state = AppStateManager.of(context).state;
    if (mounted) {
      try {
        if (state.userInfo != null && state.locale != null) {
          var pronunciations = await getUserTermPronunciationsByLang(context,
              username: _followedUserName,
              languageCode: _languages[_selectedLanguageIndex].code,
              term: value,
              interfaceLanguageCode: state.locale.languageCode,
              page: _page);

          _searchTermPronunciations = pronunciations;
          if (mounted) {
            setState(() {
              _failed = false;
              _hasConnection = true;
            });
          }
        } else {
          throw Exception();
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        var localizations = AppLocalizations.of(context);
        if (_deviceHasConnection) {
          if (mounted) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                localizations.translate('error.default'),
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } else {
          String message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
          if (mounted) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                message,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        }
        if (mounted) {
          setState(() {
            _pronunciations = null;
            _languages = null;
            _failed = true;
            _hasConnection = _deviceHasConnection;
          });
        }
      }
    }
  }

  void _searchPronunciations(String value) {
    if (value != '') {
      _fetchFollowedUserPronunciationsTermData(value);
    } else {
      _searchTermPronunciations = null;
      _fetchFollowedUserPronunciationsData();
    }
    if (mounted) {
      setState(() {
        _filterText = value;
      });
    }
  }

  _fetchFollowedUser() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    if (mounted) {
      try {
        _following = await followingUserLangs(
          context,
          _followedUserName,
          interfaceLanguageCode: state.locale.languageCode,
        );
        if (mounted) {
          setState(() {
            _initialized = true;
          });
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String messages;
        for (ApiError _error in errors) {
          switch (_error.errorApiCode) {
            default:
              String errorMessage =
                  localizations.translate('error.api.${_error.errorApiCode}');
              if (messages == null) {
                messages = errorMessage;
              } else {
                messages = '$messages\n$errorMessage';
              }
              break;
          }
        }
        if (messages.isNotEmpty && mounted) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        if (mounted) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      }
    }
  }

  List<Language> getLanguages(List<Language> languagesList) {
    var languages = languagesList;
    if (languages != null && languages.isNotEmpty) {
      languages.sort((a, b) {
        var order = 0;
        if (_followedUserLanguageCode != null &&
            _followedUserLanguageCode.isNotEmpty) {
          if (a.code == _followedUserLanguageCode) {
            order = -1;
          } else if (b.code == _followedUserLanguageCode) {
            order = 1;
          } else {
            order = a.name.compareTo(b.name);
          }
        } else {
          order = a.name.compareTo(b.name);
        }
        return order;
      });
    } else {
      languages = [];
    }
    return languages;
  }

  bool _contains(List<Language> list, String languageName) {
    bool contains = false;
    if (list != null && list.isNotEmpty) {
      for (var i in list) {
        if (i.name == languageName) {
          contains = true;
        }
      }
    }
    return contains;
  }

  void _remove(List<Language> list, Language language) {
    if (list != null && list.isNotEmpty) {
      Language remove;
      for (var i in list) {
        if (i.name == language.name) {
          remove = i;
        }
      }
      if (remove != null) {
        list.remove(remove);
      }
    }
  }

  Widget _buildProgressIndicator() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: _isPaginationLoading ? 1.0 : 00,
            child: CircularProgressIndicator(),
          ),
        ),
      );

  Future<void> _onRefresh() async {
    _fetchFollowedUserPronunciationsData();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive = state.isMaintenanceReadOnlyActive;

    _followingUser(Language language) async {
      String followingAction = _contains(_following, language.name)
          ? PRONUNCIATION_FOLLOWING_USER_ACTION_NO
          : PRONUNCIATION_FOLLOWING_USER_ACTION_YES;

      try {
        Status status = await followUnfollowUser(
          context,
          _followedUserName,
          language.code,
          followingAction,
        );
        if (status.statusIsOk) {
          _hasChanged = true;
          if (_contains(_following, language.name)) {
            _remove(_following, language);
          } else {
            _following.add(language);
          }
          setState(() {});
          showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                localizations.translate(
                  _contains(_following, language.name)
                      ? 'shared.followingUser'
                      : 'shared.notFollowingUser',
                  params: {
                    'userName': _followedUserName,
                  },
                ),
                type: SNACKBAR_TYPE_OK,
              ));
        }
      } on ApiException catch (error) {
        List<ApiError> errors = error.getErrors();
        String messages;
        for (ApiError _error in errors) {
          switch (_error.errorApiCode) {
            default:
              String errorMessage =
                  localizations.translate('error.api.${_error.errorApiCode}');
              if (messages == null) {
                messages = errorMessage;
              } else {
                messages = '$messages\n$errorMessage';
              }
              break;
          }
        }
        if (messages.isNotEmpty) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        var localizations = AppLocalizations.of(context);
        if (_deviceHasConnection) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        } else {
          String message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      }
    }

    Widget _buildList() {
      String languageCode;
      if (_languages != null && _languages.isNotEmpty) {
        languageCode = _languages[_selectedLanguageIndex].code;
      }
      LanguageWithPronunciations pronunciations;
      if (_searchTermPronunciations != null) {
        pronunciations = _searchTermPronunciations;
      } else if (_languages != null && _languages.isNotEmpty) {
        pronunciations = _pronunciations[languageCode];
      }
      return !_isLoading &&
              (_languages == null ||
                  _languages.isEmpty ||
                  pronunciations.items.isEmpty)
          ? Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  localizations.translate(
                      'account.myFollowedUsers.emptyPronunciationsMessage'),
                  textAlign: TextAlign.center,
                  style: themeData.textTheme.subtitle1,
                ),
              ],
            )
          : _isLoading
              ? CustomCircularProgressIndicator()
              : ListView.builder(
                  controller: _scrollController,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 22.0,
                  ),
                  itemCount: pronunciations.items.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == pronunciations.items.length) {
                      return _buildProgressIndicator();
                    } else {
                      var item = pronunciations.items[index];
                      return ListTile(
                        contentPadding: const EdgeInsets.only(top: 4.0),
                        leading: PlayButton(
                          id: item.id,
                          url: item.audioRealMp3,
                          folderPath: item.languageCode,
                        ),
                        title: Text(
                          item.original,
                          style: themeData.textTheme.bodyText2,
                        ),
                      );
                    }
                  },
                );
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      headerAppBarTitle: _followedUserName,
      headerAppBarCenterTitle: false,
      headerAppBarActions: _followedUserName != state.userInfo.user.username
          ? <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 15.0, left: 15.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    HeaderAppIconItem(
                      icon: Icon(
                        ForvoIcons.followUsers,
                        size: 20,
                      ),
                      activeIcon: Icon(
                        ForvoIcons.followUsers,
                        size: 20,
                        color: colorBlue,
                      ),
                      active: !_isLoading &&
                          _following != null &&
                          _following.isNotEmpty,
                      text: !_isLoading &&
                              _following != null &&
                              _following.isNotEmpty
                          ? localizations
                              .translate('menu.modal.follow.following')
                          : localizations.translate('menu.modal.follow.follow'),
                      onTap: !_isMaintenanceActive
                          ? () async {
                              if (!_isLoading) {
                                if (_languages.length > 1) {
                                  _hasChanged = await onFollowedUsersMenuOpen(
                                    _scaffoldMessengerKey.currentContext,
                                    _followedUserName,
                                    _onRefresh,
                                    languages: _languages,
                                  );
                                  if (_hasChanged) {
                                    _fetchFollowedUser();
                                  }
                                } else {
                                  _followingUser(_languages.first);
                                }
                              }
                            }
                          : () => showMaintenanceSnackBar(
                              _scaffoldMessengerKey.currentContext,
                              localizations),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: HeaderAppIconItem(
                        icon: Icon(
                          ForvoIcons.publicProfile,
                          size: 20,
                        ),
                        active: false,
                        text: localizations.translate('account.profile.title'),
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            RouteConstants.PUBLIC_PROFILE,
                            arguments: PublicProfileArguments(
                              username: _followedUserName,
                            ),
                          );
                        },
                      ),
                    ),
                    HeaderAppIconItem(
                      icon: Icon(
                        ForvoIcons.chat,
                        size: 20,
                      ),
                      active: false,
                      text: localizations.translate('shared.messages'),
                      onTap: () {
                        var state = AppStateManager.of(context).state;
                        if (state.isUserLogged) {
                          if (_followedUserAcceptUserMessages) {
                            Navigator.pushNamed(
                              context,
                              RouteConstants.CHAT_DETAIL,
                              arguments: ChatConversationDetailArguments(
                                chatConversation: null,
                                userTo: _followedUserName,
                              ),
                            );
                          } else {
                            showCustomSnackBar(
                              _scaffoldMessengerKey.currentContext,
                              themedSnackBar(
                                context,
                                localizations.translate(
                                  'messages.notAllowedUserMessagesWarning',
                                  params: {'userName': _followedUserName},
                                ),
                                type: SNACKBAR_TYPE_ERROR,
                              ),
                            );
                          }
                        } else {
                          Navigator.of(context).pushNamed(RouteConstants.LOGIN);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ]
          : <Widget>[],
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context, _hasChanged);
          return _hasChanged;
        },
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _isLanguageListInitialized && _languages.isNotEmpty
                ? LanguageButtonBar(
                    languages: _languages,
                    onPressed: (index) {
                      setState(() {
                        _selectedLanguageIndex = index;
                      });
                      _searchTermPronunciations = null;
                      _filterText = '';
                      _filterController.text = _filterText;
                      _fetchFollowedUserPronunciationsData();
                      _scrollController
                          .jumpTo(_scrollController.position.minScrollExtent);
                    },
                    selectedIndex: _selectedLanguageIndex,
                  )
                : Container(),
            Card(
              margin: EdgeInsets.all(0.0),
              color: themeData.primaryColor,
              elevation: 3.0,
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: TextFormField(
                  controller: _filterController,
                  focusNode: _textEditingFocusNode,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  style: themeData.textTheme.bodyText2,
                  decoration: InputDecoration(
                    fillColor:
                        themeData.inputDecorationTheme.border.borderSide.color,
                    hintText: localizations.translate('shared.search'),
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.cancel,
                        color: themeData.iconTheme.color,
                      ),
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _filterText = '';
                        _filterController.text = _filterText;
                        _searchPronunciations(_filterText);
                      },
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: getBorder(themeData),
                    focusedBorder: getBorder(themeData),
                    enabledBorder: getBorder(themeData),
                  ),
                  onChanged: _searchPronunciations,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    localizations.translate('shared.pronunciations'),
                    style: themeData.textTheme.headline6,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: _isLoading && !_isLanguageListInitialized
                    ? CustomCircularProgressIndicator()
                    : _failed
                        ? _buildErrorMessage()
                        : _buildList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  InputBorder getBorder(ThemeData themeData) => OutlineInputBorder(
        borderSide: BorderSide(
          color: themeData.inputDecorationTheme.border.borderSide.color,
          width: 1.0,
        ),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      );
}
