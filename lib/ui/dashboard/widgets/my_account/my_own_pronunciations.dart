import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_word_original_data.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/word/word_detail.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class MyOwnPronunciations extends StatefulWidget {
  @override
  _MyOwnPronunciationsState createState() => _MyOwnPronunciationsState();
}

class _MyOwnPronunciationsState extends State<MyOwnPronunciations> {
  ScrollController _scrollController = ScrollController();
  List<Language> _languages;
  Map _pages = {};
  Map<String, LanguageWithPronunciations> _loadedOwnPronunciations = {};
  int _selectedLanguageIndex = 0;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _isLoading = false;
  bool _isPaginationLoading = false;
  bool _isLanguageListInitialized = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  void _fetchMyOwnPronunciations() async {
    setState(() {
      _isLoading = true;
    });
    var state = AppStateManager.of(context).state;
    try {
      LanguageWithPronunciations pronunciations;
      if (state.userInfo != null && state.locale != null) {
        String langCode;
        if (_languages != null && _languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;

          if (_loadedOwnPronunciations != null &&
              _loadedOwnPronunciations.isNotEmpty &&
              _loadedOwnPronunciations[langCode] != null &&
              _loadedOwnPronunciations[langCode].items.isNotEmpty) {
            pronunciations = _loadedOwnPronunciations[langCode];
          } else {
            var data = await Future.wait([
              getUserPronunciations(
                context,
                username: state.userInfo.user.username,
                languageCode: langCode,
                interfaceLanguageCode: state.locale.languageCode,
                page: _pages[_languages[_selectedLanguageIndex].code],
              ),
            ]);
            pronunciations = data[0];
          }
        } else {
          _languages = await getUserPronunciationLangs(
            context,
            state.userInfo.user.username,
            interfaceLanguageCode: state.locale.languageCode,
          );
          if (_languages.isNotEmpty) {
            pronunciations = await getUserPronunciations(
              context,
              username: state.userInfo.user.username,
              languageCode: _languages[_selectedLanguageIndex].code,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code] ?? 1,
            );
          } else {
            pronunciations = null;
          }
        }
        if (_languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;
          if (_pages == null || _pages.isEmpty) {
            for (var value in _languages) {
              _pages[value.code] = 1;
            }
          }
          if (_loadedOwnPronunciations == null ||
              _loadedOwnPronunciations.isEmpty) {
            for (Language language in _languages) {
              _loadedOwnPronunciations[language.code] = null;
            }
          }
          _loadedOwnPronunciations[langCode] = pronunciations;
        }
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isLoading = false;
          _apiError = false;
          _isLanguageListInitialized = true;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on NotConnectedException catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _isLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  void _fetchMyOwnPronunciationsNewPageData() async {
    setState(() {
      _isPaginationLoading = true;
    });
    var state = AppStateManager.of(context).state;
    try {
      LanguageWithPronunciations pronunciations;
      if (state.userInfo != null && state.locale != null) {
        String langCode;
        if (_languages != null && _languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;

          var data = await Future.wait([
            getUserPronunciations(
              context,
              username: state.userInfo.user.username,
              languageCode: langCode,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code],
            ),
          ]);
          pronunciations = data[0];
        } else {
          _languages = await getUserPronunciationLangs(
            context,
            state.userInfo.user.username,
            interfaceLanguageCode: state.locale.languageCode,
          );
          if (_languages.isNotEmpty) {
            pronunciations = await getUserPronunciations(
              context,
              username: state.userInfo.user.username,
              languageCode: _languages[_selectedLanguageIndex].code,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code],
            );
          } else {
            pronunciations = null;
          }
        }
        if (_languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;
          _loadedOwnPronunciations[langCode].items.addAll(pronunciations.items);
        }
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isPaginationLoading = false;
          _apiError = false;
          _isLanguageListInitialized = true;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on NotConnectedException catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _pages[_languages[_selectedLanguageIndex].code] -= 1;
        _isPaginationLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isLoading) {
      _fetchMyOwnPronunciations();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (!_isLoading) {
        _fetchMyOwnPronunciations();
      }
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_isPaginationLoading &&
            _pages[_languages[_selectedLanguageIndex].code] + 1 <=
                _loadedOwnPronunciations[
                        _languages[_selectedLanguageIndex].code]
                    .pages) {
          _pages[_languages[_selectedLanguageIndex].code] += 1;
          _fetchMyOwnPronunciationsNewPageData();
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Widget _buildProgressIndicator() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: _isPaginationLoading ? 1.0 : 00,
            child: CircularProgressIndicator(),
          ),
        ),
      );

  Future<void> _onRefresh() async {
    _fetchMyOwnPronunciations();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    Widget _buildList() {
      String languageCode;
      if (_languages.isNotEmpty) {
        languageCode = _languages[_selectedLanguageIndex].code;
      }

      return !_isLoading &&
              (_languages.isEmpty ||
                  _loadedOwnPronunciations[languageCode].items.isEmpty)
          ? Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    localizations
                        .translate('account.myOwnPronunciations.emptyMessage'),
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.subtitle1,
                  ),
                ),
              ],
            )
          : _isLoading
              ? CustomCircularProgressIndicator()
              : ListView.builder(
                  controller: _scrollController,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                  ),
                  itemCount:
                      _loadedOwnPronunciations[languageCode].items.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index ==
                        _loadedOwnPronunciations[languageCode].items.length) {
                      return _buildProgressIndicator();
                    } else {
                      var item =
                          _loadedOwnPronunciations[languageCode].items[index];
                      return ListTile(
                        contentPadding: const EdgeInsets.only(top: 4.0),
                        leading: PlayButton(
                          id: item.id,
                          url: item.audioRealMp3,
                          folderPath: item.languageCode,
                        ),
                        title: PronunciationWordOriginalData(
                          pronunciation: item,
                        ),
                        onTap: () {
                          WordDetailsArguments arguments = WordDetailsArguments(
                            wordOriginal: item.original,
                            languageName: item.languageName,
                            languageAccentName: item.languageAccentName,
                            categorizationGroupIndex:
                                item.pronunciationGroupIndex,
                          );

                          Navigator.pushNamed(
                            context,
                            RouteConstants.WORD,
                            arguments: arguments,
                          );
                        },
                      );
                    }
                  },
                );
    }

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_MY_ACCOUNT,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _isLanguageListInitialized && _languages.isNotEmpty
              ? LanguageButtonBar(
                  languages: _languages,
                  onPressed: (index) {
                    setState(() {
                      _selectedLanguageIndex = index;
                    });
                    _fetchMyOwnPronunciations();
                    _scrollController
                        .jumpTo(_scrollController.position.minScrollExtent);
                  },
                  selectedIndex: _selectedLanguageIndex,
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  localizations.translate('account.myOwnPronunciations.title'),
                  style: themeData.textTheme.headline6,
                ),
                Divider(color: themeData.dividerColor),
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: _isLoading && !_isLanguageListInitialized
                  ? CustomCircularProgressIndicator()
                  : _failed
                      ? _buildErrorMessage()
                      : _buildList(),
            ),
          ),
        ],
      ),
    );
  }
}
