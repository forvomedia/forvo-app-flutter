import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/attributes_and_user_notifications_data.dart';
import 'package:forvo/model/user_notification.dart';
import 'package:forvo/service/api/notification_api_service.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

import '../../colors.dart';

class HomeNotifications extends StatefulWidget {
  final String sectionName;
  final bool isUserLogged;

  HomeNotifications({this.sectionName, this.isUserLogged});

  @override
  _HomeNotificationsState createState() => _HomeNotificationsState();
}

class _HomeNotificationsState extends State<HomeNotifications> {
  ScrollController _scrollController = ScrollController();
  AttributesAndUserNotificationsData _attributes;
  List<UserNotification> _notifications;
  List<int> _markAsRead = [];
  int _page = 1;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _isLoading = false;
  bool _isPaginationLoading = false;

  void _fetchNotifications() async {
    setState(() {
      _isLoading = true;
    });
    var state = AppStateManager.of(context).state;
    try {
      if (state.userInfo != null && state.locale != null) {
        _attributes = await getUserNotifications(
          context,
          username: state.userInfo.user.username,
          languageCode: state.locale.languageCode,
          page: _page,
        );
        _notifications = _attributes.data;
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isLoading = false;
          _apiError = false;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _isLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  void _fetchNewNotificationsPage() async {
    setState(() {
      _isPaginationLoading = true;
    });
    var state = AppStateManager.of(context).state;
    try {
      if (state.userInfo != null && state.locale != null) {
        _attributes = await getUserNotifications(
          context,
          username: state.userInfo.user.username,
          languageCode: state.locale.languageCode,
          page: _page,
        );
        _notifications.addAll(_attributes.data);
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isPaginationLoading = false;
          _apiError = false;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _page = 1;
        _isPaginationLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  void _deleteNotification(UserNotification notification) async {
    try {
      var state = AppStateManager.of(context).state;
      var localizations = AppLocalizations.of(context);
      if (state.userInfo != null && state.locale != null) {
        bool status = await deleteNotification(
          context,
          notification: notification,
        );
        if (status) {
          setState(() {
            _failed = false;
            _hasConnection = true;
            _isPaginationLoading = false;
            _apiError = false;
          });
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              localizations.translate('notifications.deleteSuccess'),
              type: SNACKBAR_TYPE_OK,
            ),
          );
        } else {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              localizations.translate('error.notifications.deleteFailed'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          _fetchNotifications();
        }
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isLoading) {
      _fetchNotifications();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (!_isLoading) {
        _fetchNotifications();
      }
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_isPaginationLoading &&
            _page + 1 <= _attributes.attributes.pages) {
          _page += 1;
          _fetchNewNotificationsPage();
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Widget _buildProgressIndicator() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: _isLoading ? 1.0 : 00,
            child: CircularProgressIndicator(),
          ),
        ),
      );

  Future<void> _onRefresh() async {
    _fetchNotifications();
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive = state.isMaintenanceReadOnlyActive;

    Widget _getListItem(int index) => Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 15.0,
          ),
          color: _notifications[index].featured
              ? isDarkThemeActive(context)
                  ? colorDarkThemeLightGrey
                  : colorLightBlue
              : themeData.backgroundColor,
          child: ListTile(
            isThreeLine: true,
            contentPadding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            title: Text(
              _notifications[index].title,
              style: themeData.textTheme.bodyText2,
            ),
            subtitle: Text(
              _notifications[index].message,
              style: themeData.textTheme.subtitle1,
            ),
            trailing: getLocaleFormattedDateWidgets(
              themeData,
              _notifications[index].notificationDate,
              DEFAULT_DATETIME_INPUT_FORMAT,
              localizations.locale.languageCode,
              isUtc: true,
            ),
            onTap: () => null,
          ),
        );

    Widget _buildList() => !_isLoading && _notifications.isEmpty
        ? Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  localizations.translate('notifications.emptyMessage'),
                  textAlign: TextAlign.center,
                  style: themeData.textTheme.subtitle1,
                ),
              ),
            ],
          )
        : _isLoading
            ? CustomCircularProgressIndicator()
            : RefreshIndicator(
                onRefresh: () async {
                  _fetchNotifications();
                },
                child: ListView.builder(
                  controller: _scrollController,
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 22.0,
                  ),
                  itemCount: _notifications.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == _notifications.length) {
                      return _buildProgressIndicator();
                    } else {
                      if (_notifications[index].featured) {
                        _markAsRead.add(_notifications[index].id);
                      }
                      return !_isMaintenanceActive
                          ? Dismissible(
                              key: Key(_notifications[index].id.toString()),
                              direction: DismissDirection.horizontal,
                              background: Container(
                                color: colorDeleteNotificationRed,
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(Icons.delete, color: colorWhite),
                                    ],
                                  ),
                                ),
                              ),
                              secondaryBackground: Container(
                                color: colorDeleteNotificationRed,
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Icon(Icons.delete, color: colorWhite),
                                    ],
                                  ),
                                ),
                              ),
                              onDismissed: (DismissDirection direction) {
                                if (direction == DismissDirection.endToStart ||
                                    direction == DismissDirection.startToEnd) {
                                  _deleteNotification(_notifications[index]);
                                }
                                setState(() {
                                  _notifications.removeAt(index);
                                });
                              },
                              child: _getListItem(index),
                            )
                          : _getListItem(index);
                    }
                  },
                ),
              );

    return Container(
      width: getDeviceWidth(context),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _isMaintenanceActive
                    ? Padding(
                        padding: const EdgeInsets.only(
                          right: 20.0,
                          bottom: 20.0,
                        ),
                        child: MaintenanceWarningMessage(),
                      )
                    : Container(),
                Text(
                  localizations.translate('shared.notifications'),
                  style: themeData.textTheme.headline6,
                ),
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: _isLoading
                  ? CustomCircularProgressIndicator()
                  : _failed
                      ? _buildErrorMessage()
                      : _buildList(),
            ),
          ),
        ],
      ),
    );
  }
}
