import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/secondary_button.dart';
import 'package:forvo/util/media_query_util.dar.dart';

class SignUpLoginHelp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    return Container(
      width: getDeviceWidth(context),
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              localizations.translate('signup.loginText'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
            child: MainButton(
              caption: localizations.translate('signup.button'),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                context,
                RouteConstants.SIGN_UP,
                ModalRoute.withName(RouteConstants.START),
              ),
              isEnabled: true,
              isLoading: false,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
            child: SecondaryButton(
              caption: localizations.translate('shared.login'),
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                context,
                RouteConstants.LOGIN,
                ModalRoute.withName(RouteConstants.START),
              ),
              isEnabled: true,
              isLoading: false,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
            child: SecondaryButton(
              caption: localizations.translate('shared.changeLanguage'),
              onPressed: () => Navigator.pushNamed(
                context,
                RouteConstants.INTERFACE_LANGUAGE,
              ),
              isEnabled: true,
              isLoading: false,
            ),
          ),
        ],
      ),
    );
  }
}
