import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';

const int BOTTOM_BAR_SECTION_SEARCH = 0;
const int BOTTOM_BAR_SECTION_FAVORITES = 1;
const int BOTTOM_BAR_SECTION_NOTIFICATIONS = 2;
const int BOTTOM_BAR_SECTION_MY_ACCOUNT = 3;

class DashboardBottomBar extends BottomNavigationBar {
  DashboardBottomBar({
    @required ThemeData themeData,
    @required AppLocalizations localizations,
    Key key,
    int currentIndex,
    bool currentIndexIsActive,
    bool themeIsDark,
    ValueChanged<int> onTap,
  }) : super(
          key: key,
          currentIndex: currentIndex ?? BOTTOM_BAR_SECTION_SEARCH,
          onTap: onTap,
          type: BottomNavigationBarType.fixed,
          backgroundColor: themeData.bottomAppBarColor,
          selectedItemColor:
              currentIndexIsActive ? colorBlue : themeData.iconTheme.color,
          selectedFontSize: currentIndexIsActive ? 14.0 : 12.0,
          unselectedItemColor: themeData.iconTheme.color,
          items: [
            BottomNavigationBarItem(
              label: localizations.translate('shared.sectionStart'),
              activeIcon: currentIndexIsActive
                  ? Icon(ForvoIcons.searchActive, size: 20)
                  : Icon(
                      ForvoIcons.search,
                      size: 20,
                      color: themeIsDark
                          ? colorDarkThemeGrey
                          : colorLightThemeGrey,
                    ),
              icon: Icon(ForvoIcons.search, size: 20),
            ),
            BottomNavigationBarItem(
              label: localizations.translate('shared.favorites'),
              activeIcon: currentIndexIsActive
                  ? Icon(ForvoIcons.favoritesActive, size: 20)
                  : Icon(ForvoIcons.favorites, size: 20),
              icon: Icon(ForvoIcons.favorites, size: 20),
            ),
            BottomNavigationBarItem(
              label: localizations.translate('shared.notifications'),
              activeIcon: currentIndexIsActive
                  ? Icon(ForvoIcons.notificationsActive, size: 20)
                  : Icon(ForvoIcons.notifications, size: 20),
              icon: Icon(ForvoIcons.notifications, size: 20),
            ),
            BottomNavigationBarItem(
              label: localizations.translate('shared.myAccount'),
              activeIcon: currentIndexIsActive
                  ? Icon(ForvoIcons.userActive, size: 20)
                  : Icon(ForvoIcons.user, size: 20),
              icon: Icon(ForvoIcons.user, size: 20),
            )
          ],
        );
}
