import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/search_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language_with_search_result_items.dart';
import 'package:forvo/model/translation_language_pair.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/service/api/trending_words_api_service.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_search_bar.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_search_history.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_search_no_results.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_search_results.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_trending.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/search_mode_modal_menu.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/appsflyer_util.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/search_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class HomeStart extends StatefulWidget {
  final String sectionName;
  final bool isUserLogged;

  HomeStart({this.sectionName, this.isUserLogged});

  @override
  _HomeStartState createState() => _HomeStartState();
}

class _HomeStartState extends State<HomeStart>
    with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _searchController = TextEditingController();
  TabController _tabController;
  int _searchMode = SearchConstants.SEARCH_MODE_PRONUNCIATION;
  TranslationLanguagePair _selectedTranslationLanguagePair;
  List<LanguageWithSearchResultItems> _searchWords;
  List<String> _historyWords = [];
  List<Word> _trendingWords = [];
  bool _isLoading = false;
  bool _failed = false;
  bool _hasConnection = true;
  bool _isSearching = false;
  bool _isSearchComplete = false;
  bool _searchHasResults = false;
  String _searchTerm;
  String _lastSearchTerm;
  bool _isInitialized = false;

  @override
  void initState() {
    _tabController = TabController(
      vsync: this,
      length: 2,
      initialIndex: 0,
    );
    _searchController.addListener(() {
      setState(() {
        _searchTerm = _searchController.text;
      });
    });
    super.initState();
  }

  void _initData() async {
    if (!_isLoading && !_isInitialized && _hasConnection) {
      setState(() {
        _isLoading = true;
      });
      var state = AppStateManager.of(context).state;
      var localizations = AppLocalizations.of(context);
      try {
        var homeData = await Future.wait([
          getHistorySearchWords(),
          getTrendingWords(context, languageCode: state.locale.languageCode)
        ]);
        var historyWords = homeData[0];
        var trendingWords = homeData[1];
        if (mounted) {
          setState(() {
            _historyWords = historyWords;
            _trendingWords = trendingWords;
            _isLoading = false;
            _isInitialized = true;
            _failed = false;
            _hasConnection = true;
          });
        }
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        if (messages != null) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on NotConnectedException catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        setState(() {
          _failed = true;
          _hasConnection = _deviceHasConnection;
          _isLoading = false;
          _isInitialized = false;
        });
      } on Exception catch (_) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        bool _deviceHasConnection = await deviceHasConnection();
        setState(() {
          _failed = true;
          _hasConnection = _deviceHasConnection;
          _isLoading = false;
          _isInitialized = true;
        });
      }
    }
  }

  @override
  void didChangeDependencies() {
    _initData();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void _retry() {
    _hasConnection = true;
    _initData();
  }

  Widget _buildErrorMessage() => _failed
      ? _hasConnection
          ? ServiceError(_retry)
          : NetworkError(_retry)
      : Container();

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;
    final _focusNode = FocusScope.of(context);

    void _onSearch(String searchTerm) async {
      _focusNode.unfocus();
      if (searchTerm.isNotEmpty) {
        if (_searchTerm == null || _searchTerm.isEmpty) {
          _searchTerm = searchTerm;
          _searchController.text = _searchTerm;
        }
        setState(() {
          _isSearching = true;
          _isSearchComplete = false;
          _searchHasResults = false;
        });
        try {
          if (_searchMode == SearchConstants.SEARCH_MODE_PRONUNCIATION) {
            _searchWords = await getSearchWords(
              context,
              searchTerm: searchTerm,
              interfaceLanguageCode: state.locale.languageCode,
              isUserLogged: state.isUserLogged,
            );
          } else {
            _searchWords = await getSearchWordTranslations(
              context,
              searchTerm: searchTerm,
              translationLanguagePairCodes:
                  _selectedTranslationLanguagePair.codes,
              interfaceLanguageCode: state.locale.languageCode,
              isUserLogged: state.isUserLogged,
            );
          }

          _lastSearchTerm = searchTerm;
          if (_searchWords.isEmpty) {
            setState(() {
              _searchHasResults = false;
            });
          } else {
            setState(() {
              _searchHasResults = true;
            });
            saveWordIntoHistorySearchWords(searchTerm);
          }
          Map eventValues = {
            APPSFLYER_AF_SEARCH_STRING: _searchTerm,
          };
          appsflyerEventLog(APPSFLYER_AF_SEARCH, eventValues);
        } on ApiException catch (error) {
          String messages = error.getErrorMessages(context);
          messages ??= localizations.translate('error.default');
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          setState(() {
            _failed = true;
            _hasConnection = true;
            _isLoading = false;
          });
        } on NotConnectedException catch (_) {
          String message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';

          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          setState(() {
            _failed = true;
            _hasConnection = false;
            _isLoading = false;
          });
        } on Exception catch (_) {
          bool _deviceHasConnection = await deviceHasConnection();
          var localizations = AppLocalizations.of(context);
          String message;
          if (_deviceHasConnection) {
            message = '${localizations.translate('error.service.title')}\n'
                '${localizations.translate('error.service.body')}';
          } else {
            message = '${localizations.translate('error.network.title')}\n'
                '${localizations.translate('error.network.body')}';
          }
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              message,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
          setState(() {
            _failed = true;
            _hasConnection = _deviceHasConnection;
            _isLoading = false;
          });
        } finally {
          setState(() {
            _isSearching = false;
            _isSearchComplete = true;
          });
        }
      }
    }

    _onChangeSearchMode(TranslationLanguagePair languagePair) async {
      Navigator.of(context).pop();
      if (languagePair != null) {
        setState(() {
          _searchMode = SearchConstants.SEARCH_MODE_TRANSLATION;
          _selectedTranslationLanguagePair = languagePair;
        });
      } else {
        setState(() {
          _searchMode = SearchConstants.SEARCH_MODE_PRONUNCIATION;
          _selectedTranslationLanguagePair = null;
        });
      }
      if (_searchTerm != null && _searchTerm.isNotEmpty) {
        _onSearch(_searchTerm);
      }
    }

    _onSearchModeMenuOpen() async {
      if (state.translationLanguageGroups == null ||
          state.translationLanguageGroups.isEmpty) {
        await getLanguagesAndPopularLanguages(
          context,
          state,
          state.locale.languageCode,
        );
      }
      if (state.translationLanguageGroups != null &&
          state.translationLanguageGroups.isNotEmpty) {
        onSearchModeMenuOpen(
          context,
          translationLanguageGroups: state.translationLanguageGroups,
          onChangeSearchMode: _onChangeSearchMode,
        );
      }
    }

    _onClearSearchText() async {
      _searchController.clear();
      var historyWords = await getHistorySearchWords();
      setState(() {
        _historyWords = historyWords;
        _isSearching = false;
        _isSearchComplete = false;
        _searchHasResults = false;
      });
    }

    final _tabs = <Tab>[
      Tab(text: localizations.translate('home.tabs.trending')),
      Tab(text: localizations.translate('home.tabs.history')),
    ];

    final _tabPages = <Widget>[
      !_failed
          ? _isLoading
              ? CustomCircularProgressIndicator()
              : HomeTrending(
                  trendingWords: _trendingWords,
                  onTap: _onSearch,
                )
          : _buildErrorMessage(),
      _isLoading
          ? CustomCircularProgressIndicator()
          : HomeSearchHistory(
              historyWords: _historyWords,
              onTap: _onSearch,
            ),
    ];

    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        HomeSearchBar(
          formKey: _formKey,
          searchController: _searchController,
          searchMode: _searchMode,
          selectedTranslationLanguagePair: _selectedTranslationLanguagePair,
          searchTerm: _searchTerm,
          onSearch: _onSearch,
          onSearchModeMenuOpen: _onSearchModeMenuOpen,
          onClearSearchText: _onClearSearchText,
        ),
        Divider(
          height: 0,
          thickness: 1,
        ),
        _isSearching
            ? Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomCircularProgressIndicator(),
                  ],
                ),
              )
            : !_isSearchComplete
                ? Expanded(
                    child: Column(
                      children: [
                        Card(
                          margin: EdgeInsets.all(0.0),
                          color: themeData.appBarTheme.backgroundColor,
                          elevation: 3.0,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(4.0),
                              bottomRight: Radius.circular(4.0),
                            ),
                          ),
                          child: TabBar(
                            controller: _tabController,
                            indicatorColor: colorBlue,
                            indicatorWeight: 3.0,
                            tabs: _tabs,
                            labelStyle: themeData.textTheme.bodyText2.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                            labelColor: colorBlue,
                            unselectedLabelStyle: themeData.textTheme.bodyText2,
                            unselectedLabelColor:
                                themeData.textTheme.bodyText2.color,
                          ),
                        ),
                        Expanded(
                          child: TabBarView(
                            controller: _tabController,
                            children: _tabPages,
                          ),
                        ),
                      ],
                    ),
                  )
                : Expanded(
                    child: _searchHasResults
                        ? HomeSearchResults(
                            searchTerm: _searchTerm,
                            languagesWithSearchResultItems: _searchWords,
                            searchMode: _searchMode,
                            requestPronunciationCallback: () =>
                                _onSearch(_searchTerm),
                          )
                        : Center(
                            child: ListView(
                              shrinkWrap: true,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: HomeSearchNoResults(
                                    searchTerm: _lastSearchTerm ?? '',
                                    searchMode: _searchMode,
                                  ),
                                )
                              ],
                            ),
                          ),
                  ),
      ],
    );
  }
}
