import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_accent.dart';
import 'package:forvo/model/language_accent_with_search_result_items.dart';
import 'package:forvo/model/language_with_search_result_items.dart';
import 'package:forvo/model/search_result_items.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/add_word.dart';
import 'package:forvo/ui/dashboard/widgets/home/home_search_no_exact_match.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/grouped_pronunciation_item.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/standard_pronunciation_item.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/inkwell/themed_bubble_inkwell.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/tab_bar/tab_bar_with_flexibe_tabs.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/ui/widgets/tooltip/custom_tooltip.dart';
import 'package:forvo/ui/word/word_detail.dart';
import 'package:forvo/util/media_query_util.dar.dart';

class HomeSearchResults extends StatefulWidget {
  final String searchTerm;
  final List<LanguageWithSearchResultItems> languagesWithSearchResultItems;
  final int searchMode;
  final Function requestPronunciationCallback;

  HomeSearchResults({
    @required this.searchTerm,
    @required this.languagesWithSearchResultItems,
    @required this.searchMode,
    this.requestPronunciationCallback,
  });

  @override
  _HomeSearchResultsState createState() => _HomeSearchResultsState();
}

class _HomeSearchResultsState extends State<HomeSearchResults>
    with SingleTickerProviderStateMixin {
  TabController _accentsTabController;
  int _selectedLanguageIndex = 0;
  List<LanguageWithSearchResultItems> _languagesWithSearchResultItems;
  Language _selectedLanguage;
  LanguageWithSearchResultItems _selectedLanguageWithSearchResultItems;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    _accentsTabController = TabController(
      vsync: this,
      length: LanguageConstants.LANGUAGE_ACCENT_COUNT,
      initialIndex: 0,
    );
    _initData();
    super.initState();
  }

  void _initData() async {
    _languagesWithSearchResultItems = widget.languagesWithSearchResultItems;
    _accentsTabController.index =
        _getPreferredLanguageAccentIndex(_selectedLanguageIndex);
  }

  @override
  void dispose() {
    _accentsTabController.dispose();
    super.dispose();
  }

  Language _getLanguageFromLanguageWithSearchResultItems(
          LanguageWithSearchResultItems languageWithSearchResultItems) =>
      Language(
        id: languageWithSearchResultItems.languageName,
        name: languageWithSearchResultItems.languageName,
        code: languageWithSearchResultItems.accents != null &&
                languageWithSearchResultItems.accents.isNotEmpty
            ? languageWithSearchResultItems.accents.first.languageCode
            : null,
        accents: (languageWithSearchResultItems.accents?.isNotEmpty ?? false)
            ? languageWithSearchResultItems.accents
                .map(
                  (LanguageAccentWithSearchResultItems
                          languageAccentWithWords) =>
                      LanguageAccent(
                    id: languageAccentWithWords.name,
                    name: languageAccentWithWords.name,
                    code: languageAccentWithWords.code,
                    languageName: languageAccentWithWords.languageName,
                    languageCode: languageAccentWithWords.languageCode,
                  ),
                )
                .toSet()
                .toList()
            : [],
      );

  List<Language> _getLanguages(
      List<LanguageWithSearchResultItems> languagesWithItems) {
    var languages = languagesWithItems
        .map(
          _getLanguageFromLanguageWithSearchResultItems,
        )
        .toSet()
        .toList();
    return languages;
  }

  int _getPreferredLanguageAccentIndex(int languageIndex) {
    int preferredLanguageAccentIndex;
    if (_languagesWithSearchResultItems[languageIndex] != null &&
        _languagesWithSearchResultItems[languageIndex].accents != null &&
        _languagesWithSearchResultItems[languageIndex].accents.isNotEmpty) {
      for (LanguageAccentWithSearchResultItems accent
          in _languagesWithSearchResultItems[languageIndex].accents) {
        if (accent.items != null &&
            accent.items.exactMatch != null &&
            accent.items.exactMatch.numPronunciations > 0 &&
            preferredLanguageAccentIndex == null) {
          preferredLanguageAccentIndex =
              _languagesWithSearchResultItems[languageIndex]
                  .accents
                  .indexOf(accent);
        }
      }
    }
    return preferredLanguageAccentIndex ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    _selectedLanguage = _getLanguageFromLanguageWithSearchResultItems(
        _languagesWithSearchResultItems[_selectedLanguageIndex]);

    _selectedLanguageWithSearchResultItems =
        _languagesWithSearchResultItems[_selectedLanguageIndex];

    _goToWord(Word word, int categorizationGroupIndex) {
      String languageName;
      String languageAccentName;
      if (_selectedLanguage != null && _selectedLanguage.name.isNotEmpty) {
        languageName = _selectedLanguage.name;
        if (_selectedLanguage.hasAccents &&
            _selectedLanguage.accents.isNotEmpty) {
          languageAccentName =
              _selectedLanguage.accents[_accentsTabController.index].name;
        }
      }

      WordDetailsArguments arguments = WordDetailsArguments(
        wordOriginal: word.original,
        languageName: languageName,
        languageAccentName: languageAccentName,
        categorizationGroupIndex: categorizationGroupIndex,
      );

      Navigator.pushNamed(
        context,
        RouteConstants.WORD,
        arguments: arguments,
      );
    }

    Widget _getStandardPronunciationTile(Word word) =>
        StandardPronunciationItem(
          word: word,
          onTap: _goToWord,
          languageCode: _selectedLanguage.code,
          languageAccentCode: _selectedLanguage.hasAccents
              ? _selectedLanguage.accents[_accentsTabController.index].code
              : null,
          requestPronunciationCallback: widget.requestPronunciationCallback,
        );

    Widget _getGroupedPronunciationTile(Word word) => GroupedPronunciationItem(
          word: word,
          onTap: _goToWord,
          languageCode: _selectedLanguage.code,
          languageAccentCode: _selectedLanguage.hasAccents
              ? _selectedLanguage.accents[_accentsTabController.index].code
              : null,
          requestPronunciationCallback: widget.requestPronunciationCallback,
        );

    Widget _getRelatedContentView(List<Word> relatedContent) {
      List<Widget> widgets = [];

      widgets.add(
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              const Radius.circular(10),
            ),
            color: isDarkThemeActive(context)
                ? colorDarkThemeLightGrey
                : colorLightThemeLightGrey,
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 15.0,
            vertical: 8.0,
          ),
          margin: EdgeInsets.only(bottom: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14.0),
                  child: HtmlTagTextFormatted(
                    text: localizations.translate('home.search.relatedContent'),
                    openTag: '<b>',
                    closeTag: '</b>',
                    normalTextStyle: themeData.textTheme.bodyText2,
                    boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );

      for (Word word in relatedContent) {
        Widget widget;
        if (word.hasCategorizationData) {
          widget = _getGroupedPronunciationTile(word);
        } else {
          widget = _getStandardPronunciationTile(word);
        }
        widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: widget,
          ),
        );
        widgets.add(Divider());
      }

      return Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        ),
      );
    }

    Widget _getExactMatchView(Word word) => Padding(
          padding: const EdgeInsets.only(bottom: 20.0),
          child: word.hasCategorizationData &&
                  word.categorizationGroups != null &&
                  word.categorizationGroups.isNotEmpty
              ? _getGroupedPronunciationTile(word)
              : _getStandardPronunciationTile(word),
        );

    Widget _getContentView(SearchResultItems items) => Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: ListView(
            children: [
              items.exactMatch == null
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: HomeSearchNoExactMatch(
                        searchTerm: widget.searchTerm,
                        language: _selectedLanguage,
                        searchMode: widget.searchMode,
                        translation: items.translation ?? '',
                      ),
                    )
                  : _getExactMatchView(items.exactMatch),
              items.relatedContent != null && items.relatedContent.isNotEmpty
                  ? _getRelatedContentView(items.relatedContent)
                  : Container(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0),
                child: Container(
                  height: 40.0,
                ),
              ),
            ],
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      bottomNavBarHidden: true,
      headerAppBarHidden: true,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _languagesWithSearchResultItems?.isNotEmpty ?? false
              ? LanguageButtonBar(
                  languages: _getLanguages(_languagesWithSearchResultItems),
                  onPressed: (index) {
                    setState(() {
                      _selectedLanguageIndex = index;
                      _accentsTabController.index =
                          _getPreferredLanguageAccentIndex(index);
                    });
                  },
                  selectedIndex: _selectedLanguageIndex,
                )
              : Container(),
          _selectedLanguage?.hasAccents ?? false
              ? Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Card(
                        margin: EdgeInsets.all(0.0),
                        color: themeData.appBarTheme.backgroundColor,
                        elevation: 3.0,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(4.0),
                            bottomRight: Radius.circular(4.0),
                          ),
                        ),
                        child: TabBarWithFlexibleTabs(
                          key: GlobalKey(),
                          child: TabBar(
                            controller: _accentsTabController,
                            indicatorColor: colorBlue,
                            indicatorWeight: 3.0,
                            tabs: [
                              Tab(
                                text: _selectedLanguage.accents[0].name ??
                                    _selectedLanguage.accents[0].code,
                              ),
                              Tab(
                                text: _selectedLanguage.accents[1].name ??
                                    _selectedLanguage.accents[1].code,
                              ),
                              Tab(
                                text: _selectedLanguage.accents[2].name ??
                                    _selectedLanguage.accents[2].code,
                              ),
                            ],
                            labelStyle: themeData.textTheme.bodyText2.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                            labelColor: colorBlue,
                            unselectedLabelStyle:
                                themeData.textTheme.bodyText2.copyWith(
                              fontSize: 16,
                            ),
                            unselectedLabelColor:
                                themeData.textTheme.bodyText2.color,
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          key: GlobalKey(),
                          controller: _accentsTabController,
                          children: _selectedLanguageWithSearchResultItems
                              .accents
                              .map(
                                (LanguageAccentWithSearchResultItems accent) =>
                                    _getContentView(accent.items),
                              )
                              .toList(),
                        ),
                      ),
                    ],
                  ),
                )
              : Expanded(
                  child: _getContentView(
                      _selectedLanguageWithSearchResultItems.items),
                ),
        ],
      ),
      floatingActionButtonActive: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: CustomTooltip(
        message: localizations
            .translate('home.search.suggestionRequestNewPronunciation'),
        child: ThemedBubbleInkWell(
          message: localizations.translate('shared.request'),
          iconType: THEMED_BUBBLE_ICON_TYPE_WORD,
          onTap: () {
            var state = AppStateManager.of(context).state;
            if (state.isUserLogged) {
              var arguments = AddWordArguments(
                word: widget.searchTerm,
              );
              Navigator.pushNamed(
                context,
                RouteConstants.ADD_WORD,
                arguments: arguments,
              );
            } else {
              Navigator.of(context).pushNamed(RouteConstants.LOGIN);
            }
          },
        ),
      ),
    );
  }
}
