import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/non_pronounced_language_message.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button/record_button.dart';

class StandardPronunciationItem extends StatelessWidget {
  final Word word;
  final Function(Word, int) onTap;
  final String languageCode;
  final String languageAccentCode;
  final Function requestPronunciationCallback;

  StandardPronunciationItem({
    @required this.word,
    @required this.onTap,
    @required this.languageCode,
    this.languageAccentCode,
    this.requestPronunciationCallback,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    Widget _getPronunciationDataColumn() {
      List<Widget> widgets = [];
      widgets.add(
        Text(
          word.original,
          style: themeData.textTheme.bodyText2,
        ),
      );

      if (word.alternativeSpellings != null &&
          word.alternativeSpellings.isNotEmpty) {
        for (WordSpelling spelling in word.alternativeSpellings) {
          if (spelling.spelling.isNotEmpty) {
            widgets.add(
              Text(
                spelling.spelling,
                style: themeData.textTheme.bodyText2,
              ),
            );
          }
        }
      }

      widgets.add(
        Text(
          localizations.translate(
            'shared.pronunciationsNumber',
            params: {'pronunciations': '${word.numPronunciations.toString()}'},
          ),
          style: themeData.textTheme.subtitle1,
        ),
      );
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      );
    }

    return word.isPendingPronunciation != null &&
            word.isPendingPronunciation &&
            word.hasPronunciationRequest != null &&
            !word.hasPronunciationRequest
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () => onTap(word, null),
                  child: Row(
                    children: [
                      Expanded(
                        child: _getPronunciationDataColumn(),
                      ),
                      Icon(Icons.arrow_forward_ios),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 30.0),
                  child: NonPronouncedLanguageMessage(
                    languageAccentCode: languageAccentCode,
                  ),
                ),
              ],
            ),
          )
        : InkWell(
            onTap: () => onTap(word, null),
            child: Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Row(
                children: [
                  word.standardPronunciation != null
                      ? PlayButton(
                          id: word.standardPronunciation.id,
                          url: word.standardPronunciation.audioRealMp3,
                          folderPath: word.standardPronunciation.languageCode,
                        )
                      : RecordButton(),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: _getPronunciationDataColumn(),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Icon(Icons.arrow_forward_ios),
                ],
              ),
            ),
          );
  }
}
