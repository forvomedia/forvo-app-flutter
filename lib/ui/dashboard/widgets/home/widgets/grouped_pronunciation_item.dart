import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/non_pronounced_language_message.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button/record_button.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_categorization_information_icon.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class GroupedPronunciationItem extends StatelessWidget {
  final Word word;
  final Function(Word, int) onTap;
  final String languageCode;
  final String languageAccentCode;
  final Function requestPronunciationCallback;

  GroupedPronunciationItem({
    @required this.word,
    @required this.onTap,
    @required this.languageCode,
    this.languageAccentCode,
    this.requestPronunciationCallback,
  });

  @override
  Widget build(BuildContext context) {
    {
      var themeData = Theme.of(context);
      var localizations = AppLocalizations.of(context);
      var state = AppStateManager.of(context).state;

      Widget _getPronunciationDataColumn(
          WordCategorizationGroup categorizationGroup) {
        List<Widget> widgets = [];

        if (categorizationGroup.minimumCategories != null &&
            categorizationGroup.minimumCategories.isNotEmpty) {
          int numPronunciations = categorizationGroup.numPronunciations;
          for (WordCategory category in categorizationGroup.minimumCategories) {
            for (WordCategorySpelling categorySpelling in category.data) {
              String categoryName =
                  word.languageCode == state.locale.languageCode
                      ? categorySpelling.nativeCategoryName
                      : categorySpelling.englishCategoryName;
              widgets.add(
                HtmlTagTextFormatted(
                  text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
                  openTag: '<b>',
                  closeTag: '</b>',
                  boldTextStyle: themeData.textTheme.bodyText2,
                  normalTextStyle: themeData.textTheme.subtitle1,
                ),
              );
            }
          }
          widgets.add(
            Text(
              localizations.translate('shared.pronunciationsNumber', params: {
                'pronunciations': '${numPronunciations.toString()}'
              }),
              style: themeData.textTheme.subtitle1,
            ),
          );
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        );
      }

      List<Widget> wordWidgets = [];
      for (WordCategorizationGroup categorizationGroup
          in word.categorizationGroups) {
        bool showPronunciationRequest = word
                .groupedPronunciations[categorizationGroup.groupIndex]
                .isPendingPronunciation &&
            !word.groupedPronunciations[categorizationGroup.groupIndex]
                .hasPronunciationRequest;

        if (showPronunciationRequest) {
          wordWidgets.add(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () => onTap(word, categorizationGroup.groupIndex),
                  child: Row(
                    children: [
                      Expanded(
                        child: _getPronunciationDataColumn(categorizationGroup),
                      ),
                      Icon(Icons.arrow_forward_ios),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 30.0),
                  child: NonPronouncedLanguageMessage(
                    languageAccentCode: languageAccentCode,
                  ),
                ),
              ],
            ),
          );
        } else {
          wordWidgets.add(
            Padding(
              padding: EdgeInsets.only(
                  top: word.categorizationGroups.first == categorizationGroup
                      ? 0.0
                      : 8.0),
              child: InkWell(
                onTap: () => onTap(word, categorizationGroup.groupIndex),
                child: Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      word.groupedPronunciations != null &&
                              word.groupedPronunciations.isNotEmpty &&
                              word.groupedPronunciations[
                                      categorizationGroup.groupIndex] !=
                                  null &&
                              word
                                      .groupedPronunciations[
                                          categorizationGroup.groupIndex]
                                      .pronunciation !=
                                  null
                          ? PlayButton(
                              id: word
                                  .groupedPronunciations[
                                      categorizationGroup.groupIndex]
                                  .pronunciation
                                  .id,
                              url: word
                                  .groupedPronunciations[
                                      categorizationGroup.groupIndex]
                                  .pronunciation
                                  .audioRealMp3,
                              folderPath: word
                                  .groupedPronunciations[
                                      categorizationGroup.groupIndex]
                                  .pronunciation
                                  .languageCode,
                            )
                          : RecordButton(),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: _getPronunciationDataColumn(
                                    categorizationGroup),
                              ),
                              categorizationGroup.showFullInformation
                                  ? PronunciationCategorizationInformationIcon(
                                      categorizationGroup: categorizationGroup,
                                      languageCode: word.languageCode,
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                      Icon(Icons.arrow_forward_ios),
                    ],
                  ),
                ),
              ),
            ),
          );
        }
        if (word.categorizationGroups.last != categorizationGroup) {
          wordWidgets.add(Divider());
        }
      }

      return Column(
        children: wordWidgets,
      );
    }
  }
}
