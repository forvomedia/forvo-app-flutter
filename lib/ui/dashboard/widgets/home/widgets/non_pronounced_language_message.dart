import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';

class NonPronouncedLanguageMessage extends StatelessWidget {
  final String languageAccentCode;

  NonPronouncedLanguageMessage({
    this.languageAccentCode,
  });

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          languageAccentCode != null
              ? localizations
                  .translate('word.languageAccentWithoutPronunciations')
              : localizations.translate('word.languageWithoutPronunciations'),
          style: themeData.textTheme.bodyText2.copyWith(
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
