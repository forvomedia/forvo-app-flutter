import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';

class HomeSearchHistory extends StatelessWidget {
  final List<String> historyWords;
  final Function(String) onTap;

  HomeSearchHistory({
    @required this.historyWords,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    Iterable<Widget> _getHistoryWordTiles() => historyWords.map(
          (item) => ListTile(
            contentPadding: const EdgeInsets.only(top: 4.0),
            title: Text(
              item,
              style: themeData.textTheme.bodyText2,
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () => onTap(item),
          ),
        );

    return Padding(
      padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Center(
              child: ListView(
                shrinkWrap: historyWords.isEmpty,
                children: historyWords.isEmpty
                    ? [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            localizations.translate('home.search.emptyHistory'),
                            textAlign: TextAlign.center,
                            style: themeData.textTheme.subtitle1,
                          ),
                        ),
                      ]
                    : ListTile.divideTiles(
                        context: context,
                        color: themeData.dividerColor,
                        tiles: _getHistoryWordTiles(),
                      ).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
