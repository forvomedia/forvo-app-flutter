import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/search_constants.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/add_word.dart';
import 'package:forvo/ui/widgets/suggestion/blue_bubble_suggestion_message.dart';

class HomeSearchNoResults extends StatelessWidget {
  final String searchTerm;
  final int searchMode;

  HomeSearchNoResults({
    this.searchTerm,
    this.searchMode,
  });

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          localizations.translate('home.search.noResults'),
          style: themeData.textTheme.subtitle1,
          textAlign: TextAlign.center,
        ),
        searchMode != null &&
                searchMode == SearchConstants.SEARCH_MODE_PRONUNCIATION &&
                searchTerm != null &&
                searchTerm.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: BlueBubbleSuggestionMessage(
                  message: localizations.translate(
                    'home.search.suggestionAddWord',
                    params: {
                      'termToAdd': searchTerm,
                    },
                  ),
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      RouteConstants.ADD_WORD,
                      arguments: AddWordArguments(
                        word: searchTerm,
                      ),
                    );
                  },
                  secondaryIcon: Icon(
                    ForvoIcons.addWord,
                    size: 25,
                    color: colorBlue,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
