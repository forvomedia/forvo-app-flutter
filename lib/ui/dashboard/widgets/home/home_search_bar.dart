import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/search_constants.dart';
import 'package:forvo/model/translation_language_pair.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/util/theme_util.dart';

class HomeSearchBar extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final TextEditingController searchController;
  final int searchMode;
  final TranslationLanguagePair selectedTranslationLanguagePair;
  final String searchTerm;
  final Function(String) onSearch;
  final Function onSearchModeMenuOpen;
  final Function onClearSearchText;

  HomeSearchBar({
    @required this.formKey,
    @required this.searchController,
    @required this.searchMode,
    @required this.selectedTranslationLanguagePair,
    @required this.searchTerm,
    @required this.onSearch,
    @required this.onSearchModeMenuOpen,
    @required this.onClearSearchText,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    return Container(
      color: themeData.appBarTheme.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
          vertical: 8.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Form(
                key: formKey,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    color: themeIsDark(context)
                        ? themeData.inputDecorationTheme.border.borderSide.color
                        : colorLightThemeLightGrey,
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () => onSearch(searchTerm),
                      ),
                      Expanded(
                        child: TextFormField(
                          autocorrect: true,
                          controller: searchController,
                          textInputAction: TextInputAction.search,
                          onFieldSubmitted: onSearch,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: searchMode ==
                                    SearchConstants.SEARCH_MODE_PRONUNCIATION
                                ? localizations
                                    .translate('home.search.pronunciation')
                                : localizations
                                    .translate('home.search.translation'),
                            hintStyle: themeData.textTheme.subtitle1,
                            isDense: true,
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            fillColor: themeIsDark(context)
                                ? themeData.inputDecorationTheme.border
                                    .borderSide.color
                                : colorLightThemeLightGrey,
                          ),
                          style: themeData.textTheme.bodyText2.copyWith(
                            fontStyle: FontStyle.normal,
                          ),
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return localizations
                                  .translate('error.shared.requiredField');
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      searchTerm?.isNotEmpty ?? false
                          ? IconButton(
                              icon: Icon(Icons.cancel),
                              onPressed: onClearSearchText,
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4.0),
            ),
            Expanded(
              flex: 0,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: themeIsDark(context)
                      ? themeData.inputDecorationTheme.border.borderSide.color
                      : colorLightThemeLightGrey,
                ),
                child: Row(
                  children: [
                    searchMode == SearchConstants.SEARCH_MODE_TRANSLATION
                        ? Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(selectedTranslationLanguagePair.codes),
                          )
                        : Container(),
                    IconButton(
                      icon: Icon(Icons.filter_list),
                      onPressed: onSearchModeMenuOpen,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
