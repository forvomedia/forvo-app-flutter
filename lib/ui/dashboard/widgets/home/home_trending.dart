import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/grouped_pronunciation_item.dart';
import 'package:forvo/ui/dashboard/widgets/home/widgets/standard_pronunciation_item.dart';
import 'package:forvo/ui/word/word_detail.dart';

class HomeTrending extends StatelessWidget {
  final List<Word> trendingWords;
  final Function(String) onTap;

  HomeTrending({
    @required this.trendingWords,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    _goToWord(Word word, int categorizationGroupIndex) {
      String languageName;
      String languageAccentName;

      WordDetailsArguments arguments = WordDetailsArguments(
        wordOriginal: word.original,
        languageName: languageName,
        languageAccentName: languageAccentName,
        categorizationGroupIndex: categorizationGroupIndex,
      );

      Navigator.pushNamed(
        context,
        RouteConstants.WORD,
        arguments: arguments,
      );
    }

    Widget _getStandardPronunciationTile(Word word) =>
        StandardPronunciationItem(
          word: word,
          onTap: _goToWord,
          languageCode: word.languageCode,
        );

    Widget _getGroupedPronunciationTile(Word word) => GroupedPronunciationItem(
          word: word,
          onTap: _goToWord,
          languageCode: word.languageCode,
        );

    Widget _getTrendingContentView(List<Word> trendContent) {
      List<Widget> widgets = [];

      for (Word word in trendContent) {
        Widget widget;
        if (word.hasCategorizationData) {
          widget = _getGroupedPronunciationTile(word);
        } else {
          widget = _getStandardPronunciationTile(word);
        }
        widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0),
            child: widget,
          ),
        );
        widgets.add(Divider());
      }

      return Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        ),
      );
    }

    Widget _getContentView(List<Word> items) => Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: Center(
            child: ListView(
              shrinkWrap: items.isEmpty,
              children: items.isEmpty
                  ? [
                      Text(
                        localizations.translate('error.service.title'),
                        textAlign: TextAlign.center,
                        style: themeData.textTheme.subtitle1,
                      ),
                    ]
                  : [
                      _getTrendingContentView(items),
                    ],
            ),
          ),
        );

    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: _getContentView(trendingWords),
        ),
      ],
    );
  }
}
