import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/search_constants.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/add_word.dart';
import 'package:forvo/ui/widgets/suggestion/blue_bubble_suggestion_message.dart';

class HomeSearchNoExactMatch extends StatelessWidget {
  final String searchTerm;
  final Language language;
  final int searchMode;
  final String translation;

  HomeSearchNoExactMatch({
    @required this.searchTerm,
    this.language,
    this.searchMode,
    this.translation,
  });

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    String termToAdd = '';
    if (searchMode == SearchConstants.SEARCH_MODE_PRONUNCIATION &&
        searchTerm != null &&
        searchTerm.isNotEmpty) {
      termToAdd = searchTerm;
    } else if (searchMode == SearchConstants.SEARCH_MODE_TRANSLATION &&
        translation != null &&
        translation.isNotEmpty) {
      termToAdd = translation;
    }

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          searchMode == SearchConstants.SEARCH_MODE_TRANSLATION &&
                  translation.isNotEmpty
              ? localizations
                  .translate('home.search.noExactMatchButTranslation')
              : localizations.translate('home.search.noExactMatch'),
          style: themeData.textTheme.subtitle1,
          textAlign: TextAlign.center,
        ),
        searchMode == SearchConstants.SEARCH_MODE_TRANSLATION &&
                translation.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Text(
                  translation,
                  style: themeData.textTheme.bodyText2,
                  textAlign: TextAlign.left,
                ),
              )
            : Container(),
        termToAdd.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: BlueBubbleSuggestionMessage(
                  message: localizations.translate(
                    'home.search.suggestionAddWord',
                    params: {
                      'termToAdd': termToAdd,
                    },
                  ),
                  onTap: () {
                    var arguments = AddWordArguments(
                      word: termToAdd,
                    );
                    if (language != null) {
                      arguments = AddWordArguments(
                        word: termToAdd,
                        language: language,
                      );
                    }
                    Navigator.pushNamed(
                      context,
                      RouteConstants.ADD_WORD,
                      arguments: arguments,
                    );
                  },
                  secondaryIcon: Icon(
                    ForvoIcons.addWord,
                    size: 25,
                    color: colorBlue,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
