import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_icon_item.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/app_review_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:forvo/util/user_util.dart';

const int HEADER_SECTION_PRONOUNCE = 0;
const int HEADER_SECTION_ADD = 1;
const int HEADER_SECTION_MESSAGES = 2;

class HeaderAppBar extends AppBar {
  HeaderAppBar({
    @required BuildContext context,
    @required AppLocalizations localizations,
    int currentIndex,
    String title,
    bool centerTitle,
    bool hideActions,
    List<Widget> headerActions,
    Key key,
  }) : super(
          key: key,
          title: Text(
            title ??= '',
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                  fontSize: 16,
                ),
          ),
          centerTitle: centerTitle ?? true,
          foregroundColor: themeIsDark(context) ? colorWhite : colorBlack,
          actions: hideActions
              ? null
              : headerActions != null
                  ? headerActions
                  : <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            HeaderAppIconItem(
                              icon: Icon(
                                ForvoIcons.mic,
                                color: themeIsDark(context)
                                    ? colorWhite
                                    : colorBlack,
                                size: 20,
                              ),
                              activeIcon: Icon(
                                ForvoIcons.micActive,
                                color: colorBlue,
                                size: 20,
                              ),
                              text: localizations
                                  .translate('home.appBar.pronounceButton'),
                              active: currentIndex != null &&
                                  currentIndex == HEADER_SECTION_PRONOUNCE,
                              onTap: currentIndex != null &&
                                      currentIndex == HEADER_SECTION_PRONOUNCE
                                  ? null
                                  : () async {
                                      var state =
                                          AppStateManager.of(context).state;
                                      bool isUserLogged =
                                          state?.isUserLogged ?? false;
                                      bool isUserContributor = state
                                              ?.userInfo?.user?.isContributor ??
                                          false;
                                      bool result = await showAppReviewMessage(
                                          isUserLogged);
                                      if (result) {
                                        showAlertDialog(context);
                                      } else if (isUserContributor) {
                                        Navigator.of(context)
                                            .pushNamedAndRemoveUntil(
                                          RouteConstants.PRONOUNCE,
                                          ModalRoute.withName(
                                              RouteConstants.START),
                                        );
                                      } else {
                                        showLinguisticProfileAlertDialog(
                                            context);
                                      }
                                    },
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: HeaderAppIconItem(
                                icon: Icon(
                                  ForvoIcons.addWord,
                                  color: themeIsDark(context)
                                      ? colorWhite
                                      : colorBlack,
                                  size: 20,
                                ),
                                activeIcon: Icon(
                                  ForvoIcons.addWord,
                                  color: colorBlue,
                                  size: 20,
                                ),
                                text: localizations
                                    .translate('home.appBar.addWordButton'),
                                active: currentIndex != null &&
                                    currentIndex == HEADER_SECTION_ADD,
                                onTap: currentIndex != null &&
                                        currentIndex == HEADER_SECTION_ADD
                                    ? null
                                    : () async {
                                        var state =
                                            AppStateManager.of(context).state;
                                        bool isUserLogged =
                                            state?.isUserLogged ?? false;
                                        bool result =
                                            await showAppReviewMessage(
                                                isUserLogged);
                                        if (result) {
                                          showAlertDialog(context);
                                        } else {
                                          Navigator.of(context)
                                              .pushNamedAndRemoveUntil(
                                            RouteConstants.ADD_WORD,
                                            ModalRoute.withName(
                                                RouteConstants.START),
                                          );
                                        }
                                      },
                              ),
                            ),
                            HeaderAppIconItem(
                              icon: Icon(
                                ForvoIcons.chat,
                                color: themeIsDark(context)
                                    ? colorWhite
                                    : colorBlack,
                                size: 20,
                              ),
                              activeIcon: Icon(
                                ForvoIcons.chatActive,
                                color: colorBlue,
                                size: 20,
                              ),
                              text: localizations.translate('shared.messages'),
                              active: currentIndex != null &&
                                  currentIndex == HEADER_SECTION_MESSAGES,
                              onTap: currentIndex != null &&
                                      currentIndex == HEADER_SECTION_MESSAGES
                                  ? null
                                  : () async {
                                      var state =
                                          AppStateManager.of(context).state;
                                      bool isUserLogged =
                                          state?.isUserLogged ?? false;
                                      bool result = await showAppReviewMessage(
                                          isUserLogged);
                                      if (result) {
                                        showAlertDialog(context);
                                      } else {
                                        Navigator.of(context)
                                            .pushNamedAndRemoveUntil(
                                          RouteConstants.CHAT_LIST,
                                          ModalRoute.withName(
                                              RouteConstants.START),
                                        );
                                      }
                                    },
                            ),
                          ],
                        ),
                      ),
                    ],
        );
}
