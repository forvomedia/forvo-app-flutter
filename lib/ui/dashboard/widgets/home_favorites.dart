import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/pronunciation_modal_menu.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_word_original_data.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/word/word_detail.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class HomeFavorites extends StatefulWidget {
  final String sectionName;
  final bool isUserLogged;

  HomeFavorites({this.sectionName, this.isUserLogged});

  @override
  _HomeFavoritesState createState() => _HomeFavoritesState();
}

class _HomeFavoritesState extends State<HomeFavorites> {
  ScrollController _scrollController = ScrollController();
  Map<String, LanguageWithPronunciations> _favoritePronunciations = {};
  List<Language> _languages;
  Map _pages = {};
  int _selectedLanguageIndex = 0;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  bool _isLoading = false;
  bool _isPaginationLoading = false;
  bool _isLanguageListInitialized = false;

  void _fetchData({bool forceFetch = false}) async {
    bool isLanguageListInitialized = _isLanguageListInitialized;
    if (forceFetch) {
      isLanguageListInitialized = false;
    }

    setState(() {
      _isLoading = true;
      _isLanguageListInitialized = isLanguageListInitialized;
    });
    var state = AppStateManager.of(context).state;
    try {
      LanguageWithPronunciations pronunciations;
      if (state.userInfo != null && state.locale != null) {
        String langCode;

        if (forceFetch || _languages == null || _languages.isEmpty) {
          if (forceFetch) {
            _languages.clear();
            _favoritePronunciations.clear();
            _pages.clear();
          }

          _languages = await getUserFavoritePronunciationLangs(
            context,
            username: state.userInfo.user.username,
            interfaceLanguageCode: state.locale.languageCode,
          );
        }

        if (_languages != null && _languages.isNotEmpty) {
          if (_selectedLanguageIndex >= _languages.length) {
            setState(() {
              _selectedLanguageIndex = 0;
            });
          }
          langCode = _languages[_selectedLanguageIndex].code;

          if (forceFetch ||
              _favoritePronunciations == null ||
              _favoritePronunciations.isEmpty ||
              _favoritePronunciations[langCode] == null) {
            pronunciations = await getUserFavoritePronunciationsByLang(
              context,
              username: state.userInfo.user.username,
              languageCode: _languages[_selectedLanguageIndex].code,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code] ?? 1,
            );
          } else {
            pronunciations = _favoritePronunciations[langCode];
          }

          if (_pages == null || _pages.isEmpty) {
            for (var value in _languages) {
              _pages[value.code] = 1;
            }
          }
          if (_favoritePronunciations == null ||
              _favoritePronunciations.isEmpty) {
            for (Language language in _languages) {
              _favoritePronunciations[language.code] = null;
            }
          }
          _favoritePronunciations[langCode] = pronunciations;
        }

        setState(() {
          _failed = false;
          _hasConnection = true;
          _isLoading = false;
          _apiError = false;
          _isLanguageListInitialized = true;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _isLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  void _fetchNewPageData() async {
    setState(() {
      _isPaginationLoading = true;
    });
    var state = AppStateManager.of(context).state;
    try {
      LanguageWithPronunciations pronunciations;
      if (state.userInfo != null && state.locale != null) {
        String langCode;
        if (_languages != null && _languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;

          var data = await Future.wait([
            getUserFavoritePronunciationsByLang(
              context,
              username: state.userInfo.user.username,
              languageCode: langCode,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code],
            ),
          ]);
          pronunciations = data[0];
        } else {
          _languages = await getUserFavoritePronunciationLangs(
            context,
            username: state.userInfo.user.username,
            interfaceLanguageCode: state.locale.languageCode,
          );
          if (_languages.isNotEmpty) {
            pronunciations = await getUserFavoritePronunciationsByLang(
              context,
              username: state.userInfo.user.username,
              languageCode: _languages[_selectedLanguageIndex].code,
              interfaceLanguageCode: state.locale.languageCode,
              page: _pages[_languages[_selectedLanguageIndex].code],
            );
          } else {
            pronunciations = null;
          }
        }
        if (_languages.isNotEmpty) {
          langCode = _languages[_selectedLanguageIndex].code;
          _favoritePronunciations[langCode].items.addAll(pronunciations.items);
        }
        setState(() {
          _failed = false;
          _hasConnection = true;
          _isPaginationLoading = false;
          _apiError = false;
          _isLanguageListInitialized = true;
        });
      } else {
        var localizations = AppLocalizations.of(context);
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      if (_deviceHasConnection) {
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            localizations.translate('error.default'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } else {
        String message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _pages[_languages[_selectedLanguageIndex].code] -= 1;
        _isPaginationLoading = false;
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isLoading) {
      _fetchData();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (!_isLoading) {
        _fetchData();
      }
    });
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (!_isPaginationLoading &&
            _pages[_languages[_selectedLanguageIndex].code] + 1 <=
                _favoritePronunciations[_languages[_selectedLanguageIndex].code]
                    .pages) {
          _pages[_languages[_selectedLanguageIndex].code] += 1;
          _fetchNewPageData();
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Widget _buildProgressIndicator() => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Opacity(
            opacity: _isPaginationLoading ? 1.0 : 00,
            child: CircularProgressIndicator(),
          ),
        ),
      );

  Future<void> _onRefresh() async {
    _fetchData(forceFetch: true);
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    Widget _buildListItem(Pronunciation item) => ListTile(
          contentPadding: const EdgeInsets.only(top: 4.0),
          leading: PlayButton(
            id: item.id,
            url: item.audioRealMp3,
            folderPath: item.languageCode,
          ),
          title: PronunciationWordOriginalData(
            pronunciation: item,
          ),
          subtitle: Text(
            item.isUserGenreFemale
                ? item.countryName?.isNotEmpty ?? false
                    ? localizations.translate(
                        'shared.pronunciationByFemaleFromCountry',
                        params: {'country': '${item.countryName}'},
                      )
                    : localizations.translate('shared.gender.female')
                : item.countryName?.isNotEmpty ?? false
                    ? localizations.translate(
                        'shared.pronunciationByMaleFromCountry',
                        params: {'country': '${item.countryName}'},
                      )
                    : localizations.translate('shared.gender.male'),
            style: themeData.textTheme.subtitle1,
          ),
          trailing: IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () => onPronunciationMenuOpen(
              context,
              item,
              _onRefresh,
              wordInformation: item.wordInformation,
            ),
          ),
          onTap: () {
            WordDetailsArguments arguments = WordDetailsArguments(
              wordOriginal: item.original,
              languageName: item.languageName,
              languageAccentName: item.languageAccentName,
              categorizationGroupIndex: item.pronunciationGroupIndex,
            );

            Navigator.pushNamed(
              context,
              RouteConstants.WORD,
              arguments: arguments,
            );
          },
        );

    Widget _buildList() {
      String languageCode;
      if (_languages.isNotEmpty) {
        languageCode = _languages[_selectedLanguageIndex].code;
      }
      return RefreshIndicator(
        onRefresh: _onRefresh,
        child: !_isLoading &&
                (_languages.isEmpty ||
                    _favoritePronunciations[languageCode].items.isEmpty)
            ? Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      localizations.translate(
                          'account.myFavoritePronunciations.emptyMessage'),
                      textAlign: TextAlign.center,
                      style: themeData.textTheme.subtitle1,
                    ),
                  ),
                ],
              )
            : _isLoading
                ? CustomCircularProgressIndicator()
                : ListView.builder(
                    physics: AlwaysScrollableScrollPhysics(),
                    controller: _scrollController,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                    ),
                    itemCount:
                        _favoritePronunciations[languageCode].items.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      if (index ==
                          _favoritePronunciations[languageCode].items.length) {
                        return _buildProgressIndicator();
                      } else {
                        Pronunciation item =
                            _favoritePronunciations[languageCode].items[index];
                        return _buildListItem(item);
                      }
                    },
                  ),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _isLanguageListInitialized && _languages.isNotEmpty
            ? LanguageButtonBar(
                languages: _languages,
                onPressed: (index) {
                  _selectedLanguageIndex = index;
                  _fetchData();
                  _scrollController
                      .jumpTo(_scrollController.position.minScrollExtent);
                },
                selectedIndex: _selectedLanguageIndex,
              )
            : Container(),
        Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                localizations
                    .translate('account.myFavoritePronunciations.title'),
                style: themeData.textTheme.headline6,
              ),
              Divider(color: themeData.dividerColor),
            ],
          ),
        ),
        Expanded(
          child: Center(
            child: _isLoading && !_isLanguageListInitialized
                ? CustomCircularProgressIndicator()
                : _failed
                    ? _buildErrorMessage()
                    : _buildList(),
          ),
        ),
      ],
    );
  }
}
