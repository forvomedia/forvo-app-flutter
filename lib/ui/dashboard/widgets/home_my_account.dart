import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/auth_api_service.dart';
import 'package:forvo/ui/account/public_profile.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';

class HomeMyAccount extends StatelessWidget {
  final String sectionName;
  final bool isUserLogged;

  HomeMyAccount({
    this.sectionName,
    this.isUserLogged,
  });

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    logOut() async {
      try {
        await logout(context);
      } on Exception catch (_) {
        return;
      } finally {
        Navigator.of(context).pushNamedAndRemoveUntil(
            RouteConstants.HOME, (Route<dynamic> route) => false);
      }
    }

    var menuItems = [
      {
        'icon': ForvoIcons.userActive,
        'title': localizations.translate('account.basicInfo'),
        'onTap': () =>
            Navigator.pushNamed(context, RouteConstants.ACCOUNT_INFO),
      },
      {
        'icon': ForvoIcons.publicProfile,
        'title': localizations.translate('account.publicProfile'),
        'onTap': () => state.userInfo != null
            ? Navigator.pushNamed(
                context,
                RouteConstants.PUBLIC_PROFILE,
                arguments: PublicProfileArguments(
                  username: state.userInfo.user.username,
                ),
              )
            : {
                showCustomSnackBar(
                  context,
                  themedSnackBar(
                    context,
                    localizations.translate('error.default'),
                    type: SNACKBAR_TYPE_ERROR,
                  ),
                )
              },
      },
      {
        'icon': ForvoIcons.favoritesActive,
        'title':
            localizations.translate('account.myFavoritePronunciations.title'),
        'onTap': () => Navigator.pushNamedAndRemoveUntil(context,
            RouteConstants.MY_FAVORITE_PRONUNCIATIONS, (route) => false),
      },
      {
        'icon': ForvoIcons.micActive,
        'title': localizations.translate('account.myOwnPronunciations.title'),
        'onTap': () =>
            Navigator.pushNamed(context, RouteConstants.MY_OWN_PRONUNCIATIONS),
      },
      {
        'icon': ForvoIcons.mic,
        'title':
            localizations.translate('account.myRequestedPronunciations.title'),
        'onTap': () => Navigator.pushNamed(
            context, RouteConstants.MY_REQUESTED_PRONUNCIATIONS),
      },
      {
        'icon': ForvoIcons.followUsers,
        'title': localizations.translate('account.myFollowedUsers.title'),
        'onTap': () =>
            Navigator.pushNamed(context, RouteConstants.FOLLOWED_USERS),
      },
      {
        'icon': ForvoIcons.notifications1,
        'title':
            localizations.translate('account.notificationsAndPrivacy.title'),
        'onTap': () => Navigator.pushNamed(
            context, RouteConstants.NOTIFICATIONS_AND_PRIVACY),
      },
      {
        'icon': ForvoIcons.settings,
        'title': localizations.translate('shared.settings'),
        'onTap': () => Navigator.pushNamed(context, RouteConstants.SETTINGS),
      },
      {
        'icon': ForvoIcons.about,
        'title': localizations.translate('shared.about.about'),
        'onTap': () => Navigator.pushNamed(context, RouteConstants.ABOUT),
      },
      {
        'icon': ForvoIcons.logout,
        'title': localizations.translate('shared.logout'),
        'onTap': logOut,
      },
    ];

    return Container(
      width: getDeviceWidth(context),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              localizations.translate('account.title'),
              style: themeData.textTheme.headline6,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
            ),
            Expanded(
              child: ListView(
                children: ListTile.divideTiles(
                  color: themeData.dividerColor,
                  context: context,
                  tiles: menuItems.map(
                    (item) => ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 5.0, horizontal: 15.0),
                      leading: Icon(
                        item['icon'],
                        color: themeData.textTheme.bodyText2.color,
                      ),
                      title: Text(
                        item['title'],
                        style: themeData.textTheme.bodyText2,
                      ),
                      onTap: item['onTap'],
                    ),
                  ),
                ).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
