import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/model/word_check_alphabet.dart';
import 'package:forvo/model/word_check_exists.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/language_modal_menu.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/suggestion/blue_bubble_suggestion_message.dart';
import 'package:forvo/ui/widgets/text_form_field/clean_text_form_field.dart';
import 'package:forvo/ui/word/widgets/word_categorization_group_data.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';

class AddWordForm extends StatefulWidget {
  final String word;
  final Language language;
  final String buttonText;
  final Function(String, PendingPronounceWord) onPressButton;
  final bool isFormEnabled;

  AddWordForm({
    @required this.buttonText,
    @required this.onPressButton,
    this.word,
    this.language,
    this.isFormEnabled = true,
  });

  @override
  _AddWordFormState createState() => _AddWordFormState();
}

class _AddWordFormState extends State<AddWordForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _wordController = TextEditingController();
  Timer _debounce;
  int _debounceTime = 1300;
  bool _isLoading = false;
  bool _isButtonEnabled = false;
  bool _isButtonLoading = false;
  String _lastSearchedWord;
  String _lastSearchedWordLanguage;
  Language _selectedLanguage;
  LanguageWordInformation _selectedLanguageWordInformation;
  int _selectedCategorizationGroupIndex = 0;
  bool _showIsPersonNameQuestion = false;
  bool _showIsPersonNameCheckbox = false;
  bool _enableIsPersonNameCheckBox = true;
  bool _isPersonName = false;
  bool _showIsPhraseQuestion = false;
  bool _isPhrase;
  WordCheckExists _wordCheckExists;
  WordCheckAlphabet _wordCheckAlphabet;
  FocusNode _focusNode;

  @override
  void initState() {
    _lastSearchedWord = null;
    _selectedLanguage = widget.language;
    _lastSearchedWordLanguage =
        _lastSearchedWord != null && _selectedLanguage != null
            ? '$_lastSearchedWord-${_selectedLanguage.code}'
            : null;
    _wordController = TextEditingController(text: widget.word);
    _wordController.addListener(_onWordChanged);
    if (_wordController.value.text != null) {
      _onWordChanged();
    }
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _focusNode = FocusScope.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _wordController.removeListener(_onWordChanged);
    _wordController.dispose();
    super.dispose();
  }

  bool _isNewSearchWordLanguage() =>
      _wordController.text.isNotEmpty &&
      _selectedLanguage != null &&
      (_lastSearchedWordLanguage == null ||
          (_lastSearchedWordLanguage != null &&
              _lastSearchedWordLanguage !=
                  '${_wordController.text}-${_selectedLanguage.code}'));

  _onWordChanged() {
    if (_debounce?.isActive ?? false) {
      _debounce.cancel();
    }
    if (_wordController.text.isEmpty && mounted) {
      setState(() {
        _isButtonEnabled = false;
        _isPersonName = false;
        _showIsPersonNameCheckbox = false;
        _isPhrase = null;
        _showIsPhraseQuestion = false;
        _wordCheckExists = null;
        _wordCheckAlphabet = null;
        _selectedLanguage = null;
        _selectedLanguageWordInformation = null;
        _selectedCategorizationGroupIndex = 0;
      });
    }
    _debounce = Timer(Duration(milliseconds: _debounceTime), () async {
      if (mounted) {
        if (_wordController.text.isEmpty) {
          _lastSearchedWord = null;
        } else {
          if (_lastSearchedWord == null ||
              (_lastSearchedWord != null &&
                  _wordController.text != _lastSearchedWord)) {
            _focusNode.unfocus();
            _wordCheckAlphabet = null;
            _checkExists();
          } else if (_lastSearchedWord != null &&
              _wordController.text == _lastSearchedWord) {
            if (_isNewSearchWordLanguage()) {
              _focusNode.unfocus();
              _checkAlphabet();
            }
          }
        }
      }
    });
  }

  _onWordSubmit(String value) {
    _checkExists();
  }

  String _onWordValidate(String value) {
    var localizations = AppLocalizations.of(context);
    if (value == null || value.isEmpty) {
      return localizations.translate('error.shared.requiredField');
    }
    if (_wordCheckExists != null &&
        _wordCheckExists.invalidChars != null &&
        _wordCheckExists.invalidChars.isNotEmpty) {
      if (_wordCheckExists.correctedWord.isNotEmpty) {
        Timer(Duration(seconds: 3), () {
          if (mounted) {
            setState(() {
              _wordController.text = _wordCheckExists.correctedWord;
            });
          }
        });
      }
      return localizations.translate('error.addWord.invalidChars');
    }

    if (_wordCheckAlphabet != null) {
      if (_wordCheckAlphabet.statusIsWordForbidden) {
        return localizations.translate(
          'error.addWord.wordForbidden',
          params: {'word': _wordController.text},
        );
      } else if (_wordCheckAlphabet.statusIsInvalidChars) {
        String message =
            localizations.translate('error.addWord.alphabetInvalidChars');
        if (_wordCheckAlphabet.invalidChars != null &&
            _wordCheckAlphabet.invalidChars.isNotEmpty) {
          message =
              // ignore: lines_longer_than_80_chars
              '$message => [${_wordCheckAlphabet.invalidChars.map((String char) => char).toList().join(', ')}]';
        }
        return message;
      } else if (_isPhrase != null) {
        int maxPhraseChars = MAX_PHRASE_CHARS;
        int maxWordChars = MAX_WORD_CHARS;
        if (_wordCheckExists.applyMaxCharsWord != null &&
            _wordCheckExists.applyMaxCharsWord &&
            _wordCheckExists.maxCharsPhrase != null &&
            _wordCheckExists.maxCharsWord != null) {
          maxPhraseChars = _wordCheckExists.maxCharsPhrase;
          maxWordChars = _wordCheckExists.maxCharsWord;
        }
        if (_isPhrase) {
          if (_wordController.text.length > maxPhraseChars) {
            return localizations.translate(
              'error.addWord.maxLength',
              params: {'maxLength': maxPhraseChars},
            );
          }
        } else {
          if (_wordController.text.length > maxWordChars) {
            return localizations.translate(
              'error.addWord.maxLength',
              params: {'maxLength': maxWordChars},
            );
          }
        }
      }
    }
    return null;
  }

  _checkExists() async {
    if (mounted) {
      var state = AppStateManager.of(context).state;
      var localizations = AppLocalizations.of(context);

      setState(() {
        _lastSearchedWord = _wordController.text;
        _isLoading = true;
      });

      try {
        if (_wordController.text.isNotEmpty) {
          WordCheckExists checkExists = await addWordCheckExists(
            context,
            _wordController.text,
            state.locale.languageCode,
          );
          setState(() {
            _wordCheckExists = checkExists;
          });
        }

        if (_formKey.currentState.validate()) {
          // IF WORD NOT EXISTS
          if (!_wordCheckExists.exists) {
            if (_selectedLanguage != null) {
              _checkAlphabet();
            }
          } else {
            int wordLangPhraseCoincidences = 0;
            if (_wordCheckExists.languages != null &&
                _wordCheckExists.languages.isNotEmpty) {
              for (Language lang in _wordCheckExists.languages) {
                if (lang.wordInformation != null &&
                    lang.wordInformation.isPhrase) {
                  wordLangPhraseCoincidences++;
                }
              }
            }

            bool isPersonName = _wordCheckExists.isPersonName;
            bool enableIsPersonNameCheckBox = !isPersonName;

            bool showIsPersonNameQuestion;
            if (wordLangPhraseCoincidences == 0) {
              showIsPersonNameQuestion = true;
            } else {
              showIsPersonNameQuestion = false;
            }

            setState(() {
              _showIsPersonNameCheckbox = showIsPersonNameQuestion;
              _isPersonName = isPersonName;
              _enableIsPersonNameCheckBox = enableIsPersonNameCheckBox;
            });

            // CHECK TEXT AGAINST LANGUAGE ALPHABET
            if (_selectedLanguage != null) {
              int langCoincidences = 0;
              if (_wordCheckExists.languages != null &&
                  _wordCheckExists.languages.isNotEmpty) {
                for (Language lang in _wordCheckExists.languages) {
                  if (lang.code == _selectedLanguage.code) {
                    langCoincidences++;
                  }
                }
              }

              if (langCoincidences == 0) {
                _checkAlphabet();
              }
            }
          }
        } else {
          setState(() {
            _isButtonEnabled = false;
          });
        }
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        if (messages != null) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  _checkAlphabet() async {
    if (_selectedLanguage != null && _wordCheckExists != null) {
      var state = AppStateManager.of(context).state;
      var localizations = AppLocalizations.of(context);

      setState(() {
        _isLoading = true;
        _lastSearchedWordLanguage =
            '${_wordController.text}-${_selectedLanguage.code}';
      });
      try {
        WordCheckAlphabet wordCheckAlphabet = await addWordCheckAlphabet(
          context,
          _wordController.text,
          _selectedLanguage.code,
          state.locale.languageCode,
        );
        setState(() {
          _wordCheckAlphabet = wordCheckAlphabet;
        });

        if (_formKey.currentState.validate()) {
          // STATUS: SPECIAL CHARS => PHRASE
          if (_wordCheckAlphabet.statusIsSpecialChars) {
            setState(() {
              _isPersonName = false;
              _showIsPersonNameQuestion = false;
              _showIsPersonNameCheckbox = false;
              _isPhrase = true;
              _showIsPhraseQuestion = false;
              _isButtonEnabled = true;
            });
          }
          // STATUS: CHECK IS PHRASE
          else if (_wordCheckAlphabet.statusIsCheckIsPhrase) {
            setState(() {
              _isPersonName = false;
              _showIsPersonNameQuestion = false;
              _showIsPersonNameCheckbox = false;
              _isPhrase = null;
              _showIsPhraseQuestion = true;
              _isButtonEnabled = false;
            });
          }
          // STATUS: CHECK IS PERSON NAME
          else if (_wordCheckAlphabet.statusIsCheckIsPersonName) {
            setState(() {
              _isPhrase = null;
              _showIsPhraseQuestion = false;
              _showIsPersonNameQuestion =
                  _wordCheckExists != null && !_wordCheckExists.isPersonName;
              _showIsPersonNameCheckbox =
                  _wordCheckExists != null && _wordCheckExists.isPersonName;
              _isPersonName =
                  _wordCheckExists != null && _wordCheckExists.isPersonName;
              _isButtonEnabled =
                  _wordCheckExists != null && _wordCheckExists.isPersonName;
            });
          }
          // STATUS: MAYBE WORD
          else {
            setState(() {
              _isPhrase = false;
              _isButtonEnabled = true;
            });
          }
        } else {
          setState(() {
            _isPersonName = false;
            _showIsPersonNameCheckbox = false;
            _isPhrase = null;
            _showIsPhraseQuestion = false;
            _isButtonEnabled = false;
          });
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  _onIsPersonNameAnswer(bool value) {
    setState(() {
      _showIsPersonNameQuestion = false;
      _isPersonName = value;
      _showIsPersonNameCheckbox = value;
      _showIsPhraseQuestion = !value;
      _isButtonEnabled = value;
    });
  }

  _onIsPersonNameChange(bool value) {
    setState(() {
      _isPersonName = value;
    });
  }

  _onIsPhraseChange(bool value) {
    setState(() {
      _isPhrase = value;
      _showIsPhraseQuestion = false;
      _showIsPersonNameCheckbox = !_isPhrase;
      _isButtonEnabled = _wordController.text.isNotEmpty &&
          _selectedLanguage != null &&
          _wordCheckExists != null &&
          (_wordCheckExists.invalidChars == null ||
              _wordCheckExists.invalidChars.isEmpty) &&
          (_wordCheckAlphabet != null &&
              (_wordCheckAlphabet.statusIsCheckIsPhrase ||
                  (_wordCheckAlphabet.statusIsCheckIsPersonName &&
                      !_isPersonName))) &&
          _formKey.currentState.validate() &&
          _isPhrase != null;
    });
  }

  _onLanguageMenuOpen() async {
    var state = AppStateManager.of(context).state;
    if (state.languages == null && state.popularLanguages == null) {
      await getLanguagesAndPopularLanguages(
        context,
        state,
        state.locale.languageCode,
      );
    }
    if (state.languages != null &&
        state.languages.isNotEmpty &&
        state.popularLanguages != null &&
        state.popularLanguages.isNotEmpty) {
      onLanguageMenuOpen(
        context,
        languages: state.languages,
        onChangeLanguage: _onChangeLanguage,
        popularLanguages: state.popularLanguages,
        userLanguages: state.userInfo.languages,
        suggestedLanguages: _wordCheckExists?.suggestedLanguages ?? [],
      );
    }
  }

  _onChangeLanguage(Language language) async {
    Navigator.of(context).pop();
    if (mounted) {
      setState(() {
        _selectedLanguage = language;
        _wordCheckAlphabet = null;
      });
      if (_wordController.text.isNotEmpty && _isNewSearchWordLanguage()) {
        _checkAlphabet();
      }
      if (_wordCheckExists != null) {
        setState(() {
          var selectedWordInformationExits = _wordCheckExists.languages
              .firstWhere((language) => language.code == _selectedLanguage.code,
                  orElse: () => null);
          _selectedLanguageWordInformation =
              selectedWordInformationExits != null
                  ? selectedWordInformationExits.wordInformation
                  : selectedWordInformationExits;
        });
      }
    }
  }

  _onSelectGroupIndex(int index) {
    Navigator.pop(context);
    setState(() {
      _selectedCategorizationGroupIndex = index;
    });
  }

  _onPressAddWordButton() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);

    if (_formKey.currentState.validate()) {
      try {
        setState(() {
          _isButtonLoading = true;
        });
        String accentCode;
        if (_selectedLanguage.hasAccents &&
            state.userInfo.accents != null &&
            state.userInfo.accents.isNotEmpty) {
          accentCode = state.userInfo.accents
              .firstWhere(
                  (accent) => accent.languageCode == _selectedLanguage.code,
                  orElse: () => null)
              ?.code;
        }
        PendingPronounceWord pendingPronounceWord = await addModifyWord(
          context,
          word: _wordController.text,
          languageCode: _selectedLanguage.code,
          interfaceLanguageCode: state.locale.languageCode,
          modify: _wordCheckExists.exists,
          isPersonName: _isPersonName,
          isPhrase: _isPhrase,
          accentCode: _selectedLanguage.hasAccents ? accentCode : null,
          hasCategorizationGroup: _selectedLanguageWordInformation != null
              ? _selectedLanguageWordInformation.hasCategorizationData
              : null,
          pronunciationGroupIndex: _selectedCategorizationGroupIndex != null
              ? _selectedCategorizationGroupIndex
              : null,
        );
        widget.onPressButton(_wordController.text, pendingPronounceWord);
      } on ApiException catch (error) {
        String messages = error.getErrorMessages(context);
        if (messages != null) {
          showCustomSnackBar(
            context,
            themedSnackBar(
              context,
              messages,
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      } on Exception catch (_) {
        bool _deviceHasConnection = await deviceHasConnection();
        String message;
        if (_deviceHasConnection) {
          message = '${localizations.translate('error.service.title')}\n'
              '${localizations.translate('error.service.body')}';
        } else {
          message = '${localizations.translate('error.network.title')}\n'
              '${localizations.translate('error.network.body')}';
        }
        showCustomSnackBar(
          context,
          themedSnackBar(
            context,
            message,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      } finally {
        setState(() {
          _isButtonLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: 8.0),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.all(
                const Radius.circular(10),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 12.0),
                  child: Text(
                    localizations.translate(
                      'addWord.wordOrPhrase',
                    ),
                    style: themeData.textTheme.subtitle1,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: CleanTextFormField(
                        themeData: themeData,
                        editingController: _wordController,
                        required: true,
                        onValidate: _onWordValidate,
                        onSubmit: _onWordSubmit,
                      ),
                    ),
                    _isLoading
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: themeData.dividerColor,
                                ),
                              ),
                            ),
                            padding: const EdgeInsets.only(
                              bottom: 27.5,
                            ),
                            child: Container(
                              height: 20.0,
                              width: 20.0,
                              margin: const EdgeInsets.only(
                                right: 20.0,
                              ),
                              child: CustomCircularProgressIndicator(),
                            ),
                          )
                        : (_wordCheckAlphabet != null &&
                                (_wordCheckAlphabet.statusIsInvalidChars ||
                                    _wordCheckAlphabet.statusIsWordForbidden))
                            ? Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                                child: Container(
                                  padding: const EdgeInsets.only(
                                    right: 20.0,
                                    bottom: 23.5,
                                  ),
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.red,
                                  ),
                                ),
                              )
                            : _wordCheckExists != null &&
                                    !_wordCheckExists.exists
                                ? Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: themeData.dividerColor,
                                        ),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        right: 20.0,
                                        bottom: 23.5,
                                      ),
                                      child: Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      ),
                                    ),
                                  )
                                : Container()
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0, left: 12.0),
                  child: Text(
                    localizations.translate(
                      'shared.language',
                    ),
                    style: themeData.textTheme.subtitle1,
                  ),
                ),
                GestureDetector(
                  onTap: widget.isFormEnabled ? _onLanguageMenuOpen : null,
                  child: Container(
                    padding: EdgeInsets.only(left: 12.0, right: 8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(_selectedLanguage != null
                              ? getLanguageNameAndCode(_selectedLanguage)
                              : ''),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.filter_list,
                            color: themeData.iconTheme.color,
                          ),
                          onPressed: widget.isFormEnabled
                              ? _onLanguageMenuOpen
                              : null,
                        )
                      ],
                    ),
                  ),
                ),
                _selectedLanguage != null &&
                        _wordCheckExists != null &&
                        _selectedLanguageWordInformation != null &&
                        _selectedLanguageWordInformation.hasCategorizationData
                    ? Divider()
                    : Container(),
                _selectedLanguage != null &&
                        _wordCheckExists != null &&
                        _selectedLanguageWordInformation != null &&
                        _selectedLanguageWordInformation.hasCategorizationData
                    ? Padding(
                        padding: const EdgeInsets.only(top: 0),
                        child: WordCategorizationGroupData(
                          categorizationGroups: _selectedLanguageWordInformation
                              .categorizationGroups,
                          wordLanguageCode: _selectedLanguage.code,
                          onSelectGroup: _onSelectGroupIndex,
                          groupIndex: _selectedCategorizationGroupIndex,
                          useBottomBorder: false,
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          _showIsPersonNameQuestion
              ? Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          localizations.translate('addWord.addingPersonName'),
                          style: themeData.textTheme.subtitle1,
                        ),
                      ),
                      IntrinsicHeight(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              child: Text(
                                localizations.translate('shared.yes'),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Theme.of(context).primaryColor,
                                onPrimary: themeIsDark(context)
                                    ? colorWhite
                                    : colorBlack,
                              ),
                              onPressed: () => _onIsPersonNameAnswer(true),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20.0),
                            ),
                            ElevatedButton(
                              child: Text(
                                localizations.translate('shared.no'),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Theme.of(context).primaryColor,
                                onPrimary: themeIsDark(context)
                                    ? colorWhite
                                    : colorBlack,
                              ),
                              onPressed: () => _onIsPersonNameAnswer(false),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          _showIsPersonNameCheckbox
              ? GestureDetector(
                  onTap: () => _enableIsPersonNameCheckBox
                      ? _onIsPersonNameChange(!_isPersonName)
                      : null,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Checkbox(
                          value: _isPersonName,
                          onChanged: _enableIsPersonNameCheckBox
                              ? _onIsPersonNameChange
                              : null,
                        ),
                        Expanded(
                          child: Text(
                            localizations.translate('addWord.addingPersonName'),
                            style: themeData.textTheme.subtitle1,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Container(),
          _showIsPhraseQuestion
              ? Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          localizations.translate('addWord.addingPhrase'),
                          style: themeData.textTheme.subtitle1,
                        ),
                      ),
                      IntrinsicHeight(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              child: Text(
                                localizations.translate('shared.yes'),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: _isPhrase != null && _isPhrase
                                    ? colorBlue
                                    : Theme.of(context).primaryColor,
                                onPrimary: themeIsDark(context)
                                    ? colorWhite
                                    : (_isPhrase == null ||
                                            (_isPhrase != null && !_isPhrase))
                                        ? colorBlack
                                        : colorWhite,
                              ),
                              onPressed: () => _onIsPhraseChange(true),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20.0),
                            ),
                            ElevatedButton(
                              child: Text(
                                localizations.translate('shared.no'),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: _isPhrase != null && !_isPhrase
                                    ? colorBlue
                                    : Theme.of(context).primaryColor,
                                onPrimary: themeIsDark(context)
                                    ? colorWhite
                                    : (_isPhrase == null ||
                                            (_isPhrase != null && _isPhrase))
                                        ? colorBlack
                                        : colorWhite,
                              ),
                              onPressed: () => _onIsPhraseChange(false),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : Container(),
          _wordCheckExists != null && _wordCheckExists.exists
              ? Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: BlueBubbleSuggestionMessage(
                    message: localizations.translate(
                      'addWord.wordAlreadyExists',
                      params: {
                        'word': _lastSearchedWord,
                        'language': _wordCheckExists.languages
                            .map(
                              getLanguageNameAndCode,
                            )
                            .toList()
                            .join(', '),
                      },
                    ),
                  ),
                )
              : Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 10.0,
                  ),
                ),
          MainButton(
            caption: widget.buttonText,
            isLoading: _isButtonLoading,
            onPressed: _onPressAddWordButton,
            isEnabled: widget.isFormEnabled &&
                !_isLoading &&
                !_isButtonLoading &&
                _isButtonEnabled,
          )
        ],
      ),
    );
  }
}
