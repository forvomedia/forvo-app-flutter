import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/widgets/add_word_form.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_bar.dart';
import 'package:forvo/ui/dashboard/widgets/signup_login_help.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:url_launcher/url_launcher.dart';

class AddWordArguments {
  final String word;
  final Language language;

  AddWordArguments({
    @required this.word,
    this.language,
  });
}

class AddWord extends StatefulWidget {
  @override
  _AddWordState createState() => _AddWordState();
}

class _AddWordState extends State<AddWord> {
  bool _isUserLogged = false;
  String _word;
  Language _language;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    _extractArguments();
    var state = AppStateManager.of(context).state;
    _isUserLogged = state?.isUserLogged ?? false;
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args = ModalRoute.of(context).settings.arguments as AddWordArguments;
    if (args != null) {
      if (args.word != null && args.word.isNotEmpty) {
        _word = args.word;
      }
      if (args.language != null) {
        _language = args.language;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenanceAddWordActive;

    Widget _getContent() => SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 20.0,
          ),
          child: Container(
            width: getDeviceWidth(context),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _isMaintenanceActive
                          ? Padding(
                              padding: const EdgeInsets.only(bottom: 20.0),
                              child: MaintenanceWarningMessage(),
                            )
                          : Container(),
                      _word == null || (_word != null && _word.isEmpty)
                          ? Text(
                              localizations.translate('addWord.title'),
                              style: themeData.textTheme.headline6,
                            )
                          : Text(
                              localizations
                                  .translate('addWord.titleWordNotEmpty'),
                              style: themeData.textTheme.headline6,
                            ),
                      InkWell(
                        onTap: () async {
                          String url = getAddWordRecommendationsUrl(
                              state.locale.languageCode);
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: false);
                          }
                        },
                        child: HtmlTagTextFormatted(
                          text: _word == null ||
                                  (_word != null && _word.isEmpty)
                              ? localizations.translate('addWord.subtitle')
                              : localizations
                                  .translate('addWord.subtitleWordNotEmpty'),
                          openTag: '<b>',
                          closeTag: '</b>',
                          normalTextStyle:
                              themeData.textTheme.bodyText2.copyWith(
                            fontSize: 16,
                          ),
                          boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                            fontSize: 16,
                            color: colorBlue,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                AddWordForm(
                  word: _word,
                  language: _language,
                  buttonText: localizations.translate('addWord.requestRecord'),
                  isFormEnabled: !_isMaintenanceActive,
                  onPressButton:
                      (String word, PendingPronounceWord pendingPronounceWord) {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteConstants.START,
                      ModalRoute.withName(RouteConstants.START),
                    );
                    showCustomSnackBar(
                      _scaffoldMessengerKey.currentContext,
                      themedSnackBar(
                        context,
                        localizations.translate(
                          'addWord.ok',
                          params: {'word': word},
                        ),
                        type: SNACKBAR_TYPE_OK,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarIndex: HEADER_SECTION_ADD,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_SEARCH,
      bottomNavBarIndexIsActive: false,
      body: _isUserLogged ? _getContent() : SignUpLoginHelp(),
    );
  }
}
