import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/record.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/pending_pronunciation/pending_pronunciation_word_original_data.dart';

class PronounceCommunity extends StatelessWidget {
  final List<PendingPronounceWord> pendingPronounceWords;
  final Function addPronunciationCallback;

  PronounceCommunity({
    @required this.pendingPronounceWords,
    @required this.addPronunciationCallback,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenancePronounceActive;

    Iterable<Widget> getPendingPronounceWordTiles() =>
        pendingPronounceWords.map(
          (item) => ListTile(
            contentPadding: const EdgeInsets.only(top: 4.0),
            title: PendingPronunciationWordOriginalData(
              word: item,
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () async {
              RecordArguments args = RecordArguments(
                word: item,
              );
              var result = await Navigator.pushNamed(
                context,
                RouteConstants.RECORD,
                arguments: args,
              );
              if (result != null) {
                if (addPronunciationCallback != null) {
                  addPronunciationCallback();
                }
              }
            },
          ),
        );

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _isMaintenanceActive
                ? Padding(
                    padding: const EdgeInsets.only(bottom: 15.0),
                    child: MaintenanceWarningMessage(),
                  )
                : Container(),
            Text(
              localizations.translate('pronounce.communityRequested'),
              style: themeData.textTheme.headline6.copyWith(fontSize: 20),
            ),
            Expanded(
              child: Center(
                child: ListView(
                  padding: EdgeInsets.only(top: 15.0, bottom: 20.0),
                  physics: AlwaysScrollableScrollPhysics(),
                  children: pendingPronounceWords.isNotEmpty
                      ? ListTile.divideTiles(
                          color: themeData.dividerColor,
                          context: context,
                          tiles: getPendingPronounceWordTiles(),
                        ).toList()
                      : [],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
