import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/pronounce_community.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/pronounce_others.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/record.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_bar.dart';
import 'package:forvo/ui/dashboard/widgets/signup_login_help.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/menu/language_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/language_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class Pronounce extends StatefulWidget {
  @override
  _PronounceState createState() => _PronounceState();
}

class _PronounceState extends State<Pronounce>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  bool _isUserLogged = false;
  bool _initialized = false;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  Language _communitySelectedLanguage;
  Future<List<PendingPronounceWord>> _pendingPronounceWords;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: 2,
      initialIndex: 0,
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    var state = AppStateManager.of(context).state;
    _isUserLogged = state?.isUserLogged ?? false;
    if (_communitySelectedLanguage == null) {
      if (_isUserLogged) {
        Language userMainLanguage = Language(
          code: state.userInfo.user.languageCode,
          name: state.userInfo.user.languageName,
        );
        _communitySelectedLanguage = userMainLanguage;
      }
    }

    _fetchPendingPronounceWords();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  _fetchPendingPronounceWords() async {
    if (!_initialized && _isUserLogged) {
      var state = AppStateManager.of(context).state;
      _pendingPronounceWords = getPendingPronounceWords(
        context,
        languageCode: _communitySelectedLanguage.code,
        interfaceLanguageCode: state.locale.languageCode,
      );
      setState(() {
        _initialized = true;
        _failed = false;
        _hasConnection = true;
        _apiError = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    Future<void> _onRefresh() async {
      _initialized = false;
      _failed = false;
      _hasConnection = true;
      _apiError = false;
      await _fetchPendingPronounceWords();
      setState(() {});
    }

    _onChangeCommunityLanguage(Language language) async {
      Navigator.of(context).pop();
      setState(() {
        _initialized = false;
        _failed = false;
        _hasConnection = true;
        _apiError = false;
        _communitySelectedLanguage = language;
      });
      await _fetchPendingPronounceWords();
    }

    _onCommunityLanguageMenuOpen() async {
      if (state.languages == null && state.popularLanguages == null) {
        await getLanguagesAndPopularLanguages(
          _scaffoldMessengerKey.currentContext,
          state,
          state.locale.languageCode,
        );
      }
      if (state.languages != null &&
          state.languages.isNotEmpty &&
          state.popularLanguages != null &&
          state.popularLanguages.isNotEmpty) {
        onLanguageMenuOpen(
          context,
          languages: state.languages,
          onChangeLanguage: _onChangeCommunityLanguage,
          popularLanguages: state.popularLanguages,
          userLanguages: state.userInfo.languages,
        );
      }
    }

    _onAddPronunciation() {
      _initialized = false;
      _failed = false;
      _hasConnection = true;
      _apiError = false;
      _fetchPendingPronounceWords();
    }

    Widget _buildErrorMessage({message}) {
      if (_failed) {
        if (_apiError) {
          return ServiceError(_onRefresh, message: message);
        } else if (_hasConnection) {
          return ServiceError(_onRefresh);
        } else {
          return NetworkError(_onRefresh);
        }
      } else {
        return Container();
      }
    }

    Widget _getCommunityView() => RefreshIndicator(
          onRefresh: _onRefresh,
          child: FutureBuilder(
            future: _pendingPronounceWords,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.done) {
                return PronounceCommunity(
                  pendingPronounceWords: snapshot.data,
                  addPronunciationCallback: _onAddPronunciation,
                );
              } else if (snapshot.hasError) {
                _failed = true;
                if (snapshot.error is ApiException) {
                  String messages = (snapshot.error as ApiException)
                      .getErrorMessages(context);
                  _apiError = true;
                  return _buildErrorMessage(message: messages);
                }
                if (snapshot.error is NotConnectedException) {
                  _hasConnection = false;
                } else {
                  _hasConnection = true;
                }
                return _buildErrorMessage();
              }
              return CustomCircularProgressIndicator();
            },
          ),
        );

    Widget _getOthersView() => PronounceOthers(
        buttonText: localizations.translate('shared.continue'),
        onPressButton:
            (String word, PendingPronounceWord pendingPronounceWord) async {
          if (state.userInfo.languages.any((language) =>
              language.code == pendingPronounceWord.languageCode)) {
            var result = await Navigator.pushNamed(
              context,
              RouteConstants.RECORD,
              arguments: RecordArguments(
                word: pendingPronounceWord,
              ),
            );
            if (result != null) {
              Navigator.pushNamedAndRemoveUntil(
                context,
                RouteConstants.START,
                (route) => false,
              );
            }
          } else {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                localizations.translate(
                  'error.record.noNativeLanguage',
                  params: {
                    'language': pendingPronounceWord.languageName,
                  },
                ),
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        });

    final _tabs = <Tab>[
      Tab(text: localizations.translate('pronounce.tabs.community')),
      Tab(text: localizations.translate('pronounce.tabs.others')),
    ];

    final _tabPages = <Widget>[
      _getCommunityView(),
      _getOthersView(),
    ];

    Widget _getContent() => Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: themeData.appBarTheme.backgroundColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 8.0,
                ),
                child: GestureDetector(
                  onTap: _onCommunityLanguageMenuOpen,
                  child: Container(
                    padding: const EdgeInsets.only(left: 16.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: themeIsDark(context)
                          ? themeData
                              .inputDecorationTheme.border.borderSide.color
                          : colorLightThemeLightGrey,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(_communitySelectedLanguage != null
                            ? getLanguageNameAndCode(_communitySelectedLanguage)
                            : ''),
                        IconButton(
                          icon: Icon(Icons.filter_list),
                          onPressed: _onCommunityLanguageMenuOpen,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              height: 0,
              thickness: 1,
            ),
            Card(
              margin: EdgeInsets.all(0.0),
              color: themeData.appBarTheme.backgroundColor,
              elevation: 3.0,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(4.0),
                  bottomRight: Radius.circular(4.0),
                ),
              ),
              child: TabBar(
                controller: _tabController,
                indicatorColor: colorBlue,
                indicatorWeight: 3.0,
                tabs: _tabs,
                labelStyle: themeData.textTheme.bodyText2.copyWith(
                  fontWeight: FontWeight.bold,
                ),
                labelColor: colorBlue,
                unselectedLabelStyle: themeData.textTheme.bodyText2,
                unselectedLabelColor: themeData.textTheme.bodyText2.color,
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: _tabPages,
              ),
            ),
          ],
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarIndex: HEADER_SECTION_PRONOUNCE,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_SEARCH,
      bottomNavBarIndexIsActive: false,
      body: _isUserLogged ? _getContent() : SignUpLoginHelp(),
    );
  }
}
