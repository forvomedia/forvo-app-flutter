import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/widgets/add_word_form.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';

class PronounceOthers extends StatelessWidget {
  final Function(String, PendingPronounceWord) onPressButton;
  final String buttonText;

  PronounceOthers({
    @required this.onPressButton,
    @required this.buttonText,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenancePronounceActive;

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Center(
                child: ListView(
                  padding: EdgeInsets.only(bottom: 20.0),
                  physics: AlwaysScrollableScrollPhysics(),
                  children: [
                    _isMaintenanceActive
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 15.0),
                            child: MaintenanceWarningMessage(),
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Text(
                        localizations.translate('pronounce.othersRequested'),
                        style: themeData.textTheme.headline6
                            .copyWith(fontSize: 20),
                      ),
                    ),
                    AddWordForm(
                      buttonText: localizations.translate('shared.continue'),
                      onPressButton: onPressButton,
                      isFormEnabled: !_isMaintenanceActive,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
