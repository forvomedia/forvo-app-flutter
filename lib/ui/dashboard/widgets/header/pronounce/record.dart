import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/device_constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_bar.dart';
import 'package:forvo/ui/dashboard/widgets/signup_login_help.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/main_button.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/pending_pronunciation/pending_pronunciation_word_original_data.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/file_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:logger/logger.dart' show Level;
import 'package:permission_handler/permission_handler.dart';

class RecordArguments {
  final PendingPronounceWord word;

  RecordArguments({
    @required this.word,
  });
}

class Record extends StatefulWidget {
  @override
  _RecordState createState() => _RecordState();
}

const double TWO_PI = 3.14 * 2;
const double size = 100;
const int WORD_RECORDER_DURATION = 2500;
const int PHRASE_RECORDER_DURATION = 5000;
const String RECORD_PRONUNCIATION_TEMPORARY_FILE_NAME_PREFIX = 'captura_';
const String RECORD_PRONUNCIATION_FILE_EXTENSION = '.mp3';

class _RecordState extends State<Record> with TickerProviderStateMixin {
  final LocalFileSystem _localFileSystem = LocalFileSystem();
  bool _isUserLogged = false;
  bool _alreadyPronouncedByUser = false;
  PendingPronounceWord _word;
  FlutterSoundRecorder _recording =
      FlutterSoundRecorder(logLevel: Level.nothing);
  File _file;
  bool _isLoading = false;
  bool _isRecording = false;
  bool _isRecordingFinished = false;
  String _recordingPath;
  Animation<double> _animation;
  AnimationController _controller;
  double _animationProgress = 0.0;
  PermissionStatus _microphonePermissionStatus;
  PermissionStatus _storagePermissionStatus;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    openTheRecorder().then((value) {
      setState(() {});
    });

    _controller = AnimationController(
      duration: const Duration(
        milliseconds: WORD_RECORDER_DURATION,
      ),
      vsync: this,
    );
    _animation = Tween<double>(begin: 0, end: 1).animate(_controller)
      ..addListener(_onAnimationProgress);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _extractArguments();
    var state = AppStateManager.of(context).state;
    _isUserLogged = state?.isUserLogged ?? false;
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args = ModalRoute.of(context).settings.arguments as RecordArguments;
    if (args != null) {
      if (args.word != null) {
        _word = args.word;
        if (_word.isPhrase) {
          _controller = AnimationController(
              duration: const Duration(
                milliseconds: PHRASE_RECORDER_DURATION,
              ),
              vsync: this);
          _animation = Tween<double>(begin: 0, end: 1).animate(_controller)
            ..addListener(_onAnimationProgress);
        }
        //TODO: if null, make request
        if (_word.alreadyPronouncedByUser != null ){
          _alreadyPronouncedByUser = _word.alreadyPronouncedByUser;
        }

      }
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _recording.closeAudioSession();
    _recording = null;
    if (_file != null && _file.existsSync()) {
      _file.delete();
    }
    super.dispose();
  }

  Future<void> openTheRecorder() async {
    if (!kIsWeb) {
      var microphonePermissionStatus = PermissionStatus.denied;
      var storagePermissionStatus = PermissionStatus.denied;

      microphonePermissionStatus = await Permission.microphone.request();
      if (microphonePermissionStatus == PermissionStatus.granted) {
        storagePermissionStatus = await Permission.storage.request();
      }

      setState(() {
        _microphonePermissionStatus = microphonePermissionStatus;
        _storagePermissionStatus = storagePermissionStatus;
      });
    }
    await _recording.openAudioSession();
  }

  _onStartRecording() async {
    if (_isRecordingFinished) {
      _controller.reset();
      _file.delete();
      setState(() {
        _isRecordingFinished = false;
      });
    } else {
      try {
        if (await Permission.microphone.isGranted) {
          DateTime now = DateTime.now();

          String _folderPath = DeviceConstants.DEVICE_RECORDS_FOLDER_NAME;
          bool _directoryExists = await directoryExists(_folderPath);

          if (_directoryExists == null || _directoryExists == false) {
            await createDirectory(_folderPath);
          }

          String _fileNamePrefix =
              RECORD_PRONUNCIATION_TEMPORARY_FILE_NAME_PREFIX;
          String _filePath =
              '$_folderPath/$_fileNamePrefix${now.millisecondsSinceEpoch.toString()}';
          String path = await getLocalFilePath(_filePath);

          _recording.startRecorder(
            toFile: path,
          );
          bool isRecording = _recording.isRecording;
          setState(() {
            _isRecordingFinished = false;
            _isRecording = isRecording;
          });
          _controller.forward();
        }
      } on Exception catch (e) {
        print(e);
      }
    }
  }

  _onStopRecording() async {
    _controller.reset();

    _recording.stopRecorder().then((url) {
      _file = _localFileSystem.file(url);
      _file.lengthSync();
      _recordingPath = url;
      bool isRecording = _recording.isRecording;

      setState(() {
        _isRecording = isRecording;
        _isRecordingFinished = true;
      });
    });
  }

  _onAnimationProgress() {
    setState(() {
      _animationProgress = _animation.value;
    });
    if (_animationProgress == 1) {
      _onStopRecording();
    }
  }

  _sendPronunciation() async {
    if (await _file.exists() && await _file.length() > AUDIO_MIN_SIZE) {
      String fileName = _file.path.split('/').last;
      String fileExtension = RECORD_PRONUNCIATION_FILE_EXTENSION;
      fileName = '$fileName$fileExtension';
      try {
        setState(() {
          _isLoading = true;
        });
        bool saveAudioOk;
        try {
          saveAudioOk =
              await saveAudio(context, fileName: fileName, file: _file);
        } on FormatException catch (_) {
          saveAudioOk = await saveAudio(
            context,
            fileName: fileName,
            file: _file,
            forceUseDevEnvironmentBasicAuthentication: true,
          );
        } on Exception catch (_) {
          rethrow;
        }
        if (saveAudioOk) {
          _addPronunciation(fileName);
        }
      } on Exception catch (_) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            AppLocalizations.of(context)
                .translate('error.record.saveAudioFailed'),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          AppLocalizations.of(context)
              .translate('error.record.recordingFailed'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  _addPronunciation(String fileName) async {
    String ip = await getIP();
    try {
      await addPronunciation(
        context,
        word: _word.word,
        languageCode: _word.languageCode,
        audioName: fileName,
        ip: ip,
        pronunciationGroupIndex: _word.pronunciationGroupIndex.toString(),
      );
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          AppLocalizations.of(context).translate('record.addPronunciationOk'),
          type: SNACKBAR_TYPE_OK,
        ),
      );
      _file.delete();
      Navigator.pop(context, 1);
    } on ApiException catch (error) {
      String messages = error.getErrorMessages(context);
      if (messages != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _isLoading = false;
      });
    } on Exception catch (_) {
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          AppLocalizations.of(context)
              .translate('error.record.addPronunciationFailed'),
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      setState(() {
        _isLoading = false;
      });
    }
  }

  bool _isMicrophonePermissionGranted() =>
      _microphonePermissionStatus == PermissionStatus.granted;

  bool _isStoragePermissionGranted() =>
      _storagePermissionStatus == PermissionStatus.granted;

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;
    TextStyle normalTextStyle = themeData.textTheme.bodyText2.copyWith(
      color: colorSnackBarRed,
    );
    TextStyle boldTextStyle = normalTextStyle.copyWith(
      fontWeight: FontWeight.bold,
    );

    bool _isMaintenanceActive =
        state.isMaintenanceReadOnlyActive || state.isMaintenancePronounceActive;

    Widget _getPlayButtonRow() => _isRecordingFinished
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PlayButton(
                  id: null,
                  url: _recordingPath,
                  folderPath: _word.languageCode,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    localizations.translate('record.listenPronunciation'),
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.bodyText2.copyWith(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();

    Widget _getRecorderAnimatedButton(double value) => Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: !_isMaintenanceActive ? _onStartRecording : () => null,
              child: Container(
                width: size,
                height: size,
                child: Stack(
                  children: [
                    ShaderMask(
                      shaderCallback: (rect) => SweepGradient(
                        startAngle: 0.0,
                        endAngle: TWO_PI,
                        stops: [value, value],
                        center: Alignment.center,
                        colors: [
                          Colors.red,
                          themeIsDark(context)
                              ? colorDarkThemeGrey.withAlpha(55)
                              : colorLightThemeGrey.withAlpha(55)
                        ],
                      ).createShader(rect),
                      child: Container(
                        width: size,
                        height: size,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: themeIsDark(context) ? colorWhite : colorWhite,
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        width: size - 10,
                        height: size - 10,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: themeIsDark(context) ? colorBlack : colorWhite,
                        ),
                        padding: EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _isRecording
                                ? Colors.red
                                : Colors.red.withAlpha(150),
                          ),
                          child: Icon(
                            _isRecordingFinished
                                ? Icons.replay
                                : _isRecording
                                    ? Icons.mic
                                    : Icons.mic_none,
                            color: colorWhite,
                            size: 40,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: !_isMaintenanceActive
                  ? Text(
                      localizations.translate(_isRecordingFinished
                          ? 'record.recordAgain'
                          : _isRecording
                              ? 'record.recording'
                              : 'record.pressToRecord'),
                      textAlign: TextAlign.center,
                      style: themeData.textTheme.bodyText2.copyWith(
                        fontSize: 16,
                      ),
                    )
                  : Container(),
            ),
          ],
        );

    Widget _getRecordingMessage() => _isRecordingFinished
        ? Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Text(
              localizations.translate('record.recordEndMessage'),
              textAlign: TextAlign.center,
              style: themeData.textTheme.bodyText2.copyWith(
                color: Colors.red,
                fontSize: 14,
              ),
            ),
          )
        : Container();

    Widget _getAlreadyPronouncedByUserWarning() => _alreadyPronouncedByUser
        ? Padding(
            padding: const EdgeInsets.only(
              bottom: 20.0,
              left: 20.0,
              right: 20.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.info_outline,
                  color: colorSnackBarRed,
                  size: 30,
                ),
                Expanded(
                  child: Text(
                    localizations
                        .translate('record.alreadyPronouncedByUserWarning'),
                    textAlign: TextAlign.center,
                    style: normalTextStyle.copyWith(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();

    Widget _recorderWidgetsColumn() => Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _getAlreadyPronouncedByUserWarning(),
            state.userInfo.languages
                    .any((language) => language.code == _word.languageCode)
                ? _getRecorderAnimatedButton(_animationProgress)
                : Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30.0, vertical: 8.0),
                    child: HtmlTagTextFormatted(
                      text: localizations.translate(
                        'error.record.noNativeLanguage',
                        params: {
                          'language': _word.languageName,
                        },
                      ),
                      openTag: '<b>',
                      closeTag: '</b>',
                      boldTextStyle: boldTextStyle,
                      normalTextStyle: normalTextStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
            _getPlayButtonRow(),
            _getRecordingMessage(),
            state.userInfo.languages
                    .any((language) => language.code == _word.languageCode)
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 30.0),
                    child: MainButton(
                      caption:
                          localizations.translate('record.sendPronunciation'),
                      isEnabled: !_isMaintenanceActive &&
                          !_isLoading &&
                          !_isRecording &&
                          _isRecordingFinished,
                      isLoading: _isLoading,
                      onPressed: _sendPronunciation,
                    ),
                  )
                : Container(),
          ],
        );

    Widget _permissionRequestWidgets() => Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: HtmlTagTextFormatted(
                text:
                    localizations.translate('record.permissionsWarningMessage'),
                openTag: '<b>',
                closeTag: '</b>',
                boldTextStyle: themeData.textTheme.bodyText2,
                normalTextStyle: themeData.textTheme.subtitle1,
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: MainButton(
                onPressed: openTheRecorder,
                caption:
                    localizations.translate('record.permissionsGrantButton'),
              ),
            ),
          ],
        );

    Widget _getContent() => ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _isMaintenanceActive
                      ? Padding(
                          padding: const EdgeInsets.only(
                            left: 20.0,
                            right: 20.0,
                            bottom: 20.0,
                          ),
                          child: MaintenanceWarningMessage(),
                        )
                      : Container(),
                  state.userInfo.languages.any(
                          (language) => language.code == _word.languageCode)
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Text(
                            localizations.translate('record.title'),
                            style: themeData.textTheme.headline6
                                .copyWith(fontSize: 20),
                          ),
                        )
                      : Container(
                          height: 30.0,
                        ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: PendingPronunciationWordOriginalData(
                      word: _word,
                      textStyle: themeData.textTheme.bodyText2.copyWith(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 20.0),
                    child: HtmlTagTextFormatted(
                      text: _word.languageAccentName != null &&
                              _word.languageAccentName.isNotEmpty &&
                              _word.languageAccentCode != null &&
                              _word.languageAccentCode.isNotEmpty
                          ? '<b>${_word.languageName}</b> (${_word.languageAccentName}) [${_word.languageAccentCode}]'
                          : '<b>${_word.languageName}</b> [${_word.languageCode}]',
                      openTag: '<b>',
                      closeTag: '</b>',
                      boldTextStyle: themeData.textTheme.bodyText2,
                      normalTextStyle: themeData.textTheme.subtitle1,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  _microphonePermissionStatus != null &&
                          _storagePermissionStatus != null
                      ? _isMicrophonePermissionGranted() &&
                              _isStoragePermissionGranted()
                          ? _recorderWidgetsColumn()
                          : _permissionRequestWidgets()
                      : CustomCircularProgressIndicator(),
                ],
              ),
            ),
          ],
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarIndex: HEADER_SECTION_PRONOUNCE,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_SEARCH,
      bottomNavBarIndexIsActive: false,
      body: _isUserLogged ? _getContent() : SignUpLoginHelp(),
    );
  }
}
