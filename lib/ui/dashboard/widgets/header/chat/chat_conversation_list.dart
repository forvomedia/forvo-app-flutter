import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/constants/websocket_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/blocked_user.dart';
import 'package:forvo/model/chat_conversation.dart';
import 'package:forvo/model/chat_message.dart';
import 'package:forvo/model/database_chat_conversation.dart';
import 'package:forvo/model/websocket_last_message_and_chat_conversations.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/service/chat/websocket_service.dart';
import 'package:forvo/service/database/database_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/bottom_bar.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_detail.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/widgets/chat_reconnection_indicator.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_bar.dart';
import 'package:forvo/ui/dashboard/widgets/signup_login_help.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/media_query_util.dar.dart';
import 'package:forvo/util/messages_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:intl/intl.dart';

class ChatConversationList extends StatefulWidget {
  final String sectionName;
  final bool isLogged;

  ChatConversationList({this.sectionName, this.isLogged});

  @override
  _ChatConversationListState createState() => _ChatConversationListState();
}

class _ChatConversationListState extends State<ChatConversationList> {
  AppState _state;
  AppLocalizations localizations;
  bool _isLoading = true;
  List<ChatConversation> _chatConversations = [];
  bool _isUserLogged = false;
  String _username, _md5Password, _deviceId;
  WebSocketService _webSocketService;
  DatabaseService _databaseService;
  bool _webSocketCallbacksInitialized = false;
  bool _skipCheckLastMessageOnAuthentication = false;
  GlobalKey _chatReconnectionIndicatorKey;
  bool _inChatDetail = false;

  int _forceTruncateDatabaseTapCount = 0;
  int _initialForceTruncateDatabaseTapTimeInMillis;
  int _secondsSinceFirstForceTruncateDatabaseTapLimit = 5;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _databaseService = DatabaseService();
    _webSocketService = WebSocketService();
    _chatReconnectionIndicatorKey = GlobalKey();
  }

  @override
  void didChangeDependencies() {
    if (mounted && !_inChatDetail) {
      _state = AppStateManager.of(context).state;
      localizations = AppLocalizations.of(context);
      _isUserLogged = _state?.isUserLogged ?? false;
      _username = _state.userInfo.user.username;
      _md5Password = _state.userInfo.user.md5Password;

      _connectToWebSocket();
    }
    super.didChangeDependencies();
  }

  _setWebSocketCallbacks() {
    if (!_webSocketCallbacksInitialized) {
      _webSocketCallbacksInitialized = true;

      // TODO: Definir Listeners y pasarlos al webSocketService

      _webSocketService.on(WebSocketConstants.EVENT_AUTHENTICATED, (_) async {
        if (_skipCheckLastMessageOnAuthentication) {
          return;
        }

        await getLastMessagesAndSendAllLocalMessages(_username);
        _skipCheckLastMessageOnAuthentication = true;
      });

      _webSocketService.on(WebSocketConstants.EVENT_RECEIVE_MESSAGES,
          (data) async {
        try {
          WebSocketLastMessageAndChatConversations
              _webSocketLastMessageAndChatConversations =
              await receiveMessages(data);

          if (_webSocketLastMessageAndChatConversations
              .conversations.isNotEmpty) {
            bool _callGetLocalConversations = true;
            if (_webSocketLastMessageAndChatConversations.lastMessageId ==
                '0') {
              if (mounted) {
                setState(() {
                  _chatConversations =
                      _webSocketLastMessageAndChatConversations.conversations;
                  _isLoading = false;
                });
              }
              _callGetLocalConversations = false;
            }

            if (_callGetLocalConversations) {
              _getLocalConversations();
            }
          } else {
            _getLocalConversations();
          }
        } on ApiException catch (e) {
          String messages = e.getErrorMessages(context);
          if (messages != null && messages.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                messages,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (_) {
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      });

      _webSocketService
          .on(WebSocketConstants.EVENT_RECEIVE_NEW_OR_ACCEPTED_MESSAGE,
              (data) async {
        String idLocal;
        try {
          var webSocketData = json.decode(data);
          idLocal = webSocketData['idLocal'];
          if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_OK) {
            ChatMessage chatMessage =
                ChatMessage.fromJson(webSocketData['item']);
            await _databaseService.saveMessage(chatMessage);
            _getLocalConversations(showLoading: false);
          } else if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_ERROR) {
            throw ApiException(webSocketData);
          } else {
            throw Exception();
          }
        } on ApiException catch (e) {
          List<ApiError> errors = e.getErrors();
          String errorMessages;
          for (ApiError _error in errors) {
            if (_error.errorApiCode == ApiErrorCodeConstants.ERROR_1055) {
              Future.delayed(Duration(milliseconds: 500), () {
                _databaseService.deleteMessage(idLocal,
                    controlBlockedNotify: true);
                _getLocalConversations(showLoading: false);
              });
            } else {
              String translatedMessage =
                  _error.getTranslatedErrorMessage(context);
              if (errorMessages == null) {
                errorMessages = translatedMessage;
              } else {
                errorMessages = '$errorMessages\n$translatedMessage';
              }
            }
          }
          if (errorMessages != null && errorMessages.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                errorMessages,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (e) {
          debugPrint('EXCEPTION ON EVENT_RECEIVE_MESSAGES $e');
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      });
    }
  }

  _connectToWebSocket() async {
    if (!_state.isMaintenanceReadOnlyActive &&
        !_state.isMaintenanceMessagesActive) {
      _deviceId ??= await getDeviceId();

      _webSocketService.initializeState(_state);
      _webSocketService.connect(_username, _md5Password, _deviceId);
      _setWebSocketCallbacks();
    }
    _getLocalConversations();
  }

  _getLocalConversations({bool showLoading = true}) async {
    if (mounted) {
      if (showLoading) {
        setState(() {
          _isLoading = true;
        });
      }
    }
    List<BlockedUser> _blockedUsers;
    try {
      _blockedUsers = await getBlockedUsers(context);
      if (_blockedUsers != null && _blockedUsers.isNotEmpty) {
        for (BlockedUser _blockedUser in _blockedUsers) {
          await _databaseService.updateUserBlockStatusByUsername(
            _blockedUser.username,
            blocked: true,
          );
        }
      }
    } on ApiException catch (e) {
      debugPrint('GET BLOCKED USERS EXCEPTION $e');
    }

    try {
      List<DatabaseChatConversation> _dbChatConversations =
          await _databaseService.getConversations();

      if (_dbChatConversations.isNotEmpty) {
        List<ChatConversation> chatConversations = [];
        for (DatabaseChatConversation dbConversation in _dbChatConversations) {
          String lastMessageAddtimeAPI = dbConversation.lastMessageAddtimeAPI;

          String date, hour = '';
          if (lastMessageAddtimeAPI != null &&
              lastMessageAddtimeAPI != 'null') {
            DateFormat inputDateFormat =
                DateFormat(DEFAULT_DATETIME_INPUT_FORMAT);
            DateTime messageDateTime =
                inputDateFormat.parse(lastMessageAddtimeAPI);
            DateTime messageUtcDateTime = DateTime.utc(
              messageDateTime.year,
              messageDateTime.month,
              messageDateTime.day,
              messageDateTime.hour,
              messageDateTime.minute,
              messageDateTime.second,
            );
            DateTime messageLocalDateTime = messageUtcDateTime.toLocal();

            hour = getDefaultFormattedTimeFromDate(messageLocalDateTime);

            date = getDefaultFormattedDate(messageLocalDateTime);
            if (isDateTimeToday(messageLocalDateTime)) {
              date = DEFAULT_TODAY_DATE_NAME;
            } else {
              if (isDateTimeYesterday(messageLocalDateTime)) {
                date = DEFAULT_YESTERDAY_DATE_NAME;
              }
            }
          }

          bool _isBlocked = dbConversation.userToBlocked > 0;

          /// Check if conversation user blocked local value is correct
          if (_isBlocked &&
              _blockedUsers.lastIndexWhere(
                      (element) => element.username == dbConversation.userTo) ==
                  -1) {
            _isBlocked = false;

            _databaseService.updateUserBlockStatusByUsername(
              dbConversation.userTo,
              blocked: _isBlocked,
            );
          }

          chatConversations.add(
            ChatConversation(
              id: int.tryParse(dbConversation.idConversation) != null
                  ? int.parse(dbConversation.idConversation)
                  : 0,
              username: dbConversation.userTo,
              isBlocked: _isBlocked,
              unreadMessages: dbConversation.unReadMessages,
              messages: [
                ChatMessage(
                  message: dbConversation.lastMessage,
                  date: date,
                  hour: hour,
                  addtime: dbConversation.lastMessageAddtimeAPI,
                  addtimeOrder:
                      int.tryParse(dbConversation.lastMessageAddtimeOrder) !=
                              null
                          ? int.parse(dbConversation.lastMessageAddtimeOrder)
                          : null,
                ),
              ],
            ),
          );
        }
        if (mounted) {
          setState(() {
            _chatConversations = chatConversations;
          });
        }
      }
    } on Exception catch (e) {
      print('EXCEPTION ON GET CONVERSATIONS > $e');
    } finally {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  _onConnect() {
    _webSocketCallbacksInitialized = false;
    _connectToWebSocket();
  }

  _onDisconnect() {
    _webSocketService.disconnect();
  }

  _onDbTruncate() async {
    await _databaseService.truncateTableConversations();
    await _databaseService.truncateTableMessages();
  }

  _onTapForceTruncateDatabase() async {
    if (_forceTruncateDatabaseTapCount == 0) {
      Future.delayed(
          Duration(
            seconds: _secondsSinceFirstForceTruncateDatabaseTapLimit,
          ), () {
        _forceTruncateDatabaseTapCount = 0;
      });
      _initialForceTruncateDatabaseTapTimeInMillis = getCurrentTimeMillis();
      _forceTruncateDatabaseTapCount++;
    } else {
      int _currentTapTimeInMillis = getCurrentTimeMillis();

      var initialTap = DateTime.fromMillisecondsSinceEpoch(
          _initialForceTruncateDatabaseTapTimeInMillis);
      var currentTap =
          DateTime.fromMillisecondsSinceEpoch(_currentTapTimeInMillis);

      Duration difference = currentTap.difference(initialTap);

      if (difference.inSeconds <=
          _secondsSinceFirstForceTruncateDatabaseTapLimit) {
        _forceTruncateDatabaseTapCount++;
      } else {
        _forceTruncateDatabaseTapCount = 0;
      }
      if (_forceTruncateDatabaseTapCount == 5) {
        await _onDbTruncate();
        _onDisconnect();
        Future.delayed(Duration(seconds: 1), () {
          _onConnect();
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              'Messages local database truncated',
              type: SNACKBAR_TYPE_OK,
            ),
          );
        });
      }
    }
  }

  @override
  void dispose() {
    _webSocketService.disconnect();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);

    bool _isMaintenanceActive = _state.isMaintenanceReadOnlyActive ||
        _state.isMaintenanceMessagesActive;

    Widget _getMessageDateWidget(ThemeData themeData, ChatMessage message) {
      String date;
      if (message.date == DEFAULT_TODAY_DATE_NAME) {
        date = getHourStringFromDateString(
          message.addtime,
          DEFAULT_DATETIME_UTC_INPUT_FORMAT,
          isUtc: true,
        );
      } else if (message.date == DEFAULT_YESTERDAY_DATE_NAME) {
        date = localizations.translate('shared.yesterday');
      } else {
        return getLocaleFormattedDateWidgets(
          themeData,
          message.addtime,
          DEFAULT_DATETIME_INPUT_FORMAT,
          _state.locale.languageCode,
          showTime: false,
          isUtc: true,
        );
      }
      return Text(
        date,
        style: themeData.textTheme.subtitle1.copyWith(fontSize: 14.0),
      );
    }

    String _getMessageText(ChatMessage message) {
      String pattern = '\n';
      String firstLine = '';
      if (message != null && message.message != null) {
        firstLine = message.message.split(pattern).first;
        return firstLine.length > 50
            ? '${firstLine.substring(0, 50)}...'
            : firstLine;
      }
      return firstLine;
    }

    Iterable<Widget> _getChatConversationTiles(
      Iterable<ChatConversation> chats,
      ThemeData themeData,
    ) =>
        chats.map(
          (item) => Container(
            child: ListTile(
              contentPadding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              title: Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            item?.username ??
                                localizations.translate('shared.anonymousUser'),
                            style: themeData.textTheme.bodyText2,
                          ),
                          item.isBlocked
                              ? Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      // ignore: lines_longer_than_80_chars
                                      '(${localizations.translate('messages.list.userBlocked')})',
                                      style: themeData.textTheme.subtitle1
                                          .copyWith(fontSize: 12.0),
                                      softWrap: false,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    _getMessageDateWidget(themeData, item.messages.last),
                  ],
                ),
              ),
              subtitle: item.unreadMessages > 0
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            _getMessageText(item.messages.last),
                            style: themeData.textTheme.subtitle1,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: colorBlue,
                            ),
                            padding: EdgeInsets.all(0),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                item.unreadMessages.toString(),
                                textAlign: TextAlign.center,
                                style: themeData.textTheme.subtitle1.copyWith(
                                  color: colorWhite,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  : Text(
                      _getMessageText(item.messages.last),
                      style: themeData.textTheme.subtitle1,
                    ),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: _isMaintenanceActive
                  ? () => null
                  : () async {
                      setState(() {
                        item.unreadMessages = 0;
                      });
                      _webSocketService.disconnect();
                      _inChatDetail = true;
                      await Navigator.pushNamed(
                        context,
                        RouteConstants.CHAT_DETAIL,
                        arguments: ChatConversationDetailArguments(
                          chatConversation: item,
                          userTo: item.username,
                        ),
                      );
                      _inChatDetail = false;
                      setState(() {
                        _chatReconnectionIndicatorKey = GlobalKey();
                      });
                      Future.delayed(Duration(milliseconds: 500), () {
                        if (!_webSocketService.isAuthenticated) {
                          _webSocketCallbacksInitialized = false;
                          _connectToWebSocket();
                        }
                      });
                      _getLocalConversations(showLoading: false);
                    },
            ),
          ),
        );

    Widget _getContent() => Container(
          width: getDeviceWidth(context),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ChatReconnectionIndicator(
                key: _chatReconnectionIndicatorKey,
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 20.0, left: 20.0, bottom: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _isMaintenanceActive
                        ? Padding(
                            padding: const EdgeInsets.only(
                              bottom: 15.0,
                              right: 20.0,
                            ),
                            child: MaintenanceWarningMessage(),
                          )
                        : Container(),
                    GestureDetector(
                      onTap: _onTapForceTruncateDatabase,
                      child: Text(
                        localizations.translate('shared.messages'),
                        style: themeData.textTheme.headline6,
                      ),
                    ),
                    Text(
                      localizations.translate('messages.list.subtitle'),
                      style: themeData.textTheme.bodyText2,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: _isLoading || _chatConversations == null
                    ? CustomCircularProgressIndicator()
                    : _chatConversations.isNotEmpty
                        ? ListView(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 22.0,
                            ),
                            children: ListTile.divideTiles(
                              color: themeData.dividerColor,
                              context: context,
                              tiles: _getChatConversationTiles(
                                _chatConversations.reversed,
                                themeData,
                              ),
                            ).toList(),
                          )
                        : Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                child: Text(
                                  localizations
                                      .translate('messages.list.emptyMessage'),
                                  textAlign: TextAlign.center,
                                  style: themeData.textTheme.subtitle1,
                                ),
                              ),
                            ],
                          ),
              ),
            ],
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarIndex: HEADER_SECTION_MESSAGES,
      bottomNavBarIndex: BOTTOM_BAR_SECTION_SEARCH,
      bottomNavBarIndexIsActive: false,
      resizeToAvoidBottomInset: false,
      body: _isUserLogged ? _getContent() : SignUpLoginHelp(),
    );
  }
}
