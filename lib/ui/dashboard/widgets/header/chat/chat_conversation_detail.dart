import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/websocket_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/chat_conversation.dart';
import 'package:forvo/model/chat_message.dart';
import 'package:forvo/model/database_chat_conversation.dart';
import 'package:forvo/model/database_chat_message.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/user_api_service.dart';
import 'package:forvo/service/chat/websocket_service.dart';
import 'package:forvo/service/database/database_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/widgets/chat_message_view.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/widgets/chat_reconnection_indicator.dart';
import 'package:forvo/ui/dashboard/widgets/header_app_icon_item.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/themes/styles.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/maintenance/maintenance_warning_message.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/crypto_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/theme_util.dart';
import 'package:intl/intl.dart';

class ChatConversationDetailArguments {
  final String userTo;
  final ChatConversation chatConversation;

  const ChatConversationDetailArguments({
    @required this.userTo,
    this.chatConversation,
  });
}

class ChatConversationDetail extends StatefulWidget {
  const ChatConversationDetail({
    Key key,
  }) : super(key: key);

  @override
  _ChatConversationDetailState createState() => _ChatConversationDetailState();
}

class _ChatConversationDetailState extends State<ChatConversationDetail>
    with TickerProviderStateMixin {
  final List<ChatMessageView> _messages = [];
  final _textController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  AppState _state;
  AppLocalizations localizations;
  bool _isLoading = false;
  bool _isComposing = false;
  bool _webSocketCallbacksInitialized = false;
  WebSocketService _webSocketService;
  DatabaseService _databaseService;
  String _username, _md5Password, _deviceId;
  String _idConversation = CHAT_NOT_DEFINED_CONVERSATION;
  String _userTo;
  bool _isUserBlocked = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    _databaseService = DatabaseService();
    _webSocketService = WebSocketService();
  }

  @override
  void didChangeDependencies() {
    _extractArguments();
    _state = AppStateManager.of(context).state;
    localizations = AppLocalizations.of(context);
    _username = _state.userInfo.user.username;
    _md5Password = _state.userInfo.user.md5Password;
    _connectToWebSocket();
    super.didChangeDependencies();
  }

  _extractArguments() async {
    var args = ModalRoute.of(context).settings.arguments
        as ChatConversationDetailArguments;
    if (args != null) {
      if (args.chatConversation != null) {
        _idConversation = args.chatConversation.id.toString();
        _userTo = args.chatConversation.username;
        _isUserBlocked = args.chatConversation.isBlocked;
      } else {
        if (args.userTo != null) {
          _userTo = args.userTo;
        }
      }
    }
  }

  _connectToWebSocket() async {
    _deviceId ??= await getDeviceId();

    _loadChatMessages();

    if (!_state.isMaintenanceReadOnlyActive &&
        !_state.isMaintenanceMessagesActive) {
      if (!_webSocketService.isStateInitialized()) {
        _webSocketService.initializeState(_state);
      }

      if (!_webSocketService.isAuthenticated) {
        _webSocketService.connect(_username, _md5Password, _deviceId);
      }
      _setWebSocketCallbacks();
    }
  }

  _setWebSocketCallbacks() {
    if (!_webSocketCallbacksInitialized) {
      _webSocketCallbacksInitialized = true;

      _webSocketService.on(WebSocketConstants.EVENT_AUTHENTICATED, (_) async {
        if (_idConversation != CHAT_NOT_DEFINED_CONVERSATION) {
          await _databaseService.markMessagesAsRead(_idConversation);
          _webSocketService.markConversationAsRead(
            _idConversation,
            _state.userInfo.user.username,
          );
        }
      });

      _webSocketService
          .on(WebSocketConstants.EVENT_RECEIVE_CONVERSATION_MESSAGES_AS_READ,
              (data) async {
        try {
          var webSocketData = json.decode(data);
          if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_OK) {
            if (webSocketData['id'] != null) {
              String idConversation = webSocketData['id'];
              if (idConversation.isNotEmpty && idConversation != 'null') {
                await _databaseService.markMessagesAsRead(idConversation);
              }
            }
          } else if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_ERROR) {
            throw ApiException(webSocketData);
          } else {
            throw Exception();
          }
        } on ApiException catch (e) {
          String messages = e.getErrorMessages(context);
          if (messages != null && messages.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                messages,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (e) {
          debugPrint('EXCEPTION ON EVENT_RECEIVE_MESSAGES $e');
          var localizations = AppLocalizations.of(context);
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      });

      _webSocketService
          .on(WebSocketConstants.EVENT_RECEIVE_NEW_OR_ACCEPTED_MESSAGE_DIRECTLY,
              (data) async {
        try {
          var webSocketData = json.decode(data);
          if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_OK) {
            ChatMessage chatMessage =
                ChatMessage.fromJson(webSocketData['item']);

            if (chatMessage.userTo == _userTo ||
                chatMessage.userFrom == _userTo) {
              int pos = _messages.lastIndexWhere(
                  (element) => element.idLocal == chatMessage.idLocal);

              if (pos < 0) {
                _addMessageToList(
                  chatMessage.idLocal,
                  chatMessage.message,
                  chatMessage.userFrom == _state.userInfo.user.username,
                );
              }

              if (_idConversation != CHAT_NOT_DEFINED_CONVERSATION) {
                _databaseService.markMessagesAsRead(_idConversation);
                _webSocketService.markConversationAsRead(
                  _idConversation,
                  _state.userInfo.user.username,
                );
              }
            }
          } else if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_ERROR) {
            throw ApiException(webSocketData);
          } else {
            throw Exception();
          }
        } on ApiException catch (e) {
          String errorMessages = e.getErrorMessages(context);

          if (errorMessages != null && errorMessages.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                errorMessages,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (e) {
          debugPrint('EXCEPTION ON EVENT_RECEIVE_MESSAGES $e');
          var localizations = AppLocalizations.of(context);
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      });

      _webSocketService
          .on(WebSocketConstants.EVENT_RECEIVE_NEW_OR_ACCEPTED_MESSAGE,
              (data) async {
        String idLocal;
        try {
          var webSocketData = json.decode(data);
          idLocal = webSocketData['idLocal'];
          if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_OK) {
            ChatMessage chatMessage =
                ChatMessage.fromJson(webSocketData['item']);
            await _databaseService.saveMessage(chatMessage);

            if (chatMessage.userTo == _userTo ||
                chatMessage.userFrom == _userTo) {
              if (_idConversation == CHAT_NOT_DEFINED_CONVERSATION) {
                _idConversation = chatMessage.idConversation.toString();
                await _databaseService.markMessagesAsRead(_idConversation);
                _webSocketService.markConversationAsRead(
                  _idConversation,
                  _state.userInfo.user.username,
                );
              }
            }
          } else if (webSocketData['status'] != null &&
              webSocketData['status'] == ApiConstants.STATUS_ERROR) {
            throw ApiException(webSocketData);
          } else {
            throw Exception();
          }
        } on ApiException catch (e) {
          List<ApiError> errors = e.getErrors();
          String errorMessages;

          for (ApiError _error in errors) {
            String translatedMessage =
                _error.getTranslatedErrorMessage(context);
            if (_error.errorApiCode == ApiErrorCodeConstants.ERROR_1055) {
              _deleteLocalMessageAndRemoveFromChatList(
                  idLocal, translatedMessage);
            } else {
              if (errorMessages == null) {
                errorMessages = translatedMessage;
              } else {
                errorMessages = '$errorMessages\n$translatedMessage';
              }
            }
          }
          if (errorMessages != null && errorMessages.isNotEmpty) {
            showCustomSnackBar(
              _scaffoldMessengerKey.currentContext,
              themedSnackBar(
                context,
                errorMessages,
                type: SNACKBAR_TYPE_ERROR,
              ),
            );
          }
        } on Exception catch (e) {
          debugPrint('EXCEPTION ON EVENT_RECEIVE_MESSAGES $e');
          showCustomSnackBar(
            _scaffoldMessengerKey.currentContext,
            themedSnackBar(
              context,
              localizations.translate('error.default'),
              type: SNACKBAR_TYPE_ERROR,
            ),
          );
        }
      });
    }
  }

  void _deleteLocalMessageAndRemoveFromChatList(
      String idLocal, String errorMessages) async {
    _databaseService.deleteMessage(idLocal, controlBlockedNotify: true);
    ChatMessageView invalidMessage =
        _messages.firstWhere((element) => element.idLocal == idLocal);
    _messages.removeWhere((element) => element.idLocal == idLocal);
    ChatMessageView blockedUserMessage = ChatMessageView(
      idLocal: invalidMessage.idLocal,
      text: errorMessages,
      isMine: invalidMessage.isMine,
      isMessage: invalidMessage.isMessage,
      hour: invalidMessage.hour,
      addtime: invalidMessage.addtime,
    );
    setState(() {
      _messages.insert(0, blockedUserMessage);
    });
  }

  void _loadChatMessages() async {
    if (_idConversation == CHAT_NOT_DEFINED_CONVERSATION &&
        _userTo.isNotEmpty) {
      DatabaseChatConversation _databaseChatConversation =
          await _databaseService.getConversationByUser(_userTo);
      if (_databaseChatConversation != null) {
        _idConversation = _databaseChatConversation.idConversation;
      }
    }

    if (mounted && _idConversation != CHAT_NOT_DEFINED_CONVERSATION) {
      setState(() {
        _isLoading = true;
      });

      List<DatabaseChatMessage> _dbChatMessages =
          await _databaseService.getMessages(_idConversation);

      List<ChatMessage> chatMessages = [];
      if (_dbChatMessages.isNotEmpty) {
        for (DatabaseChatMessage dbChatMessage in _dbChatMessages) {
          int _idMessageAPI = dbChatMessage.idMessageAPI != null &&
                  dbChatMessage.idMessageAPI.isNotEmpty &&
                  dbChatMessage.idMessageAPI != 'null'
              ? int.parse(dbChatMessage.idMessageAPI)
              : null;
          int _messageAddTimeOrder = dbChatMessage.addTimeOrder != null &&
                  dbChatMessage.addTimeOrder.isNotEmpty &&
                  dbChatMessage.addTimeOrder != 'null'
              ? int.parse(dbChatMessage.addTimeOrder)
              : null;
          String _messageAddtime = dbChatMessage.addTimeAPI;

          if (_messageAddtime == null ||
              _messageAddtime.isEmpty ||
              _messageAddtime == 'null') {
            int addtimeInsertToMillis = int.parse(dbChatMessage.addTimeInsert);
            _messageAddtime = formatMillis(
              addtimeInsertToMillis,
              DEFAULT_DATETIME_INPUT_FORMAT,
            );
          }

          String _messageDate, _messageHour = '';
          if (_messageAddtime != null &&
              _messageAddtime.isNotEmpty &&
              _messageAddtime != 'null') {
            DateTime messageLocalDateTime = getDateTimeFromDateString(
              _messageAddtime,
              DEFAULT_DATETIME_INPUT_FORMAT,
              isUtc: true,
            );

            _messageHour = getHourStringFromDateString(
              _messageAddtime,
              DEFAULT_DATETIME_INPUT_FORMAT,
              isUtc: true,
            );

            _messageDate = getDefaultFormattedDate(messageLocalDateTime);

            if (isDateTimeToday(messageLocalDateTime)) {
              _messageDate = DEFAULT_TODAY_DATE_NAME;
            } else {
              if (isDateTimeYesterday(messageLocalDateTime)) {
                _messageDate = DEFAULT_YESTERDAY_DATE_NAME;
              }
            }
          }

          chatMessages.add(
            ChatMessage(
              id: _idMessageAPI,
              idLocal: dbChatMessage.idMessageLocal,
              idConversation: int.parse(_idConversation),
              userFrom: dbChatMessage.userFrom,
              userTo: dbChatMessage.userTo,
              message: dbChatMessage.message,
              addtime: _messageAddtime,
              isRead: true,
              date: _messageDate,
              hour: _messageHour,
              addtimeOrder: _messageAddTimeOrder,
            ),
          );
        }
      }

      String currentDate;
      for (ChatMessage userChatMessage in chatMessages) {
        if (currentDate != userChatMessage.date) {
          currentDate = userChatMessage.date;

          String formattedDate = '';
          if (currentDate == DEFAULT_TODAY_DATE_NAME) {
            formattedDate = localizations.translate('shared.today');
          } else if (currentDate == DEFAULT_YESTERDAY_DATE_NAME) {
            formattedDate = localizations.translate('shared.yesterday');
          } else {
            if (userChatMessage.addtime != null) {
              formattedDate = getLocaleFormattedDateString(
                userChatMessage.addtime,
                DEFAULT_DATETIME_INPUT_FORMAT,
                _state.locale.languageCode,
                isUtc: true,
              );
            }
          }
          ChatMessageView message = ChatMessageView(
            idLocal: '',
            text: formattedDate,
            addtime: '',
            isMine: false,
            hour: '',
            isMessage: false,
          );
          _messages.insert(0, message);
        }

        ChatMessageView message = ChatMessageView(
          idLocal: userChatMessage.idLocal,
          text: userChatMessage.message,
          addtime: userChatMessage.addtime,
          isMine: userChatMessage.userFrom == _state.userInfo.user.username,
          hour: userChatMessage.hour,
        );
        _messages.insert(0, message);
      }
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    _webSocketService.disconnect();
    for (ChatMessageView message in _messages) {
      if (message.animationController != null) {
        message.animationController.dispose();
      }
    }
    super.dispose();
  }

  void _checkIfTodayMessageMustBeAdded(DateFormat inputDateFormat) {
    DateTime today = DateTime.now();

    ChatMessageView todayMessage = ChatMessageView(
      idLocal: '',
      text: localizations.translate('shared.today'),
      addtime: inputDateFormat.format(today),
      hour: '',
      isMine: false,
      isMessage: false,
    );

    /// Add "Today" message if previous message is from other day

    if (_messages.isEmpty) {
      setState(() {
        _messages.insert(0, todayMessage);
      });
    } else {
      ChatMessageView lastMessage = _messages?.reversed?.last;
      if (lastMessage != null &&
          lastMessage.addtime != null &&
          lastMessage.addtime.isNotEmpty) {
        DateTime lastMessageDate = inputDateFormat.parse(lastMessage.addtime);
        if (lastMessageDate.year == today.year &&
            lastMessageDate.month == today.month &&
            lastMessageDate.day != today.day) {
          setState(() {
            _messages.insert(0, todayMessage);
          });
        }
      }
    }
  }

  void _addMessageToList(String idLocal, String text, bool isMine) {
    DateTime today = DateTime.now();
    DateFormat inputDateFormat = DateFormat(DEFAULT_DATETIME_INPUT_FORMAT);
    String hour = getDefaultFormattedTimeFromDate(today);

    if (mounted) {
      setState(() {
        _isComposing = false;
      });
      _checkIfTodayMessageMustBeAdded(inputDateFormat);

      ChatMessageView message = ChatMessageView(
        idLocal: idLocal,
        text: text,
        addtime: '',
        hour: hour,
        animationController: AnimationController(
          duration: const Duration(
            milliseconds: 700,
          ),
          vsync: this,
        ),
        isMine: isMine,
      );
      setState(() {
        _messages.insert(0, message);
      });
      message.animationController.forward();
    }
  }

  void _sendMessage(String text) async {
    _textController.clear();

    String idLocal =
        generateMd5('${_userTo}_${getCurrentTimeMillis().toString()}');

    _addMessageToList(idLocal, text, true);
    _focusNode.requestFocus();

    ChatMessage chatMessage = ChatMessage(
      idLocal: idLocal,
      idConversation: int.parse(_idConversation),
      userFrom: _state.userInfo.user.username,
      userTo: _userTo,
      message: text,
      isRead: true,
    );
    _databaseService.saveMessage(
      chatMessage,
      idConversation: _idConversation,
      controlNotify: false,
      controlInsert: false,
    );
    _webSocketService.sendMessage(
      idLocal,
      _idConversation,
      _state.userInfo.user.username,
      _userTo,
      text,
    );
  }

  void _blockUser() async {
    setState(() {
      _isUserBlocked = !_isUserBlocked;
    });
    try {
      String blocked = _isUserBlocked
          ? BLOCK_USER_MESSAGES_ACTION_YES
          : BLOCK_USER_MESSAGES_ACTION_NO;
      await blockUserMessages(
        context,
        idConversation: _idConversation,
        blocked: blocked,
      );
      _databaseService.updateUserBlockStatusByIdConversation(
        _idConversation,
        blocked: _isUserBlocked,
      );
      String confirmationMessageKey = _isUserBlocked
          ? 'messages.detail.userBlockConfirmationMessage'
          : 'messages.detail.userUnblockConfirmationMessage';
      String confirmationMessage = localizations.translate(
        confirmationMessageKey,
        params: {'username': _userTo},
      );
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          confirmationMessage,
          type: SNACKBAR_TYPE_OK,
        ),
      );
    } on ApiException catch (e) {
      String errorMessages = e.getErrorMessages(context);
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          errorMessages,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    bool _isMaintenanceActive = _state.isMaintenanceReadOnlyActive ||
        _state.isMaintenanceMessagesActive;

    Widget _buildTextComposer() => Container(
          margin: EdgeInsets.symmetric(
            horizontal: 12.0,
            vertical: 12.0,
          ),
          child: Row(
            children: [
              Flexible(
                child: Container(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: TextField(
                    controller: _textController,
                    decoration: InputDecoration(
                      border: inputTextChatBorder,
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 10.0),
                      disabledBorder: inputTextChatBorder,
                      enabledBorder: inputTextChatBorder,
                      fillColor: colorLightGray,
                      filled: true,
                      focusedBorder: inputTextChatBorder,
                    ),
                    focusNode: _focusNode,
                    keyboardType: TextInputType.multiline,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    onChanged: (String text) {
                      setState(() {
                        _isComposing = text.isNotEmpty;
                      });
                    },
                    onSubmitted: (String text) {
                      if (!_isMaintenanceActive && _isComposing) {
                        _sendMessage(text);
                      }
                      return null;
                    },
                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontStyle: FontStyle.normal,
                          color: colorBlack,
                        ),
                  ),
                ),
              ),
              Container(
                height: 48,
                width: 52,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    5.0,
                  ),
                  color: !_isMaintenanceActive && _isComposing
                      ? Colors.blue
                      : Colors.grey,
                ),
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                  icon: const Icon(
                    Icons.send,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: !_isMaintenanceActive && _isComposing
                      ? () => _sendMessage(_textController.text)
                      : null,
                ),
              )
            ],
          ),
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      headerAppBarTitle: _userTo ?? '',
      headerAppBarHideActions: false,
      headerAppBarActions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0, top: 8.0),
          child: HeaderAppIconItem(
            icon: Icon(
              ForvoIcons.userActive,
              color: themeIsDark(context) ? colorWhite : colorBlack,
              size: 20,
            ),
            activeIcon: Icon(
              ForvoIcons.userActive,
              color: colorBlue,
              size: 20,
            ),
            active: _isUserBlocked,
            text: _isUserBlocked
                ? localizations.translate('messages.detail.userUnblockAction')
                : localizations.translate('messages.detail.userBlockAction'),
            onTap: !_isMaintenanceActive ? _blockUser : null,
          ),
        ),
      ],
      bottomNavBarHidden: true,
      resizeToAvoidBottomInset: false,
      body: Container(
        child: Column(
          children: [
            ChatReconnectionIndicator(),
            _isMaintenanceActive
                ? Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: MaintenanceWarningMessage(),
                  )
                : Container(),
            _isLoading || _messages == null
                ? Expanded(
                    child: CustomCircularProgressIndicator(),
                  )
                : Flexible(
                    child: ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      reverse: true,
                      itemBuilder: (_, int index) => _messages[index],
                      itemCount: _messages.length,
                    ),
                  ),
            Divider(height: 1.0),
            Container(
              decoration: BoxDecoration(color: Theme.of(context).cardColor),
              child: _buildTextComposer(),
            ),
          ],
        ),
        decoration: Theme.of(context).platform == TargetPlatform.iOS //new
            ? BoxDecoration(
                border: Border(
                  top: BorderSide(color: Colors.grey[200]),
                ),
              )
            : null,
      ),
    );
  }
}
