import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/service/chat/websocket_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/notifiers/chat_connection_change_notifier.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';

class ChatReconnectionIndicator extends StatefulWidget {
  ChatReconnectionIndicator({Key key}) : super(key: key);

  @override
  State<ChatReconnectionIndicator> createState() =>
      _ChatReconnectionIndicatorState();
}

class _ChatReconnectionIndicatorState extends State<ChatReconnectionIndicator> {
  bool _initialized = false;
  AppState _state;
  AppLocalizations localizations;
  WebSocketService _webSocketService;
  bool _chatHasConnection = true;
  ChatConnectionChangeNotifier _chatConnectionChangeNotifier;

  @override
  void initState() {
    super.initState();
    _webSocketService = WebSocketService();
  }

  @override
  void didChangeDependencies() {
    _state = AppStateManager.of(context).state;
    localizations = AppLocalizations.of(context);
    _initData();
    super.didChangeDependencies();
  }

  _initData() {
    if (!_initialized) {
      _initialized = true;

      if (!_webSocketService.isStateInitialized()) {}
    }
    _webSocketService.initializeState(_state);
    _chatConnectionChangeNotifier = _state.chatConnectionChangeNotifier;
    _registerToChatConnectionChangeNotifications();
  }

  _registerToChatConnectionChangeNotifications() {
    _chatConnectionChangeNotifier.addListener(_refreshConnectionStatus);
  }

  _refreshConnectionStatus() {
    if (mounted && _chatHasConnection != _webSocketService.isAuthenticated) {
      setState(() {
        _chatHasConnection = _webSocketService.isAuthenticated;
      });
    }
  }

  @override
  void dispose() {
    _chatConnectionChangeNotifier.removeListener(_refreshConnectionStatus);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    return _chatHasConnection
        ? Container()
        : Container(
            color: colorSnackBarRed,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(
                      localizations.translate('messages.reconnecting'),
                      style: themeData.textTheme.subtitle2.copyWith(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                    width: 20,
                    child: CustomCircularProgressIndicator(
                      valueColor: colorWhite,
                      strokeWidth: 2.0,
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
