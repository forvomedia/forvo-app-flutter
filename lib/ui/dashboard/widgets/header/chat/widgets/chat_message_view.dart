import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';
import 'package:forvo/util/theme_util.dart';

class ChatMessageView extends StatelessWidget {
  final String idLocal;
  final String text;
  final String addtime;
  final String hour;
  final bool isMine;
  final bool isMessage;
  final AnimationController animationController;

  ChatMessageView({
    @required this.idLocal,
    @required this.text,
    @required this.addtime,
    @required this.hour,
    @required this.isMine,
    this.isMessage = true,
    this.animationController,
  });

  @override
  Widget build(BuildContext context) => text != null && text.isNotEmpty
      ? animationController != null
          ? SizeTransition(
              sizeFactor: CurvedAnimation(
                parent: animationController,
                curve: Curves.easeOut,
              ),
              axisAlignment: 0.0,
              child: _messageBubble(context),
            )
          : _messageBubble(context)
      : Container();

  Container _messageBubble(BuildContext context) {
    var themeData = Theme.of(context);

    TextStyle normalTextStyle = themeData.textTheme.bodyText2;
    TextStyle boldTextStyle = normalTextStyle.copyWith(
      fontWeight: FontWeight.bold,
    );
    TextStyle hourTextStyle = themeData.textTheme.subtitle1.copyWith(
      fontSize: 12,
    );

    BubbleEdges bubbleMarginEdges = BubbleEdges.only(top: 5.0);
    BubbleEdges bubblePaddingEdges = BubbleEdges.only(left: 15.0, right: 15.0);

    Widget _bubbleContent = Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        HtmlTagTextFormatted(
          text: text,
          openTag: '<b>',
          closeTag: '</b>',
          normalTextStyle: normalTextStyle,
          boldTextStyle: boldTextStyle,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              hour,
              style: hourTextStyle,
              textAlign: TextAlign.end,
            ),
          ],
        ),
      ],
    );

    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: isMessage
                      ? isMine
                          ? Bubble(
                              color: Theme.of(context).disabledColor,
                              margin: bubbleMarginEdges,
                              padding: bubblePaddingEdges,
                              alignment: Alignment.topRight,
                              nip: BubbleNip.rightTop,
                              child: _bubbleContent,
                            )
                          : Bubble(
                              color: Theme.of(context).primaryColor,
                              margin: bubbleMarginEdges,
                              padding: bubblePaddingEdges,
                              alignment: Alignment.topLeft,
                              nip: BubbleNip.leftTop,
                              child: _bubbleContent,
                            )
                      : Bubble(
                          color: themeIsDark(context)
                              ? colorDarkThemeGrey
                              : colorLightThemeGrey,
                          alignment: Alignment.center,
                          child: Text(
                            text,
                            style: themeData.textTheme.subtitle1.copyWith(
                              fontSize: 14,
                              color: colorWhite,
                            ),
                          ),
                        ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
