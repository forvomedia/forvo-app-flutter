import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/service/database/database_service.dart';
import 'package:forvo/ui/dashboard/widgets/home_favorites.dart';
import 'package:forvo/ui/dashboard/widgets/home_my_account.dart';
import 'package:forvo/ui/dashboard/widgets/home_notifications.dart';
import 'package:forvo/ui/dashboard/widgets/home_start.dart';
import 'package:forvo/ui/dashboard/widgets/signup_login_help.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/util/app_review_util.dart';
import 'package:forvo/util/app_version_util.dart';

const int HOME_SECTION_START = 0;
const int HOME_SECTION_FAVORITES = 1;
const int HOME_SECTION_NOTIFICATIONS = 2;
const int HOME_SECTION_MY_ACCOUNT = 3;

class Home extends StatelessWidget {
  final int bottomNavBarIndex;

  Home({Key key, this.bottomNavBarIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      _HomePage(bottomNavBarIndex: bottomNavBarIndex);
}

class _HomePage extends StatefulWidget {
  final int bottomNavBarIndex;

  _HomePage({Key key, this.bottomNavBarIndex}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  int _bottomNavBarIndex = HOME_SECTION_START;
  List<Widget> _children = [];
  bool _isUserLogged = false;

  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
  GlobalKey<ScaffoldMessengerState>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    var state = AppStateManager.of(context).state;
    _isUserLogged = state?.isUserLogged ?? false;
    _bottomNavBarIndex = widget.bottomNavBarIndex != null
        ? widget.bottomNavBarIndex
        : HOME_SECTION_START;

    checkVersionValidity(context);
    if (_isUserLogged) {
      var databaseService = DatabaseService();
      if (!databaseService.isDatabaseOpen) {
        databaseService.resetDatabase();
      }
    }
  }

  initBottomNavigation(AppLocalizations localizations) {
    setState(() {
      _children = [
        _isUserLogged
            ? HomeStart(
                sectionName: localizations.translate('shared.sectionStart'),
                isUserLogged: _isUserLogged,
              )
            : SignUpLoginHelp(),
        _isUserLogged
            ? HomeFavorites(
                sectionName: localizations.translate('shared.favorites'),
                isUserLogged: _isUserLogged,
              )
            : SignUpLoginHelp(),
        _isUserLogged
            ? HomeNotifications(
                sectionName: localizations.translate('shared.notifications'),
                isUserLogged: _isUserLogged,
              )
            : SignUpLoginHelp(),
        _isUserLogged
            ? HomeMyAccount(
                sectionName: localizations.translate('shared.myAccount'),
                isUserLogged: _isUserLogged,
              )
            : SignUpLoginHelp(),
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);
    initBottomNavigation(localizations);
    return WillPopScope(
      onWillPop: () async => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            localizations.translate('shared.appCloseWarning'),
            style: themeData.textTheme.bodyText2,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                localizations.translate('shared.close'),
              ),
              onPressed: () async {
                if (_isUserLogged) {
                  await DatabaseService().close();
                }
                Navigator.of(context).pop(true);
              },
            ),
            ElevatedButton(
              child: Text(
                localizations.translate('shared.cancel'),
              ),
              onPressed: () => Navigator.of(context).pop(false),
            ),
          ],
        ),
      ),
      child: CustomScaffold(
        scaffoldMessengerKey: _scaffoldMessengerKey,
        bottomNavBarIndex: _bottomNavBarIndex,
        bottomNavBarOnTap: (int index) async {
          bool result = await showAppReviewMessage(_isUserLogged);
          if (result) {
            showAlertDialog(context);
          }
          setState(() {
            _bottomNavBarIndex = index;
          });
        },
        body: _children[_bottomNavBarIndex],
      ),
    );
  }
}
