import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';

final ThemeData lightTheme = ThemeData(
  fontFamily: 'Lato',
  brightness: Brightness.light,
  canvasColor: Colors.transparent,
  disabledColor: colorLightThemeLightGrey,
  primaryColor: colorWhite,
  backgroundColor: colorWhite,
  bottomAppBarColor: colorWhite,
  scaffoldBackgroundColor: colorDarkWhite,
  dividerColor: colorLightThemeLightGrey,
  toggleableActiveColor: colorBlue,
  appBarTheme: AppBarTheme(
    elevation: 1,
    backgroundColor: colorWhite,
  ),
  bottomAppBarTheme: BottomAppBarTheme(
    elevation: 1,
    color: colorWhite,
  ),
  primaryIconTheme: IconThemeData(color: colorBlack),
  iconTheme: IconThemeData(color: colorLightThemeGrey),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: colorWhite,
    border: OutlineInputBorder(
      borderSide: BorderSide(color: colorLightThemeLightGrey),
    ),
    focusColor: colorBlue,
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: colorBlue),
    ),
  ),
  textTheme: TextTheme(
    headline1: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.bold,
      fontSize: 32.0,
    ),
    headline3: TextStyle(
      color: colorBlue,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    ),
    headline4: TextStyle(
      color: colorBlue,
      fontWeight: FontWeight.normal,
      fontSize: 16.0,
    ),
    headline5: TextStyle(
      color: colorLightThemeGrey,
      fontWeight: FontWeight.normal,
      fontSize: 18.0,
    ),
    headline6: TextStyle(
      color: colorBlack,
      fontWeight: FontWeight.bold,
      fontSize: 28.0,
    ),
    subtitle1: TextStyle(
      color: colorLightThemeGrey,
      fontWeight: FontWeight.normal,
      fontSize: 16.0,
    ),
    subtitle2: TextStyle(
      color: colorWhite,
      fontSize: 22.0,
    ),
    button: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15.0,
      color: colorWhite,
    ),
    bodyText1: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.normal,
      fontSize: 22.0,
    ),
    bodyText2: TextStyle(
      color: colorBlack,
      fontWeight: FontWeight.normal,
      fontSize: 18.0,
    ),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: colorBlue,
    height: 54.0,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5.0),
    ),
  ),
);

class LightTheme extends Theme {
  LightTheme({@required Widget child, Key key})
      : super(key: key, data: lightTheme, child: child);
}
