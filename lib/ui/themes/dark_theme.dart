import 'package:flutter/material.dart';
import 'package:forvo/ui/colors.dart';

final ThemeData darkTheme = ThemeData(
  fontFamily: 'Lato',
  brightness: Brightness.dark,
  canvasColor: Colors.transparent,
  disabledColor: colorDarkThemeLightGrey,
  primaryColor: colorLightBlack,
  backgroundColor: colorBlack,
  bottomAppBarColor: colorLightBlack,
  scaffoldBackgroundColor: colorBlack,
  dividerColor: colorDarkThemeLightGrey,
  toggleableActiveColor: colorBlue,
  appBarTheme: AppBarTheme(
    elevation: 1,
    backgroundColor: colorLightBlack,
  ),
  bottomAppBarTheme: BottomAppBarTheme(
    elevation: 1,
    color: colorLightBlack,
  ),
  primaryIconTheme: IconThemeData(color: colorWhite),
  iconTheme: IconThemeData(color: colorDarkThemeGrey),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: colorDarkThemeLightGrey,
    border: OutlineInputBorder(
      borderSide: BorderSide(color: colorDarkThemeLightGrey, width: 0),
    ),
    focusColor: colorBlue,
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: colorBlue),
    ),
  ),
  textTheme: TextTheme(
    headline1: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.bold,
      fontSize: 32.0,
    ),
    headline3: TextStyle(
      color: colorBlue,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    ),
    headline4: TextStyle(
      color: colorBlue,
      fontWeight: FontWeight.normal,
      fontSize: 16.0,
    ),
    headline5: TextStyle(
      color: colorDarkThemeGrey,
      fontWeight: FontWeight.normal,
      fontSize: 18.0,
    ),
    headline6: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.bold,
      fontSize: 28.0,
    ),
    subtitle1: TextStyle(
      color: colorDarkThemeGrey,
      fontWeight: FontWeight.normal,
      fontSize: 16.0,
    ),
    subtitle2: TextStyle(
      color: colorWhite,
      fontSize: 22.0,
    ),
    button: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15.0,
      color: colorWhite,
    ),
    bodyText1: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.normal,
      fontSize: 22.0,
    ),
    bodyText2: TextStyle(
      color: colorWhite,
      fontWeight: FontWeight.normal,
      fontSize: 18.0,
    ),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: colorBlue,
    height: 54.0,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5.0),
    ),
  ),
);

class DarkTheme extends Theme {
  DarkTheme({@required Widget child, Key key})
      : super(key: key, data: darkTheme, child: child);
}
