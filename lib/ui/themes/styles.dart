import 'package:flutter/material.dart';

final inputTextChatBorder = OutlineInputBorder(
  borderRadius: BorderRadius.all(
    Radius.circular(5.0),
  ),
  borderSide: BorderSide(
    color: Colors.transparent,
  ),
);
