import 'package:flutter/material.dart';

const colorWhite = ColorSwatch(0xFFFFFFFF, {});
const colorDarkWhite = ColorSwatch(0xFFFAFAFD, {});

const colorLightBlack = ColorSwatch(0xFF1D1D1B, {});
const colorBlack = ColorSwatch(0xFF000000, {});

const colorLightBlue = ColorSwatch(0xFFC5DFF0, {});
const colorBlue = ColorSwatch(0xFF1D84C6, {});
const colorDarkBlue = ColorSwatch(0xFF09557E, {});

const colorLightThemeLightGrey = ColorSwatch(0xFFEEEEF4, {});
const colorDarkThemeLightGrey = ColorSwatch(0xFF3B3B3B, {});
const colorLightThemeGrey = ColorSwatch(0xFF9FACBA, {});
const colorDarkThemeGrey = ColorSwatch(0xFF8E8E8D, {});

const colorFacebookBlue = ColorSwatch(0xFF4267B2, {});

const colorFavouriteMarked = ColorSwatch(0xFFCF414D, {});

const colorGreenGood = ColorSwatch(0xFF4EB586, {});
const colorRedBad = ColorSwatch(0xFFFF0000, {});

const colorSnackBarGreen = ColorSwatch(0xFF0CAC7C, {});
const colorSnackBarRed = ColorSwatch(0xFFD9254C, {});

const colorDeleteNotificationRed = ColorSwatch(0xFFC5053E, {});

const colorLightGray = ColorSwatch(0xFFEFEFF4, {});

const colorBackgroundGray = ColorSwatch(0XFFEEF0F4, {});