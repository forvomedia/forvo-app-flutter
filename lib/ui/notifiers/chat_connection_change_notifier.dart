import 'package:flutter/material.dart';

class ChatConnectionChangeNotifier extends ChangeNotifier {
  bool _hasConnection;

  ChatConnectionChangeNotifier();

  bool get hasConnection => _hasConnection;

  set hasConnection(bool connection) {
    debugPrint('chat_connection_change_notifier.dart > '
        'currentConnectionStatus: $_hasConnection - '
        'newConnectionStatus: $connection - '
        'UPDATE AND NOTIFY LISTENERS');
      _hasConnection = connection;
      notifyListeners();
  }

  updateConnectionStatus({bool connectionStatus}) =>
      hasConnection = connectionStatus;
}
