import 'package:flutter/material.dart';

class LocaleChangeNotifier extends ChangeNotifier {
  Locale _locale;

  Locale get locale => _locale;

  set locale(Locale value) {
    _locale = value;
    notifyListeners();
  }
}
