import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/model/word_category.dart';
import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/menu/word_categorization_groups_modal_menu.dart';
import 'package:forvo/ui/widgets/pronunciation/pronunciation_categorization_information_icon.dart';
import 'package:forvo/ui/widgets/text/html_tag_text_formatted.dart';

class WordCategorizationGroupData extends StatelessWidget {
  final List<WordCategorizationGroup> categorizationGroups;
  final String wordLanguageCode;
  final Function(int) onSelectGroup;
  final int groupIndex;
  final bool useBottomBorder;

  WordCategorizationGroupData({
    @required this.categorizationGroups,
    @required this.wordLanguageCode,
    @required this.onSelectGroup,
    this.groupIndex,
    this.useBottomBorder = true,
  });

  @override
  Widget build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);
    var state = AppStateManager.of(context).state;

    int _selectedGroupIndex = groupIndex ?? 0;

    Widget _getCategories() {
      List<Widget> widgets = [];

      WordCategorizationGroup categorizationGroup =
          categorizationGroups[_selectedGroupIndex];

      if (categorizationGroup.minimumCategories != null &&
          categorizationGroup.minimumCategories.isNotEmpty) {
        for (WordCategory category in categorizationGroup.minimumCategories) {
          for (WordCategorySpelling categorySpelling in category.data) {
            String categoryName = wordLanguageCode == state.locale.languageCode
                ? categorySpelling.nativeCategoryName
                : categorySpelling.englishCategoryName;
            widgets.add(
              HtmlTagTextFormatted(
                text: '<b>${categorySpelling.spelling}</b>\n$categoryName',
                openTag: '<b>',
                closeTag: '</b>',
                boldTextStyle: themeData.textTheme.bodyText2.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
                normalTextStyle: themeData.textTheme.subtitle1.copyWith(
                  fontSize: 14,
                ),
              ),
            );
          }
        }
      }

      Widget column = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      );

      if (categorizationGroup.showFullInformation) {
        return Row(
          children: [
            Expanded(
              child: column,
            ),
            PronunciationCategorizationInformationIcon(
              categorizationGroup: categorizationGroup,
              languageCode: wordLanguageCode,
            )
          ],
        );
      }

      return column;
    }

    return Container(
      decoration: BoxDecoration(
        color: themeData.appBarTheme.backgroundColor,
        border: useBottomBorder
            ? Border(
                bottom: BorderSide(color: themeData.dividerColor),
              )
            : null,
      ),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 20.0,
                ),
                child: _getCategories(),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  color: themeData.appBarTheme.backgroundColor,
                  border: Border(
                    left: BorderSide(color: themeData.dividerColor),
                  ),
                ),
                child: InkWell(
                  onTap: () => onWordCategorizationGroupsMenuOpen(
                    context,
                    wordLanguageCode,
                    categorizationGroups,
                    onSelectGroup,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          localizations.translate('word.changeGroup'),
                          textAlign: TextAlign.center,
                          style: themeData.textTheme.bodyText2.copyWith(
                            fontSize: 12,
                          ),
                        ),
                        Icon(Icons.filter_list),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
