import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_accent.dart';
import 'package:forvo/model/language_accent_with_pronunciations.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/presentation/forvo_icons_icons.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/dashboard/home.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/record.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/button/play_button.dart';
import 'package:forvo/ui/widgets/button/record_button.dart';
import 'package:forvo/ui/widgets/button_bar/language_button_bar.dart';
import 'package:forvo/ui/widgets/circular_progress_indicator/custom_circular_progress_indicator.dart';
import 'package:forvo/ui/widgets/inkwell/themed_bubble_inkwell.dart';
import 'package:forvo/ui/widgets/menu/pronunciation_modal_menu.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/ui/widgets/suggestion/blue_bubble_suggestion_message.dart';
import 'package:forvo/ui/widgets/tab_bar/tab_bar_with_flexibe_tabs.dart';
import 'package:forvo/ui/widgets/tooltip/custom_tooltip.dart';
import 'package:forvo/ui/word/widgets/word_categorization_group_data.dart';
import 'package:forvo/util/connectivity_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:forvo/widgets/errors/network_error.dart';
import 'package:forvo/widgets/errors/service_error.dart';

class WordDetailsArguments {
  final String wordOriginal;
  final String languageName;
  final String languageAccentName;
  final int categorizationGroupIndex;

  WordDetailsArguments({
    this.wordOriginal,
    this.languageName,
    this.languageAccentName,
    this.categorizationGroupIndex,
  });
}

class WordDetail extends StatefulWidget {
  @override
  _WordDetailState createState() => _WordDetailState();
}

class _WordDetailState extends State<WordDetail>
    with SingleTickerProviderStateMixin {
  bool _initialized = false;
  String _wordOriginal;
  String _initialLanguageName;
  String _initialLanguageAccentName;
  int _selectedCategorizationGroupIndex;
  TabController _accentsTabController;
  List<LanguageWithPronunciations> _languagesWithPronunciations = [];
  int _selectedLanguageIndex = 0;
  Language _selectedLanguage;
  List<Language> _languages;
  bool _isLoading = false;
  bool _failed = false;
  bool _hasConnection = true;
  bool _apiError = false;
  String _messages = '';
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    _accentsTabController = TabController(
      vsync: this,
      length: LanguageConstants.LANGUAGE_ACCENT_COUNT,
      initialIndex: 0,
    );
    _accentsTabController.addListener(() {
      // Add listener get 2 notifications: when the animation starts to change
      // between tabs, and when animation ends. If indexIsChanging == false,
      // animation has ended and the code can be executed
      if (!_accentsTabController.indexIsChanging) {
        if (_initialLanguageAccentName != null) {
          if (_languagesWithPronunciations[_selectedLanguageIndex].hasAccents) {
            _initialLanguageAccentName =
                _languagesWithPronunciations[_selectedLanguageIndex]
                    .accents[_accentsTabController.index]
                    .name;
          }
        }
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_initialized) {
      _extractArguments();
    }
    _accentsTabController.index =
        _getPreferredLanguageAccentIndex(_selectedLanguageIndex);
    _fetchWordPronunciations();
    super.didChangeDependencies();
  }

  _extractArguments() {
    var args =
        ModalRoute.of(context).settings.arguments as WordDetailsArguments;
    if (args != null) {
      if (args.wordOriginal != null && args.wordOriginal.isNotEmpty) {
        _wordOriginal = args.wordOriginal;
      }
      if (args.languageName != null) {
        _initialLanguageName = args.languageName;
      }
      if (args.languageAccentName != null) {
        _initialLanguageAccentName = args.languageAccentName;
      }
      if (args.categorizationGroupIndex != null) {
        _selectedCategorizationGroupIndex = args.categorizationGroupIndex;
      }
    }
  }

  _fetchWordPronunciations() async {
    var state = AppStateManager.of(context).state;

    try {
      if (!_initialized && !_isLoading) {
        _initialized = true;
        setState(() {
          _isLoading = true;
        });
        _languagesWithPronunciations = await getWordPronunciations(
          context,
          word: _wordOriginal,
          interfaceLanguageCode: state.locale.languageCode,
          isUserLogged: state.isUserLogged,
        );

        if (_initialLanguageName != null &&
            _initialLanguageName.isNotEmpty &&
            _languagesWithPronunciations != null &&
            _languagesWithPronunciations.isNotEmpty) {
          for (LanguageWithPronunciations languageWithPronunciations
              in _languagesWithPronunciations) {
            if (languageWithPronunciations.languageName ==
                _initialLanguageName) {
              _selectedLanguageIndex = _languagesWithPronunciations
                  .indexOf(languageWithPronunciations);
            }
          }
        }

        _languages = _getLanguages(_languagesWithPronunciations);
        if (_selectedLanguage == null && _languages.isNotEmpty) {
          _selectedLanguage = _languages.first;
        }
        _failed = false;
      }
    } on ApiException catch (error) {
      _messages = error.getErrorMessages(context);
      if (_messages != null) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            _messages,
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
      setState(() {
        _failed = true;
        _apiError = true;
      });
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
      setState(() {
        _failed = true;
        _hasConnection = _deviceHasConnection;
      });
    } finally {
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  _onRequestPronunciation() async {
    var state = AppStateManager.of(context).state;
    var localizations = AppLocalizations.of(context);
    LanguageAccent languageAccent;
    if (_selectedLanguage.hasAccents) {
      languageAccent = _selectedLanguage.accents[_accentsTabController.index];
    }

    WordCategorizationGroup categorizationGroup;
    LanguageWithPronunciations selectedLanguageWithPronunciations =
        _languagesWithPronunciations[_selectedLanguageIndex];
    if (selectedLanguageWithPronunciations.wordInformation != null &&
        selectedLanguageWithPronunciations.wordInformation.isValidated &&
        selectedLanguageWithPronunciations
            .wordInformation.hasCategorizationData &&
        selectedLanguageWithPronunciations
            .wordInformation.categorizationGroups.isNotEmpty &&
        _selectedCategorizationGroupIndex != null) {
      categorizationGroup = selectedLanguageWithPronunciations.wordInformation
          .categorizationGroups[_selectedCategorizationGroupIndex];
    }
    try {
      await requestPronunciation(
        context,
        word: _wordOriginal,
        languageCode: _languages[_selectedLanguageIndex].code,
        languageAccentCode: languageAccent != null ? languageAccent.code : null,
        pronunciationGroupIndex:
            categorizationGroup != null ? categorizationGroup.groupIndex : null,
        interfaceLanguageCode: state.locale.languageCode,
      );

      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          languageAccent != null
              ? localizations.translate(
                  'word.pronunciationLanguageWithAccentRequested',
                  params: {
                      'languageName': _languages[_selectedLanguageIndex].name,
                      'accentName': languageAccent.name,
                    })
              : localizations
                  .translate('word.pronunciationLanguageRequested', params: {
                  'languageName': _languages[_selectedLanguageIndex].name,
                }),
          duration: Duration(seconds: 10),
          type: SNACKBAR_TYPE_OK,
        ),
      );
    } on ApiException catch (error) {
      List<ApiError> errors = error.getErrors();
      String message;
      for (ApiError _error in errors) {
        String translate =
            localizations.translate('error.api.${_error.errorApiCode}');
        if (message == null) {
          message = translate;
        } else {
          message = '$message\n$translate';
        }
      }
      if (message.isNotEmpty) {
        showCustomSnackBar(
          _scaffoldMessengerKey.currentContext,
          themedSnackBar(
            context,
            message,
            duration: Duration(seconds: 10),
            type: SNACKBAR_TYPE_ERROR,
          ),
        );
      }
    } on Exception catch (_) {
      bool _deviceHasConnection = await deviceHasConnection();
      var localizations = AppLocalizations.of(context);
      String message;
      if (_deviceHasConnection) {
        message = '${localizations.translate('error.service.title')}\n'
            '${localizations.translate('error.service.body')}';
      } else {
        message = '${localizations.translate('error.network.title')}\n'
            '${localizations.translate('error.network.body')}';
      }
      showCustomSnackBar(
        _scaffoldMessengerKey.currentContext,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  @override
  void dispose() {
    _accentsTabController.dispose();
    super.dispose();
  }

  Future<void> _onRefresh() async {
    setState(() {
      _initialized = false;
    });
    await _fetchWordPronunciations();
    setState(() {
      _initialized = true;
    });
  }

  _onSelectGroupIndex(int index) {
    Navigator.pop(context);
    setState(() {
      _selectedCategorizationGroupIndex = index;
    });
  }

  Widget _buildErrorMessage({message}) {
    if (_failed) {
      if (_apiError) {
        return ServiceError(_onRefresh, message: message);
      } else if (_hasConnection) {
        return ServiceError(_onRefresh);
      } else {
        return NetworkError(_onRefresh);
      }
    } else {
      return Container();
    }
  }

  Language _getLanguageFromLanguageWithPronunciations(
      LanguageWithPronunciations languageWithPronunciations) {
    var language = Language(
      id: languageWithPronunciations.languageName,
      name: languageWithPronunciations.languageName,
      code: languageWithPronunciations.accents != null &&
              languageWithPronunciations.accents.isNotEmpty
          ? languageWithPronunciations.accents.first.languageCode
          : languageWithPronunciations.items != null &&
                  languageWithPronunciations.items.isNotEmpty
              ? languageWithPronunciations.items.first.languageCode
              : null,
      wordInformation: languageWithPronunciations.wordInformation != null
          ? languageWithPronunciations.wordInformation
          : null,
      accents: (languageWithPronunciations.accents?.isNotEmpty ?? false)
          ? languageWithPronunciations.accents
              .map(
                (LanguageAccentWithPronunciations
                    languageAccentWithPronunciations) {
                  if (_initialLanguageAccentName != null &&
                      _initialLanguageAccentName.isNotEmpty &&
                      _initialLanguageAccentName ==
                          languageAccentWithPronunciations.name) {
                    _accentsTabController.index = languageWithPronunciations
                        .accents
                        .indexOf(languageAccentWithPronunciations);
                  }

                  var languageAccent = LanguageAccent(
                    id: languageAccentWithPronunciations.name,
                    name: languageAccentWithPronunciations.name,
                    code: languageAccentWithPronunciations.code,
                    languageName: languageAccentWithPronunciations.languageName,
                    languageCode: languageAccentWithPronunciations.languageCode,
                  );

                  return languageAccent;
                },
              )
              .toSet()
              .toList()
          : [],
    );
    if (_initialLanguageName != null && _initialLanguageName.isNotEmpty) {
      if (language.name == _initialLanguageName) {
        _selectedLanguage = language;
      }
    }
    return language;
  }

  List<Language> _getLanguages(
      List<LanguageWithPronunciations> languagesWithItems) {
    var languages = languagesWithItems
        .map(
          _getLanguageFromLanguageWithPronunciations,
        )
        .toSet()
        .toList();
    return languages;
  }

  int _getPreferredLanguageAccentIndex(int languageIndex) {
    int preferredLanguageAccentIndex;
    if (_languagesWithPronunciations != null &&
        _languagesWithPronunciations.isNotEmpty &&
        _languagesWithPronunciations[languageIndex] != null &&
        _languagesWithPronunciations[languageIndex].accents != null &&
        _languagesWithPronunciations[languageIndex].accents.isNotEmpty) {
      for (LanguageAccentWithPronunciations accent
          in _languagesWithPronunciations[languageIndex].accents) {
        if (accent.items != null &&
            accent.items != null &&
            accent.items.isNotEmpty &&
            preferredLanguageAccentIndex == null) {
          preferredLanguageAccentIndex =
              _languagesWithPronunciations[languageIndex]
                  .accents
                  .indexOf(accent);
        }
      }
    }
    return preferredLanguageAccentIndex ?? 0;
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    var themeData = Theme.of(context);

    Iterable<Widget> _getPronunciationTiles(
            List<Pronunciation> itemsByLanguage) =>
        itemsByLanguage.map(
          (item) => ListTile(
            contentPadding: const EdgeInsets.only(top: 4.0),
            leading: PlayButton(
              id: item.id,
              url: item.audioRealMp3,
              folderPath: item.languageCode,
            ),
            title: Text(
              item.isUserGenreFemale
                  ? item.countryName?.isNotEmpty ?? false
                      ? localizations.translate(
                          'shared.pronunciationByFemaleFromCountry',
                          params: {'country': '${item.countryName}'},
                        )
                      : localizations.translate('shared.gender.female')
                  : item.countryName?.isNotEmpty ?? false
                      ? localizations.translate(
                          'shared.pronunciationByMaleFromCountry',
                          params: {'country': '${item.countryName}'},
                        )
                      : localizations.translate('shared.gender.male'),
              style: themeData.textTheme.subtitle1,
            ),
            trailing: IconButton(
                icon: Icon(Icons.more_horiz),
                onPressed: () {
                  var state = AppStateManager.of(context).state;
                  if (state.isUserLogged) {
                    onPronunciationMenuOpen(
                      _scaffoldMessengerKey.currentContext,
                      item,
                      () => null,
                      wordInformation: _selectedLanguage.wordInformation,
                    );
                  } else {
                    Navigator.of(context).pushNamed(RouteConstants.LOGIN);
                  }
                }),
          ),
        );

    Widget _getPronunciationListView(List<Pronunciation> items) {
      List<Pronunciation> pronunciations;
      if (_selectedCategorizationGroupIndex != null) {
        pronunciations = items
            .where((Pronunciation pronunciation) =>
                pronunciation.pronunciationGroupIndex ==
                _selectedCategorizationGroupIndex)
            .toList();
      } else {
        pronunciations = items;
      }

      String message = pronunciations.isNotEmpty
          ? localizations.translate('word.requestNewPronunciation')
          : _selectedLanguage?.hasAccents ?? false
              ? localizations
                  .translate('word.requestFirstLanguageAccentPronunciation')
              : localizations
                  .translate('word.requestFirstLanguagePronunciation');

      Widget requestNewPronunciation = Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomTooltip(
            message: message,
            child: ThemedBubbleInkWell(
              message: localizations.translate('shared.request'),
              iconType: THEMED_BUBBLE_ICON_TYPE_PRONUNCIATION,
              onTap: () {
                var state = AppStateManager.of(context).state;
                if (state.isUserLogged) {
                  _onRequestPronunciation();
                } else {
                  Navigator.of(context).pushNamed(RouteConstants.LOGIN);
                }
              },
            ),
          ),
        ],
      );

      List<Widget> widgets = [];
      if (pronunciations.isNotEmpty) {
        widgets.addAll(
          ListTile.divideTiles(
            color: themeData.dividerColor,
            context: context,
            tiles: _getPronunciationTiles(pronunciations),
          ).toList(),
        );
        widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: requestNewPronunciation,
          ),
        );
      } else {
        widgets.add(
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
                child: Text(
                  _selectedLanguage?.hasAccents ?? false
                      ? localizations
                          .translate('word.languageAccentWithoutPronunciations')
                      : localizations
                          .translate('word.languageWithoutPronunciations'),
                  style: themeData.textTheme.bodyText2,
                ),
              ),
            ],
          ),
        );
        widgets.add(
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: requestNewPronunciation,
          ),
        );
      }

      return Padding(
        padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: ListView(
          shrinkWrap: pronunciations.isEmpty,
          children: widgets,
        ),
      );
    }

    Widget _getWordSpellings() {
      List<Widget> widgets = [];
      widgets.add(
        Text(
          _wordOriginal,
          style: themeData.textTheme.bodyText2
              .copyWith(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      );
      if (_selectedLanguage != null &&
          _selectedLanguage.wordInformation != null &&
          _selectedLanguage.wordInformation.alternativeSpellings != null &&
          _selectedLanguage.wordInformation.alternativeSpellings.isNotEmpty) {
        for (WordSpelling spelling
            in _selectedLanguage.wordInformation.alternativeSpellings) {
          if (spelling.spelling.isNotEmpty) {
            widgets.add(
              Text(
                spelling.spelling,
                style: themeData.textTheme.bodyText2
                    .copyWith(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            );
          }
        }
      }
      return Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets,
            ),
          ),
        ],
      );
    }

    Widget _getLanguageAccentTabBarViewContent(
        LanguageAccentWithPronunciations accent) {
      if (_selectedCategorizationGroupIndex != null &&
          (accent.items.isEmpty ||
              accent.items
                  .where((pronunciation) =>
                      pronunciation.pronunciationGroupIndex ==
                      _selectedCategorizationGroupIndex)
                  .isEmpty) &&
          ((accent.pendingPronounceInformation
                  .isPendingPronunciationAndHasPronunciationRequest) ||
              (accent.pendingPronounceInformation.categorizationGroups !=
                      null &&
                  accent
                      .pendingPronounceInformation
                      .categorizationGroups[_selectedCategorizationGroupIndex]
                      .isPendingPronunciationAndHasPronunciationRequest))) {
        return Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: ListTile(
            contentPadding: const EdgeInsets.only(top: 4.0),
            leading: RecordButton(),
            title: Text(
              _wordOriginal,
              style: themeData.textTheme.subtitle1,
            ),
            onTap: () async {
              var state = AppStateManager.of(context).state;
              if (state.userInfo.user.isContributor) {
                RecordArguments args = RecordArguments(
                  word: PendingPronounceWord(
                    word: _wordOriginal,
                    original: _wordOriginal,
                    languageName: _selectedLanguage.name,
                    languageCode: _selectedLanguage.code,
                    languageAccentCode: _selectedLanguage.hasAccents
                        ? _selectedLanguage
                            .accents[_accentsTabController.index].code
                        : null,
                    languageAccentName: _selectedLanguage.hasAccents
                        ? _selectedLanguage
                            .accents[_accentsTabController.index].name
                        : null,
                    isPhrase: _selectedLanguage.wordInformation.isPhrase,
                    isValidated: _selectedLanguage.wordInformation.isValidated,
                  ),
                );
                await Navigator.pushNamed(
                  context,
                  RouteConstants.RECORD,
                  arguments: args,
                );
              } else {
                showLinguisticProfileAlertDialog(context);
              }
            },
          ),
        );
      } else {
        return _getPronunciationListView(
          accent.items,
        );
      }
    }

    Widget _getLanguageWithAccentsContent() => Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                margin: EdgeInsets.all(0.0),
                color: themeData.appBarTheme.backgroundColor,
                elevation: 3.0,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(4.0),
                    bottomRight: Radius.circular(4.0),
                  ),
                ),
                child: TabBarWithFlexibleTabs(
                  key: GlobalKey(),
                  child: TabBar(
                    controller: _accentsTabController,
                    indicatorColor: colorBlue,
                    indicatorWeight: 3.0,
                    tabs: [
                      Tab(
                        text: _selectedLanguage.accents[0].name ??
                            _selectedLanguage.accents[0].code,
                      ),
                      Tab(
                        text: _selectedLanguage.accents[1].name ??
                            _selectedLanguage.accents[1].code,
                      ),
                      Tab(
                        text: _selectedLanguage.accents[2].name ??
                            _selectedLanguage.accents[2].code,
                      ),
                    ],
                    labelStyle: themeData.textTheme.bodyText2.copyWith(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                    labelColor: colorBlue,
                    unselectedLabelStyle:
                        themeData.textTheme.bodyText2.copyWith(
                      fontSize: 16,
                    ),
                    unselectedLabelColor: themeData.textTheme.bodyText2.color,
                  ),
                ),
              ),
              Expanded(
                child: TabBarView(
                  key: GlobalKey(),
                  controller: _accentsTabController,
                  children: _languagesWithPronunciations[_selectedLanguageIndex]
                      .accents
                      .map(_getLanguageAccentTabBarViewContent)
                      .toList(),
                ),
              ),
            ],
          ),
        );

    Widget _getLanguageWithoutAccentsContent() => Expanded(
          child: _languagesWithPronunciations != null &&
                  _languagesWithPronunciations.isNotEmpty &&
                  (_selectedLanguage.wordInformation != null &&
                      ((!_selectedLanguage
                                  .wordInformation.hasCategorizationData &&
                              // ignore: lines_longer_than_80_chars
                              _languagesWithPronunciations[
                                      _selectedLanguageIndex]
                                  .hasItems) ||
                          (_selectedLanguage
                                  .wordInformation.hasCategorizationData &&
                              _languagesWithPronunciations[
                                      _selectedLanguageIndex]
                                  .items
                                  .where((pronunciation) =>
                                      pronunciation.pronunciationGroupIndex ==
                                      _selectedCategorizationGroupIndex)
                                  .isNotEmpty)))
              ? _getPronunciationListView(
                  _languagesWithPronunciations[_selectedLanguageIndex].items,
                )
              : _languagesWithPronunciations != null &&
                      _languagesWithPronunciations.isNotEmpty &&
                      ((_languagesWithPronunciations[_selectedLanguageIndex]
                                      .pendingPronounceInformation !=
                                  null &&
                              _languagesWithPronunciations[
                                      _selectedLanguageIndex]
                                  .pendingPronounceInformation
                                  // ignore: lines_longer_than_80_chars
                                  .isPendingPronunciationAndHasPronunciationRequest) ||
                          (_languagesWithPronunciations[_selectedLanguageIndex]
                                      .pendingPronounceInformation
                                      .categorizationGroups !=
                                  null &&
                              _selectedCategorizationGroupIndex != null &&
                              _languagesWithPronunciations[
                                      _selectedLanguageIndex]
                                  .pendingPronounceInformation
                                  .categorizationGroups[
                                      _selectedCategorizationGroupIndex]
                                  // ignore: lines_longer_than_80_chars
                                  .isPendingPronunciationAndHasPronunciationRequest))
                  ? Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 20.0, right: 20.0),
                      child: ListTile(
                        contentPadding: const EdgeInsets.only(top: 4.0),
                        leading: RecordButton(),
                        title: Text(
                          _wordOriginal,
                          style: themeData.textTheme.subtitle1,
                        ),
                        onTap: () async {
                          var state = AppStateManager.of(context).state;
                          if (state.userInfo.user.isContributor) {
                            RecordArguments args = RecordArguments(
                              word: PendingPronounceWord(
                                word: _wordOriginal,
                                original: _wordOriginal,
                                languageName: _selectedLanguage.name,
                                languageCode: _selectedLanguage.code,
                                isPhrase:
                                    _selectedLanguage.wordInformation.isPhrase,
                                isValidated: _selectedLanguage
                                    .wordInformation.isValidated,
                              ),
                            );
                            await Navigator.pushNamed(
                              context,
                              RouteConstants.RECORD,
                              arguments: args,
                            );
                          } else {
                            showLinguisticProfileAlertDialog(context);
                          }
                        },
                      ),
                    )
                  : Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 12.0, bottom: 12.0),
                            child: Text(
                              localizations.translate(
                                  'word.languageWithoutPronunciations'),
                            ),
                          ),
                          BlueBubbleSuggestionMessage(
                            message: localizations.translate(
                                'word.requestFirstLanguagePronunciation'),
                            onTap: () {
                              var state = AppStateManager.of(context).state;
                              if (state.isUserLogged) {
                                _onRequestPronunciation();
                              } else {
                                Navigator.of(context)
                                    .pushNamed(RouteConstants.LOGIN);
                              }
                            },
                            secondaryIcon: Icon(
                              ForvoIcons.mic,
                              size: 25,
                              color: colorBlue,
                            ),
                          ),
                        ],
                      ),
                    ),
        );

    Widget _getContent() => Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _languagesWithPronunciations.isNotEmpty
                ? LanguageButtonBar(
                    languages: _languages,
                    onPressed: (index) {
                      setState(() {
                        _selectedLanguageIndex = index;
                        _selectedLanguage = _languages[index];
                        _initialLanguageName = null;
                        _initialLanguageAccentName = null;
                        _selectedCategorizationGroupIndex = null;
                        _accentsTabController.index =
                            _getPreferredLanguageAccentIndex(index);
                        if (_selectedLanguage.wordInformation != null &&
                            _selectedLanguage
                                .wordInformation.hasCategorizationData &&
                            _selectedCategorizationGroupIndex == null) {
                          _selectedCategorizationGroupIndex = 0;
                        }
                      });
                    },
                    selectedIndex: _selectedLanguageIndex,
                  )
                : Container(),
            _languagesWithPronunciations.isNotEmpty &&
                    _languagesWithPronunciations[_selectedLanguageIndex] !=
                        null &&
                    _languagesWithPronunciations[_selectedLanguageIndex]
                        .hasCategorizationGroups
                ? WordCategorizationGroupData(
                    categorizationGroups:
                        _languagesWithPronunciations[_selectedLanguageIndex]
                            .wordInformation
                            .categorizationGroups,
                    wordLanguageCode: _selectedLanguage.code,
                    onSelectGroup: _onSelectGroupIndex,
                    groupIndex: _selectedCategorizationGroupIndex,
                  )
                : Container(
                    decoration: BoxDecoration(
                      color: themeData.appBarTheme.backgroundColor,
                      border: Border(
                        bottom: BorderSide(color: themeData.dividerColor),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 12.0,
                        horizontal: 20.0,
                      ),
                      child: _getWordSpellings(),
                    ),
                  ),
            _selectedLanguage?.hasAccents ?? false
                ? _getLanguageWithAccentsContent()
                : _getLanguageWithoutAccentsContent(),
          ],
        );

    return CustomScaffold(
      scaffoldMessengerKey: _scaffoldMessengerKey,
      bottomNavBarIndexIsActive: false,
      bottomNavBarIndex: HOME_SECTION_START,
      body: _isLoading
          ? CustomCircularProgressIndicator()
          : _failed
              ? _buildErrorMessage(message: _messages)
              : _getContent(),
    );
  }
}
