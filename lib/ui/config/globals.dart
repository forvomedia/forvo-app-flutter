import 'package:flutter/material.dart';

AppGlobals appGlobals = AppGlobals();

class AppGlobals {
  GlobalKey<ScaffoldState> _scaffoldKey;
  GlobalKey<NavigatorState> _navigatorKey;
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey;

  AppGlobals() {
    _scaffoldKey = GlobalKey();
    _navigatorKey = GlobalKey();
    _scaffoldMessengerKey = GlobalKey();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey =>
      _scaffoldMessengerKey;
}
