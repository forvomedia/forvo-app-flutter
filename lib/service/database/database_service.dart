import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/chat_conversation.dart';
import 'package:forvo/model/chat_message.dart';
import 'package:forvo/model/database_chat_conversation.dart';
import 'package:forvo/model/database_chat_message.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/db_util.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static final DatabaseService _instance = DatabaseService._internal();
  Database _database;
  String _username;
  bool _isOpenOrOpening = false;
  @protected
  static const String _tableConversations = 'conversations';
  @protected
  static const String _tableMessages = 'messages';
  @protected
  static const String _createTableConversations = 'CREATE TABLE conversations ('
      '_id TEXT PRIMARY KEY NOT NULL, '
      'idConversation TEXT, '
      'userTo TEXT, '
      'userToBlocked INTEGER, '
      'unReadMessages INTEGER, '
      'lastMessage TEXT, '
      'lastMessageAddtimeAPI TEXT, '
      'lastMessageAddtimeOrder TEXT, '
      'lastMessageAddtimeInsert TEXT)';
  @protected
  static const String _createTableMessages = 'CREATE TABLE messages ('
      '_id TEXT PRIMARY KEY NOT NULL, '
      'idMessageAPI TEXT , '
      'idMessageLocal TEXT , '
      'idConversation TEXT, '
      'userFrom TEXT, '
      'userTo TEXT DEFAULT (null), '
      'isRead INTEGER, '
      'message TEXT, '
      'addTimeAPI TEXT, '
      'addTimeOrder TEXT, '
      'addTimeInsert TEXT)';
  static const int DB_VERSION = 1;

  factory DatabaseService() => _instance;

  Database get db => _database;

  bool get isDatabaseOpen =>
      (_database != null && _database.isOpen) || _isOpenOrOpening;

  DatabaseService._internal() {
    // TODO: remove comment
    print('DatabaseService > Create instance');
    if (!isDatabaseOpen) {
      _getInstance();
    }
  }

  Future<void> _getInstance() async {
    // TODO: remove comment
    print('DatabaseService > get instance');
    _isOpenOrOpening = true;
    _username = await getUserUsername();
    // TODO: remove comment
    print('DatabaseService > get instance > ${_username}_database.db');
    final database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), '${_username}_database.db'),
      // When the database is first created, create a tables
      onCreate: (db, version) {
        db.execute(_createTableConversations);
        db.execute(_createTableMessages);
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: DB_VERSION,
    );
    _database = await database;
    var sqliteVersion = (await _database.rawQuery('SELECT sqlite_version()'))
        .first
        .values
        .first;
    // TODO: remove comment
    print('SQLite version: $sqliteVersion');
  }

  Future<void> resetDatabase() async => await _getInstance();

  close() async {
    if (_database.isOpen) {
      await _database.close();
      _database = null;
      _isOpenOrOpening = false;
    }
  }

  truncateTableConversations() async {
    await _database.delete(_tableConversations);
  }

  truncateTableMessages() async {
    await _database.delete(_tableMessages);
  }

  Future<String> getLastMessageID() async {
    String idMessage = '0';
    List<Map> maps =
        await _database.rawQuery('SELECT MAX(idMessageAPI) AS `idMessageAPI` '
            'FROM $_tableMessages '
            'WHERE idMessageAPI <> "" AND idMessageAPI <> "null"');
    if (maps.isNotEmpty) {
      var idMessageAPI = maps.last['idMessageAPI'];
      if (idMessageAPI != null && idMessageAPI != 'null') {
        idMessage = idMessageAPI;
      }
    }
    return idMessage;
  }

  Future<List<DatabaseChatMessage>> getMessagesToForvo(String userFrom) async {
    List<Map> maps = await _database.rawQuery('SELECT * FROM $_tableMessages '
        'WHERE idMessageAPI = "" AND userFrom = "$userFrom" '
        'ORDER BY addTimeInsert');
    List<DatabaseChatMessage> messages =
        DatabaseChatMessage.databaseChatMessageListFromJson(maps);
    return messages;
  }

  saveConversations(List<ChatConversation> conversations) async {
    for (ChatConversation conversation in conversations) {
      // TODO: remove comment
      // print('database_service.dart > saveConversations > '
      //     'CONVERSATION TO INSERT: ${conversation.toString()}');

      DatabaseChatConversation dbConversation = DatabaseChatConversation(
        id: conversation.username,
        idConversation: conversation.id.toString(),
        userTo: conversation.username,
        userToBlocked: getIntValueFromBool(boolValue: conversation.isBlocked),
        unReadMessages: conversation.unreadMessages,
        lastMessage: conversation.messages?.last?.message ?? '',
        lastMessageAddtimeAPI: conversation.messages?.last?.addtime ?? '',
        lastMessageAddtimeOrder:
            conversation.messages?.last?.addtimeOrder?.toString() ?? '',
      );
      // TODO: remove comment
      // print('database_service.dart > saveConversations > '
      //     'DATABASE CONVERSATION TO INSERT: ${dbConversation.toString()}');
      await insertOrUpdateConversation(dbConversation);
      if (conversation.messages.isNotEmpty) {
        for (ChatMessage message in conversation.messages) {
          if ((message.userTo == _username &&
                  message.userFrom == conversation.username) ||
              (message.userFrom == _username &&
                  message.userTo == conversation.username)) {
            saveMessage(
              message,
              idConversation: conversation.id.toString(),
            );
          }
        }
      }
    }
  }

  saveMessage(
    ChatMessage message, {
    String idConversation,
    bool controlNotify = true,
    bool controlInsert = false,
  }) async {
    // TODO: remove comment
    print('database_service.dart > saveMessage > '
        'MESSAGE TO INSERT: ${message.toString()}');
    int currentMillis = getCurrentTimeMillis();
    String addTimeInsert = currentMillis.toString();
    // Hack for order with old messages
    String addTimeOrder = message.addtimeOrder?.toString() == '10000000'
        ? addTimeInsert.substring(0, addTimeInsert.length - 1)
        : addTimeInsert;
    DatabaseChatMessage dbMessage = DatabaseChatMessage(
      id: (message.idLocal != null &&
              message.idLocal.isNotEmpty &&
              message.idLocal != 'null')
          ? message.idLocal
          : message.id.toString(),
      idMessageAPI: message.id.toString(),
      idMessageLocal: (message.idLocal != null &&
              message.idLocal.isNotEmpty &&
              message.idLocal != 'null')
          ? message.idLocal
          : '',
      idConversation: idConversation != null
          ? idConversation
          : message.idConversation.toString(),
      userFrom: message.userFrom,
      userTo: message.userTo,
      message: message.message,
      isRead: getIntValueFromBool(
          boolValue: message.userFrom == _username || message.isRead),
      addTimeAPI: message.addtime?.isNotEmpty ?? false
          ? message.addtime
          : formatMillis(currentMillis, DEFAULT_DATETIME_UTC_INPUT_FORMAT),
      addTimeOrder: addTimeOrder,
      addTimeInsert: addTimeInsert,
    );
    // TODO: remove comment
    print('database_service.dart > saveMessage > '
        'DATABASE MESSAGE TO INSERT: ${dbMessage.toString()}');
    await insertOrUpdateMessage(
      dbMessage,
      controlNotify: controlNotify,
      controlInsert: controlInsert,
    );
  }

  insertOrUpdateConversation(DatabaseChatConversation conversation) async {
    int updatedRows = await _database.update(
      _tableConversations,
      conversation.toMap(),
      where: '_id = ?',
      whereArgs: [conversation.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    if (updatedRows == 0) {
      // TODO: remove comment
      // print('database_service.dart > insertOrUpdateConversation > '
      //     'BEFORE INSERT CONVERSATION: $conversation');
      int lastRowId = await _database.insert(
        _tableConversations,
        conversation.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      // TODO: remove comment
      print('database_service.dart > insertOrUpdateConversation > '
          'INSERTED CONVERSATION ID: $lastRowId');
    } else {
      // TODO: remove comment
      print('database_service.dart > insertOrUpdateConversation > '
          'UPDATED ROWS: $updatedRows');
    }

    // TODO: remove comment
    // int totalConversations = await totalConversationsById(conversation.id);
    // if (totalConversations > 0) {
    // } else {
    //   await _database.insert(tableConversation, conversation.toMap());
    // }
  }

  insertOrUpdateMessage(DatabaseChatMessage message,
      {bool controlNotify, bool controlInsert}) async {
    bool _isUserBlocked = await isUserBlocked(message.idConversation);
    if (controlInsert && _isUserBlocked && message.userTo == _username) {
      print('database_service.dart > insertOrUpdateMessage > '
          'USER BLOCKED. STOP THE MESSAGE');
    } else {
      // Know if the message exists.
      bool messageExists = false;
      if (message.idMessageLocal.isNotEmpty) {
        messageExists =
            await totalMessagesByIdMessageLocal(message.idMessageLocal) > 0;
      } else {
        messageExists =
            await totalMessagesByIdMessageAPI(message.idMessageAPI) > 0;
      }

      if (messageExists) {
        int updatedRows = await _database.update(
          _tableMessages,
          message.toMapWithoutAddTimeInsert(),
          where: 'idMessageAPI = ? OR idMessageLocal = ?',
          whereArgs: [message.idMessageAPI, message.idMessageLocal],
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
        // TODO: remove comment
        print('database_service.dart > insertOrUpdateMessage > '
            'UPDATED MESSAGES: $updatedRows');
      } else {
        int lastRowId = await _database.insert(
          _tableMessages,
          message.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
        // TODO: remove comment
        print('database_service.dart > insertOrUpdateMessage > '
            'INSERTED MESSAGE ID: $lastRowId');
      }

      // Know if the conversation exists
      String user = message.userFrom;
      if (user == _username) {
        user = message.userTo;
      }

      bool conversationExists = await totalConversationsById(user) > 0;
      if (conversationExists) {
        int unreadMessages = await totalUnreadMessagesByConversation(
            message.idConversation, user);

        List<Map> maps = await _database
            .rawQuery('SELECT message, addTimeAPI, addTimeOrder, addTimeInsert '
                'FROM $_tableMessages '
                'WHERE addTimeInsert = ('
                'SELECT MAX(addTimeInsert) FROM $_tableMessages '
                'WHERE idConversation = "${message.idConversation}"'
                ')');

        String messageString = '';
        String addTimeAPI;
        String addTimeOrder;
        String addTimeInsert;
        if (maps.isNotEmpty) {
          var lastMessage = maps.last;
          messageString = lastMessage['message'];
          addTimeAPI = lastMessage['addTimeAPI'] != ''
              ? lastMessage['addTimeAPI']
              : null;
          addTimeOrder = lastMessage['addTimeOrder'] != ''
              ? lastMessage['addTimeOrder']
              : null;
          addTimeInsert = lastMessage['addTimeInsert'] != ''
              ? lastMessage['addTimeInsert']
              : null;
        }

        DatabaseChatConversation conversationToUpdate =
            DatabaseChatConversation(
          idConversation: message.idConversation,
          unReadMessages: unreadMessages,
          lastMessage: messageString,
          lastMessageAddtimeAPI: addTimeAPI,
          lastMessageAddtimeOrder: addTimeOrder,
          lastMessageAddtimeInsert: addTimeInsert,
        );

        // TODO: remove comment
        print('database_service.dart > insertOrUpdateMessage > '
            'CONVERSATION TO UPDATE: $conversationToUpdate');

        int updatedRows = await _database.update(
          _tableConversations,
          conversationToUpdate.toMapToUpdateConversation(),
          where: 'userTo = ?',
          whereArgs: [user],
          conflictAlgorithm: ConflictAlgorithm.replace,
        );
        // TODO: remove comment
        print('database_service.dart > insertOrUpdateMessage > '
            'UPDATED CONVERSATIONS: $updatedRows');
      } else {
        DatabaseChatConversation newConversation = DatabaseChatConversation(
          id: user,
          idConversation: user,
          userTo: user,
          userToBlocked: 0,
          unReadMessages: message.isRead,
          lastMessage: message.message,
          lastMessageAddtimeAPI: message.addTimeAPI,
          lastMessageAddtimeOrder: message.addTimeOrder,
          lastMessageAddtimeInsert: message.addTimeInsert,
        );
        await insertOrUpdateConversation(newConversation);
      }
    }
  }

  /// Not allowed message by the other user. The other user has blocked the
  /// conversation.
  ///
  /// @param controlBlockedNotify Control message in case of the message comes
  /// from API. If it's come from the API we will call a callback to show a
  /// dialog.
  deleteMessage(String idMessageLocal, {bool controlBlockedNotify}) async {
    // TODO: remove comment
    print('database_service.dart > deleteMessage > '
        'idMessageLocal: $idMessageLocal > '
        'controlBlockedNotify: ${controlBlockedNotify.toString()}');
    if (idMessageLocal != null && idMessageLocal.isNotEmpty) {
      String idConversation = '';
      List<Map> idConversationMaps =
          await _database.rawQuery('SELECT idConversation FROM $_tableMessages '
              'WHERE idMessageLocal = "$idMessageLocal"');
      if (idConversationMaps.isNotEmpty) {
        idConversation = idConversationMaps.last['idConversation'];
      }

      await _database.delete(
        _tableMessages,
        where: 'idMessageLocal = ?',
        whereArgs: [idMessageLocal],
      );

      if (idConversation.isNotEmpty) {
        // TODO: remove comment
        print('UPDATE CONVERSATION LAST MESSAGE > $idConversation');
        String userTo = '';
        List<Map> userToMaps =
            await _database.rawQuery('SELECT userTo FROM $_tableConversations '
                'WHERE idConversation = "$idConversation"');
        if (userToMaps.isNotEmpty) {
          userTo = userToMaps.last['userTo'];
        }

        String messageString = '';
        String addTimeAPI = '';
        String addTimeOrder = '';
        String addTimeInsert = '';
        List<Map> messageMaps = await _database
            .rawQuery('SELECT message, addTimeAPI, addTimeOrder, addTimeInsert '
                'FROM $_tableMessages WHERE addTimeInsert = '
                '(SELECT MAX(addTimeInsert) FROM $_tableMessages '
                'WHERE idConversation = "$idConversation")');
        if (messageMaps.isNotEmpty) {
          messageString = messageMaps.last['message'];
          addTimeAPI = messageMaps.last['addTimeAPI'];
          addTimeOrder = messageMaps.last['addTimeOrder'];
          addTimeInsert = messageMaps.last['addTimeInsert'];
        }

        var conversationUpdateMap = {
          'lastMessage': messageString,
          'lastMessageAddtimeAPI': addTimeAPI,
          'lastMessageAddtimeOrder': addTimeOrder,
          'lastMessageAddtimeInsert': addTimeInsert
        };
        await _database.update(
          _tableConversations,
          conversationUpdateMap,
          where: 'idConversation = ?',
          whereArgs: [idConversation],
          conflictAlgorithm: ConflictAlgorithm.replace,
        );

        // TODO: possible listener to call mMessageUpdate
        if (userTo != _username) {
          if (controlBlockedNotify) {
            // mMessageUpdate.onMessageBlocked();
          }
          // mMessageUpdate.onMessageUpdate();
        }
      }
    }
  }

  Future<bool> isUserBlocked(String idConversation) async {
    bool blocked = false;
    List<Map> maps = await _database
        .rawQuery('SELECT userToBlocked FROM $_tableConversations '
            'WHERE idConversation = "$idConversation" ');
    if (maps.isNotEmpty) {
      var userToBlocked = maps.last['userToBlocked'];
      if (userToBlocked != null) {
        blocked = userToBlocked > 0;
      }
    }
    return blocked;
  }

  Future<int> totalConversationsById(String id) async =>
      Sqflite.firstIntValue(await _database.rawQuery(
          'SELECT COUNT(*) FROM $_tableConversations WHERE _id = "$id"'));

  Future<int> totalMessagesByIdMessageLocal(String id) async =>
      Sqflite.firstIntValue(await _database.rawQuery(
          'SELECT COUNT(*) FROM $_tableMessages WHERE idMessageLocal = "$id"'));

  Future<int> totalMessagesByIdMessageAPI(String id) async =>
      Sqflite.firstIntValue(await _database.rawQuery(
          'SELECT COUNT(*) FROM $_tableMessages WHERE idMessageAPI = "$id"'));

  Future<int> totalUnreadMessagesByConversation(
          String idConversation, String user) async =>
      Sqflite.firstIntValue(await _database
          .rawQuery('SELECT  COUNT(*) FROM $_tableMessages WHERE isRead = "0" '
              'AND (idConversation = "$idConversation" '
              'OR idConversation = "user")'));

  Future<List<DatabaseChatConversation>> getConversations() async {
    List<Map> maps =
        await _database.rawQuery('SELECT * FROM $_tableConversations '
            ' ORDER BY lastMessageAddtimeAPI ASC, '
            ' CAST(lastMessageAddtimeInsert AS UNSIGNED)  ASC');

    return DatabaseChatConversation.databaseChatConversationListFromJson(maps);
  }

  Future<List<DatabaseChatMessage>> getMessages(String idConversation) async {
    List<Map> maps = await _database.rawQuery('SELECT * FROM $_tableMessages '
        'WHERE idConversation = "$idConversation" ORDER BY addTimeInsert');

    return DatabaseChatMessage.databaseChatMessageListFromJson(maps);
  }

  markMessagesAsRead(String idConversation) async {
    await _database.update(
      _tableConversations,
      {'unReadMessages': 0},
      where: 'idConversation = ?',
      whereArgs: [idConversation],
    );
    await _database.update(
      _tableMessages,
      {'isRead': 1},
      where: 'idConversation = ?',
      whereArgs: [idConversation],
    );
  }

  updateUserBlockStatusByIdConversation(
    String idConversation, {
    @required bool blocked,
  }) async {
    if (idConversation != null && idConversation.isNotEmpty) {
      int userToBlocked = blocked ? 1 : 0;
      await _database.update(
        _tableConversations,
        {'userToBlocked': userToBlocked},
        where: 'idConversation = ?',
        whereArgs: [idConversation],
      );
    }
  }

  updateUserBlockStatusByUsername(
    String username, {
    @required bool blocked,
  }) async {
    if (username != null && username.isNotEmpty) {
      int userToBlocked = blocked ? 1 : 0;
      await _database.update(
        _tableConversations,
        {'userToBlocked': userToBlocked},
        where: 'userTo = ?',
        whereArgs: [username],
      );
    }
  }

  Future<DatabaseChatConversation> getConversationByUser(
      String username) async {
    List<Map> maps =
        await _database.rawQuery('SELECT * FROM $_tableConversations '
            'WHERE userTo = "$username" LIMIT 1');

    if (maps.isNotEmpty) {
      return DatabaseChatConversation.fromJson(maps.last);
    }
    return null;
  }
}
