import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/attributes_and_pronunciations_data.dart';
import 'package:forvo/model/blocked_user.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/language_with_words.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/model/user_profile.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';
import 'package:http/http.dart' as http;

Future<List<LanguageWithPronunciations>> getUserFavoritePronunciations(
  BuildContext context, {
  String username,
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-favorite-pronunciations';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndPronunciationsData userFavoritePronunciations =
          AttributesAndPronunciationsData.fromJson(responseJson);
      return userFavoritePronunciations.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<LanguageWithPronunciations> getUserFavoritePronunciationsByLang(
  BuildContext context, {
  String username,
  String languageCode,
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-lang-fav-pronunciations';
  String _paramUsername = 'username';
  String _paramLanguage = 'language';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramLanguage,
    languageCode,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramLanguage/$languageCode/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      LanguageWithPronunciations userFavoritePronunciations =
          LanguageWithPronunciations.fromJson(responseJson['data']);
      return userFavoritePronunciations;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<List<Language>> getUserFavoritePronunciationLangs(
  BuildContext context, {
  String username,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-fav-pronunciations-langs';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      List<Language> languages =
          Language.languageListFromJson(responseJson['data']);
      return languages;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<LanguageWithPronunciations> getUserPronunciations(
  BuildContext context, {
  String username,
  String languageCode,
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-pronunciations';
  String _paramUsername = 'username';
  String _paramLanguage = 'language';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramLanguage,
    languageCode,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramLanguage/$languageCode/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      LanguageWithPronunciations userPronunciations =
          LanguageWithPronunciations.fromJson(responseJson['data']);
      return userPronunciations;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<LanguageWithPronunciations> getUserTermPronunciationsByLang(
  BuildContext context, {
  String username,
  String languageCode,
  String term,
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-lang-term-pronunciations';
  String _paramUsername = 'username';
  String _paramLanguage = 'language';
  String _paramTerm = 'term';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramLanguage,
    languageCode,
    _paramTerm,
    term,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramLanguage/$languageCode/'
      '$_paramTerm/$term/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      LanguageWithPronunciations userPronunciations =
          LanguageWithPronunciations.fromJson(responseJson['data']);
      return userPronunciations;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<List<Language>> getUserPronunciationLangs(
  BuildContext context,
  String username, {
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-pronunciations-langs';
  String _paramUser = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUser,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUser/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      List<Language> languages =
          Language.languageListFromJson(responseJson['data']);
      return languages;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<LanguageWithWords> getUserRequestedWords(
  BuildContext context, {
  String username,
  String languageCode,
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-requested-words';
  String _paramUsername = 'username';
  String _paramLanguage = 'language';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramLanguage,
    languageCode,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramLanguage/$languageCode/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      LanguageWithWords userRequestedWords =
          LanguageWithWords.fromJson(responseJson);
      return userRequestedWords;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<List<Language>> getUserRequestedWordsLangs(
  BuildContext context,
  String username, {
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-requested-words-langs';
  String _paramUser = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUser,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUser/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      List<Language> languages =
          Language.languageListFromJson(responseJson['data']);
      return languages;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<UserProfile> getUserProfile(
  BuildContext context, {
  String username,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'get-user-profile-info';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      UserProfile userProfile = UserProfile.fromJson(responseJson['userInfo']);

      return userProfile;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<UserInfo> editUserInfo(
  BuildContext context, {
  String email,
  String password,
  String repassword,
  String firstName,
  String lastName,
  String gender,
  String languageCode,
  String latitude,
  String longitude,
  String birthdateDay,
  String birthdateMonth,
  String birthdateYear,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'edit-user-info';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method';

  FormData formData = FormData.fromMap({
    'email': email,
    'password': password,
    'repassword': repassword,
    'first_name': firstName,
    'last_name': lastName,
    'sex': gender,
    'language': languageCode,
    'lat': latitude,
    'lng': longitude,
    'birthdate_day': birthdateDay,
    'birthdate_month': birthdateMonth,
    'birthdate_year': birthdateYear,
    'interface-language': interfaceLanguageCode,
  });

  try {
    var response = await Http.of(context).multipartPost('$_url',
        formData: formData, onSendProgressCallback: null);
    if (response != null &&
        response.statusCode != null &&
        response.statusCode == 200) {
      Map<String, dynamic> jsonData = Map<String, dynamic>.from(response.data);
      UserInfo userInfo = UserInfo.fromJson(jsonData['userInfo']);
      return userInfo;
    }
  } on ApiException catch (e, s) {
    debugPrint('$e => $s');
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<bool> updateNotificationSettings(
  BuildContext context, {
  bool publicProfile,
  bool messagesFromForvo,
  bool notificationPronWords,
  bool notificationVotes,
  bool messagesFromUsers,
  bool emailNotifyMessages,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'update-notification-settings';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'is_public': publicProfile ? 1 : 0,
    'notify': notificationPronWords ? 1 : 0,
    'votes': notificationVotes ? 1 : 0,
    'forvo': messagesFromForvo ? 1 : 0,
    'allow_messages': messagesFromUsers ? 1 : 0,
    'allow_mail_messages': emailNotifyMessages ? 1 : 0,
  });

  try {
    bool result = false;
    var response = await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );
    if (response.statusCode == 200) {
      result = true;
    }
    return result;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<bool> blockUserMessages(
  BuildContext context, {
  String idConversation,
  String blocked,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'blocked-user';
  String _paramId = 'id';
  String _paramBlocked = 'blocked';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramId,
    idConversation,
    _paramBlocked,
    blocked,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramId/$idConversation/'
      '$_paramBlocked/$blocked';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      return true;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return false;
}

Future<List<BlockedUser>> getBlockedUsers(
  BuildContext context,
) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'get-blocked-users';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      return BlockedUser.blockedUsersListFromJson(responseJson['items']);
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<UserInfo> getUserInfo(
    BuildContext context, String interfaceLanguageCode) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'get-user-info';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      return UserInfo.fromJson(responseJson['userInfo']);
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}
