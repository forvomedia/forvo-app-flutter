import 'dart:convert';

import 'package:forvo/constants/constants.dart';
import 'package:forvo/service/device/preferences_service.dart';

getHeaders({
  bool isMultipart = false,
  bool useDevEnvironmentBasicAuthentication = false,
}) async {
  String languageCode = await getPreferredLanguageCode();

  var headers = {
    'Accept': 'application/json',
    'Accept-Language': languageCode?.toString()
  };
  if (!isMultipart) {
    headers['Content-Type'] = 'application/json';
  }
  if (useDevEnvironmentBasicAuthentication) {
    String username = DEV_AUTHENTICATION_USERNAME;
    String password = DEV_AUTHENTICATION_PASSWORD;
    String token = base64Encode(utf8.encode('$username:$password'));
    headers['Authorization'] = 'Basic $token';
  }

  return headers;
}
