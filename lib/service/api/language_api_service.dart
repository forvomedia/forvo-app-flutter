import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/attributes_and_languages_data.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/translation_language_group.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/util/app_util.dart';
import 'package:http/http.dart' as http;

Future<List<Language>> getLanguages(
  BuildContext context, {
  String languageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'language-list';
  String _paramInterfaceLanguage = 'interface-language';

  String _url = '$_key/'
      '$_method/'
      '$_paramInterfaceLanguage/$languageCode/';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndLanguagesData languages =
          AttributesAndLanguagesData.fromJson(responseJson);
      return languages.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<List<Language>> getPopularLanguages(
  BuildContext context, {
  String languageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'language-popular';
  String _paramLimit = 'limit';
  String _paramInterfaceLanguage = 'interface-language';

  String limit = LanguageConstants.LANGUAGE_POPULAR_LIMIT.toString();

  String _url = '$_key/'
      '$_method/'
      '$_paramLimit/$limit/'
      '$_paramInterfaceLanguage/$languageCode/';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndLanguagesData popularLanguages =
          AttributesAndLanguagesData.fromJson(responseJson);
      return popularLanguages.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<List<TranslationLanguageGroup>> getSearchTranslationLanguages(
    BuildContext context,
    {String languageCode}) async {
  String _key = getAppApiKey();
  String _method = 'search-translation-languages';
  String _paramInterfaceLanguage = 'interface-language';

  String _url = '$_key/'
      '$_method/'
      '$_paramInterfaceLanguage/$languageCode/';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      List<TranslationLanguageGroup> translationLanguages =
          TranslationLanguageGroup.translationLanguageGroupsListFromJson(
              responseJson['response']);
      return translationLanguages;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}
