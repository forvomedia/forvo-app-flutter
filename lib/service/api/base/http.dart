import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/config/app_config.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/config/api_config.dart';
import 'package:forvo/service/api/interceptor/generic_interceptor.dart';
import 'package:http/http.dart' as http;

class Http {
  final BuildContext context;

  Http(this.context);

  static Http of(BuildContext context) => Http(context);

  Future<http.Response> get(
    String url, {
    Map<String, String> headers,
    bool useMainHost = false,
    bool forceUseDevEnvironmentBasicAuthentication = false,
    String interfaceLanguageCode,
  }) async {
    Map<String, String> requestHeaders = await _getRequestHeaders(
      headers,
      useMainHost: useMainHost,
      forceUseDevEnvironmentBasicAuthentication:
          forceUseDevEnvironmentBasicAuthentication,
    );

    String _host = useMainHost
        ? _getMainHost(interfaceLanguageCode: interfaceLanguageCode)
        : _getApiHost();
    Uri _uri = Uri.parse('$_host$url');

    debugPrint('${_uri.toString()}');
    http.Response response =
        await http.get(_uri, headers: requestHeaders).timeout(
              ApiConstants.DEFAULT_TIMEOUT,
            );

    if (response.statusCode != 200) {
      if (response.statusCode == 400 || response.statusCode == 409) {
        Map<String, dynamic> apiException =
            Map<String, dynamic>.from(json.decode(response.body));
        throw ApiException(apiException);
      } else {
        throw Exception(json.decode(response.body));
      }
    }

    await interceptResponseObject(context, response);

    if (response.body.isNotEmpty) {
      debugPrint('RESPONSE: ${response.body.toString()}');
    }
    return response;
  }

  Future<http.Response> post(
    String url, {
    Map<String, String> headers,
    body,
    Encoding encoding,
    bool useMainHost = false,
    bool forceUseDevEnvironmentBasicAuthentication = false,
  }) async {
    Map<String, String> requestHeaders = await _getRequestHeaders(
      headers,
      useMainHost: useMainHost,
      forceUseDevEnvironmentBasicAuthentication:
          forceUseDevEnvironmentBasicAuthentication,
    );

    String _host = useMainHost ? _getMainHost() : _getApiHost();
    Uri _uri = Uri.parse('$_host$url');

    debugPrint('${_uri.toString()}');
    http.Response response = await http
        .post(_uri, headers: requestHeaders, body: body, encoding: encoding)
        .timeout(
          ApiConstants.DEFAULT_TIMEOUT,
        );

    if (response.statusCode != 200) {
      if (response.statusCode == 400 || response.statusCode == 409) {
        Map<String, dynamic> apiException =
            Map<String, dynamic>.from(json.decode(response.body));
        throw ApiException(apiException);
      } else {
        throw Exception(json.decode(response.body));
      }
    }

    await interceptResponseObject(context, response);

    if (response.body.isNotEmpty) {
      debugPrint('RESPONSE: ${response.body.toString()}');
    }
    return response;
  }

  Future<Response> multipartPost(
    String url, {
    Map<String, String> headers,
    FormData formData,
    Function(int, int) onSendProgressCallback,
    bool useMainHost = false,
  }) async {
    Map<String, String> requestHeaders = await _getRequestHeaders(
      headers,
      isMultipart: true,
      useMainHost: useMainHost,
    );

    String _host = useMainHost ? _getMainHost() : _getApiHost();

    debugPrint('$_host$url');
    for (var data in formData.fields) {
      debugPrint('${data.key.toString()} > ${data.value.toString()}');
    }
    Dio dio = Dio(
      BaseOptions(
        baseUrl: _host,
        headers: requestHeaders,
        contentType: 'multipart/form-data',
        connectTimeout: ApiConstants.DEFAULT_TIMEOUT_MILLISECONDS,
        receiveTimeout: ApiConstants.DEFAULT_TIMEOUT_MILLISECONDS,
      ),
    );

    try {
      Response response;
      if (onSendProgressCallback != null) {
        response = await dio.post(
          url,
          data: formData,
          onSendProgress: onSendProgressCallback,
        );
      } else {
        response = await dio.post(
          url,
          data: formData,
        );
      }

      await interceptDioResponseObject(context, response);

      if (response.data.toString().isNotEmpty) {
        debugPrint('RESPONSE: ${response.data.toString()}');
      }
      return response;
    } on DioError catch (error) {

      await interceptDioResponseObject(context, error.response);

      if (error.response != null &&
          error.response.statusCode != null &&
          (error.response.statusCode == 400 ||
              error.response.statusCode == 409)) {
        Map<String, dynamic> apiException =
            Map<String, dynamic>.from(error?.response?.data);
        throw ApiException(apiException);
      } else {
        throw Exception(error?.response?.data);
      }
    }
  }

  Future<http.Response> put(
    String url, {
    Map<String, String> headers,
    body,
    Encoding encoding,
    bool useMainHost = false,
  }) async {
    Map<String, String> requestHeaders = await _getRequestHeaders(
      headers,
      useMainHost: useMainHost,
    );

    String _host = useMainHost ? _getMainHost() : _getApiHost();
    Uri _uri = Uri.parse('$_host$url');

    http.Response response = await http
        .put(_uri, headers: requestHeaders, body: body, encoding: encoding)
        .timeout(
          ApiConstants.DEFAULT_TIMEOUT,
        );

    if (response.statusCode != 200) {
      if (response.statusCode == 400 || response.statusCode == 409) {
        Map<String, dynamic> apiException =
            Map<String, dynamic>.from(json.decode(response.body));
        throw ApiException(apiException);
      } else {
        throw Exception(json.decode(response.body));
      }
    }

    await interceptResponseObject(context, response);

    return response;
  }

  Future<http.Response> delete(
    String url, {
    Map<String, String> headers,
    bool useMainHost = false,
  }) async {
    Map<String, String> requestHeaders = await _getRequestHeaders(
      headers,
      useMainHost: useMainHost,
    );

    String _host = useMainHost ? _getMainHost() : _getApiHost();
    Uri _uri = Uri.parse('$_host$url');

    debugPrint('${_uri.toString()}');
    http.Response response =
        await http.delete(_uri, headers: requestHeaders).timeout(
              ApiConstants.DEFAULT_TIMEOUT,
            );

    if (response.statusCode != 200) {
      if (response.statusCode == 400 || response.statusCode == 409) {
        Map<String, dynamic> apiException =
            Map<String, dynamic>.from(json.decode(response.body));
        throw ApiException(apiException);
      } else {
        throw Exception(json.decode(response.body));
      }
    }

    await interceptResponseObject(context, response);

    return response;
  }

  String _getApiHost() {
    var config = AppConfig.of(context);

    return config.apiUrl;
  }

  String _getMainHost({String interfaceLanguageCode}) {
    var config = AppConfig.of(context);

    if (interfaceLanguageCode != null &&
        interfaceLanguageCode.isNotEmpty &&
        config.environmentName == ENVIRONMENT_PROD) {
      String subdomain =
          interfaceLanguageCode != LanguageConstants.LANGUAGE_CODE_EN
              ? '$interfaceLanguageCode.'
              : '';
      return config.mainUrl.replaceFirst('app.', subdomain);
    }
    return config.mainUrl;
  }

  Future<Map<String, String>> _getRequestHeaders(
    Map<String, String> headers, {
    bool isMultipart = false,
    bool useMainHost = false,
    bool forceUseDevEnvironmentBasicAuthentication = false,
  }) async {
    var config = AppConfig.of(context);
    Map<String, String> emptyHeaders = {};
    Map<String, String> defaultHeaders = await getHeaders(
      isMultipart: isMultipart,
      useDevEnvironmentBasicAuthentication: useMainHost &&
          (config.environmentName == ENVIRONMENT_DEV ||
              forceUseDevEnvironmentBasicAuthentication),
    )
      ..addAll(headers ?? emptyHeaders);

    return defaultHeaders;
  }
}
