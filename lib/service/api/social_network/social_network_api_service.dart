import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/social_network_check_status.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

Future<SocialNetworkCheckStatus> socialNetworkCheckUser(
  BuildContext context,
  String socialNetwork,
  String token, {
  String appleLoginNonce,
  String appleLoginAuthorizationCode,
  String appleLoginEmail,
  String appleLoginGivenName,
  String appleLoginFamilyName,
}) async {
  String _key = getAppApiKey();
  String _method = 'social-network-check-user';
  String _paramSocialNetwork = 'socialNetwork';
  String _paramAccessToken = 'accessToken';
  String _url =
      '$_key/$_method/$_paramSocialNetwork/$socialNetwork/$_paramAccessToken/$token';

  if (appleLoginNonce != null && appleLoginNonce.isNotEmpty) {
    String _paramNonce = 'nonce';
    _url = '$_url/$_paramNonce/$appleLoginNonce';
  }

  if (appleLoginAuthorizationCode != null &&
      appleLoginAuthorizationCode.isNotEmpty) {
    String _paramAuthorizationCode = 'authorizationCode';
    _url = '$_url/$_paramAuthorizationCode/$appleLoginAuthorizationCode';
  }

  if (appleLoginEmail != null && appleLoginEmail.isNotEmpty) {
    String _paramEmail = 'email';
    _url = '$_url/$_paramEmail/$appleLoginEmail';
  }

  if (appleLoginGivenName != null && appleLoginGivenName.isNotEmpty) {
    String _paramGivenName = 'givenName';
    _url = '$_url/$_paramGivenName/$appleLoginGivenName';
  }

  if (appleLoginFamilyName != null && appleLoginFamilyName.isNotEmpty) {
    String _paramFamilyName = 'familyName';
    _url = '$_url/$_paramFamilyName/$appleLoginFamilyName';
  }

  try {
    http.Response response = await Http.of(context).get('$_url');

    SocialNetworkCheckStatus socialNetworkCheckStatus;
    if (response != null && response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      socialNetworkCheckStatus =
          SocialNetworkCheckStatus.fromJson(responseJson);
    }
    return socialNetworkCheckStatus;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<UserInfo> socialNetworkLogin(
  BuildContext context,
  String socialNetwork,
  String socialNetworkId,
  String email,
  String interfaceLanguageCode, {
  bool manualLogin = true,
}) async {
  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();

  FormData formData = FormData.fromMap({
    'socialNetwork': socialNetwork,
    'socialNetworkId': socialNetworkId,
    'email': email,
    'deviceId': _deviceId,
    'os_version': _osVersion,
    'deviceName': _deviceName,
    'app_version': _appVersion,
    'interface-language': interfaceLanguageCode,
    'manual-login': manualLogin,
  });

  try {
    var response = await Http.of(context).multipartPost(
      '${getAppApiKey()}/social-network-login',
      formData: formData,
      onSendProgressCallback: null,
    );

    if (response != null &&
        response.statusCode != null &&
        response.statusCode == 200) {
      Map<String, dynamic> jsonData = Map<String, dynamic>.from(response.data);
      UserInfo userInfo = UserInfo.fromJson(jsonData['userInfo']);
      await saveUserLoginCredential(userInfo.user.username);
      await saveUserUsername(userInfo.user.username, userInfo.user.username);
      await saveUserSocialNetwork(userInfo.user.username, socialNetwork);
      await saveUserSocialNetworkId(userInfo.user.username, socialNetworkId);
      await saveUserKey(userInfo.user.username, userInfo.user.key);
      await saveUserSecret(userInfo.user.username, userInfo.user.secret);

      if (manualLogin) {
        DateTime lastLoginDateTime = getCurrentUTCDateTime();
        DateFormat lastLoginDateTimeFormat =
            DateFormat(DEFAULT_DATETIME_INPUT_FORMAT);
        await saveUserLastLoginDateTime(
          userInfo.user.username,
          lastLoginDateTimeFormat.format(lastLoginDateTime),
        );
      }

      return userInfo;
    }
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<bool> socialNetworkSignup(
  BuildContext context, {
  String socialNetwork,
  String socialNetworkId,
  String username,
  String email,
  String firstName,
  String lastName,
  String gender,
  String languageCode,
  String latitude,
  String longitude,
  String ip,
  String birthdateDay,
  String birthdateMonth,
  String birthdateYear,
  String interfaceLanguageCode,
}) async {
  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();
  var _osName = getDeviceOSName();

  FormData formData = FormData.fromMap({
    'socialNetwork': socialNetwork,
    'socialNetworkId': socialNetworkId,
    'username': username,
    'email': email,
    'deviceId': _deviceId,
    'os_version': _osVersion,
    'deviceName': _deviceName,
    'app_version': _appVersion,
    'interface-language': interfaceLanguageCode,
    'first_name': firstName,
    'last_name': lastName,
    'sex': gender,
    'language': languageCode,
    'lat': latitude,
    'lng': longitude,
    'ip': ip,
    'birthdate_day': birthdateDay,
    'birthdate_month': birthdateMonth,
    'birthdate_year': birthdateYear,
    'app_os_name_registered': _osName,
  });

  try {
    await Http.of(context).multipartPost(
      '${getAppApiKey()}/social-network-signup',
      formData: formData,
      onSendProgressCallback: null,
    );
    return true;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}
