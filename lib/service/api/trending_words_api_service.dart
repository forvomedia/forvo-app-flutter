import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/attributes_and_trending_words_data.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/util/app_util.dart';
import 'package:http/http.dart' as http;

Future<List<Word>> getTrendingWords(
  BuildContext context, {
  String languageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'trending';
  String _paramInterfaceLanguage = 'interface-language';

  String _url = '$_key/'
      '$_method/'
      '$_paramInterfaceLanguage/$languageCode/';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndTrendingWordsData trendingWords =
          AttributesAndTrendingWordsData.fromJson(responseJson);
      return trendingWords.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}
