import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/attributes_and_pronunciations_data.dart';
import 'package:forvo/model/followed_users_data.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_with_followed_users.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';
import 'package:http/http.dart' as http;

import 'base/http.dart';

Future<List<LanguageWithFollowedUsers>> getUserFollowedUsers(
  BuildContext context,
  String username, {
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-following-users';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      FollowedUsersData followedUsers =
          FollowedUsersData.fromJson(responseJson);
      return followedUsers.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<List<LanguageWithPronunciations>> getFollowedUserPronunciations(
  BuildContext context,
  String username, {
  String interfaceLanguageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-pronunciations';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndPronunciationsData userPronunciations =
          AttributesAndPronunciationsData.fromJson(responseJson);
      return userPronunciations.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

Future<bool> followingUser(
  BuildContext context,
  String username,
  String language,
) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-following-user';
  String _paramPronunciationUser = 'username';
  String _paramPronunciationLangCode = 'language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramPronunciationUser,
    username,
    _paramPronunciationLangCode,
    language,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramPronunciationUser/$username/'
      '$_paramPronunciationLangCode/$language';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      return responseJson['data']['status'];
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on ApiException {
    rethrow;
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<List<Language>> followingUserLangs(
  BuildContext context,
  String username, {
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-following-langs';
  String _paramUser = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUser,
    username,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUser/$username/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      List<Language> languages =
          Language.languageListFromJson(responseJson['data']);
      return languages;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<Status> followUnfollowUser(
  BuildContext context,
  String username,
  String language,
  String follow,
) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'follow-unfollow-user';
  String _paramPronunciationUser = 'username';
  String _paramPronunciationLangCode = 'language';
  String _paramFollow = 'follow';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramPronunciationUser,
    username,
    _paramPronunciationLangCode,
    language,
    _paramFollow,
    follow,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramPronunciationUser/$username/'
      '$_paramPronunciationLangCode/$language/'
      '$_paramFollow/$follow';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      Status status = Status.fromJson(responseJson);
      return status;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on ApiException {
    rethrow;
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}
