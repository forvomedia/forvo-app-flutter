import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';

Future<bool> postComment(
  BuildContext context, {
  String username,
  String email,
  String deviceID,
  String deviceName,
  String osName,
  String osVersion,
  String appVersion,
  String comment,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'send-app-review';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'username': username,
    'email': email,
    'deviceID': deviceID,
    'deviceName': deviceName,
    'osName': osName,
    'osVersion': osVersion,
    'appVersion': appVersion,
    'comment': comment,
  });

  try {
    bool result = false;
    var response = await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );

    if (response.statusCode == 200) {
      result = true;
    }
    return result;
  } on ApiException {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}
