import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/attributes_and_pending_pronounce_words_data.dart';
import 'package:forvo/model/attributes_and_pronunciations_data.dart';
import 'package:forvo/model/attributes_and_search_results_data.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/model/language_with_search_result_items.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/model/word_check_alphabet.dart';
import 'package:forvo/model/word_check_exists.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';
import 'package:forvo/util/app_util.dart';
import 'package:http/http.dart' as http;

Future<List<PendingPronounceWord>> getPendingPronounceWords(
  BuildContext context, {
  String languageCode,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'pending-pronunciations';
  String _paramLanguage = 'language';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramLanguage,
    languageCode,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramLanguage/$languageCode/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndPendingPronounceWordsData pendingPronounceWords =
          AttributesAndPendingPronounceWordsData.fromJson(responseJson);
      return pendingPronounceWords.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<List<LanguageWithSearchResultItems>> getSearchWords(
  BuildContext context, {
  String searchTerm,
  String interfaceLanguageCode,
  bool isUserLogged,
}) async {
  String _url;
  String urlEncodedSearchTerm = Uri.encodeQueryComponent(searchTerm);
  String _method = 'words-search';
  String _paramSearch = 'search';
  String _paramInterfaceLanguage = 'interface-language';

  if (isUserLogged) {
    String _key = await getUserKey();
    String _secret = await getUserSecret();
    String _signature;

    List<String> _params = [
      _key,
      _secret,
      _method,
      _paramSearch,
      urlEncodedSearchTerm,
      _paramInterfaceLanguage,
      interfaceLanguageCode,
    ];

    _signature = generateParametersSignature(_params);
    _url = '$_key/'
        '$_signature/'
        '$_method/'
        '$_paramSearch/$urlEncodedSearchTerm/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode';
  } else {
    String _key = getAppApiKey();

    _url = '$_key/'
        '$_method/'
        '$_paramSearch/$urlEncodedSearchTerm/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode';
  }

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndSearchResultsData searchWords =
          AttributesAndSearchResultsData.fromJson(responseJson);
      return searchWords.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<List<LanguageWithSearchResultItems>> getSearchWordTranslations(
  BuildContext context, {
  String searchTerm,
  String translationLanguagePairCodes,
  String interfaceLanguageCode,
  bool isUserLogged,
}) async {
  String _url;
  String urlEncodedSearchTerm = Uri.encodeQueryComponent(searchTerm);
  String _method = 'words-search-translation';
  String _paramSearch = 'search';
  String _paramLanguages = 'languages';
  String _paramInterfaceLanguage = 'interface-language';

  if (isUserLogged) {
    String _key = await getUserKey();
    String _secret = await getUserSecret();
    String _signature;

    List<String> _params = [
      _key,
      _secret,
      _method,
      _paramSearch,
      urlEncodedSearchTerm,
      _paramLanguages,
      translationLanguagePairCodes,
      _paramInterfaceLanguage,
      interfaceLanguageCode,
    ];

    _signature = generateParametersSignature(_params);
    _url = '$_key/'
        '$_signature/'
        '$_method/'
        '$_paramSearch/$urlEncodedSearchTerm/'
        '$_paramLanguages/$translationLanguagePairCodes/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode';
  } else {
    String _key = getAppApiKey();

    _url = '$_key/'
        '$_method/'
        '$_paramSearch/$urlEncodedSearchTerm/'
        '$_paramLanguages/$translationLanguagePairCodes/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode/';
  }

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndSearchResultsData searchWords =
          AttributesAndSearchResultsData.fromJson(responseJson);
      return searchWords.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<List<LanguageWithPronunciations>> getWordPronunciations(
  BuildContext context, {
  String word,
  String interfaceLanguageCode,
  bool isUserLogged,
}) async {
  String _url;
  String urlEncodedWord = Uri.encodeQueryComponent(word);
  String _method = 'word-pronunciations';
  String _paramWord = 'word';
  String _paramGroupInLanguages = 'group-in-languages';
  String _paramOrder = 'order';
  String _paramInterfaceLanguage = 'interface-language';

  String _groupInLanguages = 'true';
  String _order = 'rate-desc';

  if (isUserLogged) {
    String _key = await getUserKey();
    String _secret = await getUserSecret();
    String _signature;

    List<String> _params = [
      _key,
      _secret,
      _method,
      _paramWord,
      urlEncodedWord,
      _paramGroupInLanguages,
      _groupInLanguages,
      _paramOrder,
      _order,
      _paramInterfaceLanguage,
      interfaceLanguageCode,
    ];

    _signature = generateParametersSignature(_params);
    _url = '$_key/'
        '$_signature/'
        '$_method/'
        '$_paramWord/$urlEncodedWord/'
        '$_paramGroupInLanguages/$_groupInLanguages/'
        '$_paramOrder/$_order/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode';
  } else {
    String _key = getAppApiKey();

    _url = '$_key/'
        '$_method/'
        '$_paramWord/$urlEncodedWord/'
        '$_paramGroupInLanguages/$_groupInLanguages/'
        '$_paramOrder/$_order/'
        '$_paramInterfaceLanguage/$interfaceLanguageCode';
  }

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndPronunciationsData languagesWithPronunciations =
          AttributesAndPronunciationsData.fromJson(responseJson);
      return languagesWithPronunciations.data;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<Status> favoritePronunciation(
    BuildContext context, int pronunciationId, String favorite) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'favorite-pronunciation';
  String _paramPronunciationId = 'id';
  String _paramFavorite = 'favorite';

  String pronunciationIdString = pronunciationId.toString();

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramPronunciationId,
    pronunciationIdString,
    _paramFavorite,
    favorite,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramPronunciationId/$pronunciationIdString/'
      '$_paramFavorite/$favorite';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      Status status = Status.fromJson(responseJson);
      return status;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on ApiException {
    rethrow;
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<Status> votePronunciation(
    BuildContext context, int pronunciationId, String vote) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'vote-pronunciation';
  String _paramPronunciationId = 'id';
  String _paramVote = 'vote';

  String pronunciationIdString = pronunciationId.toString();

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramPronunciationId,
    pronunciationIdString,
    _paramVote,
    vote,
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramPronunciationId/$pronunciationIdString/'
      '$_paramVote/$vote';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      Status status = Status.fromJson(responseJson);
      return status;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on ApiException {
    rethrow;
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<WordCheckExists> addWordCheckExists(
  BuildContext context,
  String word,
  String interfaceLanguageCode,
) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String urlEncodedWord = Uri.encodeQueryComponent(word);
  String _method = 'add-word-check-exists';
  String _paramWord = 'word';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramWord,
    urlEncodedWord,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  String _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramWord/$urlEncodedWord/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      WordCheckExists checkExists =
          WordCheckExists.fromJson(responseJson['data']);
      debugPrint(checkExists.toString());
      return checkExists;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<WordCheckAlphabet> addWordCheckAlphabet(
  BuildContext context,
  String word,
  String languageCode,
  String interfaceLanguageCode,
) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String urlEncodedWord = Uri.encodeQueryComponent(word);
  String _method = 'add-word-check-alphabet';
  String _paramWord = 'word';
  String _paramLanguage = 'language';
  String _paramInterfaceLanguage = 'interface-language';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramWord,
    urlEncodedWord,
    _paramLanguage,
    languageCode,
    _paramInterfaceLanguage,
    interfaceLanguageCode,
  ];

  String _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramWord/$urlEncodedWord/'
      '$_paramLanguage/$languageCode/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    http.Response response = await Http.of(context).get('$_url');
    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      WordCheckAlphabet checkAlphabet =
          WordCheckAlphabet.fromJson(responseJson['data']);
      debugPrint(checkAlphabet.toString());
      return checkAlphabet;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<PendingPronounceWord> addModifyWord(
  BuildContext context, {
  String word,
  String languageCode,
  String interfaceLanguageCode,
  bool modify,
  bool isPersonName,
  bool isPhrase,
  String accentCode,
  bool hasCategorizationGroup,
  int pronunciationGroupIndex,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'add-word';
  if (modify) {
    _method = 'modify-word';
  }

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'word': word,
    'language': languageCode,
    'interface-language': interfaceLanguageCode,
    'person_name': isPersonName != null ? (isPersonName ? 1 : 0) : 0,
    'is_phrase': isPhrase != null ? (isPhrase ? 1 : 0) : 0,
    'accent': accentCode != null ? accentCode : null,
    'has_categorization':
        hasCategorizationGroup != null ? (hasCategorizationGroup ? 1 : 0) : 0,
    'pronunciation_group_index':
        pronunciationGroupIndex != null ? pronunciationGroupIndex : null,
  });

  try {
    var response = await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );

    PendingPronounceWord pendingPronounceWord;
    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = response.data['data'];
      pendingPronounceWord = PendingPronounceWord.fromJson(responseJson);
    }
    return pendingPronounceWord;
  } on ApiException {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

Future<bool> saveAudio(
  BuildContext context, {
  String fileName,
  File file,
  bool forceUseDevEnvironmentBasicAuthentication = false,
}) async {
  String _url = 'recorder/saveaudio.php?recordName=$fileName';

  try {
    var response = await Http.of(context).post(
      _url,
      useMainHost: true,
      body: file.readAsBytesSync(),
      headers: <String, String>{'Content-Type': 'application/octet-stream'},
      forceUseDevEnvironmentBasicAuthentication:
          forceUseDevEnvironmentBasicAuthentication,
    );

    if (response.body.contains('save=ok')) {
      return true;
    } else {
      throw Exception();
    }
  } on FormatException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

addPronunciation(BuildContext context,
    {String word,
    String languageCode,
    String audioName,
    String ip,
    String pronunciationGroupIndex}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'add-pronunciation';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'word': word,
    'language': languageCode,
    'audio_name': audioName,
    'ip': ip,
    'pronunciation_group_index': pronunciationGroupIndex,
  });

  try {
    await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );
    return;
  } on ApiException {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

reportPronunciation(
  BuildContext context, {
  String word,
  String languageCode,
  String pronunciation,
  String pronunciationProblem,
  String problem,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'word-report';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'word': word,
    'language': languageCode,
    'pronunciation': pronunciation,
    'pronunciationProblem': pronunciationProblem,
    'problem': problem,
    'interface-language': interfaceLanguageCode,
  });

  try {
    await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );
    return;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

requestPronunciation(
  BuildContext context, {
  String word,
  String languageCode,
  String languageAccentCode,
  int pronunciationGroupIndex,
  String interfaceLanguageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'request-pronunciation';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData;
  Map<String, Object> map;
  if (languageAccentCode == null) {
    map = {'word': word, 'language': languageCode};
    if (pronunciationGroupIndex != null) {
      map = {
        'word': word,
        'language': languageCode,
        'pronunciation_group_index': pronunciationGroupIndex
      };
    }
  } else {
    map = {
      'word': word,
      'language': languageCode,
      'accent': languageAccentCode
    };
    if (pronunciationGroupIndex != null) {
      map = {
        'word': word,
        'language': languageCode,
        'accent': languageAccentCode,
        'pronunciation_group_index': pronunciationGroupIndex
      };
    }
  }
  formData = FormData.fromMap(map);

  try {
    await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );
    return;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on ApiException {
    rethrow;
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}

updatePronunciationSumHit(
  BuildContext context,
  int id, {
  bool forceUseDevEnvironmentBasicAuthentication = false,
  String interfaceLanguageCode,
}) async {
  String _url = 'sum-hit/${id.toString()}';
  try {
    await Http.of(context).get(
      '$_url',
      useMainHost: true,
      forceUseDevEnvironmentBasicAuthentication:
          forceUseDevEnvironmentBasicAuthentication,
      interfaceLanguageCode: interfaceLanguageCode,
    );
  } on FormatException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }
}
