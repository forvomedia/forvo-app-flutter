import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/attributes_and_user_notifications_data.dart';
import 'package:forvo/model/user_notification.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:http/http.dart' as http;

registerCloudMessagingToken(
  BuildContext context, {
  String token,
  String languageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'register-cloud-messaging-token';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'registrationId': token,
    'language': languageCode,
    'osName': getDeviceOSName(),
  });

  await Http.of(context).multipartPost(
    _url,
    formData: formData,
    onSendProgressCallback: null,
  );
}

unregisterCloudMessagingToken(
  BuildContext context, {
  String token,
  String languageCode,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'unregister-cloud-messaging-token';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'registrationId': token,
    'language': languageCode,
    'osName': getDeviceOSName(),
  });

  await Http.of(context).multipartPost(
    _url,
    formData: formData,
    onSendProgressCallback: null,
  );
}

Future<AttributesAndUserNotificationsData> getUserNotifications(
  BuildContext context, {
  String username,
  String languageCode,
  int page,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _signature;
  String _method = 'user-notifications';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';
  String _paramPage = 'page';

  List<String> _params = [
    _key,
    _secret,
    _method,
    _paramUsername,
    username,
    _paramInterfaceLanguage,
    languageCode,
    _paramPage,
    page.toString(),
  ];

  _signature = generateParametersSignature(_params);
  String _url = '$_key/'
      '$_signature/'
      '$_method/'
      '$_paramUsername/$username/'
      '$_paramInterfaceLanguage/$languageCode/'
      '$_paramPage/$page';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      AttributesAndUserNotificationsData attributesAndUserNotifications =
          AttributesAndUserNotificationsData.fromJson(responseJson);
      return attributesAndUserNotifications;
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
  return null;
}

Future<bool> deleteNotification(
  BuildContext context, {
  UserNotification notification,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'delete-notification';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'id': notification.id,
  });

  try {
    bool result = false;
    var response = await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );

    if (response.statusCode == 200 && response.data['errorMessage'] == null) {
      result = true;
    } else {
      result = false;
    }

    return result;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}
