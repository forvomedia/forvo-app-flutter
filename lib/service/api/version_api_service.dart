import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/version.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/user_util.dart';
import 'package:http/http.dart' as http;

Future<Version> getVersion(BuildContext context) async {
  String _key = getAppApiKey();
  String _method = 'version';

  String _url = '$_key/$_method';

  try {
    http.Response response = await Http.of(context).get('$_url');

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(response.body);
      var responseHeaders = response.headers;

      if (responseHeaders[
                  ApiConstants.APP_REQUIRED_LOGIN_DATETIME_API_HEADER_NAME] !=
              null &&
          responseHeaders[
                  ApiConstants.APP_REQUIRED_LOGIN_DATETIME_API_HEADER_NAME] !=
              '') {
        var data = await Future.wait([
          isUserLogged(),
          getUserLastLoginDateTime(),
        ]);
        bool _isUserLogged = data[0] as bool;
        String _lastLoginDateTimeString = data[1] as String;
        String _requiredLoginDateTimeString = responseHeaders[
            ApiConstants.APP_REQUIRED_LOGIN_DATETIME_API_HEADER_NAME];
        if (_isUserLogged) {
          DateTime lastLoginDateTime;
          if (_lastLoginDateTimeString != null) {
            _lastLoginDateTimeString =
                _lastLoginDateTimeString.replaceAll(' UTC', '');
            lastLoginDateTime =
                DateTime.tryParse(_lastLoginDateTimeString).toUtc();
          }
          _requiredLoginDateTimeString =
              _requiredLoginDateTimeString.replaceAll(' UTC', '');
          DateTime requiredLoginDateTime =
              DateTime.tryParse(_requiredLoginDateTimeString).toUtc();

          if (lastLoginDateTime == null ||
              lastLoginDateTime.isBefore(requiredLoginDateTime)) {
            forceLogoutUserSession(redirectToStart: true);
            return null;
          }
        }
      }

      return Version.fromJson(
        responseJson,
        responseHeaders[ApiConstants.APP_CURRENT_VERSION_API_HEADER_NAME],
        responseHeaders[ApiConstants.APP_MIN_VERSION_API_HEADER_NAME],
        forceLogout: responseHeaders[ApiConstants
                    .APP_MIN_VERSION_FORCE_LOGOUT_API_HEADER_NAME] !=
                null &&
            responseHeaders[ApiConstants
                    .APP_MIN_VERSION_FORCE_LOGOUT_API_HEADER_NAME] ==
                '1',
      );
    }
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}
