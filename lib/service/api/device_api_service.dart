import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/util/api_util.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/device_info_util.dart';

updateDeviceInfo(BuildContext context) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'update-device-info';

  List<String> _params = [_key, _secret, _method];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();

  FormData formData = FormData.fromMap({
    'deviceId': _deviceId,
    'os_version': _osVersion,
    'deviceName': _deviceName,
    'app_version': _appVersion,
  });

  try {
    await Http.of(context).multipartPost('$_url',
        formData: formData, onSendProgressCallback: null);
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } on Exception catch (e, s) {
    debugPrint('$e => $s');
  }

  throw NotFoundException();
}
