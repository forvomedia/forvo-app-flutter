import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:http/http.dart' as http;

interceptResponseRequestHeader(
    BuildContext context, http.Response response) async {
  if (response.headers.isNotEmpty) {
    await searchMaintenanceResponseHeader(context, response.headers);
  }
}

interceptDioResponseRequestHeader(
    BuildContext context, Response response) async {
  if (!response.headers.isEmpty) {
    await searchMaintenanceResponseHeader(context, response.headers.map);
  }
}

searchMaintenanceResponseHeader(
    BuildContext context, Map<String, dynamic> headers) async {
  List<String> maintenanceHeaders = [
    ApiConstants.FORVO_MAINTENANCE_READ_ONLY,
    ApiConstants.FORVO_MAINTENANCE_ADD_WORD,
    ApiConstants.FORVO_MAINTENANCE_PRONOUNCE,
    ApiConstants.FORVO_MAINTENANCE_LOGIN,
    ApiConstants.FORVO_MAINTENANCE_SIGNUP,
    ApiConstants.FORVO_MAINTENANCE_MESSAGES,
  ];

  AppState state;
  if (AppStateManager.of(context) != null) {
    state = AppStateManager.of(context).state;
  } else {
    var ctx = appGlobals.scaffoldKey.currentContext;
    if (AppStateManager.of(ctx) != null) {
      state = AppStateManager.of(ctx).state;
    }
  }

  if (state != null) {
    AppState newState = AppState();
    int maintenanceActiveHeaders = 0;
    int maintenanceStatusToDeactivate = 0;
    for (String headerName in maintenanceHeaders) {
      if (headers[headerName] != null) {
        var value = headers[headerName];
        bool mustMaintenanceBeActive = false;
        if (value is List) {
          mustMaintenanceBeActive = value.contains('1');
        } else {
          mustMaintenanceBeActive = value == '1';
        }
        if (mustMaintenanceBeActive) {
          switch (headerName) {
            case ApiConstants.FORVO_MAINTENANCE_READ_ONLY:
              if (!state.isMaintenanceReadOnlyActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenanceReadOnly: true),
                );
              }
              break;
            case ApiConstants.FORVO_MAINTENANCE_ADD_WORD:
              if (!state.isMaintenanceAddWordActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenanceAddWord: true),
                );
              }
              break;
            case ApiConstants.FORVO_MAINTENANCE_PRONOUNCE:
              if (!state.isMaintenancePronounceActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenancePronounce: true),
                );
              }
              break;
            case ApiConstants.FORVO_MAINTENANCE_LOGIN:
              if (!state.isMaintenanceLoginActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenanceLogin: true),
                );
              }
              break;
            case ApiConstants.FORVO_MAINTENANCE_SIGNUP:
              if (!state.isMaintenanceSignupActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenanceSignup: true),
                );
              }
              break;
            case ApiConstants.FORVO_MAINTENANCE_MESSAGES:
              if (!state.isMaintenanceMessagesActive) {
                maintenanceActiveHeaders++;
                newState.merge(
                  AppState(maintenanceMessages: true),
                );
              }
              break;
            default:
              break;
          }
        }
      } else {
        switch (headerName) {
          case ApiConstants.FORVO_MAINTENANCE_READ_ONLY:
            if (state.isMaintenanceReadOnlyActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenanceReadOnly: false),
              );
            }
            break;
          case ApiConstants.FORVO_MAINTENANCE_ADD_WORD:
            if (state.isMaintenanceAddWordActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenanceAddWord: false),
              );
            }
            break;
          case ApiConstants.FORVO_MAINTENANCE_PRONOUNCE:
            if (state.isMaintenancePronounceActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenancePronounce: false),
              );
            }
            break;
          case ApiConstants.FORVO_MAINTENANCE_LOGIN:
            if (state.isMaintenanceLoginActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenanceLogin: false),
              );
            }
            break;
          case ApiConstants.FORVO_MAINTENANCE_SIGNUP:
            if (state.isMaintenanceSignupActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenanceSignup: false),
              );
            }
            break;
          case ApiConstants.FORVO_MAINTENANCE_MESSAGES:
            if (state.isMaintenanceMessagesActive) {
              maintenanceStatusToDeactivate++;
              newState.merge(
                AppState(maintenanceMessages: false),
              );
            }
            break;
          default:
            break;
        }
      }
    }

    if (maintenanceActiveHeaders > 0 || maintenanceStatusToDeactivate > 0) {
      state.merge(newState);

      if (AppStateManager.of(context) != null) {
        AppStateManager.of(context).onStateChanged(state);
      } else {
        var ctx = appGlobals.scaffoldKey.currentContext;
        if (AppStateManager.of(ctx) != null) {
          AppStateManager.of(ctx).onStateChanged(state);
        }
      }
    }
  }
}
