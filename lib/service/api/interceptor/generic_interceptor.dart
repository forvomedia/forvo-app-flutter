import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/service/api/interceptor/request_response_header_interceptor.dart';
import 'package:http/http.dart' as http;

interceptResponseObject(BuildContext context, http.Response response) async {
  await interceptResponseRequestHeader(context, response);
}

interceptDioResponseObject(BuildContext context, Response response) async {
  await interceptDioResponseRequestHeader(context, response);
}
