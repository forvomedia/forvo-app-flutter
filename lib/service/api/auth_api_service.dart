import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/status.dart';
import 'package:forvo/model/user_info.dart';
import 'package:forvo/service/api/base/http.dart';
import 'package:forvo/service/database/database_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/service/social_network/social_network_service.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/api_util.dart';
import 'package:forvo/util/app_util.dart';
import 'package:forvo/util/crypto_util.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/notification_util.dart';
import 'package:intl/intl.dart';

Future<UserInfo> login(
  BuildContext context,
  String usernameEmail,
  String password,
  String languageCode, {
  bool manualLogin = true,
}) async {
  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();
  var _passwordMd5 = generateMd5(password);
  var _languageCode = languageCode;

  FormData formData = FormData.fromMap({
    'username': usernameEmail,
    'password': _passwordMd5,
    'deviceId': _deviceId,
    'os_version': _osVersion,
    'deviceName': _deviceName,
    'app_version': _appVersion,
    'interface-language': _languageCode,
    'manual-login': manualLogin,
  });

  try {
    var response = await Http.of(context).multipartPost(
      '${getAppApiKey()}/login',
      formData: formData,
      onSendProgressCallback: null,
    );

    if (response != null &&
        response.statusCode != null &&
        response.statusCode == 200) {
      Map<String, dynamic> jsonData = Map<String, dynamic>.from(response.data);
      UserInfo userInfo = UserInfo.fromJson(jsonData['userInfo']);
      await saveUserLoginCredential(userInfo.user.username);
      await saveUserUsername(userInfo.user.username, userInfo.user.username);
      var encryptedPassword = base64.encode(utf8.encode(password));
      await saveUserEncryptedPassword(
          userInfo.user.username, encryptedPassword);
      await saveUserKey(userInfo.user.username, userInfo.user.key);
      await saveUserSecret(userInfo.user.username, userInfo.user.secret);

      if (manualLogin) {
        DateTime lastLoginDateTime = getCurrentUTCDateTime();
        DateFormat lastLoginDateTimeFormat =
            DateFormat(DEFAULT_DATETIME_INPUT_FORMAT);
        await saveUserLastLoginDateTime(
          userInfo.user.username,
          lastLoginDateTimeFormat.format(lastLoginDateTime),
        );
      }

      return userInfo;
    }
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }

  return null;
}

logout(BuildContext context) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'logout';

  List<String> _params = [_key, _secret, _method];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  try {
    var state = AppStateManager.of(context).state;
    if (state.isSocialNetworkLogin) {
      socialNetworkServiceLogout(context);
    }

    await Http.of(context).get('$_url');
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  } finally {
    var state = AppStateManager.of(context).state;
    unsubscribePushNotifications(context, state.locale.languageCode);
    await deleteUserSession();
    DatabaseService().close();
    AppStateManager.of(context).onStateChanged(
      AppState(
        isUserLogged: false,
        isSocialNetworkLogin: false,
        userInfo: null,
      ),
    );
  }
}

Future<bool> signup(
  BuildContext context, {
  String username,
  String email,
  String password,
  String repassword,
  String firstName,
  String lastName,
  String gender,
  String languageCode,
  String latitude,
  String longitude,
  String ip,
  String birthdateDay,
  String birthdateMonth,
  String birthdateYear,
  String interfaceLanguageCode,
}) async {
  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();
  var _osName = getDeviceOSName();

  FormData formData = FormData.fromMap({
    'username': username,
    'email': email,
    'password': password,
    'repassword': repassword,
    'deviceId': _deviceId,
    'os_version': _osVersion,
    'deviceName': _deviceName,
    'app_version': _appVersion,
    'interface-language': interfaceLanguageCode,
    'first_name': firstName,
    'last_name': lastName,
    'sex': gender,
    'language': languageCode,
    'lat': latitude,
    'lng': longitude,
    'ip': ip,
    'birthdate_day': birthdateDay,
    'birthdate_month': birthdateMonth,
    'birthdate_year': birthdateYear,
    'app_os_name_registered': _osName,
  });

  try {
    var response = await Http.of(context).multipartPost(
      '${getAppApiKey()}/signup',
      formData: formData,
      onSendProgressCallback: null,
    );
    debugPrint(response.data.toString());
    return true;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<bool> signupCheckUsername(
  BuildContext context, {
  String username,
  String interfaceLanguageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'signup-check-username';
  String _paramUsername = 'username';
  String _paramInterfaceLanguage = 'interface-language';

  String urlEncodedUsername = Uri.encodeQueryComponent(username);

  String _url = '$_key/'
      '$_method/'
      '$_paramUsername/$urlEncodedUsername/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    await Http.of(context).get('$_url');
    return true;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<bool> signupCheckEmail(
  BuildContext context, {
  String email,
  String interfaceLanguageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'signup-check-email';
  String _paramEmail = 'email';
  String _paramInterfaceLanguage = 'interface-language';

  String urlEncodedEmail = Uri.encodeQueryComponent(email);

  String _url = '$_key/'
      '$_method/'
      '$_paramEmail/$urlEncodedEmail/'
      '$_paramInterfaceLanguage/$interfaceLanguageCode';

  try {
    await Http.of(context).get('$_url');
    return true;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<Status> passwordReminder(
  BuildContext context, {
  String email,
  String interfaceLanguageCode,
}) async {
  String _key = getAppApiKey();
  String _method = 'password-reminder';
  FormData formData = FormData.fromMap({
    'email': email,
    'interface-language': interfaceLanguageCode,
  });

  try {
    var response = await Http.of(context).multipartPost(
      '$_key/$_method',
      formData: formData,
      onSendProgressCallback: null,
    );

    Status status = Status.fromJson(response.data);
    return status;
  } on ApiException catch (_) {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}

Future<bool> deleteAccount(
  BuildContext context, {
  String reason,
}) async {
  String _key = await getUserKey();
  String _secret = await getUserSecret();
  String _method = 'delete-account';

  List<String> _params = [
    _key,
    _secret,
    _method,
  ];
  String _signature = generateParametersSignature(_params);
  String _url = '$_key/$_signature/$_method';

  FormData formData = FormData.fromMap({
    'reason': reason,
  });

  try {
    var result = false;
    var response = await Http.of(context).multipartPost(
      _url,
      formData: formData,
      onSendProgressCallback: null,
    );

    if (response.statusCode == 200) {
      var state = AppStateManager.of(context).state;
      result = true;
      unsubscribePushNotifications(context, state.locale.languageCode);
      await deleteAccountPref();
      DatabaseService().close();
      AppStateManager.of(context).onStateChanged(
        AppState(
          isUserLogged: false,
          isSocialNetworkLogin: false,
          userInfo: null,
        ),
      );
    }
    return result;
  } on ApiException {
    rethrow;
  } on SocketException catch (e, s) {
    debugPrint('$e => $s');
    throw NotConnectedException();
  }
}
