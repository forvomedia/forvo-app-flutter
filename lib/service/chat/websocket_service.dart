import 'package:forvo/config/app_config.dart';
import 'package:forvo/constants/websocket_constants.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:socket_io_client/socket_io_client.dart' as _io;
import 'package:socket_io_client/socket_io_client.dart';

class WebSocketService {
  static final WebSocketService _instance = WebSocketService._internal();
  static _io.Socket _socket;
  bool _isWebSocketConnecting = false;
  bool _isAuthenticated = false;
  bool _reconnectionAttemptInitialized = false;
  int _leavingReason = WebSocketConstants.LEAVING_REASON_OTHER;
  int _connectionAttempts = 0;
  int _disconnections = 0;
  AppState _state;

  factory WebSocketService() => _instance;

  bool get isConnected => _socket.connected;

  bool get isAuthenticated => isConnected && _isAuthenticated;

  WebSocketService._internal() {
    _socket ??= _io.io(
        AppConfig.of(appGlobals.scaffoldKey.currentContext).chatUrl,
        OptionBuilder().setTransports(['websocket']) // for Flutter or Dart VM
            .build());
  }

  bool isStateInitialized() => _state != null;

  // ignore: use_setters_to_change_properties
  void initializeState(AppState state) => _state = state;

  // ignore: avoid_annotating_with_dynamic
  void on(String event, dynamic Function(dynamic data) handler) async {
    _socket.on(event, handler);
  }

  void emit(String event, [data]) async {
    _socket.emit(event, data);
  }

  void connect(
    String username,
    String password,
    String deviceId, {
    int connectionAttemptsLimit,
  }) {
    _isWebSocketConnecting = false;
    _isAuthenticated = false;
    _connectionAttempts = 0;
    _leavingReason = WebSocketConstants.LEAVING_REASON_OTHER;
    print('websocket_service.dart > connect');

    _socket.onConnect((_) {
      print('websocket_service.dart > ON CONNECT');
      authenticate(username, password, deviceId);
    });

    _socket.on('testingid', (data) {
      print('websocket_service.dart > testingId: $data');
    });

    _socket.on(WebSocketConstants.EVENT_AUTHENTICATED, (_) {
      print('websocket_service.dart > ON AUTHENTICATED');
      _isAuthenticated = true;
      _connectionAttempts = 0;
      _leavingReason = WebSocketConstants.LEAVING_REASON_OTHER;
      _state.chatConnectionChangeNotifier.updateConnectionStatus(
        connectionStatus: _isAuthenticated,
      );
    });

    _socket.onConnecting((data) {
      print('websocket_service.dart > ON CONNECTING > $data');
    });

    _socket.onDisconnect((data) {
      print('websocket_service.dart > ON DISCONNECT > '
          'LEAVING REASON: '
          '${_leavingReason == WebSocketConstants.LEAVING_REASON_OTHER ? 'OTHER'
              '' : 'USER LEFT'} > $data');
      _isWebSocketConnecting = false;
      _isAuthenticated = false;
      if (_leavingReason != WebSocketConstants.LEAVING_REASON_USER_LEFT) {
        _state.chatConnectionChangeNotifier.updateConnectionStatus(
          connectionStatus: _isAuthenticated,
        );
        if (!_reconnectionAttemptInitialized) {
          print('websocket_service.dart > ON DISCONNECT > '
              'NO RECONNECTION ATTEMPT');
          if (connectionAttemptsLimit != null && connectionAttemptsLimit > 0) {
            _disconnections++;

            print('websocket_service.dart > ON DISCONNECT > '
                'DISCONNECTIONS: $_disconnections');
            int _attempts = _connectionAttempts;
            if (_connectionAttempts == 0) {
              _attempts = _disconnections;
            }
            if (_attempts < connectionAttemptsLimit) {
              Future.delayed(
                  const Duration(
                      seconds: WebSocketConstants.RECONNECTION_SECONDS), () {
                print('websocket_service.dart > ON DISCONNECT > '
                    'LEAVING REASON > OTHER > TRYING RE CONNECT'
                    '(ATTEMPT: $_attempts - '
                    'MAX ATTEMPTS: $connectionAttemptsLimit)');
                if (!_socket.connected) {
                  _socket.connect();
                }
              });
            } else {
              disconnect();
            }
          } else {
            Future.delayed(
                const Duration(
                    seconds: WebSocketConstants.RECONNECTION_SECONDS), () {
              print('websocket_service.dart > ON DISCONNECT > '
                  'LEAVING REASON > OTHER > TRYING RE CONNECT');
              if (!_socket.connected) {
                _socket.connect();
              }
            });
          }
        }
      }
    });

    _socket.onConnectError((data) {
      print('websocket_service.dart > ON CONNECT ERROR > '
          'LEAVING REASON: '
          '${_leavingReason == WebSocketConstants.LEAVING_REASON_OTHER ? 'OTHER'
              '' : 'USER LEFT'} > $data');
      _isWebSocketConnecting = false;
      _isAuthenticated = false;
      if (_leavingReason != WebSocketConstants.LEAVING_REASON_USER_LEFT) {
        _state.chatConnectionChangeNotifier.updateConnectionStatus(
          connectionStatus: _isAuthenticated,
        );
        if (connectionAttemptsLimit != null && connectionAttemptsLimit > 0) {
          if (_connectionAttempts < connectionAttemptsLimit) {
            print('websocket_service.dart > ON CONNECT ERROR > '
                'LEAVING REASON > OTHER > TRYING RE CONNECT > '
                'CONNECTION ATTEMPTS LIMIT DEFINED > '
                '(ATTEMPT: $_connectionAttempts - '
                'MAX ATTEMPTS: $connectionAttemptsLimit)');
          } else {
            disconnect();
          }
        } else {
          print('websocket_service.dart > ON CONNECT ERROR > '
              'LEAVING REASON > OTHER > RECONNECTION ATTEMPT INITIALIZED: '
              '$_reconnectionAttemptInitialized');
          if (!_reconnectionAttemptInitialized) {
            Future.delayed(
                const Duration(
                    seconds: WebSocketConstants.RECONNECTION_SECONDS), () {
              print('websocket_service.dart > ON CONNECT ERROR > '
                  'LEAVING REASON > OTHER > TRYING RE CONNECT > '
                  'NOT CONNECTION ATTEMPTS LIMIT DEFINED '
                  '(ATTEMPT: $_connectionAttempts - '
                  'MAX ATTEMPTS: $connectionAttemptsLimit)');
              if (!_socket.connected) {
                _socket.connect();
              }
            });
          }
        }
      }
    });

    _socket.onConnectTimeout((data) {
      print('websocket_service.dart > ON CONNECT TIMEOUT > '
          'LEAVING REASON: '
          '${_leavingReason == WebSocketConstants.LEAVING_REASON_OTHER ? 'OTHER'
              '' : 'USER LEFT'} > $data');
      _isWebSocketConnecting = false;
      _isAuthenticated = false;
      if (_leavingReason != WebSocketConstants.LEAVING_REASON_USER_LEFT) {
        _state.chatConnectionChangeNotifier.updateConnectionStatus(
          connectionStatus: _isAuthenticated,
        );
        if (!_reconnectionAttemptInitialized) {
          Future.delayed(
              const Duration(seconds: WebSocketConstants.RECONNECTION_SECONDS),
              () {
            print('websocket_service.dart > ON CONNECT TIMEOUT > '
                'LEAVING REASON > OTHER > TRYING RE CONNECT'
                '(ATTEMPT: $_connectionAttempts - '
                'MAX ATTEMPTS: $connectionAttemptsLimit)');
            if (!_socket.connected) {
              _socket.connect();
            }
          });
        }
      }
    });

    _socket.onReconnect((data) {
      print('websocket_service.dart > ON RECONNECT > $data');
    });

    _socket.onReconnecting((data) {
      print('websocket_service.dart > ON RECONNECTING > #$data');
    });

    _socket.onReconnectAttempt((data) {
      print('websocket_service.dart > ON RECONNECT ATTEMPT > #$data');
      _connectionAttempts = data;
      _reconnectionAttemptInitialized = true;
    });

    _socket.onReconnectError((data) {
      print('websocket_service.dart > ON RECONNECT ERROR > $data');
      _reconnectionAttemptInitialized = false;
    });

    _socket.onReconnectFailed((data) {
      print('websocket_service.dart > ON RECONNECT FAILED > $data');
      _reconnectionAttemptInitialized = false;
    });

    if (_socket.disconnected) {
      print(
          'websocket_service.dart > connect > status: DISCONNECTED > connect');
      _socket.connect();
    }
  }

  void disconnect() {
    print('websocket_service.dart > disconnect');
    _isWebSocketConnecting = false;
    _isAuthenticated = false;
    _connectionAttempts = 0;
    if (_socket.connected) {
      _leavingReason = WebSocketConstants.LEAVING_REASON_USER_LEFT;
      print('websocket_service.dart > disconnect > '
          'status: CONNECTED > disconnect');
    } else {
      _leavingReason = WebSocketConstants.LEAVING_REASON_OTHER;
      print('websocket_service.dart > disconnect > already disconnected');
    }
    _socket.disconnect();
    _socket.dispose();
  }

  void authenticate(String username, String password, String deviceId) {
    if (!_isWebSocketConnecting) {
      print('websocket_service.dart > authenticate');
      _isWebSocketConnecting = true;
      List<String> _authenticationData = [];
      _authenticationData.addAll([
        username,
        password,
        deviceId,
      ]);
      _socket.emit(
        WebSocketConstants.EVENT_AUTHENTICATE,
        _authenticationData,
      );
    } else {
      print('websocket_service.dart > authenticate > '
          'status: ALREADY AUTHENTICATED');
    }
  }

  void markConversationAsRead(String idConversation, String username) async {
    if (isAuthenticated) {
      print('websocket_service.dart > markConversationAsRead');
      List<String> _markConversationAsReadData = [];
      _markConversationAsReadData.addAll([
        idConversation,
        username,
      ]);
      _socket.emit(
        WebSocketConstants.EVENT_MARK_CONVERSATION_AS_READ,
        _markConversationAsReadData,
      );
    } else {
      print('websocket_service.dart > markConversationAsRead > '
          'status: NOT AUTHENTICATED');
    }
  }

  void sendMessage(
    String idLocal,
    String idConversation,
    String userFrom,
    String userTo,
    String message,
  ) async {
    if (isAuthenticated) {
      print('websocket_service.dart > sendMessage >'
          'idLocal: $idLocal - idConversation: $idConversation - '
          'userFrom: $userFrom - userTo: $userTo - message: $message');
      List<String> _sendMessageData = [];
      _sendMessageData.addAll([
        idLocal,
        idConversation,
        userFrom,
        userTo,
        message,
      ]);
      _socket.emit(
        WebSocketConstants.EVENT_SEND_MESSAGE,
        _sendMessageData,
      );
    } else {
      print('websocket_service.dart > sendMessage > '
          'status: NOT AUTHENTICATED');
    }
  }
}
