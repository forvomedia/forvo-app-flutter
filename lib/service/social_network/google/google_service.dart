import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/social_network/social_network_service.dart';
import 'package:forvo/ui/registration/social_network_complete_signup.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  clientId: deviceIsAndroid()
      ? SocialNetworkConstants.GOOGLE_SERVICES_ANDROID_APP_OAUTH_CLIENT_ID
      : SocialNetworkConstants.GOOGLE_SERVICES_IOS_APP_OAUTH_CLIENT_ID,
  scopes: ['email', 'profile'],
);

Future<String> loginGoogle(BuildContext context) async {
  GoogleSignInAccount _account = await _googleSignIn.signIn();

  String loginResponse = SocialNetworkConstants.REQUEST_ERROR;
  if (_account != null) {
    GoogleSignInAuthentication _authentication = await _account.authentication;

    if (_authentication != null && _authentication.idToken != null) {
      var socialNetworkStatus = await socialNetworkCheckUser(
        context,
        SocialNetworkConstants.GOOGLE,
        _authentication.idToken,
      );

      if (socialNetworkStatus != null) {
        if (socialNetworkStatus.action ==
            SocialNetworkConstants.ACTION_SIGN_UP) {
          var signupResponse = await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => SocialNetworkCompleteSignUp(
                socialNetwork: SocialNetworkConstants.GOOGLE,
                userInfo: socialNetworkStatus.userInfo,
              ),
            ),
          );
          if (signupResponse != null &&
              signupResponse == SocialNetworkConstants.REQUEST_OK) {
            loginResponse = await socialNetworkServiceLogin(
              context,
              socialNetwork: SocialNetworkConstants.GOOGLE,
              socialNetworkId: socialNetworkStatus.userInfo.id,
              email: socialNetworkStatus.userInfo.email,
            );
          }
        } else if (socialNetworkStatus.action ==
            SocialNetworkConstants.ACTION_LOGIN) {
          loginResponse = await socialNetworkServiceLogin(
            context,
            socialNetwork: SocialNetworkConstants.GOOGLE,
            socialNetworkId: socialNetworkStatus.userInfo.id,
            email: socialNetworkStatus.userInfo.email,
          );
        }
      }
    }
  }
  return loginResponse;
}

void logoutGoogle(BuildContext context) async {
  await _googleSignIn.signOut();
}