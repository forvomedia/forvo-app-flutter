import 'package:flutter/material.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/service/social_network/facebook/facebook_service.dart';
import 'package:forvo/service/social_network/google/google_service.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/messages_util.dart';
import 'package:forvo/util/snackbar_util.dart';

Future<String> socialNetworkServiceLogin(
  BuildContext context, {
  @required String socialNetwork,
  @required String socialNetworkId,
  @required String email,
}) async {
  var state = AppStateManager.of(context).state;
  String interfaceLanguageCode = state.locale.languageCode;
  String loginResponse = SocialNetworkConstants.REQUEST_ERROR;
  bool _isUserLogged = false;

  try {
    var userInfo = await socialNetworkLogin(
      context,
      socialNetwork,
      socialNetworkId,
      email,
      interfaceLanguageCode,
    );
    _isUserLogged = userInfo != null;

    AppStateManager.of(context).onStateChanged(
      AppState(
        isUserLogged: _isUserLogged,
        isSocialNetworkLogin: _isUserLogged,
        userInfo: userInfo,
      ),
    );

    if (_isUserLogged) {
      loginResponse = SocialNetworkConstants.REQUEST_OK;
      if (_isUserLogged) {
        getMessagesAfterLogin(
          userInfo.user.username,
          userInfo.user.md5Password,
        );
      }
    }
  } on ApiException catch (error) {
    List<ApiError> errors = error.getErrors();
    String message;
    for (ApiError _error in errors) {
      switch (_error.errorApiCode) {
        case ApiErrorCodeConstants.ERROR_1059:
          var userInfo = await socialNetworkLogin(
            context,
            socialNetwork,
            socialNetworkId,
            email,
            interfaceLanguageCode,
          );
          _isUserLogged = userInfo != null;

          AppStateManager.of(context).onStateChanged(
            AppState(
              isUserLogged: _isUserLogged,
              isSocialNetworkLogin: _isUserLogged,
              userInfo: userInfo,
            ),
          );

          if (_isUserLogged) {
            loginResponse = SocialNetworkConstants.REQUEST_OK;
            if (_isUserLogged) {
              getMessagesAfterLogin(
                userInfo.user.username,
                userInfo.user.md5Password,
              );
            }
          }
          break;
        default:
          if (message == null) {
            message = _error.errorMessage;
          } else {
            message = '$message\n${_error.errorMessage}';
          }
          break;
      }
    }
    if (message != null) {
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }

  return loginResponse;
}

void socialNetworkServiceLogout(BuildContext context) async {
  String socialNetwork = await getUserSocialNetwork();
  if (socialNetwork == SocialNetworkConstants.GOOGLE) {
    logoutGoogle(context);
  } else {
    logoutFacebook(context);
  }
}
