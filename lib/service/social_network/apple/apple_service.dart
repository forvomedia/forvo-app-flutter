import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/social_network/social_network_service.dart';
import 'package:forvo/ui/registration/social_network_complete_signup.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/date_util.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

_showErrorMessage(BuildContext context, {String errorMessage}) {
  var localizations = AppLocalizations.of(context);
  return ScaffoldMessenger.of(context).showSnackBar(
    themedSnackBar(
      context,
      errorMessage ??
          localizations
              .translate('error.shared.appleButtonDefaultErrorMessage'),
      type: SNACKBAR_TYPE_ERROR,
    ),
  );
}

Future<String> loginApple(BuildContext context) async {
  var localizations = AppLocalizations.of(context);
  int millis = getCurrentTimeMillis();
  String _state = 'state-apple-sign-$millis';
  String _nonce = 'nonce-apple-sign-$millis';
  String _authorizationCode, _identityToken, _email, _givenName, _familyName;

  String loginResponse = SocialNetworkConstants.REQUEST_ERROR;

  try {
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.fullName,
        AppleIDAuthorizationScopes.email,
      ],
      state: _state,
      nonce: _nonce,
    );

    if (credential.state == _state) {
      _authorizationCode = credential.authorizationCode ?? '';
      _identityToken = credential.identityToken ?? '';
      _email = credential.email ?? '';
      _givenName = credential.givenName ?? '';
      _familyName = credential.familyName ?? '';
    } else {
      _showErrorMessage(context);
    }
  } on SignInWithAppleNotSupportedException catch (_) {
    String _deviceOSVersion = await getDeviceOSVersion();
    ScaffoldMessenger.of(context).showSnackBar(
      themedSnackBar(
        context,
        localizations.translate(
          'error.shared.appleButtonNotSupportedPlatform',
          params: {
            'iosVersion': _deviceOSVersion,
          },
        ),
        type: SNACKBAR_TYPE_ERROR,
      ),
    );
  } on SignInWithAppleAuthorizationException catch (e) {
    if (e.code == AuthorizationErrorCode.canceled){
     loginResponse = SocialNetworkConstants.REQUEST_CANCELLED;
    } else {
      _showErrorMessage(context);
    }
  } on Exception catch (_) {
    _showErrorMessage(context);
  } finally {
    /// Now send the credential (especially
    /// `credential.authorizationCode`) to your server to create a session
    /// after they have been validated with Apple (see `Integration`
    /// section for more information on how to do this)

    if (_identityToken != null && _identityToken.isNotEmpty) {
      try {
        if (_identityToken.isNotEmpty) {
          var socialNetworkStatus = await socialNetworkCheckUser(
            context,
            SocialNetworkConstants.APPLE,
            _identityToken,
            appleLoginNonce: _nonce,
            appleLoginAuthorizationCode: _authorizationCode,
            appleLoginEmail: _email,
            appleLoginGivenName: _givenName,
            appleLoginFamilyName: _familyName,
          );

          if (socialNetworkStatus != null) {
            if (socialNetworkStatus.action ==
                SocialNetworkConstants.ACTION_SIGN_UP) {
              var signupResponse = await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => SocialNetworkCompleteSignUp(
                    socialNetwork: SocialNetworkConstants.APPLE,
                    userInfo: socialNetworkStatus.userInfo,
                  ),
                ),
              );
              if (signupResponse != null &&
                  signupResponse == SocialNetworkConstants.REQUEST_OK) {
                loginResponse = await socialNetworkServiceLogin(
                  context,
                  socialNetwork: SocialNetworkConstants.APPLE,
                  socialNetworkId: socialNetworkStatus.userInfo.id,
                  email: socialNetworkStatus.userInfo.email,
                );
              }
            } else if (socialNetworkStatus.action ==
                SocialNetworkConstants.ACTION_LOGIN) {
              loginResponse = await socialNetworkServiceLogin(
                context,
                socialNetwork: SocialNetworkConstants.APPLE,
                socialNetworkId: socialNetworkStatus.userInfo.id,
                email: socialNetworkStatus.userInfo.email,
              );
            }
          }
          return loginResponse;
        }
      } on ApiException catch (e) {
        String messages = e.getErrorMessages(context);
        _showErrorMessage(context, errorMessage: messages);
      } on Exception catch (e) {
        debugPrint('SIGNUP WITH APPLE EXCEPTION: $e');
        _showErrorMessage(context);
      }
    }
  }
  return loginResponse;
}
