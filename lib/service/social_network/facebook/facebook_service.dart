import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:forvo/constants/social_network_constants.dart';
import 'package:forvo/service/api/social_network/social_network_api_service.dart';
import 'package:forvo/service/social_network/social_network_service.dart';
import 'package:forvo/ui/registration/social_network_complete_signup.dart';

Future<String> loginFacebook(BuildContext context) async {
  final LoginResult result = await FacebookAuth.instance.login();

  String loginResponse = SocialNetworkConstants.REQUEST_ERROR;
  switch (result.status) {
    case LoginStatus.operationInProgress:
      break;
    case LoginStatus.success:
      var socialNetworkStatus = await socialNetworkCheckUser(
        context,
        SocialNetworkConstants.FACEBOOK,
        result.accessToken.token,
      );

      if (socialNetworkStatus != null) {
        if (socialNetworkStatus.action ==
            SocialNetworkConstants.ACTION_SIGN_UP) {
          var signupResponse = await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => SocialNetworkCompleteSignUp(
                socialNetwork: SocialNetworkConstants.FACEBOOK,
                userInfo: socialNetworkStatus.userInfo,
              ),
            ),
          );
          if (signupResponse != null &&
              signupResponse == SocialNetworkConstants.REQUEST_OK) {
            loginResponse = await socialNetworkServiceLogin(
              context,
              socialNetwork: SocialNetworkConstants.FACEBOOK,
              socialNetworkId: socialNetworkStatus.userInfo.id,
              email: socialNetworkStatus.userInfo.email,
            );
          }
        } else if (socialNetworkStatus.action ==
            SocialNetworkConstants.ACTION_LOGIN) {
          loginResponse = await socialNetworkServiceLogin(
            context,
            socialNetwork: SocialNetworkConstants.FACEBOOK,
            socialNetworkId: socialNetworkStatus.userInfo.id,
            email: socialNetworkStatus.userInfo.email,
          );
        }
      }
      break;
    case LoginStatus.cancelled:
      loginResponse = SocialNetworkConstants.REQUEST_CANCELLED;
      break;
    case LoginStatus.failed:
      print(result.message);
      loginResponse = SocialNetworkConstants.REQUEST_ERROR;
      break;
  }
  return loginResponse;
}

void logoutFacebook(BuildContext context) async {
  await FacebookAuth.instance.logOut();
}
