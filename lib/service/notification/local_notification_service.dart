// ignore_for_file: avoid_classes_with_only_static_members

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:forvo/constants/push_notification_constants.dart';
import 'package:forvo/util/device_info_util.dart';

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize() {
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: AndroidInitializationSettings(
        '@mipmap/ic_launcher',
      ),
      iOS: IOSInitializationSettings(),
    );

    _notificationsPlugin.initialize(initializationSettings);
  }

  static void display(RemoteMessage message) async {
    try {
      final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;

      final NotificationDetails notificationDetails = NotificationDetails(
        android: AndroidNotificationDetails(
          PushNotificationConstants.CHANNEL_ID,
          PushNotificationConstants.CHANNEL_NAME,
          channelDescription: PushNotificationConstants.CHANNEL_DESCRIPTION,
          importance: Importance.high,
          priority: Priority.high,
          groupKey: message.collapseKey,
        ),
      );

      if (deviceIsAndroid()) {
        await _notificationsPlugin.show(
          id,
          message.notification.title,
          message.notification.body,
          notificationDetails,
        );
      }
    } on Exception catch (e) {
      print(e);
    }
  }
}
