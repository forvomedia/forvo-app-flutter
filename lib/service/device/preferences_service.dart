import 'dart:async';

import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/settings/theme_selector.dart';
import 'package:forvo/util/platform_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<SharedPreferences> _getPrefs() async => SharedPreferences.getInstance();

Future<String> _getPref(String key) async {
  var prefs = await _getPrefs();
  var value = prefs.getString(key);

  return value;
}

_savePref(String key, String value) async {
  var prefs = await _getPrefs();
  await prefs.setString(key, value);
}

_deletePref(String key) async {
  var prefs = await _getPrefs();
  await prefs.remove(key);
}

Future<bool> _getBoolPref(String key) async {
  var prefs = await _getPrefs();
  return prefs.getBool(key);
}

_saveBoolPref(String key, bool value) async {
  var prefs = await _getPrefs();
  await prefs.setBool(key, value);
}

Future<int> _getIntPref(String key) async {
  var prefs = await _getPrefs();
  return prefs.getInt(key);
}

_saveIntPref(String key, int value) async {
  var prefs = await _getPrefs();
  await prefs.setInt(key, value);
}

// APP VERSION
Future<String> getSavedAppVersion() async => _getPref('appVersion');

saveAppVersion(String appVersion) async {
  await _savePref('appVersion', appVersion);
}

// OS VERSION
Future<String> getSavedOsVersion() async => _getPref('osVersion');

saveOsVersion(String osVersion) async {
  await _savePref('osVersion', osVersion);
}

// USER
Future<String> getUserLoginCredential() async =>
    _getPref('userLoginCredential');

saveUserLoginCredential(String user) async {
  await _savePref('userLoginCredential', user);
}

deleteUserSession() async {
  await deleteUserUsername();
  await deleteUserEncryptedPassword();
  await deleteUserSocialNetwork();
  await deleteUserSocialNetworkId();
  await deleteUserKey();
  await deleteUserSecret();
  await deleteUserLastLoginDateTime();
  await _deletePref('userLoginCredential');
}

deleteAccountPref() async {
  await deleteUserSession();
  await deleteUserThemeOption();
  await deleteUserHistorySearchWords();
}

// USER USERNAME
Future<String> getUserUsername() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_username');
}

saveUserUsername(String user, String username) async {
  await _savePref('user_${user}_username', username);
}

deleteUserUsername() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_username');
}

// USER PASSWORD
Future<String> getUserEncryptedPassword() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_password');
}

saveUserEncryptedPassword(String user, String encryptedPassword) async {
  await _savePref('user_${user}_password', encryptedPassword);
}

deleteUserEncryptedPassword() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_password');
}

// USER SOCIAL NETWORK
Future<String> getUserSocialNetwork() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_socialNetwork');
}

saveUserSocialNetwork(String user, String socialNetwork) async {
  await _savePref('user_${user}_socialNetwork', socialNetwork);
}

deleteUserSocialNetwork() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_socialNetwork');
}

// USER SOCIAL NETWORK ID
Future<String> getUserSocialNetworkId() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_socialNetworkId');
}

saveUserSocialNetworkId(String user, String socialNetworkId) async {
  await _savePref('user_${user}_socialNetworkId', socialNetworkId);
}

deleteUserSocialNetworkId() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_socialNetworkId');
}

// USER KEY
Future<String> getUserKey() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_key');
}

saveUserKey(String user, String key) async {
  await _savePref('user_${user}_key', key);
}

deleteUserKey() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_key');
}

// USER SECRET
Future<String> getUserSecret() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_secret');
}

saveUserSecret(String user, String secret) async {
  await _savePref('user_${user}_secret', secret);
}

deleteUserSecret() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_secret');
}

// USER LANGUAGE CODE
Future<String> getUserLanguageCode(String user) async =>
    _getPref('userLanguageCode_$user');

saveUserLanguageCode(String languageCode) async {
  String user = await getUserLoginCredential();
  await _savePref('userLanguageCode_$user', languageCode);
}

// USER LAST LOGIN DATETIME
Future<String> getUserLastLoginDateTime() async {
  String user = await getUserLoginCredential();
  return _getPref('user_${user}_lastLoginDateTime');
}

saveUserLastLoginDateTime(String user, String datetime) async {
  await _savePref('user_${user}_lastLoginDateTime', datetime);
}

deleteUserLastLoginDateTime() async {
  String user = await getUserLoginCredential();
  await _deletePref('user_${user}_lastLoginDateTime');
}

// PUSH NOTIFICATIONS
initPushNotificationAuthorizationStatus({bool value}) async {
  await _saveBoolPref('pushNotificationAuthorizationStatus', value);
}

Future<bool> getPushNotificationAuthorizationStatus() async =>
    _getBoolPref('pushNotificationAuthorizationStatus');

initUserPushNotificationStatus() async {
  await saveUserPushNotificationStatus(value: true);
}

Future<bool> getUserPushNotificationsStatus() async {
  String user = await getUserLoginCredential();
  return _getBoolPref('userPushNotificationStatus_$user');
}

saveUserPushNotificationStatus({bool value}) async {
  String user = await getUserLoginCredential();
  await _saveBoolPref('userPushNotificationStatus_$user', value);
}

Future<String> getUserPushNotificationToken() async {
  String user = await getUserLoginCredential();
  return _getPref('userPushNotificationToken_$user');
}

saveUserPushNotificationToken(String token) async {
  String user = await getUserLoginCredential();
  await _savePref('userPushNotificationToken_$user', token);
}

// USER THEME
Future<String> getUserThemeOption() async {
  String user = await getUserLoginCredential();
  if (user != null) {
    return _getPref('theme_$user');
  }
  return THEME_SYSTEM;
}

saveUserThemeOption(String theme) async {
  String user = await getUserLoginCredential();
  await _savePref('theme_$user', theme);
}

deleteUserThemeOption() async {
  String user = await getUserLoginCredential();
  await _deletePref('theme_$user');
}

// USER HISTORY SEARCH WORDS
Future<String> getUserHistorySearchWords() async {
  String user = await getUserLoginCredential();
  user ??= GUEST_USER_NAME;
  return _getPref('userHistorySearchWords_$user');
}

saveUserHistorySearchWords(String theme) async {
  String user = await getUserLoginCredential();
  user ??= GUEST_USER_NAME;
  await _savePref('userHistorySearchWords_$user', theme);
}

deleteUserHistorySearchWords() async {
  String user = await getUserLoginCredential();
  await _deletePref('userHistorySearchWords_$user');
}

// APP REVIEW
initAppReviewMessageCheck() async {
  await _saveBoolPref('appReview.messageCheck', false);
}

initShowAppReviewMessageCount() async {
  await _saveIntPref('appReview.messageCount', 0);
}

Future<bool> getAppReviewMessageCheck() async =>
    _getBoolPref('appReview.messageCheck');

Future<int> getShowAppReviewMessageCount() async =>
    _getIntPref('appReview.messageCount');

updateAppReviewMessageCheck() async {
  await _saveBoolPref('appReview.messageCheck', true);
}

updateAppReviewMessageCount(int count) async {
  await _saveIntPref('appReview.messageCount', count);
}

// TUTORIAL
Future<bool> isTutorialChecked() async {
  var checked = await _getPref('tutorialCheck');
  return checked != null;
}

setTutorialChecked() async {
  await _savePref('tutorialCheck', '1');
}

Future<bool> isUserLogged() async {
  var user = await getUserLoginCredential();
  return user != null;
}

Future<String> getPreferredLanguageCode() async {
  bool logged = await isUserLogged();
  if (logged) {
    String user = await getUserLoginCredential();
    String languageCode = await getUserLanguageCode(user);
    if (languageCode != null) {
      return languageCode;
    }
  }

  if (isPlatformLanguageCodeSupported()) {
    String platformLanguageCode = getPlatformLanguageCode();
    if (platformLanguageCode != null) {
      return platformLanguageCode;
    }
  }

  return LanguageConstants.LANGUAGE_CODE_EN;
}

//LOCATION
Future<bool> isLocationPermissionRequested() async {
  var requested = await _getBoolPref('locationPermissionRequested');
  return requested != null;
}

setLocationPermissionRequested() async {
  await _saveBoolPref('locationPermissionRequested', true);
}
