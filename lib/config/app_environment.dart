import 'package:flutter/widgets.dart';
import 'package:forvo/constants/constants.dart';

class AppEnvironment {
  final String name;
  final String apiBaseUrl;
  final String mainBaseUrl;
  final String chatBaseUrl;
  final String flavor;

  AppEnvironment({
    @required this.name,
    @required this.apiBaseUrl,
    @required this.mainBaseUrl,
    @required this.chatBaseUrl,
    @required this.flavor,
  });

  factory AppEnvironment.dev() => AppEnvironment(
        name: ENVIRONMENT_DEV,
        apiBaseUrl: 'https://devapicorporate.forvo.com',
        mainBaseUrl: 'https://dev.forvo.com',
        chatBaseUrl: 'https://devchat.forvo.com',
        flavor: 'dev',
      );

  factory AppEnvironment.prod() => AppEnvironment(
        name: ENVIRONMENT_PROD,
        apiBaseUrl: 'https://appapi.forvo.com',
        mainBaseUrl: 'https://app.forvo.com',
        chatBaseUrl: 'https://appchat.forvo.com',
        flavor: 'prod',
      );
}
