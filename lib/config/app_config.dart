import 'package:flutter/material.dart';
import 'package:forvo/config/app_environment.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:meta/meta.dart';

class AppConfig extends InheritedWidget {
  final String environmentName;
  final String apiBaseUrl;
  final String mainBaseUrl;
  final String chatBaseUrl;
  final String flavor;

  AppConfig({
    @required AppEnvironment environment,
    @required Widget child,
  })  : environmentName = environment.name,
        apiBaseUrl = environment.apiBaseUrl,
        mainBaseUrl = environment.mainBaseUrl,
        chatBaseUrl = environment.chatBaseUrl,
        flavor = environment.flavor,
        super(child: child);

  static AppConfig of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<AppConfig>();

  String get apiUrl => '$apiBaseUrl/api2/${ApiConstants.CURRENT_API_VERSION}/';

  String get mainUrl => '$mainBaseUrl/';

  String get chatUrl => chatBaseUrl;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
