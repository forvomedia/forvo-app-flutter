import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/util/json.dart';

class AppLocalizations {
  Locale locale;
  static Map<String, dynamic> _localisedValues;

  AppLocalizations(this.locale) {
    _localisedValues = null;
  }

  static AppLocalizations of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations);

  static Locale localeOf(BuildContext context) =>
      Localizations.localeOf(context);

  static Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations appTranslations = AppLocalizations(locale);

    String jsonContent =
        await rootBundle.loadString('assets/l10n/${locale.languageCode}.json');
    _localisedValues = jsonDecode(jsonContent) as Map<String, dynamic>;

    return appTranslations;
  }

  String translate(String key, {Map<String, dynamic> params}) {
    if (_localisedValues == null) {
      return '';
    }

    String translated = getFromJson(key, _localisedValues) ??
        '$key not found in ${locale.languageCode}';
    if (params != null) {
      params.forEach((name, value) => translated =
          translated.replaceAll('\{\{$name\}\}', value.toString()));
    }

    return translated;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  final Locale newLocale;

  const AppLocalizationsDelegate({this.newLocale});

  @override
  bool isSupported(Locale locale) =>
      LanguageConstants.SUPPORTED_LANGUAGE_CODES.contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) =>
      AppLocalizations.load(newLocale ?? locale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => old != this;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppLocalizationsDelegate &&
          runtimeType == other.runtimeType &&
          newLocale == other.newLocale;

  @override
  int get hashCode => newLocale.hashCode;
}
