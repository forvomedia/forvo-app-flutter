import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {
  ThemeData _lightThemeData;
  ThemeData _darkThemeData;

  ThemeChanger(this._lightThemeData, this._darkThemeData);

  getLightTheme() => _lightThemeData;
  getDarkTheme() => _darkThemeData;

  setTheme(ThemeData lightTheme, ThemeData darkTheme) {
    _lightThemeData = lightTheme;
    _darkThemeData = darkTheme;

    notifyListeners();
  }
}
