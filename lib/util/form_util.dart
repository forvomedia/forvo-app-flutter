import 'package:flutter/material.dart';

void fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  if (nextFocus != null) {
    FocusScope.of(context).requestFocus(nextFocus);
  }
}