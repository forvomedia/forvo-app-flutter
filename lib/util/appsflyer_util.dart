import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:flutter/material.dart';
import 'package:forvo/constants/appsflyer_constants.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;

requestAppTrackinPermission() async {
  bool _requestPermission = false;
  if (!deviceIsAndroid()) {
    String deviceVersion = await getDeviceOSVersion();
    final splitVersion = deviceVersion.split('.');
    _requestPermission = !deviceIsAndroid() &&
        ((splitVersion.isNotEmpty && int.parse(splitVersion[0]) > 14) ||
            (splitVersion.length >= 2 &&
                (int.parse(splitVersion[0]) == 14 &&
                    int.parse(splitVersion[1]) >= 5)));
  }
  if (_requestPermission) {
    await permission_handler.Permission.appTrackingTransparency
        .request()
        .isGranted;
  }
}

appsflyerEventLog(String eventName, Map eventValues) async {
  AppsFlyerOptions appsFlyerOptions = AppsFlyerOptions(
    afDevKey: APPSFLYER_DEV_KEY,
    appId: deviceIsAndroid() ? ANDROID_APP_ID : IOS_APP_ID,
  );
  try {
    await AppsflyerSdk(appsFlyerOptions).logEvent(eventName, eventValues);
  } on Exception catch (e) {
    debugPrint('Failed to send event: ${e.toString()}');
  }
}
