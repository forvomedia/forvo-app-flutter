String getFromJson(String key, Map<String, dynamic> json) {
  var value = json[key] as String;
  if (value == null && key.contains('.')) {
    value = _getNestedFromJson(key, json);
  }

  return value;
}

String _getNestedFromJson(String key, Map<String, dynamic> json) {
  if (json == null) {
    return null;
  }

  var split = key.split('.');
  var firstKey = split.removeAt(0);
  if (split.isNotEmpty) {
    return _getNestedFromJson(
      split.join('.'),
      json[firstKey] as Map<String, dynamic>,
    );
  }

  return json[firstKey] as String;
}
