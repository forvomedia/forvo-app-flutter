import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/service/api/app_review_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'package:launch_review/launch_review.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app_util.dart';
import 'device_info_util.dart';

Future<bool> showAppReviewMessage(isUserLogged) async {
  bool isChecked = await getAppReviewMessageCheck();
  if (isChecked == null) {
    await initAppReviewMessageCheck();
    await initShowAppReviewMessageCount();
    isChecked = false;
  }

  if (isChecked || !isUserLogged) {
    return false;
  }

  int appReviewCount = await getShowAppReviewMessageCount();
  if (appReviewCount == APP_REVIEW_CHECK_STEPS_LIMIT) {
    await updateAppReviewMessageCheck();
    return true;
  }

  appReviewCount++;
  await updateAppReviewMessageCount(appReviewCount);

  return false;
}

void _launchURL(BuildContext context, String url) async {
  if (!await launch(url, forceWebView: true)) {
    var localizations = AppLocalizations.of(context);
    showCustomSnackBar(
      context,
      themedSnackBar(
        context,
        localizations.translate('error.default'),
        type: SNACKBAR_TYPE_ERROR,
      ),
    );
  }
}

void _launchAppStoreURL(BuildContext context) {
  String url = deviceIsAndroid() ? ANDROID_APP_STORE_URL : IOS_APP_STORE_URL;
  _launchURL(context, url);
}

void showAlertDialog(BuildContext context) {
  var localizations = AppLocalizations.of(context);
  showDialog(
    context: context,
    builder: (BuildContext ctx) => AlertDialog(
      title: Text(
        localizations.translate('appReview.title'),
      ),
      content: Text(
        localizations.translate('appReview.subtitle'),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            localizations.translate('shared.yes'),
            style: TextStyle(color: colorBlue),
          ),
          onPressed: () {
            try {
              LaunchReview.launch(
                  androidAppId: ANDROID_APP_ID, iOSAppId: IOS_APP_ID);
            } on Exception {
              _launchAppStoreURL(context);
            }
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(
            localizations.translate('shared.no'),
            style: TextStyle(color: colorBlue),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            showAppReviewAlertDialog(context);
          },
        ),
      ],
    ),
  );
}

void showAppReviewAlertDialog(BuildContext context) async {
  var localizations = AppLocalizations.of(context);
  var state = AppStateManager.of(context).state;
  var _textFieldController = TextEditingController();
  FocusNode _textEditingFocusNode = FocusNode(debugLabel: 'texEditing');
  var _deviceId = await getDeviceId();
  var _deviceName = await getDeviceName();
  var _osName = getDeviceOSName();
  var _osVersion = await getDeviceOSVersion();
  var _appVersion = await getAppVersion();
  showDialog(
    context: context,
    builder: (BuildContext ctx) => AlertDialog(
      title: Text(
        localizations.translate('appReview.writeComment.title'),
      ),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              localizations.translate('appReview.writeComment.subtitle'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: TextFormField(
                controller: _textFieldController,
                focusNode: _textEditingFocusNode,
                keyboardType: TextInputType.multiline,
                maxLines: 3,
                minLines: 3,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  child: Text(
                    localizations.translate('shared.send'),
                    style: TextStyle(color: colorBlue),
                  ),
                  onPressed: () async {
                    _textEditingFocusNode.unfocus();
                    if (_textFieldController.value.text.isNotEmpty) {
                      try {
                        await postComment(
                          context,
                          username: state.userInfo.user.username,
                          email: state.userInfo.user.email,
                          deviceID: _deviceId,
                          deviceName: _deviceName,
                          osName: _osName,
                          osVersion: _osVersion,
                          appVersion: _appVersion,
                          comment: _textFieldController.value.text,
                        );
                        Navigator.of(context).pop();
                        showCustomSnackBar(
                          context,
                          themedSnackBar(
                            context,
                            localizations.translate('appReview.response.send'),
                            type: SNACKBAR_TYPE_OK,
                          ),
                        );
                      } on ApiException catch (error) {
                        var localizations = AppLocalizations.of(context);
                        List<ApiError> errors = error.getErrors();
                        String message;
                        for (ApiError _error in errors) {
                          String translate = localizations
                              .translate('error.api.${_error.errorApiCode}');
                          if (message == null) {
                            message = translate;
                          } else {
                            message = '$message\n$translate';
                          }
                        }
                        if (message.isNotEmpty) {
                          showCustomSnackBar(
                            context,
                            themedSnackBar(
                              context,
                              message,
                              type: SNACKBAR_TYPE_ERROR,
                            ),
                          );
                        }
                      }
                    } else {
                      showCustomSnackBar(
                        ctx,
                        themedSnackBar(
                          context,
                          localizations
                              .translate('appReview.writeComment.notEmpty'),
                          type: SNACKBAR_TYPE_ERROR,
                        ),
                      );
                    }
                  },
                ),
                TextButton(
                  child: Text(
                    localizations.translate('shared.no'),
                    style: TextStyle(color: colorBlue),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    showCustomSnackBar(
                      context,
                      themedSnackBar(
                        context,
                        localizations.translate('appReview.response.no'),
                        type: SNACKBAR_TYPE_OK,
                      ),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}
