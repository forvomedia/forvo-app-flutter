import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/snackbar_util.dart';

showMaintenanceSnackBar(BuildContext context, AppLocalizations localizations) {
  showCustomSnackBar(
    context,
    themedSnackBar(
      context,
      localizations.translate('shared.maintenanceMessage'),
      type: SNACKBAR_TYPE_ERROR,
    ),
  );
}
