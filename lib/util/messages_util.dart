import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/constants/websocket_constants.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/database_chat_message.dart';
import 'package:forvo/model/websocket_last_message_and_chat_conversations.dart';
import 'package:forvo/model/websocket_status_and_chat_conversations.dart';
import 'package:forvo/service/chat/websocket_service.dart';
import 'package:forvo/service/database/database_service.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/device_info_util.dart';

DatabaseService _databaseService = DatabaseService();
WebSocketService _webSocketService = WebSocketService();
bool _webSocketCallbacksInitialized = false;
String _username;
String _lastMessageId;
bool _skipWebSocketCallback = false;

getMessagesAfterLogin(String username, String md5Password) async {
  String _deviceId = await getDeviceId();
  _username = username;
  _lastMessageId = '0';

  var ctx = appGlobals.scaffoldKey.currentContext;
  var state = AppStateManager.of(ctx).state;

  if (!state.isMaintenanceReadOnlyActive &&
      !state.isMaintenanceMessagesActive) {
    _webSocketService.initializeState(state);
    _webSocketService.connect(
      username,
      md5Password,
      _deviceId,
      connectionAttemptsLimit: WebSocketConstants.RECONNECTION_ATTEMPTS_LIMIT,
    );
    _setWebSocketCallbacks();
  }
}

_setWebSocketCallbacks() async {
  if (!_webSocketCallbacksInitialized) {
    _webSocketCallbacksInitialized = true;

    _webSocketService.on(WebSocketConstants.EVENT_AUTHENTICATED, (_) async {
      if (_skipWebSocketCallback) {
        return;
      }

      await getLastMessagesAndSendAllLocalMessages(_username);
    });

    _webSocketService.on(WebSocketConstants.EVENT_RECEIVE_MESSAGES,
        (data) async {
      if (_skipWebSocketCallback) {
        return;
      }
      _webSocketService.disconnect();
      try {
        await receiveMessages(data);
      } on ApiException catch (_) {
        debugPrint('messages_util.dart > _setWebSocketCallbacks > '
            'websocket > receive-messages > '
            'API EXCEPTION > $data');
      } on Exception catch (_) {
        debugPrint('messages_util.dart > _setWebSocketCallbacks > '
            'websocket > receive-messages > '
            'EXCEPTION > $data');
      } finally {
        _skipWebSocketCallback = true;
      }
    });
  }
}

getLastMessagesAndSendAllLocalMessages(String username) async {
  //ASK FOR NEW MESSAGES
  String databaseLastMessageId = await _databaseService.getLastMessageID();
  _lastMessageId = databaseLastMessageId;
  List<String> _data = [];
  _data.addAll([
    username,
    _lastMessageId,
  ]);
  _webSocketService.emit(WebSocketConstants.EVENT_GET_MESSAGES, _data);

  //SEND ALL LOCAL MESSAGES
  List<DatabaseChatMessage> messages =
      await _databaseService.getMessagesToForvo(username);
  if (messages != null && messages.isNotEmpty) {
    for (DatabaseChatMessage message in messages) {
      List<String> _messageData = [];
      _messageData.addAll([
        message.idMessageLocal,
        message.idConversation,
        message.userFrom,
        message.userTo,
        message.message,
      ]);
      _webSocketService.emit(
          WebSocketConstants.EVENT_SEND_MESSAGE, _messageData);
    }
  }
}

Future<WebSocketLastMessageAndChatConversations> receiveMessages(
    String data) async {
  try {
    var webSocketData = json.decode(data);
    if (webSocketData['status'] != null &&
        webSocketData['status'] == ApiConstants.STATUS_OK) {
      WebSocketStatusAndChatConversations webSocketResponse =
          WebSocketStatusAndChatConversations.fromJson(webSocketData);

      if (webSocketResponse.conversations.isNotEmpty) {
        await _databaseService
            .saveConversations(webSocketResponse.conversations);
      }

      return WebSocketLastMessageAndChatConversations(
        lastMessageId: _lastMessageId,
        conversations: webSocketResponse.conversations,
      );
    } else if (webSocketData['status'] != null &&
        webSocketData['status'] == ApiConstants.STATUS_ERROR) {
      throw ApiException(webSocketData);
    } else {
      throw Exception();
    }
  } on ApiException catch (_) {
    rethrow;
  } on Exception catch (e) {
    debugPrint('EXCEPTION ON EVENT_RECEIVE_MESSAGES $e');
    rethrow;
  }
}
