import 'dart:async';

import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/ui/colors.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/settings/theme_selector.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/ui/themes/dark_theme.dart';
import 'package:forvo/ui/themes/light_theme.dart';

Future<ThemeData> getUserLightTheme() async {
  String _defaultTheme = await getUserThemeOption();
  ThemeData _theme;
  switch (_defaultTheme) {
    case THEME_DARK:
      _theme = darkTheme;
      break;
    case THEME_LIGHT:
    default:
      _theme = lightTheme;
      break;
  }
  return _theme;
}

Future<ThemeData> getUserDarkTheme() async {
  String _defaultTheme = await getUserThemeOption();
  ThemeData _theme;
  switch (_defaultTheme) {
    case THEME_LIGHT:
      _theme = lightTheme;
      break;
    case THEME_DARK:
    default:
      _theme = darkTheme;
      break;
  }
  return _theme;
}

forceLogoutUserSession({bool redirectToStart = false}) {
  var state = AppStateManager.of(appGlobals.scaffoldKey.currentContext).state;
  if (state != null) {
    state.merge(
      AppState(
        isUserLogged: false,
        isSocialNetworkLogin: false,
        userInfo: null,
      ),
    );
    deleteUserSession();
    if (redirectToStart) {
      appGlobals.navigatorKey.currentState.pushNamedAndRemoveUntil(
        RouteConstants.START,
        (route) => false,
      );
    }
  }
}

void showLinguisticProfileAlertDialog(BuildContext context) {
  var localizations = AppLocalizations.of(context);
  showDialog(
    context: context,
    builder: (BuildContext ctx) => AlertDialog(
      title: Text(
        localizations.translate('linguisticProfileAlert.title'),
      ),
      content: Text(
        localizations.translate('linguisticProfileAlert.subtitle'),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            localizations.translate('shared.complete'),
            style: TextStyle(color: colorBlue),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.pushNamed(context, RouteConstants.ACCOUNT_INFO);
          },
        ),
        TextButton(
          child: Text(
            localizations.translate('shared.cancel'),
            style: TextStyle(color: colorBlue),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    ),
  );
}
