import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/exceptions/api_exceptions.dart';
import 'package:forvo/model/api_exception.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/translation_language_group.dart';
import 'package:forvo/service/api/language_api_service.dart';
import 'package:forvo/ui/state/app_state.dart';
import 'package:forvo/ui/widgets/snackbar/themed_snackbar.dart';
import 'package:forvo/util/snackbar_util.dart';
import 'connectivity_util.dart';

String getCountryCodeFromLanguage(String languageCode) {
  String countryCode = '';
  switch (languageCode) {
    case LanguageConstants.LANGUAGE_CODE_ES:
      countryCode = LanguageConstants.COUNTRY_CODE_ES;
      break;
    case LanguageConstants.LANGUAGE_CODE_EN:
      countryCode = LanguageConstants.COUNTRY_CODE_US;
      break;
    case LanguageConstants.LANGUAGE_CODE_FR:
      countryCode = LanguageConstants.COUNTRY_CODE_FR;
      break;
    case LanguageConstants.LANGUAGE_CODE_PT:
      countryCode = LanguageConstants.COUNTRY_CODE_PT;
      break;
    default:
      break;
  }
  return countryCode;
}

String getLanguageNameAndCode(Language language) =>
    language.code != null && language.code.isNotEmpty
        ? '${language.name} (${language.code})'
        : language.name;

Future<void> getLanguagesAndPopularLanguages(
  BuildContext context,
  AppState state,
  String languageCode, {
  bool callingFromApp = false,
}) async {
  // TODO: CACHE LANGUAGE LIST

  int notConnectedExceptionsCount = 0;

  try {
    var data = await Future.wait([
      getLanguages(context, languageCode: languageCode),
      getPopularLanguages(context, languageCode: languageCode),
      getSearchTranslationLanguages(context, languageCode: languageCode),
    ]);

    List<Language> languages = data[0];
    List<Language> popularLanguages = data[1];
    List<TranslationLanguageGroup> translationLanguageGroups = data[2];

    state.merge(
      AppState(
        languages: languages,
        popularLanguages: popularLanguages,
        translationLanguageGroups: translationLanguageGroups,
      ),
    );
  } on ApiException catch (error) {
    String messages = error.getErrorMessages(context);
    if (messages != null) {
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          messages,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  } on NotConnectedException catch (_) {
    if (!callingFromApp) {
      notConnectedExceptionsCount++;
    } else {
      debugPrint('NotConnectedException on getLanguagesAndPopularLanguages '
          'from app.dart');
    }
  } on Exception catch (_) {
    debugPrint('EXCEPTION on getLanguagesAndPopularLanguages');
  }
  if (notConnectedExceptionsCount > 0) {
    var localizations = AppLocalizations.of(context);
    bool _deviceHasConnection = await deviceHasConnection();
    if (_deviceHasConnection) {
      String message = '${localizations.translate('error.service.title')}\n'
          '${localizations.translate('error.service.body')}';
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    } else {
      String message = '${localizations.translate('error.network.title')}\n'
          '${localizations.translate('error.network.body')}';
      showCustomSnackBar(
        context,
        themedSnackBar(
          context,
          message,
          type: SNACKBAR_TYPE_ERROR,
        ),
      );
    }
  }
}
