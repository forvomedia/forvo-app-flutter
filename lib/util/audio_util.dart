import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:forvo/model/playable_file.dart';
import 'package:forvo/service/api/word_api_service.dart';
import 'package:forvo/ui/state/app_state_manager.dart';
import 'package:forvo/util/download_util.dart';
import 'package:forvo/util/file_util.dart';
import 'package:logger/logger.dart' show Level;

playAudio(BuildContext context, PlayableFile file) async {
  bool result;
  var state = AppStateManager.of(context).state;
  if (file.isLocal) {
    result = await _playLocalAudio(file);
  } else {
    result = await _playAudioFromUrl(file);
  }
  if (result != null && result && file.id != null) {
    try {
      updatePronunciationSumHit(
        context,
        file.id,
        interfaceLanguageCode: state.locale.languageCode,
      );
    } on FormatException catch (_) {
      updatePronunciationSumHit(
        context,
        file.id,
        forceUseDevEnvironmentBasicAuthentication: true,
        interfaceLanguageCode: state.locale.languageCode,
      );
    }
  }
}

_playLocalAudio(PlayableFile file) async {
  String _filePath = await getLocalFilePath(file.localPath);
  bool result = await _play(_filePath);
  return result;
}

_playAudioFromUrl(PlayableFile file) async {
  bool result = await _play(file.url);
  _downloadAudioFromUrl(file);
  return result;
}

_downloadAudioFromUrl(PlayableFile file) async {
  String filePath = await getLocalFilePath(file.localPath);
  downloadFile(file.url, filePath);
}

Future<bool> _play(String filePath) async {
  FlutterSoundPlayer _audioPlayer = FlutterSoundPlayer(logLevel: Level.nothing);
  _audioPlayer.openAudioSession();
  Duration duration = await _audioPlayer.startPlayer(
    fromURI: filePath,
    whenFinished: () async => _audioPlayer.closeAudioSession(),
  );
  if (duration == null) {
    throw Exception();
  }
  return true;
}
