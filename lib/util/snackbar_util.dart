import 'package:flutter/material.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/widgets/scaffold/custom_scaffold.dart';

showCustomSnackBar(BuildContext context, SnackBar themeSnackBar) {
  CustomScaffoldState scaffoldState =
      context.findAncestorStateOfType<CustomScaffoldState>();
  if (scaffoldState != null) {
    scaffoldState.showSnackBar(themeSnackBar);
  } else {
    appGlobals.scaffoldMessengerKey.currentState.showSnackBar(themeSnackBar);
  }
}
