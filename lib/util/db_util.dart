int getIntValueFromBool({bool boolValue}) => boolValue ? 1 : 0;
