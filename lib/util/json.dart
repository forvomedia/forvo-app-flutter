import 'dart:convert';

String jsonStringField(field, {String nullValue}) {
  if (field is String && field != '') {
    return field;
  } else {
    return nullValue;
  }
}

List<String> jsonStringListField(field) {
  if (field != null && field is List) {
    List<String> values =
        field.map((value) => value is String ? value : null).toList();
    return values;
  } else {
    return [];
  }
}

bool jsonBoolField(field) {
  if (field is bool) {
    return field;
  } else {
    return false;
  }
}

double jsonDoubleField(field) {
  double value;
  if (field is num) {
    value = field.toDouble();
  }
  return value;
}

List<num> jsonNumListField(field) => field != null && field is List
    ? field.map((value) => value is num ? value : null).toList()
    : [];

int jsonIntField(field) {
  int value;
  if (field is int) {
    value = field;
  }
  return value;
}

final myJsonCodec = JsonCodec.withReviver((key, value) {
  if (value is int) {
    return value.toDouble();
  }
  return value;
});

DateTime jsonDateField(field) {
  DateTime value;
  if (field is int) {
    value = DateTime.fromMillisecondsSinceEpoch(field);
  }
  return value;
}

String getFromJson(String key, Map<String, dynamic> json) {
  var value = json[key] as String;
  if (value == null && key.contains('.')) {
    value = _getNestedFromJson(key, json);
  }

  return value;
}

String _getNestedFromJson(String key, Map<String, dynamic> json) {
  if (json == null) {
    return null;
  }

  var split = key.split('.');
  var firstKey = split.removeAt(0);
  if (split.isNotEmpty) {
    return _getNestedFromJson(
      split.join('.'),
      json[firstKey] as Map<String, dynamic>,
    );
  }

  return json[firstKey] as String;
}
