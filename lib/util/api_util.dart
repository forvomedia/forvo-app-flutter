import 'package:forvo/util/crypto_util.dart';

String generateParametersSignature(List<String> params) {
  String _concatenatedParams = params.join('|');
  return generateSha1(_concatenatedParams);
}
