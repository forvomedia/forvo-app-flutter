import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:forvo/constants/push_notification_constants.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/service/api/notification_api_service.dart';
import 'package:forvo/service/device/preferences_service.dart';
import 'package:forvo/service/notification/local_notification_service.dart';
import 'package:forvo/ui/config/globals.dart';

Future<bool> isPushNotificationSettingSelected() async {
  bool authorization = await getPushNotificationAuthorizationStatus();
  if (authorization) {
    bool status = await getUserPushNotificationsStatus();
    if (status != null) {
      return status;
    }
    await initUserPushNotificationStatus();
    return true;
  }
  return authorization;
}

Future<bool> configureFirebaseNotifications(
  FirebaseMessaging firebaseMessaging,
) async {
  var settings = await firebaseMessaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  if (settings.authorizationStatus == AuthorizationStatus.authorized ||
      settings.authorizationStatus == AuthorizationStatus.provisional) {
    initPushNotificationAuthorizationStatus(value: true);

    /// Receives the message on which user taps when opens app from
    /// terminated state
    firebaseMessaging.getInitialMessage().then((RemoteMessage message) {
      if (message != null) {
        if (message.collapseKey != null &&
            message.collapseKey.isNotEmpty &&
            isValidNotificationType(message.collapseKey)) {
          navigateFromPushNotification(message.collapseKey);
        } else {
          if (message.data['collapse_key'] != null &&
              message.data['collapse_key'] != '' &&
              isValidNotificationType(message.data['collapse_key'])) {
            navigateFromPushNotification(message.data['collapse_key']);
          }
        }
      }
    });

    /// When app is in foreground
    FirebaseMessaging.onMessage.listen(LocalNotificationService.display);

    /// When app is in background but opened and user taps on notification
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      if (message.collapseKey != null &&
          message.collapseKey.isNotEmpty &&
          isValidNotificationType(message.collapseKey)) {
        navigateFromPushNotification(message.collapseKey);
      } else {
        if (message.data['collapse_key'] != null &&
            message.data['collapse_key'] != '' &&
            isValidNotificationType(message.data['collapse_key'])) {
          navigateFromPushNotification(message.data['collapse_key']);
        }
      }
    });
    return true;
  } else {
    initPushNotificationAuthorizationStatus(value: false);
    return false;
  }
}

void manageCloudMessagingToken(
  BuildContext context, {
  @required String token,
  @required String languageCode,
  @required bool status,
}) async {
  if (status == PushNotificationConstants.STATE_SUBSCRIBED) {
    await registerCloudMessagingToken(
      context,
      token: token,
      languageCode: languageCode,
    );
  } else {
    await unregisterCloudMessagingToken(
      context,
      token: token,
      languageCode: languageCode,
    );
  }
  await saveUserPushNotificationToken(token);
}

managePushNotification(
  BuildContext context, {
  @required String languageCode,
  @required bool status,
}) async {
  FirebaseMessaging.instance.getToken().then((String token) {
    manageCloudMessagingToken(context,
        token: token, languageCode: languageCode, status: status);
  });
  Stream<String> _tokenStream = FirebaseMessaging.instance.onTokenRefresh;
  _tokenStream.listen((String token) {
    manageCloudMessagingToken(context,
        token: token, languageCode: languageCode, status: status);
  });
}

Future<void> subscribePushNotifications(
    BuildContext context, String languageCode) async {
  bool status = await isPushNotificationSettingSelected();
  managePushNotification(
    context,
    languageCode: languageCode,
    status: status,
  );
}

unsubscribePushNotifications(BuildContext context, String languageCode) async {
  managePushNotification(context, languageCode: languageCode, status: false);
}

bool isValidNotificationType(String notificationType) =>
    notificationType == PushNotificationConstants.TYPE_PRONOUNCE ||
    notificationType == PushNotificationConstants.TYPE_VOTE ||
    notificationType == PushNotificationConstants.TYPE_MESSAGE;

navigateFromPushNotification(String notificationType) async {
  switch (notificationType) {
    case PushNotificationConstants.TYPE_MESSAGE:
      Navigator.pushNamed(
        appGlobals.navigatorKey.currentContext,
        RouteConstants.CHAT_LIST,
      );
      break;
    case PushNotificationConstants.TYPE_PRONOUNCE:
    case PushNotificationConstants.TYPE_VOTE:
    default:
      Navigator.pushNamed(
        appGlobals.navigatorKey.currentContext,
        RouteConstants.NOTIFICATIONS,
      );
      break;
  }
}
