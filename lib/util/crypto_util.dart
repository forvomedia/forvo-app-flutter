import 'dart:convert';
import 'package:crypto/crypto.dart' as crypto;

generateMd5(String data) {
  var bytes = utf8.encode(data);
  var md5 = crypto.md5;
  var digest = md5.convert(bytes);
  return digest.toString();
}

generateSha1(String data) {
  var bytes = utf8.encode(data);
  var sha1 = crypto.sha1;
  var digest = sha1.convert(bytes);
  return digest.toString();
}

String decryptString(String encryptedString) =>
    utf8.decode(base64.decode(encryptedString));