import 'package:flutter/material.dart';

bool themeIsDark(BuildContext context) =>
    Theme.of(context).brightness == Brightness.dark;
