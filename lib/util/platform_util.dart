import 'dart:io';

import 'package:forvo/constants/language_constants.dart';

String getPlatformLanguageCode() => Platform.localeName.split('_')[0];

String getPlatformCountryCode() => Platform.localeName.split('_')[1];

bool isPlatformLanguageCodeSupported() =>
    LanguageConstants.SUPPORTED_LANGUAGE_CODES
        .contains(getPlatformLanguageCode());
