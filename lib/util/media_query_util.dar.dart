import 'package:flutter/material.dart';
import 'package:forvo/constants/layout_constants.dart';

double getDeviceHeight(BuildContext context) =>
    MediaQuery.of(context).size.height;

double getDeviceWidth(BuildContext context) =>
    MediaQuery.of(context).size.width;

bool useMobileLayout(BuildContext context) {
  // The equivalent of the "smallestWidth" qualifier on Android.
  var shortestSide = MediaQuery.of(context).size.shortestSide;

  // Determine if we should use mobile layout or not. The
  // number 600 here is a common breakpoint for a typical
  // 7-inch tablet.
  return shortestSide < LayoutConstants.LAYOUT_TABLET_BREAKPOINT;
}

bool useSmallMobileLayout(BuildContext context) {
  // The equivalent of the "smallestWidth" qualifier on Android.
  var shortestSide = MediaQuery.of(context).size.shortestSide;

  // Determine if we should use small mobile layout or not. The
  // number 360 here is a breakpoint for a small android mobile
  // with 360 x 640 viewport
  return shortestSide < LayoutConstants.LAYOUT_SMALL_MOBILE_WIDTH_BREAKPOINT;
}

bool isDeviceUsingDarkTheme(BuildContext context) {
  var brightness = MediaQuery.of(context).platformBrightness;
  if (brightness == Brightness.dark) {
    return true;
  }
  return false;
}

bool isDarkThemeActive(BuildContext context) {
  var brightness = Theme.of(context).brightness;
  if (brightness == Brightness.dark) {
    return true;
  }
  return false;
}
