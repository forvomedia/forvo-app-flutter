import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

Future<String> _applicationLocalPath() async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

createDirectory(String folderPath) async {
  final appLocalPath = await _applicationLocalPath();
  await Directory('$appLocalPath/$folderPath').create(recursive: true);
}

Future<String> getLocalFilePath(String filePath) async {
  final appLocalPath = await _applicationLocalPath();
  return '$appLocalPath/$filePath';
}

Future<bool> fileExists(String filePath) async {
  final localFilePath = await getLocalFilePath(filePath);
  var file = File('$localFilePath');
  // ignore: avoid_slow_async_io
  return file.exists();
}

Future<bool> directoryExists(String folderPath) async {
  final appLocalPath = await _applicationLocalPath();
  var directory = Directory('$appLocalPath/$folderPath');
  // ignore: avoid_slow_async_io
  return directory.exists();
}

deleteDirectory(String folderPath) async {
  final appLocalPath = await _applicationLocalPath();
  var directory = Directory('$appLocalPath/$folderPath');
  directory.delete(recursive: true);
}

String getFilenameFromUrl(String url){
  Pattern _pattern = '/';
  return url.split(_pattern).last;
}
