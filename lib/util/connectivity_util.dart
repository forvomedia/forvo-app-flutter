import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/ui/colors.dart';
import 'package:network_info_plus/network_info_plus.dart';

Future<bool> deviceHasConnection() async {
  var connectivityResult = await Connectivity().checkConnectivity();
  if (connectivityResult == ConnectivityResult.none) {
    return false;
  }
  return true;
}

Future<String> getIP() async {
  String ipv4 = await Ipify.ipv4();
  if (ipv4 != null && ipv4.isNotEmpty) {
    return ipv4;
  }
  return await getWifiIP();
}

Future<String> getWifiIP() async {
  bool usingWifi = await usingWifiConnection();
  if (usingWifi) {
    return await NetworkInfo().getWifiIP();
  } else {
    return NOT_AVAILABLE_DATA;
  }
}

showConnectivityWarningDialog(
    BuildContext context, AppLocalizations localizations) {
  var themeData = Theme.of(context);
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: ListTile(
              leading: Icon(
                Icons.wifi,
                size: 36.0,
                color: colorBlue,
              ),
              title: Text(
                localizations.translate('connection.internetRequired'),
                style: themeData.textTheme.bodyText2,
              ),
            ),
          ),
          Divider(),
          Center(
            child: TextButton(
              child: Text(
                localizations.translate('shared.close'),
                style: themeData.textTheme.bodyText2,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    ),
  );
}

Future<bool> usingWifiConnection() async {
  var connectivityResult = await Connectivity().checkConnectivity();
  if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}
