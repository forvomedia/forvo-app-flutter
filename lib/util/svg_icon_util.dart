import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

const double iconHeight = 20.0;

final Widget svgIconForvoLogo = SvgPicture.asset(
  'assets/images/logo.svg',
  height: iconHeight,
);
