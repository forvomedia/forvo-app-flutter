import 'dart:async';

import 'package:forvo/constants/search_constants.dart';
import 'package:forvo/service/device/preferences_service.dart';

Future<List<String>> getHistorySearchWords(
    {bool getLastItemFirst = true}) async {
  List<String> _historyWords = [];

  String historySearchWords = await getUserHistorySearchWords();
  if (historySearchWords != null && historySearchWords.isNotEmpty) {
    _historyWords =
        historySearchWords.split(SearchConstants.SEARCH_HISTORY_WORD_SEPARATOR);
  }
  if (getLastItemFirst) {
    return _historyWords.reversed.toList();
  } else {
    return _historyWords;
  }
}

saveWordIntoHistorySearchWords(String word) async {
  List<String> historyWords =
      await getHistorySearchWords(getLastItemFirst: false);
  if (!historyWords.contains(word)) {
    historyWords.add(word);
  } else {
    int index = historyWords.indexOf(word);
    historyWords.removeAt(index);
    historyWords.add(word);
  }
  String historySearchWords = '';
  if (historyWords.length > SearchConstants.SEARCH_HISTORY_LIMIT) {
    historyWords.removeAt(0);
  }

  for (String _historyWord in historyWords) {
    if (historySearchWords.isEmpty) {
      historySearchWords = '$_historyWord';
    } else {
      historySearchWords =
          // ignore: lines_longer_than_80_chars
          '$historySearchWords${SearchConstants.SEARCH_HISTORY_WORD_SEPARATOR}$_historyWord';
    }
  }

  await saveUserHistorySearchWords(historySearchWords);
}
