import 'package:flutter/material.dart';
import 'package:forvo/constants/constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:intl/intl.dart';

String formatMillis(
  int millis,
  String format, {
  bool isUtc = false,
}) {
  var micros = millis * 1000;
  var dateTime = DateTime.fromMicrosecondsSinceEpoch(micros, isUtc: isUtc);
  var formatter = DateFormat(format);

  return formatter.format(dateTime);
}

String getHourStringFromDateString(
  String date,
  String inputFormat, {
  bool isUtc = false,
  bool convertToLocal = true,
}) {
  DateFormat inputDateFormat = DateFormat(inputFormat);
  DateTime inputDate = inputDateFormat.parse(date, isUtc);
  DateTime outputDate = inputDate;
  if (isUtc && convertToLocal) {
    outputDate = inputDate.toLocal();
  }
  return getDefaultFormattedTimeFromDate(outputDate);
}

DateTime getDateTimeFromDateString(
  String date,
  String inputFormat, {
  bool isUtc = false,
  bool convertToLocal = true,
}) {
  DateFormat inputDateFormat = DateFormat(inputFormat);
  DateTime inputDate = inputDateFormat.parse(date, isUtc);
  DateTime outputDate = inputDate;
  if (isUtc && convertToLocal) {
    outputDate = inputDate.toLocal();
  }
  return outputDate;
}

String getLocaleFormattedDateString(
  String date,
  String inputFormat,
  String interfaceLanguage, {
  bool isUtc = false,
}) {
  DateTime today = DateTime.now();
  DateFormat inputDateFormat = DateFormat(inputFormat);
  DateTime inputDate = inputDateFormat.parse(date, isUtc);
  DateTime outputDate = inputDate;
  if (isUtc) {
    outputDate = inputDate.toLocal();
  }
  DateFormat outputDateFormat = DateFormat(
    localeFormatDate(interfaceLanguage,
        isCurrentYear: today.year == outputDate.year),
    interfaceLanguage,
  );
  return outputDateFormat.format(outputDate);
}

Widget getLocaleFormattedDateWidgets(
  ThemeData themeData,
  String date,
  String inputFormat,
  String interfaceLanguage, {
  bool showTime = true,
  bool isUtc = false,
}) {
  bool isCurrentYear = false;
  DateTime today = DateTime.now();
  DateFormat outputDateFormat;
  DateTime outputDate;
  if (date != null && date.isNotEmpty && date != 'null') {
    DateFormat inputDateFormat = DateFormat(inputFormat);
    DateTime inputDate = inputDateFormat.parse(date, isUtc);
    outputDate = inputDate;
    if (isUtc) {
      outputDate = inputDate.toLocal();
    }
    isCurrentYear = today.year == outputDate.year;
    outputDateFormat = DateFormat(
      localeFormatDate(interfaceLanguage, isCurrentYear: isCurrentYear),
      interfaceLanguage,
    );
  }

  List<Widget> widgets = [];
  if (outputDate != null) {
    if (isCurrentYear) {
      if (today.month != outputDate.month || today.day != outputDate.day) {
        widgets.add(
          Text(
            outputDateFormat.format(outputDate),
            style: themeData.textTheme.subtitle1.copyWith(fontSize: 14.0),
          ),
        );
      }
    } else {
      widgets.add(
        Text(
          outputDateFormat.format(outputDate),
          style: themeData.textTheme.subtitle1.copyWith(fontSize: 14.0),
        ),
      );
    }
    if (showTime) {
      widgets.add(
        Text(
          getDefaultFormattedTimeFromDate(outputDate),
          style: themeData.textTheme.subtitle1.copyWith(fontSize: 14.0),
        ),
      );
    }
  }
  return Column(
    children: widgets,
  );
}

String localeFormatDate(String interfaceLanguage, {bool isCurrentYear}) {
  switch (interfaceLanguage) {
    case LanguageConstants.LANGUAGE_CODE_ES:
    case LanguageConstants.LANGUAGE_CODE_FR:
    case LanguageConstants.LANGUAGE_CODE_PT:
      return isCurrentYear
          ? LANGUAGE_ES_DEFAULT_DATE_FORMAT
          : LANGUAGE_ES_DEFAULT_DATE_COMPLETE_FORMAT;
      break;
    case LanguageConstants.LANGUAGE_CODE_EN:
    default:
      return isCurrentYear
          ? LANGUAGE_EN_DEFAULT_DATE_FORMAT
          : LANGUAGE_EN_DEFAULT_DATE_COMPLETE_FORMAT;
      break;
  }
}

int getCurrentTimeMillis() => DateTime.now().millisecondsSinceEpoch;

DateTime getCurrentUTCDateTime() {
  DateTime now = DateTime.now();
  return DateTime.utc(
    now.year,
    now.month,
    now.day,
    now.hour,
    now.minute,
    now.second,
  );
}

bool isDateTimeToday(DateTime date) {
  DateTime today = DateTime.now();
  return today.year == date.year &&
      today.month == date.month &&
      today.day == date.day;
}

bool isDateTimeYesterday(DateTime date) {
  DateTime yesterday = DateTime.now().subtract(Duration(days: 1));
  return yesterday.year == date.year &&
      yesterday.month == date.month &&
      yesterday.day == date.day;
}

String getDefaultFormattedTimeFromDate(DateTime date) {
  DateFormat outputTimeFormat = DateFormat(DEFAULT_TIME_FORMAT);
  return outputTimeFormat.format(date);
}

String getDefaultFormattedDate(DateTime date) {
  DateFormat outputDateFormat = DateFormat(DEFAULT_DATE_FORMAT);
  return outputDateFormat.format(date);
}

String getDefaultFormattedDayFromDate(DateTime date) {
  DateFormat outputDateFormat = DateFormat(DEFAULT_DATE_DAY_FORMAT);
  return outputDateFormat.format(date);
}

String getDefaultFormattedMonthFromDate(DateTime date) {
  DateFormat outputDateFormat = DateFormat(DEFAULT_DATE_MONTH_FORMAT);
  return outputDateFormat.format(date);
}

String getDefaultFormattedYearFromDate(DateTime date) {
  DateFormat outputDateFormat = DateFormat(DEFAULT_DATE_YEAR_FORMAT);
  return outputDateFormat.format(date);
}
