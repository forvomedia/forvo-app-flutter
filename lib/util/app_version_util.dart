import 'package:flutter/material.dart';
import 'package:forvo/constants/route_constants.dart';
import 'package:forvo/model/version.dart';
import 'package:forvo/service/api/version_api_service.dart';
import 'package:forvo/ui/config/globals.dart';
import 'package:forvo/ui/min_version/min_version.dart';
import 'package:forvo/util/app_util.dart';

const int VERSION_NUMBER_NORMALIZED_LENGTH = 6;

checkVersionValidity(BuildContext context) async {
  Version apiVersion = await getVersion(context);
  String _currentAppVersion = await getAppVersion();
  if (_appNeedsUpgrading(_currentAppVersion, apiVersion?.minVersion)) {
    appGlobals.navigatorKey.currentState.pushNamedAndRemoveUntil(
      RouteConstants.MIN_VERSION,
          (route) => false,
      arguments: MinVersionArguments(version: apiVersion),
    );
  }
}

/// Check if current app version is equal or greater
/// than the version required by the API.
bool _appNeedsUpgrading(String currentVersionName, String requiredVersionName) {
  if (currentVersionName == null || requiredVersionName == null) {
    return true;
  }

  String normalizedCurrent = _getComparableVersionName(currentVersionName);
  String normalizedRequired = _getComparableVersionName(requiredVersionName);

  return normalizedCurrent.compareTo(normalizedRequired) < 0;
}

/// Make version names alphabetically comparables.
String _getComparableVersionName(versionName) =>
    _padVersionNumbers(_standardizeVersionFormat(versionName));

/// Add zeros to the left of each version number.
String _padVersionNumbers(String versionName) {
  List<String> currentVersionParts = versionName.split('.');
  for (int i = 0; i < currentVersionParts.length; i++) {
    currentVersionParts[i] =
        currentVersionParts[i].padLeft(VERSION_NUMBER_NORMALIZED_LENGTH, '0');
  }

  return currentVersionParts.join('.');
}

/// Ensure that a version name has major, mid & minor version numbers.
///
/// A version name like 1.2 will be converted to 1.2.0
String _standardizeVersionFormat(String versionName) {
  List<String> currentVersionParts = versionName.split('.');
  for (int i = 0; i < 3; i++) {
    if (currentVersionParts.length <= i) {
      currentVersionParts.add('0');
    }
  }

  return currentVersionParts.join('.');
}
