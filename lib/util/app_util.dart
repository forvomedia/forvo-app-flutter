import 'dart:async';
import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/util/device_info_util.dart';
import 'package:package_info/package_info.dart';

Future<String> getAppVersion() async {
  var packageInfo = await PackageInfo.fromPlatform();
  var appVersion = packageInfo.version;
  String _appVersion = '';
  if (appVersion != null) {
    return appVersion.toString();
  }
  return _appVersion;
}

String getAppApiKey() {
  String appKey =
      deviceIsAndroid() ? ApiConstants.KEY_ANDROID : ApiConstants.KEY_IOS;
  return appKey;
}

String getAddWordRecommendationsUrl(String languageCode) {
  String url = 'https://CODEforvo.com/recommendations/';
  if (languageCode == LanguageConstants.LANGUAGE_CODE_EN) {
    url = url.replaceAll('CODE', '');
  } else {
    url = url.replaceAll('CODE', '$languageCode.');
  }
  return url;
}
