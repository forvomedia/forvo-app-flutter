import 'dart:async';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:forvo/constants/device_constants.dart';

bool deviceIsAndroid() => Platform.isAndroid;

String getDeviceOSName() => deviceIsAndroid()
    ? DeviceConstants.DEVICE_OS_ANDROID
    : DeviceConstants.DEVICE_OS_IOS;

Future<String> getDeviceName() async {
  String _deviceName = '';
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (deviceIsAndroid()) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    _deviceName = '${androidInfo.brand} - ${androidInfo.model}';
  } else {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    if (iosInfo.isPhysicalDevice) {
      _deviceName = iosInfo.utsname.machine;
    } else {
      _deviceName = 'Simulator (${iosInfo.name})';
    }
  }
  return _deviceName;
}

Future<String> getDeviceOSVersion() async {
  String _osVersion = '';
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (deviceIsAndroid()) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    if (androidInfo.version.release != null) {
      _osVersion = androidInfo.version.release;
    }
    if (_osVersion != '' && androidInfo.version.sdkInt != null) {
      _osVersion = '$_osVersion(${androidInfo.version.sdkInt})';
    }
  } else {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    if (iosInfo.systemVersion != null) {
      _osVersion = iosInfo.systemVersion;
    }
  }
  return _osVersion;
}

Future<String> getDeviceId() async {
  String _deviceId = '';
  var udid = await FlutterUdid.udid;
  if (udid != null) {
    _deviceId = udid;
  }
  return _deviceId;
}
