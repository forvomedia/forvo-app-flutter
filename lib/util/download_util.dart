import 'dart:io';

Future<bool> downloadFile(String url, String filePath) async {
  HttpClient client = HttpClient();
  Pattern pattern = 'http';
  if (url.contains(pattern)) {
    try {
      await client
          .getUrl(Uri.parse(url))
          .then((HttpClientRequest request) => request.close())
          .then((HttpClientResponse response) {
        response.pipe(File('$filePath').openWrite());
      });
    } on Exception catch (e) {
      print('DOWNLOAD FILE ERROR => ${e.toString()}');
      return false;
    }
  }
  return true;
}
