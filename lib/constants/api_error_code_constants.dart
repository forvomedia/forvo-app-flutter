class ApiErrorCodeConstants {
  // No active session with this device
  static const int ERROR_1000 = 1000;

  // Parameter (.+) is required.
  static const int ERROR_1001 = 1001;

  // Invalid value in (.+): (.+).
  static const int ERROR_1002 = 1002;

  // Answer not valid
  static const int ERROR_1003 = 1003;

  // Social network id required
  static const int ERROR_1004 = 1004;

  // Favorite required
  static const int ERROR_1005 = 1005;

  // Cannot create a profile for the device
  static const int ERROR_1006 = 1006;

  // Longitude required
  static const int ERROR_1007 = 1007;

  // Password is too short
  static const int ERROR_1008 = 1008;

  // Language not valid
  static const int ERROR_1009 = 1009;

  // Login and password required
  static const int ERROR_1010 = 1010;

  // User add failed
  static const int ERROR_1011 = 1011;

  // Pronunciation not valid
  static const int ERROR_1012 = 1012;

  // Password is too long
  static const int ERROR_1013 = 1013;

  // (.+) is already in Forvo in this language
  static const int ERROR_1014 = 1014;

  // Username is too long
  static const int ERROR_1015 = 1015;

  // Sex not valid
  static const int ERROR_1016 = 1016;

  // Username is too short
  static const int ERROR_1017 = 1017;

  // User to send message required
  static const int ERROR_1018 = 1018;

  // User has previously associated this device
  static const int ERROR_1019 = 1019;

  // Vote required
  static const int ERROR_1020 = 1020;

  // Search required
  static const int ERROR_1021 = 1021;

  // Vote not valid (Values: good/report)
  static const int ERROR_1022 = 1022;

  // Vote is not allowed
  static const int ERROR_1023 = 1023;

  // Problem not valid
  static const int ERROR_1024 = 1024;

  // Languages are not valid
  static const int ERROR_1025 = 1025;

  // Missing parameters
  static const int ERROR_1026 = 1026;

  // User account not available
  static const int ERROR_1027 = 1027;

  // Password required
  static const int ERROR_1028 = 1028;

  // User cannot vote a pronunciation not related with his speak language
  static const int ERROR_1029 = 1029;

  // Username not valid
  static const int ERROR_1030 = 1030;

  // Audio name required
  static const int ERROR_1031 = 1031;

  // Passwords do not match
  static const int ERROR_1032 = 1032;

  // Pronunciation required
  static const int ERROR_1033 = 1033;

  // Favorite pronunciation not allowed
  static const int ERROR_1034 = 1034;

  // Latitude required
  static const int ERROR_1035 = 1035;

  // Languages required
  static const int ERROR_1036 = 1036;

  // Username not available
  static const int ERROR_1037 = 1037;

  // Course not valid
  static const int ERROR_1038 = 1038;

  // Email bad format
  static const int ERROR_1039 = 1039;

  // Word not valid
  static const int ERROR_1040 = 1040;

  // Word could not be reported
  static const int ERROR_1041 = 1041;

  // Empty message
  static const int ERROR_1042 = 1042;

  // Social network user info is empty
  static const int ERROR_1043 = 1043;

  // Pronunciation is not valid
  static const int ERROR_1044 = 1044;

  // Course required
  static const int ERROR_1045 = 1045;

  // Social network not valid
  static const int ERROR_1046 = 1046;

  // IP required"
  static const int ERROR_1047 = 1047;

  // Sex required
  static const int ERROR_1048 = 1048;

  // Lesson not valid
  static const int ERROR_1049 = 1049;

  // Problem required
  static const int ERROR_1050 = 1050;

  // Default language not valid
  static const int ERROR_1051 = 1051;

  // DeviceId required
  static const int ERROR_1052 = 1052;

  // Sorry, your account is not active.
  static const int ERROR_1053 = 1053;

  // Word required
  static const int ERROR_1054 = 1054;

  // Sorry, (.+) doesn´t accept messages
  static const int ERROR_1055 = 1055;

  // User required
  static const int ERROR_1056 = 1056;

  // Language required
  static const int ERROR_1057 = 1057;

  // Favorite is not valid (Values: yes/no)
  static const int ERROR_1058 = 1058;

  // This device has another session started. Please try again.
  static const int ERROR_1059 = 1059;

  // Conversation required
  static const int ERROR_1060 = 1060;

  // Lesson session expired
  static const int ERROR_1061 = 1061;

  // Audio file not valid
  static const int ERROR_1062 = 1062;

  // Social network required
  static const int ERROR_1063 = 1063;

  // Max\. length exceeded (.+) characters/tag max.)
  static const int ERROR_1064 = 1064;

  // Invalid application key
  static const int ERROR_1065 = 1065;

  // Answer required
  static const int ERROR_1066 = 1066;

  // Message could not be saved
  static const int ERROR_1067 = 1067;

  // User to send message not valid
  static const int ERROR_1068 = 1068;

  // Word: invalid chars removed. Check it!
  static const int ERROR_1069 = 1069;

  // Lesson required
  static const int ERROR_1070 = 1070;

  // Username required
  static const int ERROR_1071 = 1071;

  // Access token required
  static const int ERROR_1072 = 1072;

  // Country not valid
  static const int ERROR_1073 = 1073;

  // Max. length exceeded (.+) characters max.
  static const int ERROR_1074 = 1074;

  //Another lesson in progress
  static const int ERROR_1075 = 1075;

  // Tags: invalid chars removed. Check it!
  static const int ERROR_1076 = 1076;

  // Email required
  static const int ERROR_1077 = 1077;

  // Login incorrect
  static const int ERROR_1078 = 1078;

  // (.+) is not allowed in Forvo.
  static const int ERROR_1079 = 1079;

  // User cannot vote his own pronunciation
  static const int ERROR_1080 = 1080;

  // Email not available
  static const int ERROR_1081 = 1081;

  // Message required
  static const int ERROR_1082 = 1082;

  // Missing all parameters
  static const int ERROR_1083 = 1083;

  // User not valid"
  static const int ERROR_1084 = 1084;

  // User is inactive
  static const int ERROR_1085 = 1085;

  // Username has invalid characters (only a-z, 0-9)
  static const int ERROR_1086 = 1086;

  // User not found
  static const int ERROR_1087 = 1087;

  // Interface language not valid
  static const int ERROR_1088 = 1088;

  // Calling from incorrect domain.
  static const int ERROR_1089 = 1089;

  // Invalid key.
  static const int ERROR_1090 = 1090;

  // Account disabled.
  static const int ERROR_1091 = 1091;

  // Account expired.
  static const int ERROR_1092 = 1092;

  // Limit/day reached
  static const int ERROR_1093 = 1093;

  // Person name required
  static const int ERROR_1094 = 1094;

  // Is phrase required
  static const int ERROR_1095 = 1095;
}
