class ApiConstants {
  static const String KEY_ANDROID = 'd6a0d68b18fbcf26bcbb66ec20739492';
  static const String KEY_IOS = 'd040fd90ec0f39197d1ffd0d3030690b';
  static const String API_VERSION_1_2 = 'v1.2';
  static const String API_VERSION_2_0 = 'v2.0';
  static const String API_VERSION_2_1 = 'v2.1';
  static const String API_VERSION_2_2 = 'v2.2';
  static const String CURRENT_API_VERSION = API_VERSION_2_2;
  static const String APP_MIN_VERSION_API_HEADER_NAME = 'forvo-min-version';
  static const String APP_CURRENT_VERSION_API_HEADER_NAME = 'forvo-version';
  static const String APP_MIN_VERSION_FORCE_LOGOUT_API_HEADER_NAME =
      'forvo-min-version-force-logout';
  static const String APP_REQUIRED_LOGIN_DATETIME_API_HEADER_NAME =
      'forvo-required-login-datetime';
  static const String FORVO_MAINTENANCE_READ_ONLY =
      'forvo-maintenance-read-only';
  static const String FORVO_MAINTENANCE_ADD_WORD = 'forvo-maintenance-add-word';
  static const String FORVO_MAINTENANCE_PRONOUNCE =
      'forvo-maintenance-pronounce';
  static const String FORVO_MAINTENANCE_LOGIN = 'forvo-maintenance-login';
  static const String FORVO_MAINTENANCE_SIGNUP = 'forvo-maintenance-signup';
  static const String FORVO_MAINTENANCE_MESSAGES = 'forvo-maintenance-messages';

  static const String STATUS_OK = 'ok';
  static const String STATUS_ERROR = 'error';

  static const int DEFAULT_TIMEOUT_SECONDS = 30;
  static const int DEFAULT_TIMEOUT_MILLISECONDS = 30000;
  static const Duration DEFAULT_TIMEOUT =
      Duration(seconds: DEFAULT_TIMEOUT_SECONDS);
}
