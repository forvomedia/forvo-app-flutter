class DeviceConstants {
  static const String DEVICE_OS_ANDROID = 'android';
  static const String DEVICE_OS_IOS = 'ios';

  static const String DEVICE_RECORDS_FOLDER_NAME = 'records';
}