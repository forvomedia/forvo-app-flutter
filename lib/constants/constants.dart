const String ENVIRONMENT_LOCAL = 'local';
const String ENVIRONMENT_DEV = 'dev';
const String ENVIRONMENT_PROD = 'prod';

const String DEV_AUTHENTICATION_USERNAME = 'forvo';
const String DEV_AUTHENTICATION_PASSWORD = '20htp4sswd17';

const String DEFAULT_LANG = 'en';

const String DEFAULT_DATETIME_INPUT_FORMAT = 'yyyy-MM-dd HH:mm:ss';
const String DEFAULT_DATETIME_UTC_INPUT_FORMAT = 'yyyy-MM-dd HH:mm:ss UTC';
const String DEFAULT_DATE_FORMAT = 'yyyy-MM-dd';
const String DEFAULT_TIME_FORMAT = 'HH:mm';
const String DEFAULT_DATE_DAY_FORMAT = 'dd';
const String DEFAULT_DATE_MONTH_FORMAT = 'MM';
const String DEFAULT_DATE_YEAR_FORMAT = 'yyyy';

const String NOT_AVAILABLE_DATA = 'n/a';

const String GUEST_USER_NAME = 'forvo_GUEST_user';

const String PRONUNCIATION_FAVORITE_ACTION_YES = 'yes';
const String PRONUNCIATION_FAVORITE_ACTION_NO = 'no';

const String PRONUNCIATION_VOTE_ACTION_POSITIVE = 'good';
const String PRONUNCIATION_VOTE_ACTION_NEGATIVE = 'report';

const int PRONUNCIATION_VOTE_VALUE_POSITIVE = 1;
const int PRONUNCIATION_VOTE_VALUE_NEUTRAL = 0;
const int PRONUNCIATION_VOTE_VALUE_NEGATIVE = -1;

const String PRONUNCIATION_FOLLOWING_USER_ACTION_YES = 'yes';
const String PRONUNCIATION_FOLLOWING_USER_ACTION_NO = 'no';

const int MAX_WORD_CHARS = 40;
const int MAX_PHRASE_CHARS = 255;

const int AUDIO_MIN_SIZE = 5000;

const int MIN_PASSWORD_LIMIT = 4;
const int MAX_PASSWORD_LIMIT = 255;

const String TYPE_INITIAL = 'initial';
const String TYPE_CONTRIBUTOR = 'contributor';

const int ROL_STUDENT = 0;
const int ROL_TEACHER = 1;
const int ROL_NONE = 2;

const String LOCATION_SELECTOR_MESSAGE_ACCOUNT = 'account';
const double LOCATION_DEFAULT_MAP_LATITUDE = 24.1969;
const double LOCATION_DEFAULT_MAP_LONGITUDE = -43.8089;

const String LANGUAGE_EN_DEFAULT_DATE_FORMAT = 'MMM d';
const String LANGUAGE_EN_DEFAULT_DATE_COMPLETE_FORMAT = 'y MMM d';
const String LANGUAGE_ES_DEFAULT_DATE_FORMAT = 'd MMM';
const String LANGUAGE_ES_DEFAULT_DATE_COMPLETE_FORMAT = 'd MMM y';

const int APP_VERSION_CHECK_TIMER_MINUTES = 10;

const String SNACKBAR_TYPE_ERROR = 'error';
const String SNACKBAR_TYPE_OK = 'ok';
const String SNACKBAR_TYPE_STANDARD = 'standard';
const int SNACKBAR_DURATION_IN_SECONDS = 6;

const String TERMS_OF_USE_URL = 'https://forvo.com/terms-and-conditions/';
const String PRIVACY_POLICY_URL = 'https://forvo.com/privacy/';

const String DEFAULT_TODAY_DATE_NAME = 'today';
const String DEFAULT_YESTERDAY_DATE_NAME = 'yesterday';

const String BLOCK_USER_MESSAGES_ACTION_YES = 'yes';
const String BLOCK_USER_MESSAGES_ACTION_NO = 'no';

const String CHAT_NOT_DEFINED_CONVERSATION = '0';

const int APP_REVIEW_CHECK_STEPS_LIMIT = 5;
const String ANDROID_APP_ID = 'com.forvo.pronunciationguide.app';
const String IOS_APP_ID = '375819093';
const String ANDROID_APP_STORE_URL = 'https://play.google.com/store/apps/details?id=com.forvo.pronunciationguide.app';
const String IOS_APP_STORE_URL = 'https://itunes.apple.com/app/id375819093?mt=8';

const int CUSTOM_TOOLTIP_SHOW_DURATION_SECONDS = 2;
const int CUSTOM_TOOLTIP_WAIT_DURATION_SECONDS = 1;

const String THEMED_BUBBLE_ICON_TYPE_WORD = 'word';
const String THEMED_BUBBLE_ICON_TYPE_PRONUNCIATION = 'pronunciation';

const int DATE_PICKER_YEARS_13 = 13;
const int DATE_PICKER_INITIAL_YEAR = 1900;

const int PREMIUM_KEY_LENGTH = 32;

