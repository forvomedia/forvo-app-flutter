class PushNotificationConstants {
  static const bool STATE_SUBSCRIBED = true;
  static const bool STATE_UNSUBSCRIBED = false;

  static const String TYPE_PRONOUNCE = 'pronunciation';
  static const String TYPE_VOTE = 'vote';
  static const String TYPE_MESSAGE = 'message';

  static const String CHANNEL_ID = 'high_importance_channel';
  static const String CHANNEL_NAME = 'Forvo Notifications Channel';
  static const String CHANNEL_DESCRIPTION =
      'This channel is used for Forvo notifications.';
}
