class LayoutConstants {
  static const double LAYOUT_TABLET_BREAKPOINT = 600.0;
  static const double LAYOUT_SMALL_MOBILE_WIDTH_BREAKPOINT = 360.0;
  static const double LAYOUT_HEIGHT_IPHONE_X = 812;
  static const double LAYOUT_HEIGHT_IPHONE_XR = 896;
  static const double LAYOUT_HEIGHT_BREAKPOINT = 600;
}