class SearchConstants {
  static const int SEARCH_MODE_PRONUNCIATION = 0;
  static const int SEARCH_MODE_TRANSLATION = 1;

  static const String SEARCH_HISTORY_WORD_SEPARATOR = '@#@';
  static const int SEARCH_HISTORY_LIMIT = 10;
}
