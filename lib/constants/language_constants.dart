class LanguageConstants {
  static const String LANGUAGE_CODE_EN = 'en';
  static const String LANGUAGE_CODE_ES = 'es';
  static const String LANGUAGE_CODE_FR = 'fr';
  static const String LANGUAGE_CODE_PT = 'pt';

  static const String COUNTRY_CODE_ES = 'ES';
  static const String COUNTRY_CODE_FR = 'FR';
  static const String COUNTRY_CODE_PT = 'PT';
  static const String COUNTRY_CODE_US = 'US';

  static const List<String> SUPPORTED_LANGUAGE_CODES = [
    LanguageConstants.LANGUAGE_CODE_EN,
    LanguageConstants.LANGUAGE_CODE_ES,
    LanguageConstants.LANGUAGE_CODE_FR,
    // LanguageConstants.LANGUAGE_CODE_PT,
  ];

  static const int LANGUAGE_POPULAR_LIMIT = 10;

  static const int LANGUAGE_ACCENT_COUNT = 3;

  static const String CHECK_ALPHABET_STATUS_MAYBE_WORD = 'maybe_word';
  static const String CHECK_ALPHABET_STATUS_FORBIDDEN = 'word_forbidden';
  static const String CHECK_ALPHABET_STATUS_INVALID_CHARS = 'invalid_chars';
  static const String CHECK_ALPHABET_STATUS_SPECIAL_CHARS = 'special_chars';
  static const String CHECK_ALPHABET_STATUS_NOT_AVAILABLE = 'not_available';
  static const String CHECK_ALPHABET_STATUS_CHECK_IS_PERSON_NAME =
      'check_is_person_name';
  static const String CHECK_ALPHABET_STATUS_CHECK_IS_PHRASE = 'check_is_phrase';
}
