import 'package:flutter/material.dart';
import 'package:forvo/ui/account/account_info.dart';
import 'package:forvo/ui/account/public_profile.dart';
import 'package:forvo/ui/dashboard/home.dart';
import 'package:forvo/ui/dashboard/widgets/header/add_word/add_word.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_detail.dart';
import 'package:forvo/ui/dashboard/widgets/header/chat/chat_conversation_list.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/pronounce.dart';
import 'package:forvo/ui/dashboard/widgets/header/pronounce/record.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/about.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/about/delete_account.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/followed_user_detail.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/followed_users.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/my_own_pronunciations.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/my_requested_pronunciations.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/notifications_privacy.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/settings.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/settings/interface_language.dart';
import 'package:forvo/ui/dashboard/widgets/my_account/settings/theme_selector.dart';
import 'package:forvo/ui/min_version/min_version.dart';
import 'package:forvo/ui/registration/login.dart';
import 'package:forvo/ui/registration/signup.dart';
import 'package:forvo/ui/report/report_pronunciation.dart';
import 'package:forvo/ui/tutorial/tutorial_home.dart';
import 'package:forvo/ui/tutorial/tutorial_steps.dart';
import 'package:forvo/ui/word/word_detail.dart';

class RouteConstants {
  static const String ABOUT = '/about';
  static const String ACCOUNT_INFO = '/account-info';
  static const String ADD_WORD = '/add-word';
  static const String CHAT_DETAIL = '/chat-detail';
  static const String CHAT_LIST = '/chat-list';
  static const String DELETE_ACCOUNT = '/delete-account';
  static const String FOLLOWED_USERS = '/followed-users';
  static const String FOLLOWED_USER_DETAIL = '/followed-user-detail';
  static const String HOME = '/home';
  static const String INTERFACE_LANGUAGE = '/interface-language';
  static const String LOGIN = '/login';
  static const String MIN_VERSION = '/min-version';
  static const String MY_ACCOUNT = '/my-account';
  static const String MY_FAVORITE_PRONUNCIATIONS =
      '/my-favorite-pronunciations';
  static const String MY_OWN_PRONUNCIATIONS = '/my-own-pronunciations';
  static const String MY_REQUESTED_PRONUNCIATIONS =
      '/my-requested-pronunciations';
  static const String NOTIFICATIONS = '/notifications';
  static const String NOTIFICATIONS_AND_PRIVACY = '/notifications-and-privacy';
  static const String PRONOUNCE = '/pronounce';
  static const String PUBLIC_PROFILE = '/public-profile';
  static const String RECORD = '/record';
  static const String REPORT_PRONUNCIATION = '/report-pronunciation';
  static const String SETTINGS = '/settings';
  static const String SIGN_UP = '/sign-up';
  static const String START = '/start';
  static const String TUTORIAL_HOME = '/tutorial-home';
  static const String TUTORIAL_STEPS = '/tutorial-steps';
  static const String THEME_SELECTOR = '/theme-selector';
  static const String WORD = '/word';
}

Map<String, Widget Function(BuildContext)> getRoutes(BuildContext context) => {
      RouteConstants.ABOUT: (context) => About(),
      RouteConstants.ACCOUNT_INFO: (context) => AccountInfo(),
      RouteConstants.ADD_WORD: (context) => AddWord(),
      RouteConstants.CHAT_DETAIL: (context) => ChatConversationDetail(),
      RouteConstants.CHAT_LIST: (context) => ChatConversationList(),
      RouteConstants.DELETE_ACCOUNT: (context) => DeleteAccount(),
      RouteConstants.FOLLOWED_USERS: (context) => FollowedUsers(),
      RouteConstants.FOLLOWED_USER_DETAIL: (context) => FollowedUserDetail(),
      RouteConstants.HOME: (context) => Home(
            bottomNavBarIndex: HOME_SECTION_START,
          ),
      RouteConstants.INTERFACE_LANGUAGE: (context) => InterfaceLanguage(),
      RouteConstants.LOGIN: (context) => Login(),
      RouteConstants.MIN_VERSION: (context) => MinVersion(),
      RouteConstants.MY_ACCOUNT: (context) => Home(
            bottomNavBarIndex: HOME_SECTION_MY_ACCOUNT,
          ),
      RouteConstants.MY_FAVORITE_PRONUNCIATIONS: (context) => Home(
            bottomNavBarIndex: HOME_SECTION_FAVORITES,
          ),
      RouteConstants.MY_OWN_PRONUNCIATIONS: (context) => MyOwnPronunciations(),
      RouteConstants.MY_REQUESTED_PRONUNCIATIONS: (context) =>
          MyRequestedPronunciations(),
      RouteConstants.NOTIFICATIONS: (context) => Home(
            bottomNavBarIndex: HOME_SECTION_NOTIFICATIONS,
          ),
      RouteConstants.NOTIFICATIONS_AND_PRIVACY: (context) =>
          NotificationsAndPrivacy(),
      RouteConstants.PRONOUNCE: (context) => Pronounce(),
      RouteConstants.PUBLIC_PROFILE: (context) => PublicProfile(),
      RouteConstants.RECORD: (context) => Record(),
      RouteConstants.REPORT_PRONUNCIATION: (context) => ReportPronunciation(),
      RouteConstants.SETTINGS: (context) => Settings(),
      RouteConstants.SIGN_UP: (context) => SignUp(),
      RouteConstants.START: (context) => Home(),
      RouteConstants.THEME_SELECTOR: (context) => ThemeSelector(),
      RouteConstants.TUTORIAL_HOME: (context) => TutorialHome(),
      RouteConstants.TUTORIAL_STEPS: (context) => TutorialSteps(),
      RouteConstants.WORD: (context) => WordDetail(),
    };
