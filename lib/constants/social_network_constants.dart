class SocialNetworkConstants {
  static const String FACEBOOK = 'facebook';
  static const String GOOGLE = 'google';
  static const String APPLE = 'apple';

  static const String STATUS_OK = 'ok';

  static const String ACTION_LOGIN = 'login';
  static const String ACTION_SIGN_UP = 'signup';

  static const String REQUEST_OK = 'ok';
  static const String REQUEST_ERROR = 'error';
  static const String REQUEST_CANCELLED = 'cancelledByUser';

  static const String GOOGLE_SERVICES_ANDROID_APP_OAUTH_CLIENT_ID =
      // ignore: lines_longer_than_80_chars
      '284422506869-egm4gbqbad0aktjt6q200oj0kquhlp69.apps.googleusercontent.com';

  static const String GOOGLE_SERVICES_IOS_APP_OAUTH_CLIENT_ID =
      // ignore: lines_longer_than_80_chars
      '284422506869-70k1rogn01hcqjfnmkvdasipji8ljdlv.apps.googleusercontent.com';
}
