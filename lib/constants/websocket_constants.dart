class WebSocketConstants {
  static const String EVENT_AUTHENTICATE = 'authenticate';
  static const String EVENT_AUTHENTICATED = 'authenticated';
  static const String EVENT_GET_MESSAGES = 'get-messages';
  static const String EVENT_SEND_MESSAGE = 'send-message';
  static const String EVENT_RECEIVE_MESSAGES = 'receive-messages';
  static const String EVENT_RECEIVE_NEW_OR_ACCEPTED_MESSAGE =
      'receive-new-or-accepted-message';
  static const String EVENT_RECEIVE_NEW_OR_ACCEPTED_MESSAGE_DIRECTLY =
      'receive-new-or-accepted-message-directly';
  static const String EVENT_MARK_CONVERSATION_AS_READ =
      'mark-conversation-messages-as-read';
  static const String EVENT_RECEIVE_CONVERSATION_MESSAGES_AS_READ =
      'receive-conversation-messages-as-read';

  static const int LEAVING_REASON_OTHER = 0;
  static const int LEAVING_REASON_USER_LEFT = 1;

  static const int RECONNECTION_SECONDS = 3;
  static const int RECONNECTION_ATTEMPTS_LIMIT = 5;
}
