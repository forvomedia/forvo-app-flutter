const String APPSFLYER_DEV_KEY = 'eX4EZrHQYkazaJoTF2MEWV';

//Time to wait for app tracking transparency user authorization on iOS 14.5+
const double APPSFLYER_TIME_TO_WAIT_FOR_ATT_USER_AUTHORIZATION = 150;

const String APPSFLYER_AF_REGISTRATION_METHOD = 'af_registration_method';
const String APPSFLYER_REGISTRATION_METHOD_DEFAULT = 'email, Default';
const String APPSFLYER_REGISTRATION_METHOD_FACEBOOK = 'email, Facebook';
const String APPSFLYER_REGISTRATION_METHOD_GOOGLE = 'email, Google';
const String APPSFLYER_REGISTRATION_METHOD_APPLE = 'email, Apple';

const String APPSFLYER_AF_COMPLETE_REGISTRATION = 'af_complete_registration';
const String APPSFLYER_AF_SEARCH = 'af_search';
const String APPSFLYER_AF_SEARCH_STRING = 'af_search_string';

const String APPSFLYER_AF_SUCCESS = 'af_success';
const String APPSFLYER_YES = 'yes';
const String APPSFLYER_NO = 'no';

const String APPSFLYER_AF_TUTORIAL_COMPLETION = 'af_tutorial_completion';
const String APPSFLYER_AF_TUTORIAL_ID = 'af_tutorial_id';
const int APPSFLYER_TUTORIAL_ID = 1;

const String APPSFLYER_AF_CONTENT = 'af_content';
const String APPSFLYER_CONTENT = 'tutorial inicial';

const String APPSFLYER_AF_UPDATE = 'af_update';

const String APPSFLYER_AF_LOGIN = 'af_login';
const String APPSFLYER_AF_CHANNEL = 'af_channel';
const String APPSFLYER_AF_LOGIN_DEFAULT = 'default';