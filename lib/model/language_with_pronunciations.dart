import 'package:forvo/model/language_accent_with_pronunciations.dart';
import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/pending_pronounce_information.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/util/json.dart';

class LanguageWithPronunciations {
  final int totalItems;
  final int pages;
  final String languageName;
  final List<Pronunciation> items;
  final List<LanguageAccentWithPronunciations> accents;
  final LanguageWordInformation wordInformation;
  final PendingPronounceInformation pendingPronounceInformation;

  LanguageWithPronunciations({
    this.totalItems,
    this.pages,
    this.languageName,
    this.items,
    this.accents,
    this.wordInformation,
    this.pendingPronounceInformation,
  });

  bool get hasItems => items?.isNotEmpty ?? false;

  bool get hasAccents => accents?.isNotEmpty ?? false;

  bool get hasCategorizationGroups =>
      wordInformation != null &&
      wordInformation.hasCategorizationData &&
      wordInformation.categorizationGroups.isNotEmpty;

  factory LanguageWithPronunciations.fromJson(Map<String, dynamic> json) {
    List _items = json['items'];
    List _accents = json['accents'];
    return LanguageWithPronunciations(
      totalItems: jsonIntField(json['subtotal']),
      pages:
          jsonIntField(json['pages']) != null ? jsonIntField(json['pages']) : 0,
      languageName: jsonStringField(json['language']),
      items: Pronunciation.pronunciationListFromJson(_items),
      accents: LanguageAccentWithPronunciations
          .languageAccentWithPronunciationsListFromJson(_accents),
      wordInformation: json['wordInfo'] != null
          ? LanguageWordInformation.fromJson(json['wordInfo'])
          : null,
      pendingPronounceInformation: json['pending_pronounce_info'] != null
          ? PendingPronounceInformation.fromJson(json['pending_pronounce_info'])
          : null,
    );
  }

  static List<LanguageWithPronunciations>
      languageWithPronunciationsListFromJson(List<dynamic> list) =>
          list != null && list.isNotEmpty
              ? list.map((item) {
                  Map<String, dynamic> json = item;
                  return LanguageWithPronunciations.fromJson(json);
                }).toList()
              : [];
}
