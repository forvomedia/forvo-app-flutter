import 'package:forvo/util/json.dart';

class ChatMessage {
  final int id;
  final String userFrom;
  final String userTo;
  final String message;
  final String addtime;
  final bool isRead;
  final String date;
  final String hour;
  final int addtimeOrder;
  final String idLocal;
  final int idConversation;

  ChatMessage({
    this.id,
    this.userFrom,
    this.userTo,
    this.message,
    this.addtime,
    this.isRead,
    this.date,
    this.hour,
    this.addtimeOrder,
    this.idLocal,
    this.idConversation,
  });

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
        id: jsonIntField(json['id']),
        userFrom: jsonStringField(json['user_from'], nullValue: ''),
        userTo: jsonStringField(json['user_to'], nullValue: ''),
        message: jsonStringField(json['message'], nullValue: ''),
        addtime: jsonStringField(json['addtime'], nullValue: ''),
        isRead: jsonStringField(json['is_read'], nullValue: '0') == '1',
        date: jsonStringField(json['date'], nullValue: ''),
        hour: jsonStringField(json['hour'], nullValue: ''),
        addtimeOrder: jsonIntField(json['addtimeorder']),
        idLocal: jsonStringField(json['idLocal']),
        idConversation: json['idConversation'] != null
            ? jsonIntField(json['idConversation'])
            : null,
      );

  static List<ChatMessage> chatMessageListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return ChatMessage.fromJson(json);
            }).toList()
          : [];

  @override
  String toString() => 'ChatMessage { '
      'id: $id, '
      'userFrom: $userFrom, '
      'userTo: $userTo, '
      'message: $message, '
      'addtime: $addtime, '
      'isRead: $isRead, '
      'date: $date, '
      'hour: $hour, '
      'addtimeOrder: $addtimeOrder, '
      'idLocal: $idLocal, '
      'idConversation: $idConversation, '
      '}';
}
