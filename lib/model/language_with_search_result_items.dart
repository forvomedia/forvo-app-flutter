import 'package:forvo/model/language_accent_with_search_result_items.dart';
import 'package:forvo/model/search_result_items.dart';
import 'package:forvo/util/json.dart';

class LanguageWithSearchResultItems {
  final int totalItems;
  final String languageName;
  final SearchResultItems items;
  final List<LanguageAccentWithSearchResultItems> accents;

  LanguageWithSearchResultItems({
    this.totalItems,
    this.languageName,
    this.items,
    this.accents,
  });

  factory LanguageWithSearchResultItems.fromJson(Map<String, dynamic> json) {
    var _items = json['items'];
    List _accents = json['accents'];
    return LanguageWithSearchResultItems(
      totalItems: jsonIntField(json['subtotal']),
      languageName: jsonStringField(json['language']),
      items: _items != null ? SearchResultItems.fromJson(_items) : null,
      accents: _accents != null
          ? LanguageAccentWithSearchResultItems
              .languageAccentWithSearchResultItemsListFromJson(_accents)
          : [],
    );
  }

  static List<LanguageWithSearchResultItems>
      languageWithSearchResultItemsListFromJson(List<dynamic> list) =>
          list != null && list.isNotEmpty
              ? list.map((item) {
                  Map<String, dynamic> json = item;
                  return LanguageWithSearchResultItems.fromJson(json);
                }).toList()
              : [];
}
