import 'package:forvo/util/json.dart';

import 'language_with_followed_users.dart';

class FollowedUsersData {
  final String status;
  final List<LanguageWithFollowedUsers> data;

  FollowedUsersData({
    this.status,
    this.data,
  });

  factory FollowedUsersData.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return FollowedUsersData(
          status: jsonStringField(json['status']),
          data: LanguageWithFollowedUsers.languageWithFollowedUsersListFromJson(
              _data),
        );
  }
}
