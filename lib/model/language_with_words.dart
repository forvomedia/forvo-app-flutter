import 'package:forvo/model/word.dart';
import 'package:forvo/util/json.dart';

class LanguageWithWords {
  final int totalItems;
  final String languageName;
  final List<Word> items;

  LanguageWithWords({
    this.totalItems,
    this.languageName,
    this.items,
  });

  factory LanguageWithWords.fromJson(Map<String, dynamic> json) {
    List _items = json['items'];
    return LanguageWithWords(
      totalItems: jsonIntField(json['subtotal']),
      languageName: jsonStringField(json['language']),
      items: Word.wordListFromJson(_items),
    );
  }

  static List<LanguageWithWords> languageWithWordsListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return LanguageWithWords.fromJson(json);
            }).toList()
          : [];
}
