import 'package:forvo/model/social_network_user_info.dart';
import 'package:forvo/util/json.dart';

class SocialNetworkCheckStatus {
  final String status;
  final String exists;
  final String action;
  final SocialNetworkUserInfo userInfo;

  SocialNetworkCheckStatus({
    this.status,
    this.exists,
    this.action,
    this.userInfo,
  });

  factory SocialNetworkCheckStatus.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> _userInfo = json['userInfo'];
    return SocialNetworkCheckStatus(
      status: jsonStringField(json['status'] ??= ''),
      exists: jsonStringField(json['exists'] ??= ''),
      action: jsonStringField(json['action'] ??= ''),
      userInfo: SocialNetworkUserInfo.fromJson(_userInfo),
    );
  }
}
