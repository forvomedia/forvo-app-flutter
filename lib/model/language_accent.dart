import 'package:forvo/util/json.dart';

class LanguageAccent {
  final String id;
  final String name;
  final String code;
  final String languageName;
  final String languageCode;

  LanguageAccent({
    this.id,
    this.name,
    this.code,
    this.languageName,
    this.languageCode,
  });

  factory LanguageAccent.fromJson(Map<String, dynamic> json) => LanguageAccent(
        id: jsonStringField(json['id'] ??= ''),
        name: jsonStringField(json['name'] ??= ''),
        code: jsonStringField(json['code'] ??= ''),
        languageName: jsonStringField(json['languageName'] ??= ''),
        languageCode: jsonStringField(json['languageCode'] ??= ''),
      );

  static List<LanguageAccent> languageAccentListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return LanguageAccent.fromJson(json);
            }).toList()
          : [];

  @override
  String toString() => '[LanguageAccent] '
      'id: $id, '
      'name: $name, '
      'code: $code, '
      'languageName: $languageName, '
      'languageCode: $languageCode';
}
