import 'package:forvo/util/json.dart';

class WordCategorySpelling {
  final String spelling;
  final String nativeCategoryName;
  final String nativeExplanation;
  final String englishCategoryName;
  final String englishExplanation;

  WordCategorySpelling({
    this.spelling,
    this.nativeCategoryName,
    this.nativeExplanation,
    this.englishCategoryName,
    this.englishExplanation,
  });

  factory WordCategorySpelling.fromJson(Map<String, dynamic> json) =>
      WordCategorySpelling(
        spelling: jsonStringField(json['spelling'], nullValue: ''),
        nativeCategoryName: jsonStringField(json['category'], nullValue: ''),
        nativeExplanation: jsonStringField(json['explanation'], nullValue: ''),
        englishCategoryName:
            jsonStringField(json['category_english'], nullValue: ''),
        englishExplanation:
            jsonStringField(json['explanation_english'], nullValue: ''),
      );

  static List<WordCategorySpelling> wordCategorySpellingListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return WordCategorySpelling.fromJson(json);
            }).toList()
          : [];
}
