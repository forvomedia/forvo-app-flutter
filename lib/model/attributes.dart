import 'package:forvo/util/json.dart';

class Attributes {
  final int totalItems;
  final int totalLanguages;
  final int pages;

  Attributes({
    this.totalItems,
    this.totalLanguages,
    this.pages,
  });

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        totalItems: jsonIntField(json['total']),
        totalLanguages: jsonIntField(json['total_languages']),
        pages: jsonIntField(json['pages']),
      );
}
