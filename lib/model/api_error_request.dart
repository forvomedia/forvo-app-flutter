import 'package:forvo/model/api_error.dart';
import 'package:forvo/util/json.dart';

class ApiErrorRequest {
  final String status;
  final String errorMessage;
  final int errorCode;
  final List<ApiError> errors;

  ApiErrorRequest({
    this.status,
    this.errorMessage,
    this.errorCode,
    this.errors,
  });

  factory ApiErrorRequest.fromJson(Map<String, dynamic> json) {
    List _errors = json['errors'];
    return json != null
        ? ApiErrorRequest(
            status: jsonStringField(json['status'] ??= ''),
            errorMessage: jsonStringField(json['errorMessage'] ??= ''),
            errorCode: jsonIntField(json['errorCode'] ??= ''),
            errors: ApiError.apiErrorListFromJson(_errors),
          )
        : null;
  }
}
