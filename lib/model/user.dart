import 'package:forvo/constants/constants.dart';
import 'package:forvo/util/json.dart';

class User {
  final String id;
  final String username;
  final String email;
  final String firstName;
  final String lastName;
  final String languageName;
  final String languageCode;
  final String latitude;
  final String longitude;
  final String genre;
  final String type;
  final String birthdateDay;
  final String birthdateMonth;
  final String birthdateYear;
  final String key;
  final String secret;
  String md5Password;

  User({
    this.id,
    this.username,
    this.email,
    this.firstName,
    this.lastName,
    this.languageName,
    this.languageCode,
    this.latitude,
    this.longitude,
    this.genre,
    this.type,
    this.birthdateDay,
    this.birthdateMonth,
    this.birthdateYear,
    this.key,
    this.secret,
    this.md5Password,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: jsonStringField(json['id'] ??= ''),
        username: jsonStringField(json['username'] ??= ''),
        email: jsonStringField(json['email'] ??= ''),
        firstName: jsonStringField(json['first_name'] ??= ''),
        lastName: jsonStringField(json['last_name'] ??= ''),
        languageName: jsonStringField(json['language'] ??= ''),
        languageCode: jsonStringField(json['code'] ??= ''),
        latitude: jsonStringField(json['latitude'] ??= ''),
        longitude: jsonStringField(json['longitude'] ??= ''),
        genre: jsonStringField(json['sex'] ??= ''),
        type: jsonStringField(json['type'] ??= ''),
        birthdateDay: jsonStringField(json['birthdate_day'] ??= ''),
        birthdateMonth: jsonStringField(json['birthdate_month'] ??= ''),
        birthdateYear: jsonStringField(json['birthdate_year'] ??= ''),
        key: jsonStringField(json['key'] ??= ''),
        secret: jsonStringField(json['secret'] ??= ''),
        md5Password: jsonStringField(json['password'] ??= ''),
      );

  // ignore: use_setters_to_change_properties
  void setMd5Password(String newMd5Password) => md5Password = newMd5Password;

  bool get isContributor => type == TYPE_CONTRIBUTOR;

  bool get isBirthdateNull =>
      birthdateDay == null ||
      birthdateDay.isEmpty ||
      birthdateMonth == null ||
      birthdateMonth.isEmpty ||
      birthdateYear == null ||
      birthdateYear.isEmpty;

  DateTime get birthdate => isBirthdateNull
      ? null
      : DateTime(
          int.parse(birthdateYear),
          int.parse(birthdateMonth),
          int.parse(birthdateDay),
        );
}
