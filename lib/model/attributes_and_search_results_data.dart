import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/language_with_search_result_items.dart';
import 'package:forvo/util/json.dart';

class AttributesAndSearchResultsData {
  final String status;
  final Attributes attributes;
  final List<LanguageWithSearchResultItems> data;

  AttributesAndSearchResultsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndSearchResultsData.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndSearchResultsData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: LanguageWithSearchResultItems
          .languageWithSearchResultItemsListFromJson(_data),
    );
  }
}
