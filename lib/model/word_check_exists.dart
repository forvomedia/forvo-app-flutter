import 'package:forvo/model/language.dart';
import 'package:forvo/util/json.dart';

class WordCheckExists {
  final bool exists;
  final bool isPersonName;
  final List<Language> languages;
  final List<Language> suggestedLanguages;
  final String invalidChars;
  final String correctedWord;
  final int maxCharsWord;
  final int maxCharsPhrase;
  final bool applyMaxCharsWord;

  WordCheckExists({
    this.exists,
    this.isPersonName,
    this.languages,
    this.suggestedLanguages,
    this.invalidChars,
    this.correctedWord,
    this.maxCharsWord,
    this.maxCharsPhrase,
    this.applyMaxCharsWord,
  });

  factory WordCheckExists.fromJson(Map<String, dynamic> json) {
    List _languages = json['langs'];
    List _suggestedLanguages = json['suggestedLangs'];
    return WordCheckExists(
      exists: jsonIntField(json['exists']) == 1,
      isPersonName: jsonIntField(json['isPersonName']) == 1,
      languages: Language.languageListFromJson(_languages),
      suggestedLanguages: Language.languageListFromJson(_suggestedLanguages),
      invalidChars: jsonStringField(json['invalidChars']),
      correctedWord: jsonStringField(json['correctedWord']),
      maxCharsWord: json['maxWordChars'] != null
          ? jsonIntField(json['maxWordChars'])
          : null,
      maxCharsPhrase: json['maxPhraseChars'] != null
          ? jsonIntField(json['maxPhraseChars'])
          : null,
      applyMaxCharsWord: jsonBoolField(json['applyMaxWordChars'] ?? false),
    );
  }

  @override
  String toString() => '[WordCheckExists] '
      'exists: ${exists.toString()}, '
      'isPersonName: ${isPersonName.toString()}, '
      'invalidChars: $invalidChars, '
      'correctedWord: $correctedWord, '
      'languages: ${languages.toString()}, '
      'suggestedLanguages: ${suggestedLanguages.toString()}, '
      'maxCharsWord : ${maxCharsWord.toString()}, '
      'maxCharsPhrase : ${maxCharsPhrase.toString()}, '
      'applyMaxCharsWord : ${applyMaxCharsWord.toString()}, ';
}
