import 'package:forvo/model/word_category.dart';
import 'package:forvo/util/json.dart';

class WordCategorizationGroup {
  final int groupIndex;
  final int numPronunciations;
  final List<WordCategory> categories;
  final List<WordCategory> minimumCategories;
  final bool showFullInformation;

  WordCategorizationGroup({
    this.groupIndex,
    this.numPronunciations,
    this.categories,
    this.minimumCategories,
    this.showFullInformation,
  });

  factory WordCategorizationGroup.fromJson(Map<String, dynamic> json) {
    List _categories = json['categories'];
    List _minimumCategories = json['minimumCategories'];
    return WordCategorizationGroup(
      groupIndex: jsonIntField(json['groupIndex']),
      numPronunciations:
          int.parse(jsonStringField(json['numPronunciations'], nullValue: '0')),
      categories: _categories != null
          ? WordCategory.wordCategoryListFromJson(_categories)
          : [],
      minimumCategories: _minimumCategories != null
          ? WordCategory.wordCategoryListFromJson(_minimumCategories)
          : [],
      showFullInformation: jsonBoolField(json['showFullInformation']),
    );
  }

  static List<WordCategorizationGroup> wordCategorizationGroupListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return WordCategorizationGroup.fromJson(json);
            }).toList()
          : [];
}
