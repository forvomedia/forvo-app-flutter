import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/word.dart';
import 'package:forvo/util/json.dart';

class AttributesAndTrendingWordsData {
  final String status;
  final Attributes attributes;
  final List<Word> data;

  AttributesAndTrendingWordsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndTrendingWordsData.fromJson(Map<String, dynamic> json) =>
      AttributesAndTrendingWordsData(
        status: jsonStringField(json['status']),
        attributes: Attributes.fromJson(json['attributes']),
        data: Word.wordListFromJson(json['data']),
      );
}
