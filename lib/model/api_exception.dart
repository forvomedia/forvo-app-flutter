import 'package:flutter/material.dart';
import 'package:forvo/model/api_error.dart';
import 'package:forvo/model/api_error_request.dart';

class ApiException implements Exception {
  ApiErrorRequest apiErrorRequest;

  ApiException(Map<String, dynamic> json) {
    apiErrorRequest = ApiErrorRequest.fromJson(json);
  }

  List<ApiError> getErrors() => apiErrorRequest.errors;

  String getErrorMessages(BuildContext context) {
    String messages;
    for (ApiError _error in apiErrorRequest.errors) {
      String translatedMessage = _error.getTranslatedErrorMessage(context);
      if (messages == null) {
        messages = translatedMessage;
      } else {
        messages = '$messages\n$translatedMessage';
      }
    }
    return messages;
  }
}
