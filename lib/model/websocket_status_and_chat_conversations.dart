import 'package:forvo/model/chat_conversation.dart';
import 'package:forvo/util/json.dart';

class WebSocketStatusAndChatConversations {
  final String status;
  final int totalConversations;
  final List<ChatConversation> conversations;

  WebSocketStatusAndChatConversations({
    this.status,
    this.totalConversations,
    this.conversations,
  });

  factory WebSocketStatusAndChatConversations.fromJson(
      Map<String, dynamic> json) {
    List _conversations = json['conversations'];
    return WebSocketStatusAndChatConversations(
      status: jsonStringField(json['status']),
      totalConversations: jsonIntField(json['total']),
      conversations:
          ChatConversation.chatConversationListFromJson(_conversations),
    );
  }
}
