import 'package:forvo/util/json.dart';

class DatabaseChatMessage {
  final String id;
  final String idMessageAPI;
  final String idMessageLocal;
  final String idConversation;
  final String userFrom;
  final String userTo;
  final int isRead;
  final String message;
  final String addTimeAPI;
  final String addTimeOrder;
  final String addTimeInsert;

  DatabaseChatMessage({
    this.id,
    this.idMessageAPI = '',
    this.idMessageLocal = '',
    this.idConversation = '',
    this.userFrom = '',
    this.userTo = '',
    this.isRead = 0,
    this.message = '',
    this.addTimeAPI = '',
    this.addTimeOrder = '',
    this.addTimeInsert = '',
  });

  factory DatabaseChatMessage.fromJson(Map<String, dynamic> json) =>
      DatabaseChatMessage(
        id: jsonStringField(json['_id']),
        idMessageAPI: jsonStringField(json['idMessageAPI']),
        idMessageLocal: jsonStringField(json['idMessageLocal']),
        idConversation: jsonStringField(json['idConversation']),
        userFrom: jsonStringField(json['userFrom'] ??= ''),
        userTo: jsonStringField(json['userTo'] ??= ''),
        isRead: jsonIntField(json['isRead']),
        message: jsonStringField(json['message']),
        addTimeAPI: jsonStringField(json['addTimeAPI']),
        addTimeOrder: jsonStringField(json['addTimeOrder']),
        addTimeInsert: jsonStringField(json['addTimeInsert']),
      );

  static List<DatabaseChatMessage> databaseChatMessageListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return DatabaseChatMessage.fromJson(json);
            }).toList()
          : [];

  // The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() => {
        '_id': id,
        'idMessageAPI': idMessageAPI,
        'idMessageLocal': idMessageLocal,
        'idConversation': idConversation,
        'userFrom': userFrom,
        'userTo': userTo,
        'isRead': isRead,
        'message': message,
        'addTimeAPI': addTimeAPI,
        'addTimeOrder': addTimeOrder,
        'addTimeInsert': addTimeInsert,
      };

  Map<String, dynamic> toMapWithoutAddTimeInsert() => {
        '_id': id,
        'idMessageAPI': idMessageAPI,
        'idMessageLocal': idMessageLocal,
        'idConversation': idConversation,
        'userFrom': userFrom,
        'userTo': userTo,
        'isRead': isRead,
        'message': message,
        'addTimeAPI': addTimeAPI,
        'addTimeOrder': addTimeOrder,
      };

  @override
  String toString() => 'DatabaseChatMessage { '
      '_id: $id, '
      'idMessageAPI: $idMessageAPI, '
      'idMessageLocal: $idMessageLocal, '
      'idConversation: $idConversation, '
      'userFrom: $userFrom, '
      'userTo: $userTo, '
      'isRead: $isRead, '
      'message: $message, '
      'addTimeAPI: $addTimeAPI, '
      'addTimeOrder: $addTimeOrder, '
      'addTimeInsert: $addTimeInsert '
      '}';
}
