import 'package:forvo/util/json.dart';

class Country {
  final String id;
  final String code;
  final String name;

  Country({
    this.id,
    this.code,
    this.name,
  });

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: jsonStringField(json['id'] ??= ''),
        code: jsonStringField(json['code'] ??= ''),
        name: jsonStringField(json['name'] ??= ''),
      );
}
