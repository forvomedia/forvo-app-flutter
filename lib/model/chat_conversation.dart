import 'package:forvo/model/chat_message.dart';
import 'package:forvo/util/json.dart';

class ChatConversation {
  final int id;
  final String username;
  final bool isBlocked;
  int unreadMessages;
  final int totalMessages;
  final List<ChatMessage> messages;

  ChatConversation({
    this.id,
    this.username,
    this.isBlocked,
    this.unreadMessages,
    this.totalMessages,
    this.messages,
  });

  factory ChatConversation.fromJson(Map<String, dynamic> json) {
    List _messages = json['items'];
    return ChatConversation(
      id: jsonIntField(json['id']),
      username: jsonStringField(json['user'], nullValue: ''),
      isBlocked: jsonStringField(json['user_blocked'], nullValue: '0') == '1',
      unreadMessages: jsonIntField(json['unread_messages']),
      totalMessages: jsonIntField(json['countMessages']),
      messages: ChatMessage.chatMessageListFromJson(_messages),
    );
  }

  static List<ChatConversation> chatConversationListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return ChatConversation.fromJson(json);
            }).toList()
          : [];

  @override
  String toString() => 'ChatConversation { '
      'id: $id, '
      'username: $username, '
      'isBlocked: $isBlocked, '
      'unreadMessages: $unreadMessages, '
      'totalMessages: $totalMessages, '
      'messages: $messages, '
      '}';
}
