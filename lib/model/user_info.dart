import 'package:forvo/model/country.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/model/language_accent.dart';
import 'package:forvo/model/user.dart';

class UserInfo {
  User user;
  final Country country;
  final List<Language> languages;
  final List<LanguageAccent> accents;

  UserInfo({
    this.user,
    this.country,
    this.languages,
    this.accents,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> _user = json['user'];
    Map<String, dynamic> _country = json['country'];
    List _languages = json['languages'];
    List _accents = json['accents'];
    return json != null
        ? UserInfo(
            user: User.fromJson(_user),
            country: Country.fromJson(_country),
            languages: Language.languageListFromJson(_languages),
            accents: LanguageAccent.languageAccentListFromJson(_accents),
          )
        : null;
  }
}
