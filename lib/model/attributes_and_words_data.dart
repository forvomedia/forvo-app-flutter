import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/language_with_words.dart';
import 'package:forvo/util/json.dart';

class AttributesAndWordsData {
  final String status;
  final Attributes attributes;
  final List<LanguageWithWords> data;

  AttributesAndWordsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndWordsData.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndWordsData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: LanguageWithWords.languageWithWordsListFromJson(_data),
    );
  }
}
