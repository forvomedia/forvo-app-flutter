import 'package:forvo/model/language_accent.dart';
import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/util/json.dart';

class Language {
  final String id;
  final String name;
  final String code;
  final List<LanguageAccent> accents;
  final LanguageWordInformation wordInformation;

  Language({
    this.id,
    this.name,
    this.code,
    this.accents,
    this.wordInformation,
  });

  bool get hasAccents => accents?.isNotEmpty ?? false;

  factory Language.fromJson(Map<String, dynamic> json) {
    List _accents = json['accents'];
    return Language(
      id: jsonStringField(json['id'] ??= ''),
      name: jsonStringField(json['name'] ??= ''),
      code: jsonStringField(json['code'] ??= ''),
      accents: LanguageAccent.languageAccentListFromJson(_accents),
      wordInformation: json['wordInfo'] != null
          ? LanguageWordInformation.fromJson(json['wordInfo'])
          : null,
    );
  }

  static List<Language> languageListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return Language.fromJson(json);
            }).toList()
          : [];

  @override
  String toString() => '[Language] '
      'id: $id, '
      'name: $name, '
      'code: $code, '
      'hasAccents: ${hasAccents.toString()}, '
      'accents: $accents';
}
