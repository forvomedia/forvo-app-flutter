import 'package:forvo/model/followed_user.dart';
import 'package:forvo/model/language.dart';

class LanguageWithFollowedUsers {
  final Language language;
  final List<FollowedUser> items;

  LanguageWithFollowedUsers({
    this.language,
    this.items,
  });

  factory LanguageWithFollowedUsers.fromJson(Map<String, dynamic> json) {
    List _items = json['followed'];
    Language language = Language.fromJson(json['language']);
    return LanguageWithFollowedUsers(
      language: language,
      items: FollowedUser.followedUserListFromJson(language.code, _items),
    );
  }

  static List<LanguageWithFollowedUsers> languageWithFollowedUsersListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return LanguageWithFollowedUsers.fromJson(json);
            }).toList()
          : [];
}
