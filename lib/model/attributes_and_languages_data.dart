import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/language.dart';
import 'package:forvo/util/json.dart';

class AttributesAndLanguagesData {
  final String status;
  final Attributes attributes;
  final List<Language> data;

  AttributesAndLanguagesData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndLanguagesData.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndLanguagesData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: Language.languageListFromJson(_data),
    );
  }
}
