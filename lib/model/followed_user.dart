import 'package:forvo/util/json.dart';

class FollowedUser {
  final String id;
  final String name;
  final String pronunciationsNumber;
  final String followingLangCode;

  FollowedUser({
    this.id,
    this.name,
    this.pronunciationsNumber,
    this.followingLangCode,
  });

  factory FollowedUser.fromJson(
          String languageCode, Map<String, dynamic> json) =>
      FollowedUser(
        id: jsonStringField(json['id_followed_user']),
        name: jsonStringField(json['username']),
        pronunciationsNumber: jsonStringField(json['num_pro']),
        followingLangCode: languageCode,
      );

  static List<FollowedUser> followedUserListFromJson(
          String languageCode, List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return FollowedUser.fromJson(languageCode, json);
            }).toList()
          : [];
}
