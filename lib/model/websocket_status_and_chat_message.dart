import 'package:forvo/model/chat_message.dart';
import 'package:forvo/util/json.dart';

class WebSocketStatusAndChatMessage {
  final String status;
  final String idLocal;
  final ChatMessage message;

  WebSocketStatusAndChatMessage({
    this.status,
    this.idLocal,
    this.message,
  });

  factory WebSocketStatusAndChatMessage.fromJson(Map<String, dynamic> json) =>
      WebSocketStatusAndChatMessage(
        status: jsonStringField(json['status']),
        idLocal: jsonStringField(json['idLocal']),
        message: ChatMessage.fromJson(json['item']),
      );
}
