import 'package:forvo/util/json.dart';

class UserProfileNotify {
  final bool notify;
  final bool votes;
  final bool forvo;
  final bool allowMessages;
  final bool allowMailMessages;

  UserProfileNotify({
    this.notify,
    this.votes,
    this.forvo,
    this.allowMessages,
    this.allowMailMessages,
  });

  factory UserProfileNotify.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> _messages = json['messages'];

    return UserProfileNotify(
      notify: jsonBoolField(json['notify'] != null && json['notify'] == 1),
      votes: jsonBoolField(json['votes'] != null && json['votes'] == 1),
      forvo: jsonBoolField(json['forvo'] != null && json['forvo'] == 1),
      allowMessages:
          jsonBoolField(_messages['allow'] != null && _messages['allow'] == 1),
      allowMailMessages: jsonBoolField(
          _messages['allow_mail'] != null && _messages['allow_mail'] == 1),
    );
  }
}
