import 'package:forvo/model/language.dart';
import 'package:forvo/model/translation_language_pair.dart';

class TranslationLanguageGroup {
  final Language father;
  final List<TranslationLanguagePair> children;

  TranslationLanguageGroup({
    this.father,
    this.children,
  });

  factory TranslationLanguageGroup.fromJson(Map<String, dynamic> json) {
    List _children = json['children'];
    return TranslationLanguageGroup(
      father: Language.fromJson(json['father']),
      children: TranslationLanguagePair.translationLanguagePairsListFromJson(
          _children),
    );
  }

  static List<TranslationLanguageGroup> translationLanguageGroupsListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return TranslationLanguageGroup.fromJson(json);
            }).toList()
          : [];
}
