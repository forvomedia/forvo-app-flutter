import 'package:forvo/model/categorization_group_pending_pronounce_information.dart';
import 'package:forvo/util/json.dart';

class PendingPronounceInformation {
  final bool pendingPronunciation;
  final bool hasPronunciationRequest;
  final List<CategorizationGroupPendingPronounceInformation>
      categorizationGroups;

  PendingPronounceInformation({
    this.pendingPronunciation,
    this.hasPronunciationRequest,
    this.categorizationGroups,
  });

  bool get isPendingPronunciationAndHasPronunciationRequest =>
      pendingPronunciation != null &&
      pendingPronunciation &&
      hasPronunciationRequest != null &&
      hasPronunciationRequest;

  factory PendingPronounceInformation.fromJson(Map<String, dynamic> json) {
    List _categorizationGroups = json['categorization_groups'];
    return PendingPronounceInformation(
      pendingPronunciation: json['pending_pronounce'] != null
          ? jsonBoolField(json['pending_pronounce'])
          : null,
      hasPronunciationRequest: json['has_pronunciation_request'] != null
          ? jsonBoolField(json['has_pronunciation_request'])
          : null,
      categorizationGroups: _categorizationGroups != null
          ? CategorizationGroupPendingPronounceInformation
              .categorizationGroupPendingPronounceInformationListFromJson(
                  _categorizationGroups)
          : null,
    );
  }
}
