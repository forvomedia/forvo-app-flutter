import 'package:forvo/model/pending_pronounce_information.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/util/json.dart';

class LanguageAccentWithPronunciations {
  final int totalItems;
  final String name;
  final String code;
  final String languageName;
  final String languageCode;
  final List<Pronunciation> items;
  final PendingPronounceInformation pendingPronounceInformation;

  LanguageAccentWithPronunciations({
    this.totalItems,
    this.name,
    this.code,
    this.languageName,
    this.languageCode,
    this.items,
    this.pendingPronounceInformation,
  });

  factory LanguageAccentWithPronunciations.fromJson(Map<String, dynamic> json) {
    List _items = json['items'];
    return LanguageAccentWithPronunciations(
      totalItems: jsonIntField(json['subtotal']),
      name: jsonStringField(json['name']),
      code: jsonStringField(json['code']),
      languageName: jsonStringField(json['languageName']),
      languageCode: jsonStringField(json['languageCode']),
      items: Pronunciation.pronunciationListFromJson(_items),
      pendingPronounceInformation: json['pending_pronounce_info'] != null
          ? PendingPronounceInformation.fromJson(json['pending_pronounce_info'])
          : null,
    );
  }

  static List<LanguageAccentWithPronunciations>
      languageAccentWithPronunciationsListFromJson(List<dynamic> list) =>
          list != null && list.isNotEmpty
              ? list.map((item) {
                  Map<String, dynamic> json = item;
                  return LanguageAccentWithPronunciations.fromJson(json);
                }).toList()
              : [];
}
