import 'package:forvo/model/search_result_items.dart';
import 'package:forvo/util/json.dart';

class LanguageAccentWithSearchResultItems {
  final int totalItems;
  final String name;
  final String code;
  final String languageName;
  final String languageCode;
  final SearchResultItems items;

  LanguageAccentWithSearchResultItems({
    this.totalItems,
    this.name,
    this.code,
    this.languageName,
    this.languageCode,
    this.items,
  });

  factory LanguageAccentWithSearchResultItems.fromJson(
          Map<String, dynamic> json) =>
      LanguageAccentWithSearchResultItems(
        totalItems: jsonIntField(json['subtotal']),
        name: jsonStringField(json['name']),
        code: jsonStringField(json['code']),
        languageName: jsonStringField(json['languageName']),
        languageCode: jsonStringField(json['languageCode']),
        items: SearchResultItems.fromJson(json['items']),
      );

  static List<LanguageAccentWithSearchResultItems>
      languageAccentWithSearchResultItemsListFromJson(List<dynamic> list) =>
          list != null && list.isNotEmpty
              ? list.map((item) {
                  Map<String, dynamic> json = item;
                  return LanguageAccentWithSearchResultItems.fromJson(json);
                }).toList()
              : [];
}
