import 'package:forvo/constants/language_constants.dart';
import 'package:forvo/util/json.dart';

class WordCheckAlphabet {
  final String status;
  final String message;
  final List<String> invalidChars;

  WordCheckAlphabet({
    this.status,
    this.message,
    this.invalidChars,
  });

  bool get statusIsMaybeWord =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_MAYBE_WORD;

  bool get statusIsWordForbidden =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_FORBIDDEN;

  bool get statusIsInvalidChars =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_INVALID_CHARS;

  bool get statusIsSpecialChars =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_SPECIAL_CHARS;

  bool get statusIsNotAvailable =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_NOT_AVAILABLE;

  bool get statusIsCheckIsPersonName =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_CHECK_IS_PERSON_NAME;

  bool get statusIsCheckIsPhrase =>
      status == LanguageConstants.CHECK_ALPHABET_STATUS_CHECK_IS_PHRASE;

  factory WordCheckAlphabet.fromJson(Map<String, dynamic> json) =>
      WordCheckAlphabet(
        status: jsonStringField(json['status']),
        message: jsonStringField(json['msg']),
        invalidChars: json['invalidChars'] != null
            ? jsonStringListField(json['invalidChars'])
            : null,
      );

  @override
  String toString() => '[WordCheckAlphabet] '
      'status: $status, '
      'message: $message, '
      'invalidChars: $invalidChars';
}
