import 'package:forvo/util/json.dart';

class BlockedUser {
  final String username;

  BlockedUser({
    this.username,
  });

  factory BlockedUser.fromJson(Map<String, dynamic> json) => BlockedUser(
        username: jsonStringField(json['username'] ??= ''),
      );

  static List<BlockedUser> blockedUsersListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return BlockedUser.fromJson(json);
            }).toList()
          : [];
}
