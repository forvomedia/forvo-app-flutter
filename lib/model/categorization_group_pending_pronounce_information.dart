import 'package:forvo/util/json.dart';

class CategorizationGroupPendingPronounceInformation {
  final int groupIndex;
  final bool pendingPronunciation;
  final bool hasPronunciationRequest;

  CategorizationGroupPendingPronounceInformation({
    this.groupIndex,
    this.pendingPronunciation,
    this.hasPronunciationRequest,
  });

  bool get isPendingPronunciationAndHasPronunciationRequest =>
      pendingPronunciation != null &&
      pendingPronunciation &&
      hasPronunciationRequest != null &&
      hasPronunciationRequest;

  factory CategorizationGroupPendingPronounceInformation.fromJson(
          Map<String, dynamic> json) =>
      CategorizationGroupPendingPronounceInformation(
        groupIndex: jsonIntField(json['groupIndex']),
        pendingPronunciation: json['pending_pronounce'] != null
            ? jsonBoolField(json['pending_pronounce'])
            : null,
        hasPronunciationRequest: json['has_pronunciation_request'] != null
            ? jsonBoolField(json['has_pronunciation_request'])
            : null,
      );

  static List<CategorizationGroupPendingPronounceInformation>
      categorizationGroupPendingPronounceInformationListFromJson(
              List<dynamic> list) =>
          list != null && list.isNotEmpty
              ? list.map((item) {
                  Map<String, dynamic> json = item;
                  return CategorizationGroupPendingPronounceInformation
                      .fromJson(json);
                }).toList()
              : [];
}
