import 'package:forvo/util/json.dart';

class ApiErrorParameter {
  final String parameter;

  ApiErrorParameter({
    this.parameter,
  });

  factory ApiErrorParameter.fromJson(Map<String, dynamic> json) =>
      ApiErrorParameter(
        parameter: json['param'] != null
            ? jsonStringField(json['param'] ??= '')
            : null,
      );

  static List<ApiErrorParameter> apiErrorParameterListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return ApiErrorParameter.fromJson(json);
            }).toList()
          : [];
}
