import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/util/json.dart';

class PendingPronounceWord {
  final int id;
  final int idWord;
  final String word;
  final String original;
  final String addtime;
  final String languageName;
  final String languageCode;
  final String languageAccentName;
  final String languageAccentCode;
  final int numPronunciations;
  final bool isPhrase;
  final bool isValidated;
  final bool hasCategorizationData;
  final int pronunciationGroupIndex;
  final bool alreadyPronouncedByUser;
  final WordCategorizationGroup wordCategorizationGroup;
  final LanguageWordInformation wordInformation;

  PendingPronounceWord({
    this.id,
    this.idWord,
    this.word,
    this.original,
    this.addtime,
    this.languageName,
    this.languageCode,
    this.languageAccentName,
    this.languageAccentCode,
    this.numPronunciations,
    this.isPhrase,
    this.isValidated,
    this.hasCategorizationData,
    this.pronunciationGroupIndex,
    this.alreadyPronouncedByUser,
    this.wordCategorizationGroup,
    this.wordInformation,
  });

  factory PendingPronounceWord.fromJson(Map<String, dynamic> json) =>
      PendingPronounceWord(
        id: jsonIntField(json['id']),
        idWord: jsonIntField(json['id_word']),
        word: jsonStringField(json['word'], nullValue: ''),
        original: jsonStringField(json['original'], nullValue: ''),
        addtime: jsonStringField(json['addtime'], nullValue: ''),
        languageName: jsonStringField(json['language'], nullValue: ''),
        languageCode: jsonStringField(json['code'], nullValue: ''),
        languageAccentName: jsonStringField(json['accent'], nullValue: ''),
        languageAccentCode: jsonStringField(json['accentCode'], nullValue: ''),
        numPronunciations: jsonIntField(json['num_pronunciations']),
        isPhrase: jsonStringField(json['is_phrase']) == '1',
        isValidated: jsonStringField(json['is_validated']) == '1',
        hasCategorizationData:
            jsonStringField(json['has_categorization_data']) == '1',
        pronunciationGroupIndex: json['pronunciation_group_index'] != null &&
                json['pronunciation_group_index'] != ''
            ? int.parse(jsonStringField(json['pronunciation_group_index']))
            : null,
        alreadyPronouncedByUser: json['already_pronounced_by_user'] != null &&
            json['already_pronounced_by_user'] == '1',
        wordCategorizationGroup: json['categorizationGroup'] != null
            ? WordCategorizationGroup.fromJson(json['categorizationGroup'])
            : null,
        wordInformation: json['wordInfo'] != null
            ? LanguageWordInformation.fromJson(json['wordInfo'])
            : null,
      );

  static List<PendingPronounceWord> pendingPronounceWordListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return PendingPronounceWord.fromJson(json);
            }).toList()
          : [];
}
