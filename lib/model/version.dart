class Version {
  final String currentVersion;
  final String minVersion;
  final String androidUrl;
  final String iosUrl;
  final bool forceLogout;

  Version({
    this.currentVersion,
    this.minVersion,
    this.androidUrl,
    this.iosUrl,
    this.forceLogout,
  });

  factory Version.fromJson(
    Map<String, dynamic> json,
    String currentVersion,
    String minVersion, {
    bool forceLogout = false,
  }) =>
      Version(
        currentVersion: currentVersion,
        minVersion: minVersion,
        androidUrl: json['android_url'].toString(),
        iosUrl: json['ios_url'].toString(),
        forceLogout: forceLogout,
      );

  @override
  String toString() => 'currentVersion: $currentVersion\n'
      'minVersion: $minVersion\n'
      'androidUrl: $androidUrl\n'
      'iosUrl: $iosUrl\n'
      'forceLogout: $forceLogout';
}
