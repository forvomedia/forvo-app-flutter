import 'package:forvo/constants/constants.dart';
import 'package:forvo/util/json.dart';

class UserProfileRanking {
  final String addedWords;
  final String pronunciations;

  UserProfileRanking({
    this.addedWords,
    this.pronunciations,
  });

  factory UserProfileRanking.fromJson(Map<String, dynamic> json) =>
      UserProfileRanking(
        addedWords:
            jsonStringField(json['added_words'], nullValue: NOT_AVAILABLE_DATA),
        pronunciations: jsonStringField(json['pronunciations'],
            nullValue: NOT_AVAILABLE_DATA),
      );
}
