import 'package:forvo/util/json.dart';

class DatabaseChatConversation {
  final String id;
  final String idConversation;
  final String userTo;
  final int userToBlocked;
  final int unReadMessages;
  final String lastMessage;
  final String lastMessageAddtimeAPI;
  final String lastMessageAddtimeOrder;
  final String lastMessageAddtimeInsert;

  DatabaseChatConversation({
    this.id,
    this.idConversation,
    this.userTo,
    this.userToBlocked = 0,
    this.unReadMessages,
    this.lastMessage = '',
    this.lastMessageAddtimeAPI = '',
    this.lastMessageAddtimeOrder = '',
    this.lastMessageAddtimeInsert = '',
  });

  factory DatabaseChatConversation.fromJson(Map<String, dynamic> json) =>
      DatabaseChatConversation(
        id: jsonStringField(json['_id']),
        idConversation: jsonStringField(json['idConversation']),
        userTo: jsonStringField(json['userTo'] ??= ''),
        userToBlocked: jsonIntField(json['userToBlocked']),
        unReadMessages: jsonIntField(json['unReadMessages']),
        lastMessage: jsonStringField(json['lastMessage']),
        lastMessageAddtimeAPI: jsonStringField(json['lastMessageAddtimeAPI']),
        lastMessageAddtimeOrder:
            jsonStringField(json['lastMessageAddtimeOrder']),
        lastMessageAddtimeInsert:
            jsonStringField(json['lastMessageAddtimeInsert']),
      );

  static List<DatabaseChatConversation> databaseChatConversationListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return DatabaseChatConversation.fromJson(json);
            }).toList()
          : [];

  String getLocalLastMessageAddTimeApi() {
    String localAddTimeApi = '';
    try {} on Exception catch (e) {
      print('EXCEPTION on getLocalLastMessageAddTimeApi > ${e.toString()}');
    }
    return localAddTimeApi;
  }

  // The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() => {
        '_id': id,
        'idConversation': idConversation,
        'userTo': userTo,
        'userToBlocked': userToBlocked,
        'unReadMessages': unReadMessages,
        'lastMessage': lastMessage,
        'lastMessageAddtimeAPI': lastMessageAddtimeAPI,
        'lastMessageAddtimeOrder': lastMessageAddtimeOrder,
        'lastMessageAddtimeInsert': lastMessageAddtimeInsert,
      };

  Map<String, dynamic> toMapToUpdateConversation() => {
    'idConversation': idConversation,
    'unReadMessages': unReadMessages,
    'lastMessage': lastMessage,
    'lastMessageAddtimeAPI': lastMessageAddtimeAPI,
    'lastMessageAddtimeOrder': lastMessageAddtimeOrder,
    'lastMessageAddtimeInsert': lastMessageAddtimeInsert,
  };

  @override
  String toString() => 'DatabaseChatConversation { '
      '_id: $id, '
      'idConversation: $idConversation, '
      'userTo: $userTo, '
      'userToBlocked: $userToBlocked, '
      'unReadMessages: $unReadMessages, '
      'lastMessage: $lastMessage, '
      'lastMessageAddtimeAPI: $lastMessageAddtimeAPI, '
      'lastMessageAddtimeOrder: $lastMessageAddtimeOrder, '
      'lastMessageAddtimeInsert: $lastMessageAddtimeInsert '
      '}';
}
