import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/constants/api_error_code_constants.dart';
import 'package:forvo/model/api_error_param.dart';
import 'package:forvo/util/json.dart';

class ApiError {
  final String errorMessage;
  final int errorApiCode;
  final List<ApiErrorParameter> params;

  ApiError({
    this.errorMessage,
    this.errorApiCode,
    this.params,
  });

  factory ApiError.fromJson(Map<String, dynamic> json) => ApiError(
        errorMessage: jsonStringField(json['errorMessage'] ??= ''),
        errorApiCode: jsonIntField(json['errorApiCode'] ??= ''),
        params: json['params'] != null
            ? ApiErrorParameter.apiErrorParameterListFromJson(json['params'])
            : null,
      );

  static List<ApiError> apiErrorListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return ApiError.fromJson(json);
            }).toList()
          : [];

  String getTranslatedErrorMessage(BuildContext context) {
    var localizations = AppLocalizations.of(context);
    final codeError = StringBuffer('error.api.');
    codeError.write(errorApiCode.toString());

    String translatedMessage;
    switch (errorApiCode) {
      case ApiErrorCodeConstants.ERROR_1000:
      case ApiErrorCodeConstants.ERROR_1003:
      case ApiErrorCodeConstants.ERROR_1004:
      case ApiErrorCodeConstants.ERROR_1005:
      case ApiErrorCodeConstants.ERROR_1006:
      case ApiErrorCodeConstants.ERROR_1007:
      case ApiErrorCodeConstants.ERROR_1008:
      case ApiErrorCodeConstants.ERROR_1009:
      case ApiErrorCodeConstants.ERROR_1010:
      case ApiErrorCodeConstants.ERROR_1011:
      case ApiErrorCodeConstants.ERROR_1012:
      case ApiErrorCodeConstants.ERROR_1013:
      case ApiErrorCodeConstants.ERROR_1015:
      case ApiErrorCodeConstants.ERROR_1016:
      case ApiErrorCodeConstants.ERROR_1017:
      case ApiErrorCodeConstants.ERROR_1018:
      case ApiErrorCodeConstants.ERROR_1019:
      case ApiErrorCodeConstants.ERROR_1020:
      case ApiErrorCodeConstants.ERROR_1021:
      case ApiErrorCodeConstants.ERROR_1022:
      case ApiErrorCodeConstants.ERROR_1023:
      case ApiErrorCodeConstants.ERROR_1024:
      case ApiErrorCodeConstants.ERROR_1025:
      case ApiErrorCodeConstants.ERROR_1026:
      case ApiErrorCodeConstants.ERROR_1027:
      case ApiErrorCodeConstants.ERROR_1028:
      case ApiErrorCodeConstants.ERROR_1029:
      case ApiErrorCodeConstants.ERROR_1030:
      case ApiErrorCodeConstants.ERROR_1031:
      case ApiErrorCodeConstants.ERROR_1032:
      case ApiErrorCodeConstants.ERROR_1033:
      case ApiErrorCodeConstants.ERROR_1034:
      case ApiErrorCodeConstants.ERROR_1035:
      case ApiErrorCodeConstants.ERROR_1036:
      case ApiErrorCodeConstants.ERROR_1037:
      case ApiErrorCodeConstants.ERROR_1038:
      case ApiErrorCodeConstants.ERROR_1039:
      case ApiErrorCodeConstants.ERROR_1040:
      case ApiErrorCodeConstants.ERROR_1041:
      case ApiErrorCodeConstants.ERROR_1042:
      case ApiErrorCodeConstants.ERROR_1043:
      case ApiErrorCodeConstants.ERROR_1044:
      case ApiErrorCodeConstants.ERROR_1045:
      case ApiErrorCodeConstants.ERROR_1046:
      case ApiErrorCodeConstants.ERROR_1047:
      case ApiErrorCodeConstants.ERROR_1048:
      case ApiErrorCodeConstants.ERROR_1049:
      case ApiErrorCodeConstants.ERROR_1050:
      case ApiErrorCodeConstants.ERROR_1051:
      case ApiErrorCodeConstants.ERROR_1052:
      case ApiErrorCodeConstants.ERROR_1053:
      case ApiErrorCodeConstants.ERROR_1054:
      case ApiErrorCodeConstants.ERROR_1056:
      case ApiErrorCodeConstants.ERROR_1057:
      case ApiErrorCodeConstants.ERROR_1058:
      case ApiErrorCodeConstants.ERROR_1059:
      case ApiErrorCodeConstants.ERROR_1060:
      case ApiErrorCodeConstants.ERROR_1061:
      case ApiErrorCodeConstants.ERROR_1062:
      case ApiErrorCodeConstants.ERROR_1063:
      case ApiErrorCodeConstants.ERROR_1065:
      case ApiErrorCodeConstants.ERROR_1066:
      case ApiErrorCodeConstants.ERROR_1067:
      case ApiErrorCodeConstants.ERROR_1068:
      case ApiErrorCodeConstants.ERROR_1069:
      case ApiErrorCodeConstants.ERROR_1070:
      case ApiErrorCodeConstants.ERROR_1071:
      case ApiErrorCodeConstants.ERROR_1072:
      case ApiErrorCodeConstants.ERROR_1073:
      case ApiErrorCodeConstants.ERROR_1075:
      case ApiErrorCodeConstants.ERROR_1076:
      case ApiErrorCodeConstants.ERROR_1077:
      case ApiErrorCodeConstants.ERROR_1078:
      case ApiErrorCodeConstants.ERROR_1080:
      case ApiErrorCodeConstants.ERROR_1081:
      case ApiErrorCodeConstants.ERROR_1082:
      case ApiErrorCodeConstants.ERROR_1083:
      case ApiErrorCodeConstants.ERROR_1084:
      case ApiErrorCodeConstants.ERROR_1085:
      case ApiErrorCodeConstants.ERROR_1086:
      case ApiErrorCodeConstants.ERROR_1087:
      case ApiErrorCodeConstants.ERROR_1088:
      case ApiErrorCodeConstants.ERROR_1089:
      case ApiErrorCodeConstants.ERROR_1090:
      case ApiErrorCodeConstants.ERROR_1091:
      case ApiErrorCodeConstants.ERROR_1092:
      case ApiErrorCodeConstants.ERROR_1093:
      case ApiErrorCodeConstants.ERROR_1094:
      case ApiErrorCodeConstants.ERROR_1095:
        translatedMessage = localizations.translate(codeError.toString());
        break;
      //errors with parameters
      case ApiErrorCodeConstants.ERROR_1001:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {
            'name': params.first.parameter,
          },
        );
        break;
      case ApiErrorCodeConstants.ERROR_1002:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {
            'fieldName': params.first.parameter,
            'fieldValue': params.last.parameter,
          },
        );
        break;
      case ApiErrorCodeConstants.ERROR_1014:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {'word': params.first.parameter},
        );
        break;
      case ApiErrorCodeConstants.ERROR_1055:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {'userName': params.first.parameter},
        );
        break;
      case ApiErrorCodeConstants.ERROR_1064:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {'maxValue': params.first.parameter},
        );
        break;
      case ApiErrorCodeConstants.ERROR_1074:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {'charMaxValue': params.first.parameter},
        );
        break;
      case ApiErrorCodeConstants.ERROR_1079:
        translatedMessage = localizations.translate(
          codeError.toString(),
          params: {'word': params.first.parameter},
        );
        break;
      default:
        translatedMessage = errorMessage;
        break;
    }
    return translatedMessage;
  }
}
