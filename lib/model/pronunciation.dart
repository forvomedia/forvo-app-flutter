import 'package:forvo/model/language_word_information.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/util/json.dart';

const String USER_GENRE_FEMALE = 'f';
const String USER_GENRE_MALE = 'm';
const int VOTE_GOOD = 1;
const int VOTE_BAD = -1;

class Pronunciation {
  final int id;
  final String word;
  final String original;
  final String addtime;
  final int hits;
  final String username;
  final String userGenre;
  final String countryName;
  final String languageCode;
  final String languageName;
  final String languageAccentCode;
  final String languageAccentName;
  final int rate;
  final int numVotes;
  final int numPositiveVotes;
  bool favorite;
  int vote;
  final bool pronouncerAllowsUserMessages;
  bool following;
  bool userIsActive;
  final String audioRealMp3;
  final String audioRealOgg;
  final int pronunciationGroupIndex;
  final WordCategorizationGroup wordCategorizationGroup;
  final LanguageWordInformation wordInformation;

  Pronunciation({
    this.id,
    this.word,
    this.original,
    this.addtime,
    this.hits,
    this.username,
    this.userGenre,
    this.countryName,
    this.languageCode,
    this.languageName,
    this.languageAccentCode,
    this.languageAccentName,
    this.rate,
    this.numVotes,
    this.numPositiveVotes,
    this.favorite,
    this.vote,
    this.pronouncerAllowsUserMessages,
    this.following,
    this.userIsActive,
    this.audioRealMp3,
    this.audioRealOgg,
    this.pronunciationGroupIndex,
    this.wordCategorizationGroup,
    this.wordInformation,
  });

  bool get isUserGenreMale => userGenre == USER_GENRE_MALE;

  bool get isUserGenreFemale => userGenre == USER_GENRE_FEMALE;

  bool get markedAsFavorite => favorite;

  bool get votedAsGood => vote == VOTE_GOOD;

  bool get votedAsBad => vote == VOTE_BAD;

  bool get markedAsFollowing => following;

  bool get userIsAnonymous => username.isEmpty;

  factory Pronunciation.fromJson(Map<String, dynamic> json) => Pronunciation(
        id: jsonIntField(json['id']),
        word: jsonStringField(json['word'], nullValue: ''),
        original: jsonStringField(json['original'], nullValue: ''),
        addtime: jsonStringField(json['addtime'], nullValue: ''),
        hits: jsonIntField(json['hits']),
        username: jsonStringField(json['username'], nullValue: ''),
        userGenre: jsonStringField(json['sex'], nullValue: ''),
        countryName: jsonStringField(json['country'], nullValue: ''),
        languageCode: jsonStringField(json['code'], nullValue: ''),
        languageName: jsonStringField(json['langname'], nullValue: ''),
        languageAccentCode: jsonStringField(json['accent_code'], nullValue: ''),
        languageAccentName: jsonStringField(json['accent'], nullValue: ''),
        rate: jsonIntField(json['rate']),
        numVotes: jsonIntField(json['num_votes']),
        numPositiveVotes: jsonIntField(json['num_positive_votes']),
        favorite:
            json['favorite'] != null && jsonBoolField(json['favorite'] == 1),
        vote: jsonIntField(json['vote']),
        pronouncerAllowsUserMessages: json['chat'] is bool
            ? jsonBoolField(json['chat'])
            : jsonBoolField(json['chat'] == '1'),
        following:
            json['following'] != null && jsonBoolField(json['following'] == 1),
        userIsActive: json['userIsActive'] != null &&
            jsonBoolField(json['userIsActive'] == 1),
        audioRealMp3: jsonStringField(json['realmp3']),
        audioRealOgg: jsonStringField(json['realogg']),
        pronunciationGroupIndex: json['pronunciation_group_index'] != null &&
                json['pronunciation_group_index'] != ''
            ? int.parse(jsonStringField(json['pronunciation_group_index']))
            : null,
        wordCategorizationGroup: json['categorizationGroup'] != null
            ? WordCategorizationGroup.fromJson(json['categorizationGroup'])
            : null,
        wordInformation: json['wordInfo'] != null
            ? LanguageWordInformation.fromJson(json['wordInfo'])
            : null,
      );

  static List<Pronunciation> pronunciationListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return Pronunciation.fromJson(json);
            }).toList()
          : [];
}
