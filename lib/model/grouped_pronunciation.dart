import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/util/json.dart';

class GroupedPronunciation {
  final int groupIndex;
  final bool isPendingPronunciation;
  final bool hasPronunciationRequest;
  final Pronunciation pronunciation;

  GroupedPronunciation({
    this.groupIndex,
    this.isPendingPronunciation,
    this.hasPronunciationRequest,
    this.pronunciation,
  });

  factory GroupedPronunciation.fromJson(Map<String, dynamic> json) =>
      GroupedPronunciation(
        groupIndex: jsonIntField(json['groupIndex']),
        isPendingPronunciation: json['pending_pronunciation'] != null
            ? jsonBoolField(json['pending_pronunciation'])
            : null,
        hasPronunciationRequest: json['has_pronunciation_request'] != null
            ? jsonBoolField(json['has_pronunciation_request'])
            : null,
        pronunciation: json['pronunciation'] != null
            ? Pronunciation.fromJson(json['pronunciation'])
            : null,
      );

  static List<GroupedPronunciation> groupedPronunciationListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return GroupedPronunciation.fromJson(json);
            }).toList()
          : [];
}
