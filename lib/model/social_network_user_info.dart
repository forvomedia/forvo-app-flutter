import 'package:forvo/util/json.dart';

class SocialNetworkUserInfo {
  final String id;
  final String email;
  final String name;
  final String firstName;
  final String lastName;
  final String locale;
  final String gender;
  final String verified;

  SocialNetworkUserInfo({
    this.id,
    this.email,
    this.name,
    this.firstName,
    this.lastName,
    this.locale,
    this.gender,
    this.verified,
  });

  factory SocialNetworkUserInfo.fromJson(Map<String, dynamic> json) =>
      SocialNetworkUserInfo(
        id: jsonStringField(json['id'] ??= ''),
        email: jsonStringField(json['email'] ??= ''),
        name: jsonStringField(json['name'] ??= ''),
        firstName: jsonStringField(json['first_name'] ??= ''),
        lastName: jsonStringField(json['last_name'] ??= ''),
        locale: jsonStringField(json['locale'] ??= ''),
        gender: jsonStringField(json['gender'] ??= ''),
        verified: jsonStringField(json['verified'] ??= ''),
      );
}
