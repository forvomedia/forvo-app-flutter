import 'package:forvo/util/json.dart';

class UserProfileUser {
  final String id;
  final String username;
  final String genre;
  final String languageName;
  final String languageCode;

  UserProfileUser({
    this.id,
    this.username,
    this.genre,
    this.languageName,
    this.languageCode,
  });

  factory UserProfileUser.fromJson(Map<String, dynamic> json) =>
      UserProfileUser(
        id: jsonStringField(json['id'] ??= ''),
        username: jsonStringField(json['username'] ??= ''),
        languageName: jsonStringField(json['language'] ??= ''),
        languageCode: jsonStringField(json['code'] ??= ''),
        genre: jsonStringField(json['gender'] ??= ''),
      );
}
