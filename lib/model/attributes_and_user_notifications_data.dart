import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/user_notification.dart';
import 'package:forvo/util/json.dart';

class AttributesAndUserNotificationsData {
  final String status;
  final Attributes attributes;
  final List<UserNotification> data;

  AttributesAndUserNotificationsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndUserNotificationsData.fromJson(
      Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndUserNotificationsData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: UserNotification.userNotificationListFromJson(_data),
    );
  }
}
