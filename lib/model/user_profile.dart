import 'package:forvo/model/country.dart';
import 'package:forvo/model/user_profile_notify.dart';
import 'package:forvo/model/user_profile_ranking.dart';
import 'package:forvo/model/user_profile_stats.dart';
import 'package:forvo/model/user_profile_user.dart';
import 'package:forvo/util/json.dart';

class UserProfile {
  final bool isPublic;
  final UserProfileUser user;
  final Country country;
  final UserProfileStats stats;
  final UserProfileRanking ranking;
  final bool allowsUserMessages;
  final UserProfileNotify notify;

  UserProfile({
    this.isPublic,
    this.user,
    this.country,
    this.stats,
    this.ranking,
    this.allowsUserMessages,
    this.notify,
  });

  factory UserProfile.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> _user = json['user'];
    Map<String, dynamic> _country = json['country'];
    Map<String, dynamic> _stats = json['stats'];
    Map<String, dynamic> _ranking = json['ranking'];
    Map<String, dynamic> _notify = json['notify'];
    return json != null
        ? UserProfile(
            isPublic: jsonBoolField(
                json['isPublic'] != null && json['isPublic'] == 1),
            user: _user != null ? UserProfileUser.fromJson(_user) : null,
            country: _country != null ? Country.fromJson(_country) : null,
            stats: _stats != null ? UserProfileStats.fromJson(_stats) : null,
            ranking:
                _ranking != null ? UserProfileRanking.fromJson(_ranking) : null,
            allowsUserMessages:
                jsonBoolField(json['chat'] != null && json['chat'] == 1),
            notify:
                _notify != null ? UserProfileNotify.fromJson(_notify) : null,
          )
        : null;
  }
}
