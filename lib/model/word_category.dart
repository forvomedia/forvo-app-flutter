import 'package:forvo/model/word_category_spelling.dart';
import 'package:forvo/util/json.dart';

class WordCategory {
  final String nativeCategoryName;
  final String englishCategoryName;
  final List<WordCategorySpelling> data;

  WordCategory({
    this.nativeCategoryName,
    this.englishCategoryName,
    this.data,
  });

  factory WordCategory.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return WordCategory(
      nativeCategoryName: jsonStringField(json['category'], nullValue: ''),
      englishCategoryName:
          jsonStringField(json['category_english'], nullValue: ''),
      data: _data != null
          ? WordCategorySpelling.wordCategorySpellingListFromJson(_data)
          : [],
    );
  }

  static List<WordCategory> wordCategoryListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return WordCategory.fromJson(json);
            }).toList()
          : [];
}
