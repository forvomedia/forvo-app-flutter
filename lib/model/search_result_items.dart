import 'package:forvo/model/word.dart';
import 'package:forvo/util/json.dart';

class SearchResultItems {
  final Word exactMatch;
  final List<Word> relatedContent;
  final String translation;

  SearchResultItems({
    this.exactMatch,
    this.relatedContent,
    this.translation,
  });

  factory SearchResultItems.fromJson(Map<String, dynamic> json) {
    var _exactMatch = json['exactMatch'];
    List _relatedContent = json['relatedContent'];
    return SearchResultItems(
      exactMatch: _exactMatch != null ? Word.fromJson(_exactMatch) : null,
      relatedContent:
          _relatedContent != null ? Word.wordListFromJson(_relatedContent) : [],
      translation: jsonStringField(json['translation'], nullValue: '',)
    );
  }
}
