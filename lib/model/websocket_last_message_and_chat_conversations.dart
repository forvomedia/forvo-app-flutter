import 'package:forvo/model/chat_conversation.dart';

class WebSocketLastMessageAndChatConversations {
  final String lastMessageId;
  final List<ChatConversation> conversations;

  WebSocketLastMessageAndChatConversations({
    this.lastMessageId,
    this.conversations,
  });
}
