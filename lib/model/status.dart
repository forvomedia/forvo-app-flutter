import 'package:forvo/constants/api_constants.dart';
import 'package:forvo/util/json.dart';

class Status {
  final String status;

  Status({
    this.status,
  });

  bool get statusIsOk => status == ApiConstants.STATUS_OK;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        status: jsonStringField(json['status']),
      );
}
