import 'package:forvo/util/json.dart';

class TranslationLanguagePair {
  final String pairName;
  final String codes;

  TranslationLanguagePair({
    this.pairName,
    this.codes,
  });

  factory TranslationLanguagePair.fromJson(Map<String, dynamic> json) =>
      TranslationLanguagePair(
        pairName: jsonStringField(json['pairName'] ??= ''),
        codes: jsonStringField(json['value'] ??= ''),
      );

  static List<TranslationLanguagePair> translationLanguagePairsListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return TranslationLanguagePair.fromJson(json);
            }).toList()
          : [];
}
