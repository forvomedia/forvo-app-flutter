import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/util/json.dart';

class LanguageWordInformation {
  final String original;
  final bool isPhrase;
  final bool isValidated;
  final bool hasCategorizationData;
  final List<WordCategorizationGroup> categorizationGroups;
  final List<WordSpelling> alternativeSpellings;

  LanguageWordInformation({
    this.original,
    this.isPhrase,
    this.isValidated,
    this.hasCategorizationData,
    this.categorizationGroups,
    this.alternativeSpellings,
  });

  factory LanguageWordInformation.fromJson(Map<String, dynamic> json) =>
      LanguageWordInformation(
        original: jsonStringField(json['original']),
        isPhrase: jsonStringField(json['is_phrase']) == '1',
        isValidated: jsonStringField(json['is_validated']) == '1',
        hasCategorizationData:
            jsonStringField(json['has_categorization_data']) == '1',
        categorizationGroups: json['categorizationGroups'] != null
            ? WordCategorizationGroup.wordCategorizationGroupListFromJson(
                json['categorizationGroups'])
            : null,
        alternativeSpellings: json['alternative_texts'] != null
            ? WordSpelling.wordSpellingListFromJson(json['alternative_texts'])
            : null,
      );
}
