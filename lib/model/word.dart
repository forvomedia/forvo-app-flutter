import 'package:forvo/model/grouped_pronunciation.dart';
import 'package:forvo/model/pronunciation.dart';
import 'package:forvo/model/word_alternative_spelling.dart';
import 'package:forvo/model/word_categorization_group.dart';
import 'package:forvo/util/json.dart';

class Word {
  final int id;
  final String word;
  final String original;
  final String addtime;
  final String languageName;
  final String languageCode;
  final int numPronunciations;
  final bool isPhrase;
  final bool isValidated;
  final bool hasCategorizationData;
  final bool isPendingPronunciation;
  final bool hasPronunciationRequest;
  final List<WordSpelling> alternativeSpellings;
  final List<WordCategorizationGroup> categorizationGroups;
  final Pronunciation standardPronunciation;
  final List<GroupedPronunciation> groupedPronunciations;

  Word({
    this.id,
    this.word,
    this.original,
    this.addtime,
    this.languageName,
    this.languageCode,
    this.numPronunciations,
    this.isPhrase,
    this.isValidated,
    this.hasCategorizationData,
    this.isPendingPronunciation,
    this.hasPronunciationRequest,
    this.alternativeSpellings,
    this.categorizationGroups,
    this.standardPronunciation,
    this.groupedPronunciations,
  });

  factory Word.fromJson(Map<String, dynamic> json) => Word(
        id: jsonIntField(json['id']),
        word: jsonStringField(json['word'], nullValue: ''),
        original: jsonStringField(json['original'], nullValue: ''),
        addtime: jsonStringField(json['addtime'], nullValue: ''),
        languageName: jsonStringField(json['language'], nullValue: ''),
        languageCode: jsonStringField(json['code'], nullValue: ''),
        numPronunciations: int.parse(
            jsonStringField(json['num_pronunciations'], nullValue: '0')),
        isPhrase: jsonStringField(json['is_phrase']) == '1',
        isValidated: jsonStringField(json['is_validated']) == '1',
        hasCategorizationData:
            jsonStringField(json['has_categorization_data']) == '1',
        isPendingPronunciation: json['pending_pronunciation'] != null
            ? jsonBoolField(json['pending_pronunciation'])
            : null,
        hasPronunciationRequest: json['has_pronunciation_request'] != null
            ? jsonBoolField(json['has_pronunciation_request'])
            : null,
        alternativeSpellings: json['alternative_texts'] != null &&
                json['alternative_texts'] != ''
            ? WordSpelling.wordSpellingListFromJson(json['alternative_texts'])
            : null,
        categorizationGroups: json['categorizationGroups'] != null
            ? WordCategorizationGroup.wordCategorizationGroupListFromJson(
                json['categorizationGroups'])
            : null,
        standardPronunciation: json['standard_pronunciation'] != null
            ? Pronunciation.fromJson(json['standard_pronunciation'])
            : null,
        groupedPronunciations: json['grouped_pronunciations'] != null
            ? GroupedPronunciation.groupedPronunciationListFromJson(
                json['grouped_pronunciations'])
            : null,
      );

  static List<Word> wordListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return Word.fromJson(json);
            }).toList()
          : [];
}
