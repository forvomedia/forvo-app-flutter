import 'package:flutter/material.dart';

class PlayableFile {
  final int id;
  final String url;
  final String localPath;
  final bool isLocal;


  PlayableFile({
    @required this.id,
    @required this.url,
    @required this.localPath,
    @required this.isLocal,
  });
}
