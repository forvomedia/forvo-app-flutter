import 'package:forvo/util/json.dart';

class UserNotification {
  final int id;
  final String title;
  final String message;
  final bool featured;
  final String notificationDate;

  UserNotification({
    this.id,
    this.title,
    this.message,
    this.featured,
    this.notificationDate,
  });

  factory UserNotification.fromJson(Map<String, dynamic> json) =>
      UserNotification(
        id: jsonIntField(json['id']),
        title: jsonStringField(json['title'], nullValue: ''),
        message: jsonStringField(json['message'], nullValue: ''),
        featured: !jsonBoolField(json['isRead']),
        notificationDate: jsonStringField(json['addtime']),
      );

  static List<UserNotification> userNotificationListFromJson(
          List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return UserNotification.fromJson(json);
            }).toList()
          : [];
}
