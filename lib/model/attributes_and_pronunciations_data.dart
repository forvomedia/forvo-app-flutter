import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/language_with_pronunciations.dart';
import 'package:forvo/util/json.dart';

class AttributesAndPronunciationsData {
  final String status;
  final Attributes attributes;
  final List<LanguageWithPronunciations> data;

  AttributesAndPronunciationsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndPronunciationsData.fromJson(Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndPronunciationsData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: LanguageWithPronunciations.languageWithPronunciationsListFromJson(
          _data),
    );
  }
}
