import 'package:forvo/util/json.dart';

class WordSpelling {
  final String spelling;

  WordSpelling({
    this.spelling,
  });

  factory WordSpelling.fromJson(Map<String, dynamic> json) => WordSpelling(
        spelling: jsonStringField(json['spelling'], nullValue: ''),
      );

  static List<WordSpelling> wordSpellingListFromJson(List<dynamic> list) =>
      list != null && list.isNotEmpty
          ? list.map((item) {
              Map<String, dynamic> json = item;
              return WordSpelling.fromJson(json);
            }).toList()
          : [];
}
