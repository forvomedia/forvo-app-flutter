import 'package:forvo/model/attributes.dart';
import 'package:forvo/model/pending_pronounce_word.dart';
import 'package:forvo/util/json.dart';

class AttributesAndPendingPronounceWordsData {
  final String status;
  final Attributes attributes;
  final List<PendingPronounceWord> data;

  AttributesAndPendingPronounceWordsData({
    this.status,
    this.attributes,
    this.data,
  });

  factory AttributesAndPendingPronounceWordsData.fromJson(
      Map<String, dynamic> json) {
    List _data = json['data'];
    return AttributesAndPendingPronounceWordsData(
      status: jsonStringField(json['status']),
      attributes: Attributes.fromJson(json['attributes']),
      data: PendingPronounceWord.pendingPronounceWordListFromJson(_data),
    );
  }
}
