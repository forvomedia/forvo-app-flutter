import 'package:forvo/util/json.dart';

class UserProfileStats {
  final String pronunciations;
  final String bestPronunciations;
  final String addedWords;
  final String votes;
  final String visits;

  UserProfileStats({
    this.pronunciations,
    this.bestPronunciations,
    this.addedWords,
    this.votes,
    this.visits,
  });

  int get numberOfPronunciations => int.parse(pronunciations);

  int get numberOfBestPronunciations => int.parse(bestPronunciations);

  int get numberOfAddedWords => int.parse(addedWords);

  int get numberOfVotes => int.parse(votes);

  int get numberOfVisits => int.parse(visits);

  factory UserProfileStats.fromJson(Map<String, dynamic> json) =>
      UserProfileStats(
        pronunciations: jsonStringField(json['pronunciations'], nullValue: '0'),
        bestPronunciations:
            jsonStringField(json['best_pronunciations'], nullValue: '0'),
        addedWords: jsonStringField(json['added_words'], nullValue: '0'),
        votes: jsonStringField(json['votes'], nullValue: '0'),
        visits: jsonStringField(json['visits'], nullValue: '0'),
      );
}
