import 'package:flutter/material.dart';
import 'package:forvo/config/localizations.dart';
import 'package:forvo/ui/colors.dart';

class ErrorMessage extends StatelessWidget {
  final String titleTextKey;
  final String bodyTextKey;
  final String actionTextKey;
  final VoidCallback actionCallback;

  ErrorMessage({
    @required this.titleTextKey,
    @required this.bodyTextKey,
    @required this.actionTextKey,
    @required this.actionCallback,
  });

  @override
  InkWell build(BuildContext context) {
    var themeData = Theme.of(context);
    var localizations = AppLocalizations.of(context);

    return InkWell(
      onTap: actionCallback,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 10,
              ),
              child: Text(
                localizations.translate(titleTextKey),
                style: themeData.textTheme.bodyText1.copyWith(
                  color: colorBlack,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 0),
            //calculateGridSize(context)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Text(
                localizations.translate(bodyTextKey),
                style: themeData.textTheme.bodyText2,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 15),
            Text(
              localizations.translate(actionTextKey).toUpperCase(),
              style: themeData.textTheme.bodyText2.copyWith(
                color: colorBlue,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
