import 'package:flutter/material.dart';
import 'package:forvo/widgets/errors/error_message.dart';

class ServiceError extends ErrorMessage {
  final VoidCallback retryCallback;
  final String message;

  ServiceError(this.retryCallback, {this.message})
      : super(
            titleTextKey: 'error.service.title',
            bodyTextKey: (message != null) ? message : 'error.service.body',
            actionTextKey: 'error.service.retry',
            actionCallback: retryCallback);
}
