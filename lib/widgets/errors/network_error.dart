import 'package:flutter/material.dart';
import 'package:forvo/widgets/errors/error_message.dart';

class NetworkError extends ErrorMessage {
  final VoidCallback retryCallback;

  NetworkError(this.retryCallback)
      : super(
            titleTextKey: 'error.network.title',
            bodyTextKey: 'error.network.body',
            actionTextKey: 'error.network.retry',
            actionCallback: retryCallback);
}
